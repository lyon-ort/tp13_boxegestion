<?php

class CategorieRencontre {

    // Attributs
    protected $Id_Cat_Rencontre;
    protected $Libelle_Cat_Rencontre;

    // Constructeur
    public function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas = NULL) {
        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Appelle le mutateur des attributs existant dans le tableau $datas
        foreach ($attrib as $key => $val) {
            // Si, dans vos habitudes, les champs de la bdd sont en minuscules, décommentez la ligne suivante.
            //$key = strtolower($key);
            // Si, dans vos habitudes, les champs de la bdd sont en majuscules, décommentez la ligne suivante.
            //$key = strtoupper($key);
            //  Perso, je met tout en notation camelCase... même dans la BDD.
            if (isset($datas[$key])) {
                $mutateur = 'set' . $key;
                $this->$mutateur($datas[$key]);
            }
        }

        return $this;
    }

    // GET et SET
    function getId_Cat_Rencontre() {
        return $this->Id_Cat_Rencontre;
    }

    function getLibelle_Cat_Rencontre() {
        return $this->Libelle_Cat_Rencontre;
    }

    function setId_Cat_Rencontre($Id_Cat_Rencontre): void {
        $this->Id_Cat_Rencontre = $Id_Cat_Rencontre;
    }

    function setLibelle_Cat_Rencontre($Libelle_Cat_Rencontre): void {
        $this->Libelle_Cat_Rencontre = $Libelle_Cat_Rencontre;
    }

}
