<?php

require '../src/App/Entities/Concerne.php';

class ConcerneRepository {

    protected $table;     
    protected $classMapped; 
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire)
    public function __construct() {
        $this->table = 'Concerne';
        $this->classMapped = 'App\Entities\Concerne';
        $this->idFieldName = 'Id_Concerne';
    }
    
    
    public function sauver(Concerne $entity) {
        $db = dbConnect();
        $resultSet = NULL;
        if ($entity != NULL) {
            $bindParam = $entity->__toArray();
            if ($entity->getId_Concerne() == NULL) {
                // Nouvelle entité
                $query = "INSERT INTO $this->table" .
                        " (Id_Concerne_Tab, "
                        . "Id_Concerne_Cat_Age) "
                        . " VALUES "
                        . "(:Id_Concerne_Tab, "
                        . ":Id_Concerne_Cat_Age)";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Id_Concerne_Tab', $bindParam['Id_Concerne_Tab'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Concerne_Cat_Age', $bindParam['Id_Concerne_Cat_Age'], \PDO::PARAM_INT);
                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $entity->setid($db->lastInsertId());
                    $resultSet = $entity;
                }
            } else {
                //  Entité existante
                $query = "UPDATE $this->table"
                        . " SET Id_Concerne_Tab = :Id_Concerne_Tab, "
                        . " Id_Concerne_Cat_Age = :Id_Concerne_Cat_Age "
                        . " WHERE $this->idFieldName = :Id_Concerne";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Id_Concerne_Tab', $bindParam['Id_Concerne_Tab'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Concerne_Cat_Age', $bindParam['Id_Concerne_Cat_Age'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Concerne', $bindParam[$this->idFieldName], \PDO::PARAM_INT);

                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $resultSet = $entity;
                }
            }
        }
        return $resultSet;
    }        
    
    
    // Sélectionner un tableau selon un id
    public function selectionConcerneIdTab($idTableau)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $objet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Concerne_Tab = :idTableauMark");
        $req->execute(array('idTableauMark' => $idTableau));

        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$objet = new $this->table($row);
	    }
        }
        
        // Return un objet
        return $objet;        
    }
    
    
    // Supprimer un concerne à partie d'un ID de tableau
    public function supprimerConcerneIdTab($idTableau)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Requête
        $req = $db->prepare("DELETE FROM $this->table WHERE Id_Concerne_Tab = :idTableauMark");
        $req->execute(array('idTableauMark' => $idTableau));
    }    
}