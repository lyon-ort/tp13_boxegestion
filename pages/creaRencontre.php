<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';
// Page de fonction
require_once 'Function.php';

// Si il y a déjà des rencontres, alors ça redirige vers les détails de la compétition
// Séléction des rencontres selon les tableaux
$rencontreRep = new RencontreRepository();

if ($rencontreRep->selectionRencontreIdTableau($_POST['tableau']))
{
    header('location: detailCompetitionConnect.php?connect=1&tableau='.$_POST['tableau']);
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Création / Modification des rencontres </title>
    </head>
    <body>
        <!-- Header (inc) -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>
        
        
        <!-- Section -->
        <section class='container-fluid about'>
            
            <!-- Information de l'utilisateur (inc) -->
            <?php
            include '../inc/information.php';
            ?>
            
            <h1> Création / Modification rencontres </h1>
            
            <hr class="separator">
            
            <?php            
        
            $numTableau = $_POST['tableau'];

            $typeTableauRep = new TableauRepository();
            $TypeTableauAffichage = $typeTableauRep->typeTableau($numTableau);
            //var_dump($TypeTableauAffichage);
            $renvoisTireurFonction = new TableauRepository();
            $renvoisTireurAffichage = $renvoisTireurFonction->renvoisTableau($numTableau); 

            $nbRingFonction = new CompetitionRepository();
            $nbRingAffichage = $nbRingFonction->numRing($numTableau);
 
            $dateFunction = new CompetitionRepository();
            $dateAffichage = $dateFunction->limiteDateRencontre($numTableau);


            // Le tableau est "Linéaire"
            if ($TypeTableauAffichage[0]['Id_Tableau_Typ_Tableau'] == 2)
            {
                $nbMaxRencontre = new TableauRepository();
                $nbMaxRencontreAffichage = $nbMaxRencontre->nbMaxRencontre($numTableau);

                $calculRencontre = calculNbCombat(count($renvoisTireurAffichage));

                if($nbMaxRencontreAffichage[0]['Nb_Max_Rencontre'] <= $calculRencontre)
                {
                    $maxRencontre = $nbMaxRencontreAffichage[0]['Nb_Max_Rencontre'];
                }
                else if($nbMaxRencontreAffichage[0]['Nb_Max_Rencontre'] > $calculRencontre)
                {
                    $maxRencontre = $calculRencontre;

                }
                
                $catRencontre = null;
            }
            
            // Le tableau est "Eliminatoire"
            else if ($TypeTableauAffichage[0]['Id_Tableau_Typ_Tableau'] == 1)
            {
                $maxRencontre = count($renvoisTireurAffichage)-1;
                $nbRencontreParCat = count($renvoisTireurAffichage) / 2;
                $categorieRencontreFonction = new CategorieRencontreRepository();
                $catRencontre = $categorieRencontreFonction->CatégorieRencontrePyramidale($nbRencontreParCat);
            }

            // Mode automatique 1
            // Si le nombre max de rencontre est inférieur ou égal à trois, alors le mode est automatique
            if($maxRencontre <= 3 )
            {
            ?> 
                <form method="post" class="formCreat" action="creaRencontre.php?connect=1">
                    <select name="choix">
                        <option value="manuel">Création Manuelle</option>
                        <option value="automatique">Création Automatique</option>
                    </select>
                    <input type="hidden" name="tableau" value="<?= $numTableau ?>"/>
                    <input type="submit" value="Valider"></input>
                </form>
            <?php
                $repChoix = (isset($_POST['choix'])? $_POST['choix']:'');
            }
            else
            {
                $repChoix = 'manuel';
            }

            // Mode manuel
            if ($repChoix == 'manuel')
            {
            ?>
                <form name="rencontre" action="sauverRencontre.php?connect=1" method="post">
                    <table>
                        <?php 
                        if ($TypeTableauAffichage[0]['Id_Tableau_Typ_Tableau'] == 2)
                        {
                        ?>
                        
                            <th> Date </th> 
                            <th> heure de debut </th> 
                            <th> numero de ring </th> 
                            <th> nom tireur coin rouge </th> 
                            <th> nom tireur coin bleu </th> 
                        
                        <?php
                        }
                        else if ($TypeTableauAffichage[0]['Id_Tableau_Typ_Tableau'] == 1)
                        {
                        ?>
                            <th> Date </th> 
                            <th> heure de debut </th> 
                            <th> numero de ring </th> 
                            <th> nom tireur coin rouge </th> 
                            <th> nom tireur coin bleu </th> 
                            <th> Catégorie Rencontre </th>
                        <?php
                        }
                        ?>
                            
                        <tr> </tr>
                        
                        <?php
                        
                        $objetTireurRep = new TireurRepository();
                        // Sélectionne les structures des tireurs inscrit au tableau
                        $objetStructureRep = new StructureRepository();
                        if($TypeTableauAffichage[0]['Id_Tableau_Typ_Tableau'] == 1)
                        {
                        $m = $nbRencontreParCat;
                        }
                        
                        for ($j = 0; $j < $maxRencontre ; $j++)
                        {
                        ?>
                            <!-- Date -->
                            <td>
                                <input type="Date" name="Date_Rencontre<?php echo $j; ?>" min="<?= $dateAffichage[0]['Date_Debut_Competition'] ?>" Max="<?= $dateAffichage[0]['Date_Fin_Competition'] ?>" value="<?= $dateAffichage[0]['Date_Debut_Competition'] ?>"/>
                            </td>

                            <!-- Heure de début -->
                            <td>
                                <input type="time" name="Heure_Debut_Rencontre<?php echo $j; ?>" min="08:00" max="19:00" value="08:00"/>
                            </td>
                            <!-- Heure de fin -->
                            <input type='hidden' name='Heure_Fin_Rencontre<?php echo $j; ?>'/>

                            <!-- Nombre de rings -->
                            <td>
                                <select name="Num_Ring_Rencontre<?php echo $j; ?>">
                                    <?php 
                                    for ($i = 1; $i < $nbRingAffichage['Nb_Ring'] + 1; $i++)
                                    {
                                    ?>
                                        <option value="<?= $i ?>"> ring <?= $i ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>

                            <!-- Id_Tireur_Rouge -->
                            <td>
                                <?php

                                
                                if ($TypeTableauAffichage[0]['Id_Tableau_Typ_Tableau'] == 1 && $j > (count($renvoisTireurAffichage)/2)-1)
                                {
                                ?>
                                    <input type="Text" value="" readonly="readonly" />
                                    <input type="hidden" name="Id_Tireur_Rouge<?php echo $j; ?>" value="null" readonly="readonly" />
                                <?php
                                }
                                else
                                {
                                ?>
                                    <select name="Id_Tireur_Rouge<?php echo $j; ?>">
                                <?php
                                    // La liste déroulante des tireurs rouges
                                    foreach ($renvoisTireurAffichage as $tireur) 
                                    {
                                        // Objet tireur rouge
                                        $objetTireurRouge = $objetTireurRep->selectionTireurId($tireur['Id_Tireur']);
                                        $objetStructureRouge = $objetStructureRep->selectionStructureId($objetTireurRouge->getId_Tir_Struct());
                                ?>
                                        <option style="background-color:#FF0000" value="<?php echo $tireur['Id_Tireur'] ?>"><?php echo  $tireur['Id_Tireur'], " - ", $tireur['Nom_Tireur'], " " , $tireur['Prenom_Tireur']. " (" . $objetStructureRouge->getNom_Struct() . ")" ?></option>
                                    <?php
                                    }
                                    ?>   
                                    </select>
                                <?php
                                }
                                ?>
                            </td>

                            <!-- Id_Tireur_Bleu -->
                            <td>
                                <?php
                                if ($TypeTableauAffichage[0]['Id_Tableau_Typ_Tableau'] == 1 && $j > count($renvoisTireurAffichage)/2-1)
                                {
                                ?>
                                    <input type="Text" value="" readonly="readonly" />
                                    <input type="hidden" name="Id_Tireur_Bleu<?php echo $j; ?>" value="null" readonly="readonly" />
                                <?php
                                }
                                else
                                {
                                ?>
                                    <select name="Id_Tireur_Bleu<?php echo $j; ?>">
                                <?php
                                    // La liste déroulante des tireurs bleus
                                    foreach ($renvoisTireurAffichage as $tireur) 
                                    {
                                        // Objet tireur bleu
                                        $objetTireurBleu = $objetTireurRep->selectionTireurId($tireur['Id_Tireur']);
                                        $objetStructureBleu = $objetStructureRep->selectionStructureId($objetTireurBleu->getId_Tir_Struct());
                                ?>
                                        <option style="background-color:#007FFF" value="<?php echo $tireur['Id_Tireur'] ?>"><?php echo  $tireur['Id_Tireur'], " - ", $tireur['Nom_Tireur'], " " , $tireur['Prenom_Tireur']. " (" . $objetStructureBleu->getNom_Struct() . ")" ?></option>
                                    <?php
                                    }
                                    ?>
                                    </select>
                                <?php
                                }
                                ?>
                            </td>

                            <!-- Le gagnant -->
                            <input type='hidden' name='Id_Gagnant<?php echo $j; ?>' value="null"/> 

                            <!-- La rencontre précédente 1 -->
                            <input type="hidden" name="Id_Rencontre_Precedent_1<?php echo $j; ?>" value="null"/>
                            
                            <!-- La rencontre précédente 2 -->
                            <input type="hidden" name="Id_Rencontre_Precedent_2<?php echo $j; ?>" value="null"/>

                            <!-- La rencontre suivante -->
                            <input type="hidden" name="Id_Rencontre_Suivant<?php echo $j; ?>" value="null"/>

                            
                            <?php 
                            if ($TypeTableauAffichage[0]['Id_Tableau_Typ_Tableau'] == 2)
                            {
                            ?>
                                <input type="hidden" name="Id_Cat_Rencontre<?php echo $j; ?>" value="<?= $catRencontre ?>" />
                            <?php
                            }
                            else 
                            {        
                                
                                // La condition qui utilise la variable $m
                                if ($m != 1)
                                {
                                    $catRencontre = $categorieRencontreFonction->CatégorieRencontrePyramidale($nbRencontreParCat);
                                    $m--;
                                }
                                else
                                {
                                    $nbRencontreParCat /= 2;
                                    $m = $nbRencontreParCat;
                                }
                                
                                // L'exception du 0.5
                                if ($nbRencontreParCat == 0.5)
                                {
                                    $catRencontre = $categorieRencontreFonction->CatégorieRencontrePyramidale(1); 
                                }
                                
                                // Affichage de la catégorie de rencontre
                                ?>
                                <td>
                                    <input type="text" value="<?= $catRencontre->getLibelle_Cat_Rencontre() ?>" readonly="readonly" />
                                    <input type="hidden" name="Id_Cat_Rencontre<?= $j; ?>" value="<?= $catRencontre->getId_Cat_Rencontre() ?>" readonly="readonly" />
                                </td>
                                <?php
                                
                            }
                            ?>

                            <!-- Le tableau -->
                            <input type="hidden" name="Id_Tab_Rencontre<?php echo $j; ?>" value="<?= $numTableau ?>"/>

                            <!-- L'état de la rencontre -->
                            <input type="hidden" name="Id_Etat_Rencontre<?php echo $j; ?>" value='1'/>

                            <!-- L'inactivité de la rencontre -->
                            <input type="hidden" name="Inactif_Rencontre<?php echo $j; ?>" value="0"/>

                            <tr></tr>
                        <?php
                        }
                        ?>
                            
                        <input type="hidden" name="taille" value="<?= $j ?>"/>

                    </table>
                    <br/>
                    <input style="width: auto;" type="submit" value="Validation"/>
                    
                </form>
            
            <?php
            }

            // Mode automatique 2
            if ($repChoix == 'automatique')
            { 
                $tableau_tireur = array();
                $num_rencontre = 0;
                if($TypeTableauAffichage[0]['Id_Tableau_Typ_Tableau'] == 1)
                    {
                      $m = $nbRencontreParCat;
                    }
                for ($j = 0; $j < $maxRencontre ; $j++)
                {
                    $selection_Tableau = array_rand($renvoisTireurAffichage, 2);

                    $num_rencontre++;

                    $nouvelle_rencontre = array('num_rencontre' => $num_rencontre, 
                                                'Id_Tireur_Rouge' => $renvoisTireurAffichage[$selection_Tableau[0]]['Id_Tireur'], 
                                                'Id_Tireur_Bleu' => $renvoisTireurAffichage[$selection_Tableau[1]]['Id_Tireur']);

                    $controle = true;

                    if ($nouvelle_rencontre['num_rencontre'] == 1)
                    {
                        array_push($tableau_tireur, $nouvelle_rencontre);
                    } 
                    else 
                    {
                        foreach ($tableau_tireur as $marencontre)
                        {
                            if ($nouvelle_rencontre['Id_Tireur_Bleu'] == $marencontre['Id_Tireur_Bleu'] &&
                                $nouvelle_rencontre['Id_Tireur_Rouge'] == $marencontre['Id_Tireur_Rouge'])
                            {
                                $controle = false;
                                $j--;
                                $num_rencontre--;
                            } 
                            else if ($nouvelle_rencontre['Id_Tireur_Bleu'] ==$marencontre['Id_Tireur_Rouge'] &&
                                     $nouvelle_rencontre['Id_Tireur_Rouge'] ==$marencontre['Id_Tireur_Bleu'])
                            {

                                $controle = false;
                                $j--;
                                $num_rencontre--;
                            }
                        }

                        if ($controle == true)
                        {
                            array_push($tableau_tireur, $nouvelle_rencontre);     
                        }
                    }
                }
            ?>
            
            <form name="rencontre" action="sauverRencontre.php?connect=1" method="post">
                <table>

                    <?php 
                    if ($TypeTableauAffichage[0]['Id_Tableau_Typ_Tableau'] == 2)
                    {
                    ?>
                    
                      <th> Date </th>
                      <th> heure de debut </th>
                      <th> numero de ring </th>
                      <th> nom tireur coin rouge </th>
                      <th> nom tireur coin bleu </th> 
                      
                    <?php
                    }
                    else if ($TypeTableauAffichage[0]['Id_Tableau_Typ_Tableau'] == 1)
                    {
                    ?>
                      
                        <th> Date </th>
                        <th> heure de debut </th>
                        <th> numero de ring </th>
                        <th> nom tireur coin rouge </th>
                        <th> nom tireur coin bleu </th>
                        <th> Catégorie Rencontre </th>
                        
                    <?php
                    }
                    ?>
                        
                    <tr> </tr>

                    <?php
                    // Sélectionne les tireurs inscrits au tableau
                    $arrayObjetTireurRouge = array();
                    $arrayObjetTireurBleu = array();
                    $objetTireurRep = new TireurRepository();
                    foreach ($tableau_tireur as $valueTireur)
                    {
                        $arrayObjetTireurRouge[] = $objetTireurRep->selectionTireurId($valueTireur['Id_Tireur_Rouge']);
                        $arrayObjetTireurBleu[] = $objetTireurRep->selectionTireurId($valueTireur['Id_Tireur_Bleu']);
                    }
                    
                    // Sélectionne les structures des tireurs inscrit au tableau
                    $objetStructureRep = new StructureRepository();
                    
                    for ($j = 0; $j < $maxRencontre ; $j++)
                    { 
                        // Sélection de la structure 
                        $objetStructureRouge = $objetStructureRep->selectionStructureId($arrayObjetTireurRouge[$j]->getId_Tir_Struct());
                        $objetStructureBleu = $objetStructureRep->selectionStructureId($arrayObjetTireurBleu[$j]->getId_Tir_Struct());
                    ?>

                        <!-- Date -->
                        <td>
                            <input type="Date" name="Date_Rencontre<?php echo $j; ?>" min="<?= $dateAffichage[0]['Date_Debut_Competition'] ?>" Max="<?= $dateAffichage[0]['Date_Fin_Competition'] ?>" value="<?= $dateAffichage[0]['Date_Debut_Competition'] ?>"/>
                        </td>
                        
                        <!-- Heure de début -->
                        <td>
                            <input type="time" name="Heure_Debut_Rencontre<?php echo $j; ?>" min="08:00" max="19:00" value="08:00"/>
                        </td>
                        <!-- Heure de fin -->
                        <input type='hidden' name='Heure_Fin_Rencontre<?php echo $j; ?>'/>
                        
                        <!-- Le nombre de rings -->
                        <td>
                            <select name="Num_Ring_Rencontre<?php echo $j; ?>">
                            <?php 
                            for ($i = 1 ; $i < $nbRingAffichage['Nb_Ring'] + 1  ; $i++)
                            {
                            ?>
                                <option value="<?= $i ?>"> ring <?= $i ?></option>
                            <?php
                            }
                            ?>
                          </select>
                        </td>

                        <!-- Les tireurs rouges -->
                        <td>
                            <?php
                            if ($TypeTableauAffichage[0]['Id_Tableau_Typ_Tableau'] == 1 && $j > count($renvoisTireurAffichage)/2-1)
                            {
                            ?>
                                <input type="Text" value="" readonly="readonly" />
                                <input type="hidden" name="Id_Tireur_Rouge<?php echo $j; ?>" value="null" readonly="readonly" />
                            <?php
                            }
                            else
                            {
                            ?>
                                <input type="Text" value="<?php echo $tableau_tireur[$j]['Id_Tireur_Rouge'], " - ", $arrayObjetTireurRouge[$j]->getNom_Tireur(), " ", $arrayObjetTireurRouge[$j]->getPrenom_Tireur() . " (" . $objetStructureRouge->getNom_Struct() . ")"?>" readonly="readonly" />
                                <input type="hidden" name="Id_Tireur_Rouge<?php echo $j; ?>" value="<?= $tableau_tireur[$j]['Id_Tireur_Rouge'] ?>" readonly="readonly" /> 
                            <?php
                            }
                            ?>
                        </td>

                        <!-- Les tireurs bleus -->
                        <td>
                            <?php
                            if($TypeTableauAffichage[0]['Id_Tableau_Typ_Tableau'] == 1 && $j > count($renvoisTireurAffichage)/2-1)
                            {
                            ?>
                                <input type="Text" value="" readonly="readonly" />
                                <input type="hidden" name="Id_Tireur_Bleu<?php echo $j; ?>" value="null" readonly="readonly" />
                            <?php
                            }
                            else
                            {
                            ?>
                                <input type="Text" value="<?php echo $tableau_tireur[$j]['Id_Tireur_Bleu'], " - ", $arrayObjetTireurBleu[$j]->getNom_Tireur(), " ", $arrayObjetTireurBleu[$j]->getPrenom_Tireur() . " (" . $objetStructureBleu->getNom_Struct() . ")"  ?>" readonly="readonly" />
                                <input type="hidden" name="Id_Tireur_Bleu<?php echo $j; ?>" value="<?php echo $tableau_tireur[$j]['Id_Tireur_Bleu'] ;?>" readonly="readonly" />
                            <?php
                            }
                         ?>
                        </td>

                        <!-- Le gagnant -->
                        <input type='hidden' name='Id_Gagnant<?php echo $j; ?>'/> 

                        <!-- Rencontre précédente 1 -->
                        <input type="hidden" name="Id_Rencontre_Precedent_1<?php echo $j; ?>"/>

                        <!-- Rencontre précédente 2 -->
                        <input type="hidden" name="Id_Rencontre_Precedent_2<?php echo $j; ?>"/>

                        <!-- Rencontre suivante -->
                        <input type="hidden" name="Id_Rencontre_Suivant<?php echo $j; ?>" value="null"/>


                        <?php 
                            if ($TypeTableauAffichage[0]['Id_Tableau_Typ_Tableau'] == 2)
                            {
                            ?>
                                <input type="hidden" name="Id_Cat_Rencontre<?php echo $j; ?>" value="<?= $catRencontre ?>" />
                            <?php
                            }
                            else 
                            {        
                                
                                // La condition qui utilise la variable $m
                                if ($m != 1)
                                {
                                    $catRencontre = $categorieRencontreFonction->CatégorieRencontrePyramidale($nbRencontreParCat);
                                    $m--;
                                }
                                else
                                {
                                    $nbRencontreParCat /= 2;
                                    $m = $nbRencontreParCat;
                                }
                                
                                // L'exception du 0.5
                                if ($nbRencontreParCat == 0.5)
                                {
                                    $catRencontre = $categorieRencontreFonction->CatégorieRencontrePyramidale(1); 
                                }
                                
                                // Affichage de la catégorie de rencontre
                                ?>
                                <td>
                                    <input type="text" name="Id_Cat_Rencontre<?= $j; ?>" value="<?= $catRencontre->getLibelle_Cat_Rencontre() ?>" readonly="readonly" />
                                    <input type="hidden" name="Id_Cat_Rencontre<?= $j; ?>" value="<?= $catRencontre->getId_Cat_Rencontre() ?>" readonly="readonly" />
                                </td>
                                <?php
                                
                            }
                            ?>


                        <!-- Le tableau -->
                        <input type="hidden" name="Id_Tab_Rencontre<?php echo $j; ?>" value="<?= $numTableau ?>"/>

                        <!-- L'état de la rencontre -->
                        <input type="hidden" name="Id_Etat_Rencontre<?php echo $j; ?>" value='1' />

                        <!-- L'inactivité de la rencontre -->
                        <input type="hidden" name="Inactif_Rencontre<?php echo $j; ?>" value="0" />

                        <tr> </tr>

                    <?php
                    }
                    ?>
                        
                    <input type="hidden" name="taille" value="<?= $j ?>"/>
                </table>
                
                <br/>
                <input style="width: auto;" type="submit" value="Validation"/>
            </form>
            <?php
            }
            ?>
            <br/>
        </section>
    </body>
</html>
