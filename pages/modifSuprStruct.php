<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';
$leGet = '?connect=1';

// Modification de structure
if (isset($_POST['Id_StructModif']))
{
    // Test si le nom existe déjà
    $objetStructureRep2 = new StructureRepository();
    $arrayObjetStructure2 = $objetStructureRep2->selectionStructure();
                
    $test = true;
                
    foreach ($arrayObjetStructure2 as $value)
    {
        if ($value->getNom_Struct() != $_POST['NomDeBase'])
        {
            if ($value->getNom_Struct() == $_POST['Nom_StructModif'])
            {
                $test = false;
            }
        }
    }
    
    if ($test)
    {
        // Si le nom n'existe pas
        // Déclaration du tableau des valeurs
        $valeurStructure = array(
            "Id_Struct" => $_POST['Id_StructModif'],
            "Id_Typ_Struct" => $_POST['Id_Typ_StructModif'],
            "Nom_Struct" => $_POST['Nom_StructModif'],
            "Adresse_Struct" => $_POST['Adresse_StructModif'],
            "Cp_Struct" => $_POST['Cp_StructModif'],
            "Ville_Struct" => $_POST['Ville_StructModif'],
            "Id_Responsable_Struct" => $_POST['Id_Responsable_StructModif'],
            "Tel_Fixe" => $_POST['Tel_FixeModif'],
            "Tel_Port" => $_POST['Tel_PortModif'],
            "Inactif_Struct" => 0
        );

        $structure = new Structure($valeurStructure);
        $objetStructureRep2->sauver($structure);

        // Rafraichissement de la page
        header('location: modifSuprStruct.php?connect=1&message=1');
    }
    else
    {
        ?>
            <!-- Renvoie vers la page de modif -->
            <form name="form" action="modifStruct.php?connect=1" method="POST"> 
                <input type="hidden" name="Id_Struct" value="<?= $_POST['Id_StructModif'] ?>"/>
                <input type="hidden" name="Nom_Struct" value="<?= $_POST['Nom_StructModif'] ?>"/>
                <input type="hidden" name="NomDeBase" value="<?= $_POST['NomDeBase'] ?>"/>
                <input type="hidden" name="Adresse_Struct" value="<?= $_POST['Adresse_StructModif'] ?>"/>
                <input type="hidden" name="Cp_Struct" value="<?= $_POST['Cp_StructModif'] ?>"/>
                <input type="hidden" name="Ville_Struct" value="<?= $_POST['Ville_StructModif'] ?>"/>
                <input type="hidden" name="Id_Responsable_Struct" value=" <?= $_POST['Id_Responsable_StructModif'] ?>"/>
                <input type="hidden" name="Tel_Fixe" value="<?= $_POST['Tel_FixeModif'] ?>"/>
                <input type="hidden" name="Tel_Port" value="<?= $_POST['Tel_PortModif'] ?>"/>
                <input type="hidden" name="error" />
            </form>

            <!-- Script qui renvoie les variables POST vers la page de modification -->
            <script type="text/javascript"> 
                document.form.submit(); //on envoie le formulaire vers la page de modification
            </script> 
        <?php
    }
}



?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Modifier / Supprimer structure </title>
    </head>
    
    <body>
        <!-- Header -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>
        
        
        <!-- Section -->
        <section class='container-fluid about'>

            <?php
            if (isset($_GET['connect']))
            {
            ?>
                <div class="information">
                    Login : <?= $_SESSION['login'] ?> <br/>
                    Prénom : <?= $_SESSION['Prenom_Utilisateur'] ?> <br/>
                    Nom : <?= $_SESSION['Nom_Utilisateur'] ?> <br/>
                    Rôle :
                    <?php
                    $objetTypeRoleRepUtil = new TypeRoleRepository(); 
                    $typeRole = $objetTypeRoleRepUtil->selectionTypeRoleId($_SESSION['idRole']);
                    echo $typeRole->getLibelle_Typ_Role();
                    ?>                                 
                    <br/>
                    Structure :
                    <?php
                    $objetStructureRepUtil = new StructureRepository(); 
                    $structure = $objetStructureRepUtil->selectionStructureId($_SESSION['idStruct']);
                    echo $structure->getNom_Struct();
                    ?> <br/><br/><br/>
                    <button class='btn btn-custom' onclick="Deconnexion()"> Déconnexion </button>
                </div>   
            <?php
            }
            ?>            
            
            <h1> Liste structures </h1>
            
            <hr class="separator">
            
            <?php
            // Suppression de structure
            if (isset($_POST['Id_StructSupr']))
            {   
                // STRUCTURE
                // Sélectionne la structure

                $objetStructureRep2 = new StructureRepository();
                $objetStructure = $objetStructureRep2->selectionStructureId($_POST['Id_StructSupr']);
                
                // Par rapport au script 
                $inactif = $objetStructure->getInactif_Struct();
                
                if (!$inactif)
                {
                    // Modifie le "Flag" de la structure
                    $objetStructure->setInactif_Struct(1);

                    // Supprime la structure
                    $objetStructureRep2->sauver($objetStructure);                    
                    
                    // UTILISATEUR
                    $objetUtilisateurRep = new UtilisateurRepository();
                    $arrayObjetUtilisateur = $objetUtilisateurRep->selectionArrayUtilisateurIdStructure($_POST['Id_StructSupr']);
                    
                    if ($arrayObjetUtilisateur)
                    {
                        // Modifie les "Flag" et supprime les utilisateurs de la structure
                        foreach($arrayObjetUtilisateur as $value)
                        {
                            $value->setInactif_Utilisateur(1);
                            $objetUtilisateurRep->sauver($value);
                        }                        
                    }


                    // TIREUR
                    $objetTireurRep = new TireurRepository();
                    $arrayObjetTireur = $objetTireurRep->selectionArrayTireurIdStructure($_POST['Id_StructSupr']);

                    if ($arrayObjetTireur)
                    {
                        // Modifie les "Flag" et supprime les utilisateurs de la structure
                        foreach($arrayObjetTireur as $value)
                        {
                            $value->setInactif_Tireur(1);
                            $objetTireurRep->sauver($value);
                        }                        
                    }
                    ?>
                            <!-- Pop up de confirmation de suppression -->
                            <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                                <div class="modal-content" style="height: 270px;">
                                    <!-- bouton fermer -->
                                    <div id="close" class="closeTest">+</div>

                                    <!-- Confirmation de suppression -->
                                    <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                                    La structure, ses utilisateurs et ses tireurs ont été supprimé !<br/><br/>

                                    <button class="boutonAnnulerTest"> OK </button>
                                </div>
                            </div>   

                             <script>
                                // La croix (ferme le pop-up)
                                document.querySelector('.closeTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });      

                                // Le bouton annuler (ferme le pop-up)
                                document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });       
                            </script>            
                    <?php   
                }
            }

            // Message de confirmation de modification
            if (isset($_GET['message']) && $_GET['message'] == 1)
            {
                    ?>
                            <!-- Pop up de confirmation de suppression -->
                            <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                                <div class="modal-content" style="height: 250px;">
                                    <!-- bouton fermer -->
                                    <div id="close" class="closeTest">+</div>

                                    <!-- Confirmation de suppression -->
                                    <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                                    La structure a été modifié !<br/><br/>

                                    <button class="boutonAnnulerTest"> OK </button>
                                </div>
                            </div>   

                             <script>
                                // La croix (ferme le pop-up)
                                document.querySelector('.closeTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });      

                                // Le bouton annuler (ferme le pop-up)
                                document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });       
                            </script>            
                    <?php 
            }
            ?>
            
            <table>
                
                <th> Nom </th>
                <th> Type structure </th>
                <th> Responsable structure </th>
                <th> Modifier </th>
                <th> Supprimer </th>
                
                <?php
                // Structure
                $objetStructureRep = new StructureRepository();
                $arrayObjetStructure = $objetStructureRep->selectionStructure();
                
                // Type de structure
                $objetTypeStructureRep = new TypeStructureRepository();
                
                // Utilisateur
                $objetUtilisateurRep = new UtilisateurRepository();
                
                $i = 0;
                
                // Affichage des structures
                foreach ($arrayObjetStructure as $value)
                {
                    ?>
                    <tr>
                        <td> <?= $value->getNom_Struct() ?> </td>
                        
                        <td> 
                            <?php
                                $arrayObjetTypeStructure = $objetTypeStructureRep->selectionTypeStructureId($value->getId_Typ_Struct());
                                echo $arrayObjetTypeStructure->getLibelle_Typ_Struct();
                            ?> 
                        </td>
                        
                        <td> 
                            <?php
                                $arrayObjetUtilisateur = $objetUtilisateurRep->selectionUtilisateurId($value->getId_Responsable_Struct());
                                echo $arrayObjetUtilisateur->getLogin_Utilisateur();
                            ?> 
                        </td>
                        
                        <td> 
                            <form method="POST" action="modifStruct.php?connect=1">
                                <input type="hidden" name="Id_Struct" value="<?= $value->getId_Struct() ?>"/>
                                <input id="boutonModifierBleu" type="submit" value="Modifier"/>
                            </form>
                        </td>
                        
                        <td> 
                            <?php
                            // Sélection des compétition à venir et en cours (inactif = 0)
                            $CompetitionRep = new CompetitionRepository();
                            $arrayCompetition = array();
                            // Initialisation de l'array des compétitions à venir et en cours
                            // En cours
                            $arrayCompetitionEnCours = $CompetitionRep->selectionCompetitionEnCours();
                            if ($arrayCompetitionEnCours)
                            {
                                foreach($arrayCompetitionEnCours as $value2)
                                {
                                    array_push($arrayCompetition, $value2);
                                }
                            }
                            // A venir
                            $arrayCompetitionAVenir = $CompetitionRep->selectionCompetitionAVenir();
                            if ($arrayCompetitionAVenir)
                            {
                                foreach($arrayCompetitionAVenir as $value3)
                                {
                                    array_push($arrayCompetition, $value3);
                                }  
                            }

                            // Sélection des tireurs de cette structure
                            $objetTireurRep2 = new TireurRepository();
                            $arrayObjetTireur2 = $objetTireurRep2->selectionArrayTireurIdStructure($value->getId_Struct());
                            
                            // Sélection des participations avec des tireurs de la structure dans une des compétitions
                            $objetParticiperRep = new ParticiperRepository();
                            $arrayParticipation = array();
                            
                            if ($arrayObjetTireur2)
                            {
                                if ($value->getId_Typ_Struct() == 1)
                                {
                                    foreach ($arrayCompetition as $valueCompet)
                                    {
                                        foreach ($arrayObjetTireur2 as $valueTireur)
                                        {
                                            $arrayParticipation[] = $objetParticiperRep->selectionParticipationIdCompetTireur($valueCompet->getId_Competition(), $valueTireur->getId_Tireur());
                                        }
                                    }
                                }                                
                            }
                            
                            
                            // Si une structure a des tireurs qui participe à une compétition "en cours" ou "à venir"
                            $testVide = false;
                            
                            foreach ($arrayParticipation as $valueTest)
                            {
                                if ($valueTest != NULL)
                                {
                                    $testVide = true;
                                }
                            }
                            
                            // Si une structure organise une compétition "en cours" ou "à venir"
                            foreach ($arrayCompetition as $valueCompetition)
                            {
                                if ($valueCompetition->getId_Competition_Struct() == $value->getId_Struct())
                                {
                                    $testVide = true;
                                }
                            }
                            
                            
                            
                            
                            // Test si une structure peut être supprimer
                            if ($testVide)
                            {
                                $classBoutonSupprimer = 'classBoutonSupprimerGris';
                                ?>
                                <script> $(#['boutonSupprimer<?= $i ?>']).prop(disabled, true); </script>
                                <h3> <button class="<?= $classBoutonSupprimer ?>" onMouseOver="displayDivInfo('<blockquote>Cette structure participe à <br/>une compétition \'en cours\' ou \'à venir\'</blockquote>');" onMouseOut="displayDivInfo()"> X </button> </h3>
                                <?php
                            }
                            else
                            {
                                $classBoutonSupprimer = 'classBoutonSupprimerRouge';
                                ?>
                                <h3> <button class="<?= $classBoutonSupprimer ?>" id="boutonSupprimer<?= $i ?>"> X </button> </h3>
                                <?php
                            }
                            ?>
                            

                        </td>
                    </tr>
                                    
                    <!-- Pop up de suppression -->
                    <div id="bg-modal" class="bg-modal<?= $i ?>">
                        <div class="modal-content">
                            <!-- bouton fermer -->
                            <div id="close" class="close<?= $i ?>">+</div>

                            <!-- Confirmation de suppression -->
                            <img width="50px" height="50px" src="../public/img/pictogramBoxe.png" alt=""/><br/>

                            Voulez-vous vraiment supprimer cette structure ?<br/><br/>

                                            
                            <form name="form" action="modifSuprStruct.php?connect=1" method="POST"> 
                                <input type="hidden" name="Id_StructSupr" value="<?= $value->getId_Struct() ?>"/>
                                <button> Supprimer </button> <br/>
                            </form>
                                            
                            <button class="boutonAnnuler<?= $i ?>"> Annuler </button>
                        </div>
                    </div>

                    <!-- JS -->
                    <script type="text/javascript">                 

                        // Apparition du pop-up
                        document.getElementById('boutonSupprimer<?= $i ?>').addEventListener('click', function() 
                        {
                            document.querySelector('.bg-modal<?= $i ?>').style.display = 'flex';
                        });                    

                        // La croix (ferme le pop-up)
                        document.querySelector('.close<?= $i ?>').addEventListener('click', function() 
                        {
                            document.querySelector('.bg-modal<?= $i ?>').style.display = 'none';
                        });      

                        // Le bouton annuler (ferme le pop-up)
                        document.querySelector('.boutonAnnuler<?= $i ?>').addEventListener('click', function() 
                        {
                            document.querySelector('.bg-modal<?= $i ?>').style.display = 'none';
                        }); 
                                    
                        // Fonction qui affiche du texte au survol du bouton désactiver
                        function displayDivInfo(text)
                        {
                            if(text)
                            {
                                //Détection du navigateur
                                is_ie = (navigator.userAgent.toLowerCase().indexOf("msie") != -1) && (navigator.userAgent.toLowerCase().indexOf("opera") == -1);

                                //Création d'une div provisoire
                                var divInfo = document.createElement('div');
                                divInfo.style.position = 'fixed';
                                document.onmousemove = function(e)
                                {
                                    x = (!is_ie ? e.pageX-window.pageXOffset : event.x+document.body.scrollLeft);
                                    y = (!is_ie ? e.pageY-window.pageYOffset : event.y+document.body.scrollTop);
                                    divInfo.style.left = x+15+'px';
                                    divInfo.style.top = y+15+'px';
                                }
                                divInfo.id = 'divInfo';
                                divInfo.innerHTML = text;
                                document.body.appendChild(divInfo);
                            }
                            else
                            {
                                document.onmousemove = '';
                                document.body.removeChild(document.getElementById('divInfo'));
                            }
                        }                                    
                                    
                    </script>                    
                    <?php       
                    $i++;
                }
                ?>
            </table>
            <br/>
        </section>
    </body>
</html>