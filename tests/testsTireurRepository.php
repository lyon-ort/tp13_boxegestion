<?php

require '../config/config.php';

// Déclaration TireurRepository
$tireurRepository = new TireurRepository();

// Déclaration du tableau des valeurs
$valeurTireur = array(
    "Id_Tir_Niv_Tireur" => 1,
    "Nom_Tireur" => "toto",
    "Prenom_Tireur" => "test",
    "Date_Naissance" => "2019-08-08",
    "Num_Licence" => 1,
    "Poids_Tireur" => 1,
    "Id_Tir_Cat_Poids" => 1,
    "Id_Tir_Cat_Age" => 1,
    "Id_Tir_Sexe" => 1,
    "Id_Tir_Struct" => 1
);

// Initialisation et dump() des valeurs
$tireur = new Tireur($valeurTireur);
var_dump($tireur);

// Fonction sauver pour créer ou modifier un tireur
$tireurRepository->sauver($tireur);

