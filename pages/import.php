<?php
require_once '../config/config.php';


// Réception du fichier .csv
$repertoireDestination = "../csvPourOdroid/"; // -/var/www/html/boxeApp/csvPourOdroid/
$nomDestination = "bddboxe.csv";

// Uplodad du fichier
if (is_uploaded_file($_FILES["monfichier"]["tmp_name"])) 
{
    // Déplacement dans le dossier csvPourOdroid + IMPORT
    if (rename($_FILES["monfichier"]["tmp_name"], $repertoireDestination . $nomDestination)) 
    {
        echo "Le fichier temporaire " . $_FILES["monfichier"]["tmp_name"] . " a été déplacé vers " . $repertoireDestination . $nomDestination;
        
        // Connexion à la BDD
        $db = dbConnect();
        
        // Désactive temporairement les clés étrangères
        $db->exec(sprintf("SET FOREIGN_KEY_CHECKS = 0;"));

        // Initialisation du tableaux .csv
        $bddboxeCSV = file("../csvPourOdroid/bddboxe.csv"); // -/var/www/html/boxeApp/csvPourOdroid/competition.csv         
        

        // Déclaration et initialisation du tableau
        $tableauDesTables = array("Rencontre", "End");
        
        // Initialisation de $i
        $i = 0;
        
        for ($y = 0; $y != count($tableauDesTables) - 1; $y++)
        {
            $excel = "";

            // Importation
            while ($bddboxeCSV[$i] != $tableauDesTables[$y + 1]."\n")
            {
                $excel .= $bddboxeCSV[$i];
                $i++;            
            }

            // Création du fichier .csv
            $fichierCSV = fopen("../csvPourOdroid/".$tableauDesTables[$y].".csv", "x+"); // -/var/www/html/boxeApp/csvPourOdroid/competition.csv

            // Initialisation du fichier .csv
            fputs($fichierCSV, $excel);

            $db->exec(sprintf("LOAD DATA local "
                            . "INFILE '../csvPourOdroid/".$tableauDesTables[$y].".csv' replace " // -/var/www/html/boxeApp/csvPourOdroid/competition.csv
                            . "INTO TABLE ".$tableauDesTables[$y]." "
                            . "FIELDS TERMINATED BY ',' "
                            . "LINES TERMINATED BY '\n'"
                            . "IGNORE 1 LINES;"));
            
            // Suppression de la variable et du fichier
            unset($fichierCSV);
            unlink("../csvPourOdroid/".$tableauDesTables[$y].".csv"); // -/var/www/html/boxeApp/csvPourOdroid/competition.csv
        }
        
        // Suppression du bddboxe.csv
        unlink("../csvPourOdroid/bddboxe.csv"); // -/var/www/html/boxeApp/csvPourOdroid/competition.csv
        
        // Réactive les clés étrangères
        $db->exec(sprintf("SET FOREIGN_KEY_CHECKS = 1;"));
        
        header('location: accueilConnect.php?connect=1&message=1');
    } 
    else 
    {
        echo "Le déplacement du fichier temporaire a échoué" . " vérifiez l'existence du répertoire " . $repertoireDestination;
    }
} 
else 
{
    echo "Le fichier n'a pas été uploadé (trop gros ?)";
}

