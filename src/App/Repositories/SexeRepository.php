<?php

require '../src/App/Entities/Sexe.php';

class SexeRepository {

    protected $table;     
    protected $classMapped; 
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire)
    public function __construct() {
        $this->table = 'Sexe';
        $this->classMapped = 'App\Entities\Sexe';
        $this->idFieldName = 'Id_Sexe';
    }
    
    
    
    // Sélectionne le sexe selon un id
    public function selectionSexeId($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $objet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE $this->idFieldName = :idMark");
        $req->execute(array('idMark' => $id));
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$objet = new $this->table($row);
	    }
        }
        
        // Return de l'objet
        return $objet;
    }
    
    
    
    
    // Sélectionne tous les sexes
    public function selectionSexe()
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objets à NULL si jamais elle la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->query("SELECT * FROM $this->table");
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;
    }         

















































    /*
    // Séléctionne tous les sexes
    public function selectionSexe()
    {
        $db = dbConnect();
        $req = $db->query("SELECT * FROM $this->table");
        $i = 0;
        
        while ($data = $req->fetch())
        {
            $tableauSexe[$i] = array(
                'Id_Sexe' => $data['Id_Sexe'],
                'Libelle_Sexe' => $data['Libelle_Sexe']
            );
            $i++;
        }
        
        return $tableauSexe;
    }*/
    
    
    // Sélectionne le sexe par rapport à son id, et renvoie le libelle
    public function selectionSexeIdNon($idSexe)
    {
        $db = dbConnect();
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Sexe = :idSexeMark");
        $req->execute(array('idSexeMark' => $idSexe));
        
        $data = $req->fetch();
        
        return $data['Libelle_Sexe'];
    }
}