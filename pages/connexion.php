<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';

// Initialisation de l'erreur de connexion
$error = isset($_GET['error']) ? $_GET['error'] : '';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Accueil </title>
    </head>
    
    <body>
        
        <!-- Header -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>
 
        
        <!-- Section principal -->
        <section class='container-fluid about'>
            <h1> Connexion </h1>
                    
            <hr class="separator">
            
            <form method="POST" class="formCreat" action="traitementConnexion.php">
                Connectez-vous avec votre login<br/> et votre mot de passe
                <br/><br/>
                <!-- Vérification de l'erreur de connexion -->
                <p class='messageErreur'>
                <?php
                switch ($error) 
                {
                    case 2:
                        echo "* Le login ou le mot de passe n'est pas valide";
                        break;
                    case 3:
                        echo "Vous avez été déconnecté";
                        break;
                }
                ?></p>
                <input type="text" placeholder="Login" name="login" required/> <br/><br/>
                <input type="password" placeholder="Mot de passe" name="password" required/> <br/><br/>
                <input type="submit" value="Connexion"/> <br/><br/>
            </form> 
        </section>
        <!-- End Section principal -->
        
        
        <!-- Footer -->
        <footer class='container-fluid footer'>
            
        </footer>
        <!-- End Footer -->
    </body>
</html>

