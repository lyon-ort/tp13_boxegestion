<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Modifier / Supprimer compétition </title>
    </head>
    
    <body>
        <!-- Header (inc) -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>
        
        
        <!-- Section -->
        <section class='container-fluid about'>
            
            <!-- Information de l'utilisateur (inc) -->
            <?php
            include '../inc/information.php';
            ?>
            
            <h1> Liste compétition </h1>
            
            <hr class="separator">
            
            <!-- Liste des compétitions (peut modifier ou supprimer) -->
            <?php
            // Modification compétition
            if (isset($_POST['Id_CompetitionModif']))
            {
                // Converssion "minute:seconde" en "seconde" (int)
                $secondes = $_POST['Duree_RoundModif'][1] * 60 + ($_POST['Duree_RoundModif'][3] * 10) + $_POST['Duree_RoundModif'][4];
                $sexe = ($_POST['Id_Competition_SexeModif']) ? $_POST['Id_Competition_SexeModif'] : NULL;
                $typeAffrontement = ($_POST['Id_Competition_Typ_AffrontementModif']) ? $_POST['Id_Competition_Typ_AffrontementModif'] : NULL;


                // TEST SI LE NOM EXISTE DEJA
                $competitionRep = new CompetitionRepository();
                $arrayCompetition = $competitionRep->selectionCompetition();

                $test = true;

                foreach ($arrayCompetition as $value)
                {
                    if ($value->getNom_Competition() != $_POST['NomDeBase'])
                    {
                        if ($value->getNom_Competition() == $_POST['Nom_CompetitionModif'])
                        {
                            $test = false;
                        }
                    }
                }

                if ($test)
                {
                    // Déclaration du tableau des valeurs
                    $valeurCompetition = array(
                        "Id_Competition" => $_POST['Id_CompetitionModif'],
                        "Nom_Competition" => $_POST['Nom_CompetitionModif'],
                        "Date_Debut_Competition" => $_POST['Date_Debut_CompetitionModif'],
                        "Date_Fin_Competition" => $_POST['Date_Fin_CompetitionModif'],
                        "Adresse_Competition" => $_POST['Adresse_CompetitionModif'],
                        "Cp_Competition" => $_POST['Cp_CompetitionModif'],
                        "Ville_Competition" => $_POST['Ville_CompetitionModif'],
                        "Date_Limite_Inscript_Competition" => $_POST['Date_Limite_Inscript_CompetitionModif'],
                        "Nb_Round" => $_POST['Nb_RoundModif'],
                        "Duree_Round" => $secondes,
                        "Nb_Ring" => $_POST['Nb_RingModif'],
                        "Id_Competition_Sexe" => $sexe,
                        "Id_Competition_Typ_Affrontement" => $typeAffrontement,
                        "Id_Competition_Struct" => $_POST['Id_Competition_StructModif'],
                        "Inactif_Competition" => 0
                    );

                    $competition = new Competition($valeurCompetition);
                    $competitionRep->sauver($competition);   

                    // CATEGORIE AGE
                    $objetSeFaireRep = new SeFaireRepository();

                    // Suppression des anciennes catégorie d'age
                    $objetSeFaireRep->SupprimerSeFaireIdCompetition($_POST['Id_CompetitionModif']);

                    // Boucle de sauvegarde
                    foreach ($_POST['categorieAgeCompetition'] as $value)
                    {
                        $valeurSeFaire = array(
                            "Id_SeFaire_Competition" => $_POST['Id_CompetitionModif'],
                            "Id_SeFaire_Cat_Age" => $value
                        );

                        $objetSeFaire = new SeFaire($valeurSeFaire);

                        $objetSeFaireRep->sauver($objetSeFaire);
                    } 
                    
                    //header('location: modifSuprimCompet.php?connect=1&message=2');
                ?>
                    <script>
                    window.location.href = "modifSuprimCompet.php?connect=1&message=2";
                    </script>
                    <?php
                }
                // Si il y a un problème dans le nom
                else
                {
                    ?>
                        <!-- Renvoie vers la page de modif -->
                        <form name="form" action="modifCompet.php?connect=1" method="POST"> 
                            <input type="hidden" name="Id_Competition" value="<?= $_POST['Id_CompetitionModif'] ?>"/>
                            <input type="hidden" name="Nom_Competition" value="<?= $_POST['Nom_CompetitionModif'] ?>"/>
                            <input type="hidden" name="NomDeBase" value="<?= $_POST['NomDeBase'] ?>"/>
                            <input type="hidden" name="Date_Debut_Competition" value="<?= $_POST['Date_Debut_CompetitionModif'] ?>"/>
                            <input type="hidden" name="Date_Fin_Competition" value="<?= $_POST['Date_Fin_CompetitionModif'] ?>"/>
                            <input type="hidden" name="Adresse_Competition" value="<?= $_POST['Adresse_CompetitionModif'] ?>"/>
                            <input type="hidden" name="Cp_Competition" value="<?= $_POST['Cp_CompetitionModif'] ?>"/>
                            <input type="hidden" name="Ville_Competition" value="<?= $_POST['Ville_CompetitionModif'] ?>"/>
                            <input type="hidden" name="Date_Limite_Inscript_Competition" value="<?= $_POST['Date_Limite_Inscript_CompetitionModif'] ?>"/>
                            <input type="hidden" name="Nb_Round" value="<?= $_POST['Nb_RoundModif'] ?>"/>
                            <input type="hidden" name="Duree_Round" value="<?= $_POST['Duree_RoundModif'] ?>"/>
                            <input type="hidden" name="Nb_Ring" value="<?= $_POST['Nb_RingModif'] ?>"/>
                            <input type="hidden" name="Id_Competition_Sexe" value="<?= $_POST['Id_Competition_SexeModif'] ?>"/>
                            <input type="hidden" name="Id_Competition_Typ_Affrontement" value="<?= $_POST['Id_Competition_Typ_AffrontementModif'] ?>"/>
                            <input type="hidden" name="Id_Competition_Struct" value="<?= $_POST['Id_Competition_StructModif'] ?>"/>
                            <input type="hidden" name="categorieAgeCompetition" value="<?= $_POST['categorieAgeCompetition'] ?>"/>
                            <input type="hidden" name="error" />
                        </form>

                        <!-- Script qui renvoie les variables POST vers la page de modification -->
                        <script type="text/javascript"> 
                            document.form.submit(); //on envoie le formulaire vers la page de modification
                        </script> 
                    <?php         
                }
            }
            
            
            if (isset($_POST['Id_CompetitionSupr']))
            {
                // COMPETITION
                $objetCompetitionRep2 = new CompetitionRepository();
                $objetCompetition2 = $objetCompetitionRep2->selectionCompetitionId($_POST['Id_CompetitionSupr']);

                // Par rapport au script
                $inactif = $objetCompetition2->getInactif_Competition();

                if (!$inactif)
                {
                    // Modifie les "Flag" et supprime les utilisateurs de la structure
                    $objetCompetition2->setInactif_Competition(1);
                    $objetCompetitionRep2->sauver($objetCompetition2);
                    ?>
                            <!-- Pop up de confirmation de suppression -->
                            <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                                <div class="modal-content" style="height: 250px;">
                                    <!-- bouton fermer -->
                                    <div id="close" class="closeTest">+</div>

                                    <!-- Confirmation de suppression -->
                                    <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                                    La compétition a été supprimé !<br/><br/>

                                    <button class="boutonAnnulerTest"> OK </button>
                                </div>
                            </div>   

                             <script>
                                // La croix (ferme le pop-up)
                                document.querySelector('.closeTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });      

                                // Le bouton annuler (ferme le pop-up)
                                document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });       
                            </script>            
                    <?php 
                }                
            }
            
            
            // Message de confirmation de modification
            if (isset($_GET['message']) && $_GET['message'] == 2)
            {
                ?>
                    <!-- Pop up de confirmation de suppression -->
                    <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                        <div class="modal-content" style="height: 250px;">
                            <!-- bouton fermer -->
                            <div id="close" class="closeTest">+</div>

                            <!-- Confirmation de suppression -->
                            <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                            La compétition a été modifié !<br/><br/>

                            <button class="boutonAnnulerTest"> OK </button>
                        </div>
                    </div>   

                    <script>
                        // La croix (ferme le pop-up)
                        document.querySelector('.closeTest').addEventListener('click', function() 
                        {
                            document.querySelector('.bg-modalTest').style.display = 'none';
                        });      

                        // Le bouton annuler (ferme le pop-up)
                        document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                        {
                            document.querySelector('.bg-modalTest').style.display = 'none';
                        });       
                    </script>            
                <?php                
            }
            ?>
        
            <table>
                <th> Nom </th> 
                <th> Statut </th>
                <th> Date début </th> 
                <th> Date fin </th> 
                <th> Créer / Modifier tableaux </th>
                <th> Créer / Modifier rencontres </th> 
                <th> Modifier </th> 
                <th> Suprimmer </th>
                
                <?php
                    $objetCompetitionRep = new CompetitionRepository();
                    $arrayObjetCompetition = $objetCompetitionRep->selectionCompetitionidStructure($_SESSION['idStruct']);
                    
                    $objetTableauRep = new TableauRepository();
                    
                    // Incrémentation
                    $i = 0;
                    
                    foreach ($arrayObjetCompetition as $value)
                    {
                        if (!$value->getInactif_Competition())
                        {
                             $backgroundColor = '';  
                             $classBoutonSupprimer = '';
                             $classBoutonModifier = '';
                             $disabled = '';
                             
                            // Compétition "à venir"
                            if (date("Y-m-d H:i:s") < $value->getDate_Debut_Competition())
                            {
                                $backgroundColor = 'background-color: lightgreen;';
                                
                                // Avec tableaux
                                if ($objetTableauRep->selectionTableauxIdCompet($value->getId_Competition()))
                                {
                                    $classBoutonSupprimer = 'classBoutonSupprimerGris';
                                    $classBoutonModifier = 'boutonModifierGris';  
                                    $boutonSupprimer = '';
                                    $boutonModifier = '';
                                    $test = 4;
                                }
                                // Sans tableaux
                                else
                                {
                                    $classBoutonSupprimer = 'classBoutonSupprimerRouge';
                                    $classBoutonModifier = 'boutonModifierBleu';   
                                    $boutonSupprimer = 'boutonSupprimer';  
                                    $boutonModifier = 'boutonModifier';
                                    $test = 1;                                    
                                }
                            }
                            // Compétition "en cours"
                            else if (date("Y-m-d H:i:s") == $value->getDate_Debut_Competition() || 
                                     date("Y-m-d H:i:s") > $value->getDate_Debut_Competition() && date("Y-m-d H:i:s") < $value->getDate_Fin_Competition()
                                    )
                            {
                                $backgroundColor = 'background-color: lightblue;';
                                $classBoutonSupprimer = 'classBoutonSupprimerGris';
                                $classBoutonModifier = 'boutonModifierGris';
                                $disabled = 'disabled';
                                $boutonSupprimer = '';
                                $boutonModifier = '';
                                
                                $test = 2;
                                ?>
                                
                                <?php
                            }
                            // Compétition "fini"
                            else 
                            {
                                $backgroundColor = 'background-color: lightsalmon;';
                                $classBoutonSupprimer = 'classBoutonSupprimerRouge';
                                $classBoutonModifier = 'boutonModifierGris';
                                $disabled = 'disabled';
                                $boutonSupprimer = 'boutonSupprimer';
                                $boutonModifier = '';
                                $test = 3;
                            } 
                            
                            
                ?>
                
                            <script>
                                test = <?= $test ?>;

                                // A venir sans tableaux
                                if (test == 1)
                                {
                                    message<?= $i ?> = "";                                    
                                }      
                                // A venir sans tableaux
                                else if (test == 4)
                                {
                                    message<?= $i ?> = "La compétition a des tableaux";
                                }                                  
                                // En cours
                                else if (test == 2)
                                {
                                    message<?= $i ?> = "La compétition est en 'cours'"; 
                                }
                                // Fini
                                else if (test == 3)
                                {
                                    message<?= $i ?> = "";
                                }      
                            </script>
                            
                            <tr>
                                <!-- Le nom -->
                                <td> <?= $value->getNom_Competition() ?> </td>
                                
                                <!-- Le statut de la compétition -->
                                <td style="<?= $backgroundColor ?>"> 
                                    <?php
                                    // Compétition "à venir"
                                    if (date("Y-m-d H:i:s") < $value->getDate_Debut_Competition())
                                    {
                                        echo 'A venir';
                                    }
                                    // Compétition "en cours"
                                    else if (date("Y-m-d H:i:s") == $value->getDate_Debut_Competition() || 
                                             date("Y-m-d H:i:s") > $value->getDate_Debut_Competition() && date("Y-m-d H:i:s") < $value->getDate_Fin_Competition()
                                            )
                                    {
                                        echo 'En cours ...';
                                    }
                                    // Compétition "fini"
                                    else 
                                    {
                                        echo 'Fini';
                                    }
                                    ?>                                
                                </td>

                                <!-- La date de début -->
                                <td> <?= $value->getDate_Debut_Competition() ?> </td>

                                <!-- La date de fin -->
                                <td> <?= $value->getDate_Fin_Competition() ?> </td>

                                <!-- Le bouton de création / modification de tableaux -->
                                <td>  
                                    <form method="POST" action="ajouterTableaux.php<?= $leGet ?>">
                                        <input type="hidden" name="Id_Competition" value="<?= $value->getId_Competition() ?>"/>                        
                                        <input type="submit" value="Créer / Modifier" <?= $disabled ?>/>
                                    </form>
                                </td>

                                <!-- Le bouton de création / modification de rencontre -->
                                <td> 
                                    
                                    
                                    <form method="POST" action="creaRencontre.php<?= $leGet ?>">

                                        <!-- Liste déroulante des tableaux -->
                                        <?php
                                            // Sélectionne les tableaux de la compétition
                                        if ($objetTableauRep->selectionTableauxIdCompet($value->getId_Competition()))
                                        {
                                            $arrayObjetTableau = $objetTableauRep->selectionTableauxIdCompet($value->getId_Competition());
                                        ?>  

                                            <select name="tableau" required>
                                                <option value=""> -- Choisir le tableau -- </option>
                                                <?php
                                                    foreach ($arrayObjetTableau as $value2)
                                                    {
                                                ?>
                                                        <option value="<?= $value2->getId_Tab() ?>"> <?= $value2->getLibelle_Tab() ?> </option>
                                                <?php
                                                    }
                                                ?>
                                            </select>
                                        <input type="submit" value="Créer / Modifier" <?= $disabled ?>/>
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                        <input type="text" style="text-align: center;" value="Il n'y a pas de tableaux pour le moment" <?= $disabled ?> readonly />
                                            <?php
                                        }
                                        ?>


                                    </form>                    
                                </td>

                                <!-- Le bouton de modification de compétition -->
                                <td> 
                                    <form name="form<?= $i ?>" action="modifCompet.php?connect=1" method="POST">
                                        <input type="hidden" name="Id_Competition" value="<?= $value->getId_Competition() ?>" />
                                    </form>
                                    
                                    <h3> 
                                        <button class="<?= $classBoutonModifier ?>" id="<?= $boutonModifier ?><?= $i ?>" onMouseOver="displayDivInfo(message<?= $i ?>);" onMouseOut="displayDivInfo()"> Modifier </button> 
                                    </h3>                                                                                                              
                                    
                                </td>

                                <!-- Le bouton suppression de compétition -->
                                <td> 
                                    <h3> <button class="<?= $classBoutonSupprimer ?>" id="<?= $boutonSupprimer ?><?= $i ?>" onMouseOver="displayDivInfo(message<?= $i ?>);" onMouseOut="displayDivInfo()"> X </button> </h3>
                                </td>
                            </tr>
                                  
                            
                            <!-- Pop up de suppression -->
                            <div id="bg-modal" class="bg-modal<?= $i ?>">
                                <div class="modal-content">
                                    <!-- bouton fermer -->
                                    <div id="close" class="close<?= $i ?>">+</div>

                                    <!-- Confirmation de suppression -->
                                    <img width="50px" height="50px" src="../public/img/pictogramBoxe.png" alt=""/><br/>

                                    Voulez-vous vraiment supprimer cette compétition ?<br/><br/>

                                    <form name="form" action="modifSuprimCompet.php?connect=1" method="POST"> 
                                        <input type="hidden" name="Id_CompetitionSupr" value="<?= $value->getId_Competition() ?>"/>

                                        <button> Supprimer </button> <br/>
                                    </form>

                                    <button class="boutonAnnuler<?= $i ?>"> Annuler </button>
                                </div>
                            </div>                             
                            
                            
                            <!-- Script -->
                            <script>
                                // Apparition du pop-up "supprimer"
                                document.getElementById('boutonSupprimer<?= $i ?>').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modal<?= $i ?>').style.display = 'flex';
                                });                    
                                // La croix (ferme le pop-up)
                                document.querySelector('.close<?= $i ?>').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modal<?= $i ?>').style.display = 'none';
                                });      
                                // Le bouton annuler (ferme le pop-up)
                                document.querySelector('.boutonAnnuler<?= $i ?>').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modal<?= $i ?>').style.display = 'none';
                                });      
                                // Envoie vers la page de modification
                                document.getElementById('boutonModifier<?= $i ?>').addEventListener('click', function() 
                                {
                                    //document.querySelector('.bg-modal<?= $i ?>').style.display = 'flex';
                                    document.form<?= $i ?>.submit();
                                });                                 
                            </script>                        
                <?php
                        }
                        $i++;
                    }
                ?>
                            
                <script>
                // Fonction qui affiche du texte au survol du bouton désactiver
                function displayDivInfo(text)
                {
                    if(text)
                    {
                        //Détection du navigateur
                        is_ie = (navigator.userAgent.toLowerCase().indexOf("msie") != -1) && (navigator.userAgent.toLowerCase().indexOf("opera") == -1);

                        //Création d'une div provisoire
                        var divInfo = document.createElement('div');
                        divInfo.style.position = 'fixed';
                        document.onmousemove = function(e)
                        {
                            x = (!is_ie ? e.pageX-window.pageXOffset : event.x+document.body.scrollLeft);
                            y = (!is_ie ? e.pageY-window.pageYOffset : event.y+document.body.scrollTop);
                            divInfo.style.left = x+15+'px';
                            divInfo.style.top = y+15+'px';
                        }
                        divInfo.id = 'divInfo';
                        divInfo.innerHTML = text;
                        document.body.appendChild(divInfo);
                    }
                    else
                    {
                        document.onmousemove = '';
                        document.body.removeChild(document.getElementById('divInfo'));
                    }
                } 
                </script>
            </table>
            <br/>
        </section>  
    </body>
</html>