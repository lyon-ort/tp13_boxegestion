<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';
$leGet = '?connect=1';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Modifier utilisateur </title>
    </head>
    <body>
        <!-- Header -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>
        
        
        <section class='container-fluid about'>
            
            <?php
            if (isset($_GET['connect']))
            {
            ?>
                <div class="information">
                    Login : <?= $_SESSION['login'] ?> <br/>
                    Prénom : <?= $_SESSION['Prenom_Utilisateur'] ?> <br/>
                    Nom : <?= $_SESSION['Nom_Utilisateur'] ?> <br/>
                    Rôle :
                    <?php
                    $objetTypeRoleRepUtil = new TypeRoleRepository(); 
                    $typeRole = $objetTypeRoleRepUtil->selectionTypeRoleId($_SESSION['idRole']);
                    echo $typeRole->getLibelle_Typ_Role();
                    ?>                                 
                    <br/>
                    Structure :
                    <?php
                    $objetStructureRepUtil = new StructureRepository(); 
                    $structure = $objetStructureRepUtil->selectionStructureId($_SESSION['idStruct']);
                    echo $structure->getNom_Struct();
                    ?> <br/><br/><br/>
                    <button class='btn btn-custom' onclick="Deconnexion()"> Déconnexion </button>
                </div>   
            <?php
            }
            ?>            
            
            <h1> Modifier utilisateur </h1>
            
            <hr class="separator">
            
            <?php
            // Message d'erreur si le nom existe déjà
            if (isset($_POST['error']))
            {
                echo '<p class="error">ERREUR : Ce "login" d\'utilisateur existe déjà</p> <br/><br/>';
                $error = 'error';
            }
            else
            {
                $error = '';
            }
            
            // Objet de l'a structure l'utilisateur à modifier
            $objetUtilisateurRep = new UtilisateurRepository();
            $objetUtilisateur = $objetUtilisateurRep->selectionUtilisateurId($_POST['Id_Utilisateur']);
            ?>


            <!-- Formulaire modification utilisateur -->
            <form class="formCreat" method="POST" action="modifSuprUtil.php">
                
                Type de role: 
                <?php
                    $objetTypeRoleRep = new TypeRoleRepository();
                    $objetTypeRole = $objetTypeRoleRep->selectionTypeRoleId($objetUtilisateur->getId_Typ_Role_Utilisateur());
                ?>
                <input type="text" value="<?= $objetTypeRole->getLibelle_Typ_Role() ?>" disabled>
                <input type="hidden" name="Id_Typ_Role_UtilisateurModif" value="<?= $objetTypeRole->getId_Typ_Role() ?>"/>
                <br/><br/>
                    
                Structure : 
                <select name="Id_Struct_UtilisateurModif" required>
                    <option value=""> -- Choisir une structure -- </option>
                    <?php
                    // Structure
                    $objetStructureRep = new StructureRepository();
                    $arrayObjetStructure = $objetStructureRep->selectionStructure();
                    // Type de structure
                    $objetTypeStructureRep = new TypeStructureRepository();
                                        
                    
                    // Responsable de club / Membre de club
                    if ($objetUtilisateur->getId_Typ_Role_Utilisateur() == 1 || $objetUtilisateur->getId_Typ_Role_Utilisateur() == 5) 
                    {
                        foreach ($arrayObjetStructure as $value)
                        {
                            // Club
                            if ($value->getId_Typ_Struct() == 1)
                            {
                                $POSTouGetObjet = (isset($_POST['Id_Struct_Utilisateur']) ? $_POST['Id_Struct_Utilisateur'] : $objetUtilisateur->getId_Struct_Utilisateur());
                                $selected = ($value->getId_Struct() == $POSTouGetObjet) ? 'selected' : '';
                                $objetTypeStructure = $objetTypeStructureRep->selectionTypeStructureId($value->getId_Typ_Struct());
                                ?>
                                    <option value="<?= $value->getId_Struct() ?>" <?= $selected ?>> <?= $value->getNom_Struct() . ' : ' . $objetTypeStructure->getLibelle_Typ_Struct()?> </option> 
                                <?php
                            }

                        }
                    }
                    // Administrateur
                    else if ($objetUtilisateur->getId_Typ_Role_Utilisateur() == 2)
                    {
                        foreach ($arrayObjetStructure as $value)
                        {
                            // Fédération ou comité
                            if ($value->getId_Typ_Struct() == 2 || $value->getId_Typ_Struct() == 3)
                            {
                                $POSTouGetObjet = (isset($_POST['Id_Struct_Utilisateur']) ? $_POST['Id_Struct_Utilisateur'] : $objetUtilisateur->getId_Struct_Utilisateur());
                                $selected = ($value->getId_Struct() == $POSTouGetObjet) ? 'selected' : '';
                                $objetTypeStructure = $objetTypeStructureRep->selectionTypeStructureId($value->getId_Typ_Struct());
                                ?>
                                    <option value="<?= $value->getId_Struct() ?>" <?= $selected ?>> <?= $value->getNom_Struct() . ' : ' . $objetTypeStructure->getLibelle_Typ_Struct() ?> </option> 
                                <?php
                            }

                        }
                    }
                    // Officiel et opérateur
                    else
                    {
                        foreach ($arrayObjetStructure as $value)
                        {
                            // TOUT
                            $POSTouGetObjet = (isset($_POST['Id_Struct_Utilisateur']) ? $_POST['Id_Struct_Utilisateur'] : $objetUtilisateur->getId_Struct_Utilisateur());
                            $selected = ($value->getId_Struct() == $POSTouGetObjet) ? 'selected' : '';
                            $objetTypeStructure = $objetTypeStructureRep->selectionTypeStructureId($value->getId_Typ_Struct());
                            ?>
                                <option value="<?= $value->getId_Struct() ?>" <?= $selected ?>> <?= $value->getNom_Struct() . ' : ' . $objetTypeStructure->getLibelle_Typ_Struct() ?> </option> 
                            <?php
                        }                        
                    }
                    ?>
                </select> 
                <br/><br/>
                    
                Nom : 
                <input type="text" name="Nom_UtilisateurModif" placeholder="nom" value="<?= $valueNom = (isset($_POST['Nom_Utilisateur'])) ? $_POST['Nom_Utilisateur'] : $objetUtilisateur->getNom_Utilisateur() ?>" required> 
                <br/><br/>
                    
                Prénom : 
                <input type="text" name="Prenom_UtilisateurModif" placeholder="prénom" value="<?= $valuePrenom = (isset($_POST['Prenom_Utilisateur'])) ? $_POST['Prenom_Utilisateur'] : $objetUtilisateur->getPrenom_Utilisateur() ?>" required> 
                <br/><br/>
                    
                Login : 
                <input id="<?= $error ?>" type="text" name="Login_UtilisateurModif" placeholder="login" value="<?=$valueLogin = (isset($_POST['Login_Utilisateur'])) ? $_POST['Login_Utilisateur'] : $objetUtilisateur->getLogin_Utilisateur() ?>" required> 
                <input type="hidden" name="LoginDeBase" value="<?= $valueLoginDeBase = (isset($_POST['LoginDeBase'])) ? $_POST['LoginDeBase'] : $objetUtilisateur->getLogin_Utilisateur() ?>" />
                <br/><br/>
                    
                Email : 
                <input type="email" name="EmailModif" placeholder="xyz@gmail.com" value="<?= $valueEmail = (isset($_POST['Email'])) ? $_POST['Email'] : $objetUtilisateur->getEmail() ?>" required> 
                <br/><br/>
                    
                Portable : 
                <input type="tel" minlength="10" maxlength="10" name="Tel_PortModif" placeholder="0123456789" value="<?= $valueTel = (isset($_POST['Tel_Port'])) ? $_POST['Tel_Port'] : $objetUtilisateur->getTel_Port() ?>" required> 
                <br/><br/>

                <!-- Les données à envoyé par défaut -->
                <input type="hidden" name="Id_UtilisateurModif" value="<?= $objetUtilisateur->getId_Utilisateur() ?>">
                <input type='hidden' name="MdpModif" value="<?= $objetUtilisateur->getMdp() ?>"/>               
                <input type='hidden' name="Date_Creation_CompteModif" value="<?= $objetUtilisateur->getDate_Creation_Compte() ?>"/>
                <input type='hidden' name="Dernier_Login_UtilisateurModif" value="<?= $objetUtilisateur->getDernier_Login_Utilisateur() ?>"/>

                <input type="submit" value="Modifier">
            </form>    
            <br/>
        </section>
    </body>
</html>