<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';
// Page de fonction
require_once 'Function.php';

// Si il y a déjà des rencontres, alors ça redirige vers les détails de la compétition
// Séléction des rencontres selon les tableaux
$rencontreRep = new RencontreRepository();


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Création / Modification des rencontres </title>
    </head>
    <body>
        <!-- Header (inc) -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>
        
        
        <!-- Section -->
        <section class='container-fluid about'>
            
            <!-- Information de l'utilisateur (inc) -->
            <?php
            include '../inc/information.php';
            ?>
            
            <h1> Création / Modification rencontres </h1>
            
            <hr class="separator">
<?php

// Si ils restent à true, il active la fonction sauver()
$confirmationInterne = true;
$confirmationExterne = true;

 


// Initialisation et vérification des rencontres
$taille = $_POST['taille'];
$sauverRencontreRepository = new RencontreRepository();

$typeTableauRep = new TableauRepository();
 
 

for($i = 0; $i < $taille ; $i++)
{
    $Date_Rencontre[$i] = $_POST['Date_Rencontre' .$i];
    $Heure_Debut_Rencontre[$i] = $_POST['Date_Rencontre' .$i]. " " . $_POST['Heure_Debut_Rencontre'.$i]. ":00";
    $Heure_Fin_Rencontre[$i] = $_POST['Date_Rencontre' .$i]. " " . "19:10:00";
    $Num_Ring_Rencontre[$i] = (int) $_POST['Num_Ring_Rencontre'.$i];
    if((int) $_POST['Id_Tireur_Rouge'.$i] == 0 && (int) $_POST['Id_Tireur_Bleu'.$i] == 0)
    {
    $Id_Tireur_Rouge[$i] =  null;
    $Id_Tireur_Bleu[$i] =  null;
    }
    else
    {
    $Id_Tireur_Rouge[$i] =  (int) $_POST['Id_Tireur_Rouge'.$i];
    $Id_Tireur_Bleu[$i] =  (int) $_POST['Id_Tireur_Bleu'.$i];
    }
   
    $Id_Gagnant[$i] = null;
    $Id_Rencontre_Precedent_1[$i] = null;
    $Id_Rencontre_Precedent_2[$i] = null;
    $Id_Rencontre_Suivant[$i] = $_POST['Id_Rencontre_Suivant'.$i];
    $Id_cat_Rencontre[$i] = (int)$_POST['Id_Cat_Rencontre'.$i];
    $Id_Tab_Rencontre[$i] = (int)$_POST['Id_Tab_Rencontre'.$i];
    $Id_Etat_Rencontre[$i] = (int)$_POST['Id_Etat_Rencontre'.$i];
    $Inactif_Rencontre[$i] = (int)$_POST['Inactif_Rencontre'.$i];
    
   $tableauSauver[$i] = array (
       "Date_Rencontre" => $Date_Rencontre[$i],
       "Heure_Debut_Rencontre" => $Heure_Debut_Rencontre[$i],
       "Heure_Fin_Rencontre" => $Heure_Fin_Rencontre[$i],
       "Num_Ring_Rencontre" => $Num_Ring_Rencontre[$i],
       "Id_Tireur_Rouge" => $Id_Tireur_Rouge[$i],
       "Id_Tireur_Bleu" => $Id_Tireur_Bleu[$i],
       "Id_Gagnant" => $Id_Gagnant[$i],
       "Id_Rencontre_Precedent_1" => $Id_Rencontre_Precedent_1[$i], 
       "Id_Rencontre_Precedent_2" => $Id_Rencontre_Precedent_2[$i], 
       "Id_Rencontre_Suivant" => $Id_Rencontre_Suivant[$i],
       "Id_Cat_Rencontre" => $Id_cat_Rencontre[$i],
       "Id_Tab_Rencontre" => $Id_Tab_Rencontre[$i],
       "Id_Etat_Rencontre" => $Id_Etat_Rencontre[$i],
       "Inactif_Rencontre" => $Inactif_Rencontre[$i]
   ); 
   
   $TypeTableauSauvegarde = $typeTableauRep->typeTableau($tableauSauver[0]['Id_Tab_Rencontre']);
   if($TypeTableauSauvegarde[0]['Id_Tableau_Typ_Tableau'] == 2)
   {
        // CONFIRMATION INTERNE
        $verif[$i] = vérificationTableauInterne($tableauSauver[$i]);

        if ($verif[$i] == 0)
        {
            echo "il y a une erreur à la création de la rencontre ", $i+1, " veuillez recommencer.";
            ?>
            <tr></tr>
            <?php
            echo "Verifiez qu'un tireur ne se batte pas contre lui même.<br/>";

            $confirmationInterne = false;
        }
   }
   elseif($TypeTableauSauvegarde[0]['Id_Tableau_Typ_Tableau'] == 1)
   {
       // CONFIRMATION INTERNE
        $verif[$i] = vérificationTableauInterne($tableauSauver[$i]);

        if ($verif[$i] == 0)
        {
            if($tableauSauver[$i]['Id_Tireur_Rouge'] == null && $tableauSauver[$i]['Id_Tireur_Bleu'] == null )
            {
                $confirmationInterne = true;
            }
            else
            {
            echo "il y a une erreur à la création de la rencontre ", $i+1, " veuillez recommencer.";
            ?>
            <tr></tr>
            <?php
            echo "Verifiez qu'un tireur ne se batte pas contre lui même.<br/>";

            $confirmationInterne = false;
            }
        }
   }
}

 

if($TypeTableauSauvegarde[0]['Id_Tableau_Typ_Tableau'] == 2)
{
    // CONFIRMATION EXTERNE
    for ($i = 0; $i != count($tableauSauver); $i++)
    {
        for ($y = 0; $y != count($tableauSauver); $y++)
        {
            if ($i != $y)
            {
                if ($tableauSauver[$i]['Id_Tireur_Rouge'] == $tableauSauver[$y]['Id_Tireur_Rouge'] && $tableauSauver[$i]['Id_Tireur_Bleu'] == $tableauSauver[$y]['Id_Tireur_Bleu']
                    || $tableauSauver[$i]['Id_Tireur_Rouge'] == $tableauSauver[$y]['Id_Tireur_Bleu'] && $tableauSauver[$i]['Id_Tireur_Bleu'] == $tableauSauver[$y]['Id_Tireur_Rouge'])
                {
                    $confirmationExterne = false;
                }
                elseif($tableauSauver[$i]['Heure_Debut_Rencontre'] == $tableauSauver[$y]['Heure_Debut_Rencontre'] && $tableauSauver[$i]['Num_Ring_Rencontre'] == $tableauSauver[$y]['Num_Ring_Rencontre'])
                {
                    $confirmationExterne = false;
                }
            }
        }
    }
}
elseif($TypeTableauSauvegarde[0]['Id_Tableau_Typ_Tableau'] == 1)
{
     // CONFIRMATION EXTERNE
    for ($i = 0; $i != count($tableauSauver); $i++)
    {
        for ($y = 0; $y != count($tableauSauver); $y++)
        {
            if ($i != $y)
            {
              
                
                if ($tableauSauver[$i]['Id_Tireur_Rouge'] == $tableauSauver[$y]['Id_Tireur_Rouge'] && $tableauSauver[$i]['Id_Tireur_Bleu'] == $tableauSauver[$y]['Id_Tireur_Bleu']
                    || $tableauSauver[$i]['Id_Tireur_Rouge'] == $tableauSauver[$y]['Id_Tireur_Bleu'] && $tableauSauver[$i]['Id_Tireur_Bleu'] == $tableauSauver[$y]['Id_Tireur_Rouge'])
                {
                        if( $tableauSauver[$y]['Id_Tireur_Bleu'] == null && $tableauSauver[$y]['Id_Tireur_Rouge'] == null
                        || $tableauSauver[$i]['Id_Tireur_Bleu'] == null && $tableauSauver[$i]['Id_Tireur_Rouge'] == null)
                        {
                             $confirmationExterne = true;
                        }
                        else
                        {
                            $confirmationExterne = false;
                        }
                }
                elseif($tableauSauver[$i]['Heure_Debut_Rencontre'] == $tableauSauver[$y]['Heure_Debut_Rencontre'] && $tableauSauver[$i]['Num_Ring_Rencontre'] == $tableauSauver[$y]['Num_Ring_Rencontre'])
                {
                    $confirmationExterne = false;
                }
                
            }
        }
    }
}

// MESSAGE D'ERREUR EXTERNE
if (!$confirmationExterne)
{
    echo 'Certaine rencontres ont les mêmes tireurs';
}

 



if($TypeTableauSauvegarde[0]['Id_Tableau_Typ_Tableau'] == 2)
{
// CREATION DES RENCONTRES SI LA CONFIRMATION INTERNE EST JUSTE
    if ($confirmationInterne && $confirmationExterne)
    {
        // Tableau qui stocke les objets
        $tableauObjetRencontre = array();
        // Insère les objets rencontre dans le tableau
        foreach($tableauSauver as $value)
        {
            $tableauObjetRencontre[] = new Rencontre($value);
        }
        // Sauvegarder les rencontres
        foreach($tableauObjetRencontre as $value2)
        {
            $sauverRencontreRepository->sauver($value2);
        }
        // Message de confirmation
        echo "la sauvegarde a été effectué";
    }
}
elseif($TypeTableauSauvegarde[0]['Id_Tableau_Typ_Tableau'] == 1)
{
     
     if ($confirmationInterne && $confirmationExterne)
    {
        // Tableau qui stocke les objets
        $tableauObjetRencontre = array();
        // Insère les objets rencontre dans le tableau
        foreach($tableauSauver as $value)
        {
            $tableauObjetRencontre[] = new Rencontre($value);
        }
        // Sauvegarder les rencontres
        foreach($tableauObjetRencontre as $value2)
        {
            $sauverRencontreRepository->sauver($value2);
        }
        // Message de confirmation
        
    
    $rencontreRepo = new RencontreRepository();
   (int) $idCatRencontre = $tableauSauver[0]['Id_Cat_Rencontre'];
   (int) $idTableau = $tableauSauver[0]['Id_Tab_Rencontre'];
   $idCatRencontreStatique = $idCatRencontre - 1;
   for($j = 0 ; $j < $idCatRencontreStatique ; $j++)
   {    
        $TabAUpdate = $rencontreRepo->tabRencontrePyraTireurs( (int)$idTableau, (int) $idCatRencontre);
        
        $idCatRencontre2 = $idCatRencontre - 1;
        $TabAUpdate2 = $rencontreRepo->tabRencontrePyraSansTireurs( (int) $idTableau,(int) $idCatRencontre2);
      
        for($k = 0 ; $k < count($TabAUpdate); $k++)
        {
            
             for($l = 0 ; $l < 2 ; $l++)
             {
                
                if($TabAUpdate2[0]->getId_Rencontre() != null)
                {
                    $TabAUpdate[0]->setId_Rencontre_Suivante($TabAUpdate2[0]->getId_Rencontre());
                }
                else
                {
                    $TabAUpdate[0]->setId_Rencontre_Suivante(null);
                }
                if($l == 0 )
                {
                    
                    $TabAUpdate2[0]->setId_Rencontre_Precedent_1($TabAUpdate[0]->getId_Rencontre());
                    
                }
                elseif($l == 1)
                {
                   
                   $TabAUpdate2[0]->setId_Rencontre_Precedent_2($TabAUpdate[0]->getId_Rencontre());
                     
                }
                
               
                foreach($TabAUpdate as $valueUpdate1)
                {
                  $sauverRencontreRepository->sauver($valueUpdate1);
                }
                array_shift($TabAUpdate);
             }
              
                foreach($TabAUpdate2 as $valueUpdate2)
                {
                  $sauverRencontreRepository->sauver($valueUpdate2);
                }
             
           
           array_shift($TabAUpdate2); 
          
 
        }
         $idCatRencontre = $idCatRencontre2;
   }
   echo "la sauvegarde a été effectué";
   }
}
if($confirmationInterne && $confirmationExterne)
{
   header('location: detailCompetitionConnect.php?connect=1&tableau='.$tableauSauver[0]['Id_Tab_Rencontre']);
}
else
{
    ?>
            <FORM action="creaRencontre.php?connect=1" method="POST">
                <input style="width: auto;" type="submit" value="Retournez en Arrière" />
                <input type="hidden" name="tableau" value="<?=$tableauSauver[0]['Id_Tab_Rencontre'] ?>" />  
            </FORM>    
    <?php
}

