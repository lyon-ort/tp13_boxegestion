<?php
require '../config/config.php';
// session-verif.php à inclure pour chaque page de session
require_once '../config/session-verif.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Modifier compétition </title>
    </head>
    <body>
        <!-- Header (inc) -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>


         <!-- Section -->
        <section class='container-fluid about'>
            
            <!-- Information de l'utilisateur (inc) -->
            <?php
            include '../inc/information.php';
            ?>
            
            <h1> Modifier compétition </h1>
            
            <hr class="separator">
        
        
        <?php
        // Sélection de la compétition
        $objetCompetitionRep = new CompetitionRepository();
        $objetCompetition = $objetCompetitionRep->selectionCompetitionId($_POST['Id_Competition']);
        
        $error = '';
        
        if (isset($_POST['error']))
        {
            $error = 'error';
            echo 'ERREUR : Le "Nom" de compétition est déjà pris<br/><br/>';
        }
        ?>
        
        
            <!-- Formulaire Modification de compétition -->
            <form class="formCreat" method="POST" action="modifSuprimCompet.php?connect=1">

                <!-- Le nom -->
		Nom : 
                <input id="<?= $error ?>" type="text" name="Nom_CompetitionModif" value="<?= $valueNom = (isset($_POST['Nom_Competition'])) ? $_POST['Nom_Competition'] : $objetCompetition->getNom_Competition() ?>"> 
                <input type="hidden" name="NomDeBase" value="<?= $valueNomDeBase = (isset($_POST['NomDeBase'])) ? $_POST['NomDeBase'] : $objetCompetition->getNom_Competition() ?>"> 
                <br/><br/>
 
                <!-- La date de début -->                
		Date de début : 
                <input type="Date" name="Date_Debut_CompetitionModif" value="<?= $valueDate_Debut_Competition = (isset($_POST['Date_Debut_Competition'])) ? $_POST['Date_Debut_Competition'] : $objetCompetition->getDate_Debut_Competition() ?>"> 
                <br/><br/>

                <!-- La date de fin -->                   
		Date de fin : 
                <input type="Date" name="Date_Fin_CompetitionModif" value="<?= $valueDate_Fin_Competition = (isset($_POST['Date_Fin_Competition'])) ? $_POST['Date_Fin_Competition'] : $objetCompetition->getDate_Fin_Competition() ?>"> 
                <br/><br/>

                <!-- L'adresse -->                   
		Adresse : 
                <input type="text" name="Adresse_CompetitionModif" value="<?= $valueAdresse_Competition = (isset($_POST['Adresse_Competition'])) ? $_POST['Adresse_Competition'] : $objetCompetition->getAdresse_Competition() ?>"> 
                <br/><br/>
                
                <!-- Le code postal -->                   
		Code postale : 
                <input type="text" name="Cp_CompetitionModif" value="<?= $valueCp_Competition = (isset($_POST['Cp_Competition'])) ? $_POST['Cp_Competition'] : $objetCompetition->getCp_Competition() ?>"> 
                <br/><br/>
                
                <!-- La ville -->                   
		Ville : 
                <input type="text" name="Ville_CompetitionModif" value="<?= $valueVille_Competition = (isset($_POST['Ville_Competition'])) ? $_POST['Ville_Competition'] : $objetCompetition->getVille_Competition() ?>"> 
                <br/><br/>
                
                <!-- La date de limite d'inscription -->                   
		Date limite d'inscription : 
                <input type="Date" name="Date_Limite_Inscript_CompetitionModif" value="<?= $valueDate_Limite_Inscript_Competition = (isset($_POST['Date_Limite_Inscript_Competition'])) ? $_POST['Date_Limite_Inscript_Competition'] : $objetCompetition->getDate_Limite_Inscript_Competition() ?>"> 
                <br/><br/>
                
                <!-- La catégorie d'age -->
                <!-- Les categories d'age de la compétition "sefaire" -->
                <!-- Influence la catégorie d'age des tableaux "concerne" -->
                <strong> Catégorie d'âge : </strong> <br/>
                <fieldset>
                    <?php
                    $objetCategorieAgeRep = new CategorieAgeRepository();
                    $arrayObjetCategorieAge = $objetCategorieAgeRep->selectionCategorieAge();
                    // Objet SeFaireRepository (catégorie d'age de la compétition)
                    $objetSeFaireRep = new SeFaireRepository();
                    $arrayObjetSeFaire = $objetSeFaireRep->selectionSeFaireIdCompetition($objetCompetition->getId_Competition());
                    
                    foreach ($arrayObjetCategorieAge as $value)
                    {
                        $checked = '';
                        
                        for ($x = 0; $x < count($arrayObjetSeFaire); $x++)
                        {
                            if ($arrayObjetSeFaire[$x]->getId_SeFaire_Cat_Age() == $value->getId_Cat_Age())
                            {
                                $checked = 'checked';
                            }                                
                        }
                        
                        ?>
                            <input id="<?= $value->getId_Cat_Age() ?>" type="checkbox" name="categorieAgeCompetition[]" value="<?= $value->getId_Cat_Age() ?>" <?= $checked ?>>
                            <label for="<?= $value->getId_Cat_Age() ?>"> <?= $value->getLibelle_Cat_Age() ?> </label>
                            <br/>
                        <?php
                    }
                    ?>
                </fieldset>
                <br/>                
                
                <!-- Le nombre de round -->                   
		Nombre round : 
                <input type="text" name="Nb_RoundModif" value="<?= $valueNb_Round = (isset($_POST['Nb_Round'])) ? $_POST['Nb_Round'] : $objetCompetition->getNb_Round() ?>"> 
                <br/><br/>

                
                <!-- Le temps d'un round -->
                Durée d'un round : <br/>
                <?php
                    if (isset($_POST['Duree_Round']))
                    {
                        ?>
                        <input type="time" name="Duree_RoundModif" min="00:00" max="05:00" value="<?= $_POST['Duree_Round'] ?>" required/>
                        <?php                        
                    }
                    else
                    {
                        // Conversion en "minutes : secondes"
                        $time = date('i:s', $objetCompetition->getDuree_Round());    
                        ?>
                        <input type="time" name="Duree_RoundModif" min="00:00" max="05:00" value="<?= $time ?>" required/>
                        <?php
                    }

                ?>
                <br/><br/>
                
                <!-- Le nombre de ring -->
		Nombre de ring : 
                <input type="text" name="Nb_RingModif" value="<?= $valueNb_Ring = (isset($_POST['Nb_Ring'])) ? $_POST['Nb_Ring'] : $objetCompetition->getNb_Ring() ?>"> 
                <br/><br/>
                
                <!-- Sexe -->
		Sexe : 
                <select name="Id_Competition_SexeModif">
                    <option value=""> Les 2 </option>
                    <?php
                        // Sexe
                        $objetSexeRep = new SexeRepository();
                        $arraySexe = $objetSexeRep->selectionSexe();

                        foreach ($arraySexe as $value)
                        {
                            $selected = ($value->getId_Sexe() == $objetCompetition->getId_Competition_Sexe()) ? 'selected' : '';
                            ?>
                                <option value="<?= $value->getId_Sexe() ?>" <?= $selected ?>> <?= $value->getLibelle_Sexe() ?> </option> 
                            <?php
                        }
                    ?>
                </select> 
                <br/><br/>
                
                <!-- Types d'affrontements -->
		Type d'affrontement : 
                <select name="Id_Competition_Typ_AffrontementModif">
                    <option value=""> Les 2 </option>
                    <?php
                        // Type affontement
                        $objetTypeAffrontementRep = new TypeAffrontementRepository();
                        $arrayTypeAffrontement = $objetTypeAffrontementRep->selectionTypeAffrontement();

                        foreach ($arrayTypeAffrontement as $value)
                        {
                            $selected = ($value->getId_Typ_Affrontement() == $objetCompetition->getId_Competition_Typ_Affrontement()) ? 'selected' : '';
                            ?>
                                <option value="<?= $value->getId_Typ_Affrontement() ?>" <?= $selected ?>> <?= $value->getLibelle_Typ_Affrontement() ?> </option> 
                            <?php
                        }
                    ?>
                </select> 
                <br/><br/>
                
                <!-- La structure -->
                Structure : 
                <input type="text" name="Id_Competition_StructModif" value="<?= $valueId_Competition_Struct = (isset($_POST['Id_Competition_Struct'])) ? $_POST['Id_Competition_Struct'] : $objetCompetition->getId_Competition_Struct() ?>" readonly> 
                <br/><br/>
                
                <!-- ID -->
                <input type="hidden" name="Id_CompetitionModif" value="<?= $valueId_Competition = (isset($_POST['Id_Competition'])) ? $_POST['Id_Competition'] : $objetCompetition->getId_Competition() ?>" />
                
		<input type="submit" value="Modifier" />
            </form>   
            <br/>
        </section>
    </body>
</html>