<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Créer tireur </title>
    </head>

    <body>
        <!-- Header -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>


        <section class='container-fluid about'>
            
            <!-- Information de l'utilisateur (inc) -->
            <?php
            include '../inc/information.php';
            ?>            
            
            <h1> Créer tireur </h1> 
            
            <hr class="separator">
            
            <?php
                if (isset($_POST['creation']))
                {
                    // Test si le numéro de licence existe déjà
                    $objetTireurRep = new TireurRepository();
                    $arrayTireur = $objetTireurRep->selectionTireur();

                    $confirmNumLicence = true;

                    foreach ($arrayTireur as $value)
                    {
                        if ($value->getNum_Licence() == $_POST['Num_Licence'])
                        {
                            $confirmNumLicence = false;
                            echo 'ERREUR : Ce numéro de licence existe déjà <br/><br/>';
                            $errorNumLicence = 'error';
                        }
                    }
                }
            
                if (isset($_GET['confirmationCreation']))
                {
                    ?>
                            <!-- Pop up de confirmation de suppression -->
                            <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                                <div class="modal-content" style="height: 250px;">
                                    <!-- bouton fermer -->
                                    <div id="close" class="closeTest">+</div>

                                    <!-- Confirmation de suppression -->
                                    <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                                    Le tireur a été créé !<br/><br/>

                                    <button class="boutonAnnulerTest"> OK </button>
                                </div>
                            </div>   

                             <script>
                                // La croix (ferme le pop-up)
                                document.querySelector('.closeTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });      

                                // Le bouton annuler (ferme le pop-up)
                                document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });       
                            </script>            
                    <?php
                }
            ?>

            <!-- Formulaire création tireur -->
            <form class="formCreat" method="POST" action="creatTireur.php?connect=1">

                Niveau de tireur : 
                <select name="Id_Tir_Niv_Tireur" required>
                    <option value=""> -- Choisir un niveau -- </option>
                    <?php
                        // Niveau tireur
                        $objetNiveauTireurRep = new NiveauTireurRepository();
                        $arrayNiveauTireur = $objetNiveauTireurRep->selectionNiveauTireur();
                        
                        foreach ($arrayNiveauTireur as $value)
                        {
                            $selected = ($value->getId_Niv_Tir() == $_POST['Id_Tir_Niv_Tireur']) ? 'selected' : '';
                            ?>
                                <option value="<?= $value->getId_Niv_Tir() ?>" <?= $selected ?>> <?= $value->getLibelle_Niv_Tir() ?> </option> 
                            <?php
                        }
                    ?>  
                </select> 
                <br/><br/>

                Nom : <input type="text" name="Nom_Tireur" placeholder="nom" value="<?= $valueNom = (isset($_POST['Nom_Tireur'])) ? $_POST['Nom_Tireur'] : '' ?>" required>
                <br/><br/>

                Prénom : <input type="text" name="Prenom_Tireur" placeholder="prénom" value="<?= $valuePrenom = (isset($_POST['Prenom_Tireur'])) ? $_POST['Prenom_Tireur'] : '' ?>" required> 
                <br/><br/>

                Date de naissance : <input type="date" name="Date_Naissance" max="<?= date("Y-m-d") ?>" value="<?= $valueDate = (isset($_POST['Date_Naissance'])) ? $_POST['Date_Naissance'] : '' ?>" requierd/>
                <br/><br/>

                Numéro de licence : 
                <input id="<?= $errorNumLicence = (isset($errorNumLicence)) ? $errorNumLicence : '' ?>" type="number" name="Num_Licence" value="<?= $valueNum = (isset($_POST['Num_Licence'])) ? $_POST['Num_Licence'] : '' ?>" requierd/>
                <br/><br/>

                Poids (en kg) : <input type="number" name="Poids_Tireur" value="<?= $valuePoids = (isset($_POST['Poids_Tireur'])) ? $_POST['Poids_Tireur'] : '' ?>" requierd/>
                <br/><br/>      

                <!-- Récupération des sexe -->
                <strong> Sexe : </strong> 
                <select name="Id_Tir_Sexe" required>
                    <option value=""> -- Choisir un sexe -- </option>
                    <?php
                        // Niveau tireur
                        $objetSexeRep = new SexeRepository();
                        $arraySexe = $objetSexeRep->selectionSexe();
                        
                        foreach ($arraySexe as $value)
                        {
                            $selected = ($value->getId_Sexe() == $_POST['Id_Tir_Sexe']) ? 'selected' : '';
                            ?>
                                <option value="<?= $value->getId_Sexe() ?>" <?= $selected ?>> <?= $value->getLibelle_Sexe() ?> </option> 
                            <?php
                        }
                    ?>  
                </select> 
                <br/><br/>

                <strong>Structure :</strong> 
                <?php
                $objetStructureRepUtil = new StructureRepository(); 
                $structure = $objetStructureRepUtil->selectionStructureId($_SESSION['idStruct']);
                
                ?>   
                <input type="text" value="<?= $structure->getNom_Struct() ?>" readonly> 
                <input type="hidden" name="Id_Tir_Struct" value="<?= $_SESSION['idStruct'] ?>"> 
                <br/><br/>

                <input type="hidden" name="creation"/>
                
                <input type="submit" value="Valider">
            </form>


            
            

            <?php
            // Création d'utilisateur
            if (isset($confirmNumLicence) && $confirmNumLicence) 
            {           
                // CATEGORIE DE POIDS
                $objetCategoriePoids = new CategoriePoidsRepository();
                $arrayCategoriePoids = $objetCategoriePoids->selectionCategoriePoids();
                
                // Vérification
                $idCatPoids = 0;
                
                foreach ($arrayCategoriePoids as $value)
                {
                    if ($_POST['Poids_Tireur'] > $value->getPoids_Min() && $_POST['Poids_Tireur'] < $value->getPoids_Max() || 
                        $_POST['Poids_Tireur'] == $value->getPoids_Min() || 
                        $_POST['Poids_Tireur'] == $value->getPoids_Max())
                    {
                        $idCatPoids = $value->getId_Cat_Poids();
                    }
                }
                
                // Poids Lourd
                if ($_POST['Poids_Tireur'] >= 86)
                {
                    $idCatPoids = 18;
                }
                // Poids Moustique
                if ($_POST['Poids_Tireur'] < 24)
                {
                    $idCatPoids = 1;
                }


                        
                // CATEGORIE AGE
                $dateNaissance = (int)$_POST['Date_Naissance'];
                
                $age = date('Y') - $dateNaissance;
                if (date('md') < date('md', strtotime($_POST['Date_Naissance']))) 
                { 
                    $ageReturn = $age - 1; 
                } 
                else 
                {
                    $ageReturn = $age;
                }            

                //Défini quel catégorie d'age
                $objetCategorieAgeRep = new CategorieAgeRepository();
                $arrayCategorieAge = $objetCategorieAgeRep->selectionCategorieAge();

                // Vérification
                $idCatAge = 0;
                
                foreach ($arrayCategorieAge as $value)
                {
                    if ($ageReturn > $value->getAge_Min() && $ageReturn < $value->getAge_Max() || 
                        $ageReturn == $value->getAge_Min() || 
                        $ageReturn == $value->getAge_Max())
                    {
                        $idCatAge = $value->getId_Cat_Age();
                    }
                }

                // Pré-poussin
                if ($age < 9)
                {
                    $idCatAge = 1;
                }


                // CREATION DU TIREUR
                $tireurRep = new TireurRepository();

                $valeurTireur = array(
                    "Id_Tir_Niv_Tireur" => $_POST['Id_Tir_Niv_Tireur'],
                    "Nom_Tireur" => $_POST['Nom_Tireur'],
                    "Prenom_Tireur" => $_POST['Prenom_Tireur'],
                    "Date_Naissance" => $_POST['Date_Naissance'],
                    "Num_Licence" => $_POST['Num_Licence'],
                    "Poids_Tireur" => $_POST['Poids_Tireur'],
                    "Id_Tir_Cat_Poids" => $idCatPoids,
                    "Id_Tir_Cat_Age" => $idCatAge,
                    "Id_Tir_Sexe" => $_POST['Id_Tir_Sexe'],
                    "Id_Tir_Struct" => $_SESSION['idStruct'],
                    "Inactif_Tireur" => 0
                );

                $tireur = new Tireur($valeurTireur);
                var_dump($tireurRep->sauver($tireur));

                ?>
                <script language="Javascript"> 
                    document.location.replace("creatTireur.php?confirmationCreation=1&connect=1");
                </script>
                <?php
            }
            ?>
            <br/>
        </section>
    </body>
</html>

