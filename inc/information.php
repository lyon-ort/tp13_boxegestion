            <?php
            if (isset($_GET['connect']))
            {
            ?>
                <div class="information">
                    Login : <?= $_SESSION['login'] ?> <br/>
                    Prénom : <?= $_SESSION['Prenom_Utilisateur'] ?> <br/>
                    Nom : <?= $_SESSION['Nom_Utilisateur'] ?> <br/>
                    Rôle :
                    <?php
                    $objetTypeRoleRepUtil = new TypeRoleRepository(); 
                    $typeRole = $objetTypeRoleRepUtil->selectionTypeRoleId($_SESSION['idRole']);
                    echo $typeRole->getLibelle_Typ_Role();
                    ?>                                 
                    <br/>
                    Structure :
                    <?php
                    $objetStructureRepUtil = new StructureRepository(); 
                    if ($objetStructureRepUtil->selectionStructureId($_SESSION['idStruct']))
                    {
                        $structure = $objetStructureRepUtil->selectionStructureId($_SESSION['idStruct']);
                        echo $structure->getNom_Struct();                        
                    }
                    ?> <br/><br/><br/>
                    <button class='btn btn-custom' onclick="Deconnexion()"> Déconnexion </button>
                </div>   
            <?php
            }
            ?>

