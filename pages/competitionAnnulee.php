<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Compétitions supprimés </title>
    </head>
    <body>
        
        <!-- Header (inc) -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>
        
        <!-- Section -->
        <section class='container-fluid about'>
            
            <!-- Information de l'utilisateur (inc) -->
            <?php
            include '../inc/information.php';
            ?>
            
            <h1> Compétitions supprimés </h1>
            
            <hr class="separator">            
            
            <table>
                <th> Nom </th> 
                <th> Date début </th> 
                <th> Date fin </th> 
                
                <?php
                    $objetCompetitionRep = new CompetitionRepository();
                    $arrayObjetCompetition = $objetCompetitionRep->selectionCompetitionidStructure($_SESSION['idStruct']);
                    
                    $objetTableauRep = new TableauRepository();
                    
                    foreach ($arrayObjetCompetition as $value)
                    {
                        if ($value->getInactif_Competition())
                        {
                ?>
                            <tr>
                                <td> <?= $value->getNom_Competition() ?> </td>

                                <td> <?= $value->getDate_Debut_Competition() ?> </td>

                                <td> <?= $value->getDate_Fin_Competition() ?> </td>
                            </tr>
                <?php
                        }
                    }
                ?>
            </table>
            <br/>
        </section>
    </body>
</html>
