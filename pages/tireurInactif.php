<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';

if (isset($_POST['Id_TireurRecup']))
{
    // TIREUR
    $objetTireurRep2 = new TireurRepository();
    $objetTireur2 = $objetTireurRep2->selectionTireurId($_POST['Id_TireurRecup']);
    
    // Modifie les "Flag"
    $objetTireur2->setInactif_Tireur(0);
    $objetTireurRep2->sauver($objetTireur2);
    
    // Rafraichissement de la page
    header('location: tireurInactif.php?connect=1&message=1');
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Tireur inactif </title>
    </head>
    
    <body>
        <!-- Header (inc) -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>
        
        
        <!-- Section -->
        <section class='container-fluid about'>
            
            <!-- Information de l'utilisateur (inc) -->
            <?php
            include '../inc/information.php';
            ?>
            
            <h1> Tireur Inactif </h1>
            
            <hr class="separator">
            
            <?php
            // Message de confirmation de récupération
            if (isset($_GET['message']) && $_GET['message'] == 1)
            {
                    ?>
                            <!-- Pop up de confirmation de suppression -->
                            <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                                <div class="modal-content" style="height: 250px;">
                                    <!-- bouton fermer -->
                                    <div id="close" class="closeTest">+</div>

                                    <!-- Confirmation de suppression -->
                                    <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                                    Le tireur a été récupéré !<br/><br/>

                                    <button class="boutonAnnulerTest"> OK </button>
                                </div>
                            </div>   

                             <script>
                                // La croix (ferme le pop-up)
                                document.querySelector('.closeTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });      

                                // Le bouton annuler (ferme le pop-up)
                                document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });       
                            </script>            
                    <?php                
            }
            ?>
            
            <table>
                
                <th> Prénom / Nom </th> 
                <th> Sexe </th> 
                <th> Catégorie age </th> 
                <th> Catégorie poids </th> 
                <th> Récupération </th> 
                
                <?php
                    $objetTireurRep = new TireurRepository();
                    $i = 0;
                    
                    if ($objetTireurRep->selectionArrayTireurIdStructure_INACTIF($_SESSION['idStruct']))
                    {
                        $arrayObjetTireur = $objetTireurRep->selectionArrayTireurIdStructure_INACTIF($_SESSION['idStruct']);

                        foreach ($arrayObjetTireur as $value)
                        {
                    ?>   

                    <tr>
                        <td>
                            <?= $value->getPrenom_Tireur() . ' ' . $value->getNom_Tireur() ?>
                        </td>

                        <td>
                            <?php
                            $objetSexeRep = new SexeRepository();
                            $objetSexe = $objetSexeRep->selectionSexeId($value->getId_Tir_Sexe());
                            echo $objetSexe->getLibelle_Sexe();
                            ?>
                        </td>

                        <td>
                            <?php
                            $objetCategorieAgeRep = new CategorieAgeRepository();
                            $objetCategorieAge = $objetCategorieAgeRep->selectionCategorieAgeId($value->getId_Tir_Cat_Age());
                            echo $objetCategorieAge->getLibelle_Cat_Age();
                            ?>
                        </td>

                        <td>
                            <?php
                            $objetCategoriePoidsRep = new CategoriePoidsRepository();
                            $objetCategoriePoids = $objetCategoriePoidsRep->selectionCategoriePoidsId($value->getId_Tir_Cat_Poids());
                            echo $objetCategoriePoids->getLibelle_Cat_Poids();
                            ?>
                        </td>  

                        <td>
                            <form method="POST" action="recupTireur.php">
                                <input type="hidden" name="Id_TireurRecup" value="<?= $value->getId_Tireur() ?>"/>
                            </form> 
                            
                            <h3> <button class="boutonModifierBleu" id="boutonSupprimer<?= $i ?>"> Récupérer </button> </h3>
                        </td>                 
                    </tr>
                    
                    
                    <!-- Pop-up de récupération -->
                            <div id="bg-modal" class="bg-modal<?= $i ?>">
                                <div class="modal-content">
                                    <!-- bouton fermer -->
                                    <div id="close" class="close<?= $i ?>">+</div>

                                    <!-- Confirmation de récupération -->
                                    <img width="50px" height="50px" src="../public/img/pictogramBoxe.png" alt=""/><br/>

                                    Voulez-vous vraiment récupérer ce tireur ?<br/><br/>

                                    <form name="form<?= $i ?>" action="tireurInactif.php?connect=1" method="POST"> 
                                        <input type="hidden" name="Id_TireurRecup" value="<?= $value->getId_Tireur() ?>"/>
                                        <button> Récupérer </button> <br/>
                                    </form>

                                    <button class="boutonAnnuler<?= $i ?>"> Annuler </button>
                                </div>
                            </div>
                    
                          <!-- JS -->
                            <script type="text/javascript">                 
                                // Apparition du pop-up
                                document.getElementById('boutonSupprimer<?= $i ?>').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modal<?= $i ?>').style.display = 'flex';
                                });                    

                                // La croix (ferme le pop-up)
                                document.querySelector('.close<?= $i ?>').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modal<?= $i ?>').style.display = 'none';
                                });      

                                // Le bouton annuler (ferme le pop-up)
                                document.querySelector('.boutonAnnuler<?= $i ?>').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modal<?= $i ?>').style.display = 'none';
                                }); 
                                
                                // Envoie vers la page de modification
                                document.getElementById('boutonModifier<?= $i ?>').addEventListener('click', function() 
                                {
                                    //document.querySelector('.bg-modal<?= $i ?>').style.display = 'flex';
                                    document.form<?= $i ?>.submit();
                                });  
                            </script>
                    <?php
                            $i++;
                        }
                    }
                ?>
            </table>
        </section>  
    </body>
</html>