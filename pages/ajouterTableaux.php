<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../public/css/style.css"/>
        <script type="text/javascript" src="../public/js/jquery-3.5.1.js"></script>
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Ajouter tableaux </title>
    </head>
    
    <body>
        <!-- Header (inc) -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>
        

        
        <!-- Section -->
        <section class='container-fluid about'>
            
            <!-- Information de l'utilisateur (inc) -->
            <?php
            include '../inc/information.php';
            ?>
            
            <h1> Ajout tableaux </h1>
            
            <hr class="separator">
            

            
            <!-- Création du/des tableaux + pop-up de confirmation de création
            <?php
            // Création du/des tableaux
            if (isset($_POST['creationTableau']))
            {
                $objetTableauRep = new TableauRepository();
                $objetTableauPoidsRep = new TableauPoidsRepository();
                $objetConcerneRep = new ConcerneRepository();            
                
                for ($i = 0; $i != count($_POST['Libelle_Tab']); $i++)
                {
                    // Sauver() dans la table "Tableau"
                    $valeurTableau = array (
                        "Libelle_Tab" => $_POST['Libelle_Tab'][$i],
                        "Nb_Max_Rencontre" => $_POST['Nb_Max_Rencontre'][$i],
                        "Id_Tableau_Sexe" => $_POST['Id_Tableau_Sexe'][$i],
                        "Id_Tableau_Typ_Affrontement" => $_POST['Id_Tableau_Typ_Affrontement'][$i],
                        "Id_Tableau_Typ_Tableau" => $_POST['Id_Tableau_Typ_Tableau'][$i],
                        "Id_Tableau_Competition" => $_POST['Id_Competition']);

                    $objetTableau = new Tableau($valeurTableau);
                    $tableauCreate = $objetTableauRep->sauver($objetTableau);
                    
                    // Sauver() dans la table "TableauPoids"
                    $valeurTableauPoids = array(
                        "Id_Poids_Tab" => $tableauCreate->getId_Tab(),
                        "Id_Poids_Cat" => $_POST['categoriePoidsCompetition'][$i]);
                    
                    $objetTableauPoids = new TableauPoids($valeurTableauPoids);
                    $objetTableauPoidsRep->sauver($objetTableauPoids);
                    
                    // Sauver() dans la table "Concerne"
                    $valeurConcerne = array(
                        "Id_Concerne_Tab" => $tableauCreate->getId_Tab(),
                        "Id_Concerne_Cat_Age" => $_POST['categorieAgeCompetition'][$i]);

                    $objetConcerne = new Concerne($valeurConcerne);
                    $objetConcerneRep->sauver($objetConcerne);
                }    
            ?>
                <!-- Rafraichissement de la page -->
                <form name="form" action="ajouterTableaux.php?connect=1" method="POST"> 
                    <input type="hidden" name="confirmationCreation"/>
                    <input type="hidden" name="Id_Competition" value="<?= $_POST['Id_Competition'] ?>" />
                </form>
            
                <script type="text/javascript"> 
                    document.form.submit(); 
                </script> 
                <?php
            }
            
            if (isset($_POST['confirmationCreation']))
            {
                   ?>
                            <!-- Pop up de confirmation de suppression -->
                            <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                                <div class="modal-content" style="height: 250px;">
                                    <!-- bouton fermer -->
                                    <div id="close" class="closeTest">+</div>

                                    <!-- Confirmation de suppression -->
                                    <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                                    La création de tableaux a été effectué !<br/><br/>

                                    <button class="boutonAnnulerTest"> OK </button>
                                </div>
                            </div>   

                             <script>
                                // La croix (ferme le pop-up)
                                document.querySelector('.closeTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });      

                                // Le bouton annuler (ferme le pop-up)
                                document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });       
                            </script>            
                    <?php                
            }
            ?>            
                
            <!-- Suppression de tableaux -->
            <?php
            if (isset($_POST['Id_TabSupr']))
            {
                $objetTableauRep2 = new TableauRepository();   
                
                if ($objetTableauRep2->selectionTableauxId($_POST['Id_TabSupr']))
                {
                    // Supprime le TableauPoids
                    $objetTableauPoidsRep2 = new TableauPoidsRepository();
                    $objetTableauPoidsRep2->supprimerTableauPoidsIdTab($_POST['Id_TabSupr']);
                    // Supprime le Concerne
                    $objetConcerneRep2 = new ConcerneRepository();
                    $objetConcerneRep2->supprimerConcerneIdTab($_POST['Id_TabSupr']);
                    // Supprimer le tableau
                    $objetTableauRep2->supprimerTableauId($_POST['Id_TabSupr']);
                   ?>
                            <!-- Pop up de confirmation de suppression -->
                            <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                                <div class="modal-content" style="height: 250px;">
                                    <!-- bouton fermer -->
                                    <div id="close" class="closeTest">+</div>

                                    <!-- Confirmation de suppression -->
                                    <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                                    Le tableau a été supprimé !<br/><br/>

                                    <button class="boutonAnnulerTest"> OK </button>
                                </div>
                            </div>   

                             <script>
                                // La croix (ferme le pop-up)
                                document.querySelector('.closeTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });      

                                // Le bouton annuler (ferme le pop-up)
                                document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });       
                            </script>            
                    <?php  
                }
            }
            ?>
                    
            <!-- Modification de tableaux -->
            <?php
            if (isset($_POST['Id_TabModif']))
            {
                // La table "Tableau"
                $objetTableauRep2 = new TableauRepository();             
                
                $valeurTableau = array(
                    "Id_Tab" => $_POST['Id_TabModif'],
                    "Libelle_Tab" => $_POST['Libelle_TabModif'],
                    "Nb_Max_Rencontre" => $_POST['Nb_Max_RencontreModif'],
                    "Id_Tableau_Sexe" => $_POST['Id_Tableau_SexeModif'],
                    "Id_Tableau_Typ_Affrontement" => $_POST['Id_Tableau_Typ_AffrontementModif'],
                    "Id_Tableau_Typ_Tableau" => $_POST['Id_Tableau_Typ_TableauModif'],
                    "Id_Tableau_Competition" => $_POST['Id_Competition']
                );
                
                $objetTableau = new Tableau($valeurTableau);
                $objetTableauRep2->sauver($objetTableau);
                
                // La table "TableauPoids"
                // le repository
                $objetTableauPoidsRep2 = new TableauPoidsRepository();  
                // La sélection du TableauPoids
                $objetTableauPoidsChoisi = $objetTableauPoidsRep2->selectionTableauPoidsIdTab($_POST['Id_TabModif']);
                // Modification du TableauPoids
                $objetTableauPoidsChoisi->setId_Poids_Cat($_POST['categoriePoidsCompetitionModif']);
                $objetTableauPoidsRep2->sauver($objetTableauPoidsChoisi);   
                
                // La table "Concerne"
                // le repository
                $objetConcerneRep2 = new ConcerneRepository();  
                // La sélection du Concerne
                $objetConcerneChoisi = $objetConcerneRep2->selectionConcerneIdTab($_POST['Id_TabModif']);
                // Modification du Concerne
                $objetConcerneChoisi->setId_Concerne_Cat_Age($_POST['categorieAgeCompetitionModif']);
                $objetConcerneRep2->sauver($objetConcerneChoisi);                   
                
                   ?>
                            <!-- Pop up de confirmation de suppression -->
                            <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                                <div class="modal-content" style="height: 250px;">
                                    <!-- bouton fermer -->
                                    <div id="close" class="closeTest">+</div>

                                    <!-- Confirmation de suppression -->
                                    <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                                    Le tableau a été modifié !<br/><br/>

                                    <button class="boutonAnnulerTest"> OK </button>
                                </div>
                            </div>   

                             <script>
                                // La croix (ferme le pop-up)
                                document.querySelector('.closeTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });      

                                // Le bouton annuler (ferme le pop-up)
                                document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });       
                            </script>            
                    <?php
            }
            ?>
            
            <!-- Liste des compétitions (peut modifier ou supprimer) -->
            
            <?php
            // Défini sexe
            $objetCompetitionRep = new CompetitionRepository();
            $objetCompetition = $objetCompetitionRep->selectionCompetitionId($_POST['Id_Competition']);
                
            $objetSexeRep = new SexeRepository();
            $objetSexe = $objetSexeRep->selectionSexeId($objetCompetition->getId_Competition_Sexe());
            
            // Défini type d'affrontement
            $objetTypeAffrontementRep = new TypeAffrontementRepository();
            $objetTypeAffrontement = $objetTypeAffrontementRep->selectionTypeAffrontementId($objetCompetition->getId_Competition_Typ_Affrontement());
                
            // Défini catégorie age
            $objetSeFaireRep = new SeFaireRepository();
            $arrayObjetSeFaire = $objetSeFaireRep->selectionSeFaireIdCompetition($objetCompetition->getId_Competition());
                
            $objetCategorieAgeRep = new CategorieAgeRepository();
            $arrayObjetCategorieAge = array();
                
            foreach ($arrayObjetSeFaire as $value)
            {
                $arrayObjetCategorieAge[] = $objetCategorieAgeRep->selectionCategorieAgeId($value->getId_SeFaire_Cat_Age());
            }
            
            // Sélectionne les tableaux de la compétition 
            $objetTableauRep2 = new TableauRepository();
            $arrayObjetTableau2 = $objetTableauRep2->selectionTableauxIdCompet($_POST['Id_Competition']);      
            
            // Objet ParticiperRepositoty
            $objetParticiperRep = new ParticiperRepository();
            
            // Objet TableauRepository
            $objetTableauRep3 = new TableauRepository();
            
            
            if ($arrayObjetTableau2)
            {
            ?>
                <!-- La liste des tableaux de la compétition -->
                <table>
                    <th> Nom </th>
                    <th> Modifier </th>
                    <th> Supprimer </th>
                    <?php
                    $i = 0;

                    foreach ($arrayObjetTableau2 as $value)
                    {
                        $arrayObjetParticiper = $objetParticiperRep->selectionParticipationIdTableau($value->getId_Tab());
                    ?>
                        <tr>
                            <!-- Le nom -->
                            <td>
                                <?= $value->getLibelle_Tab() ?>
                            </td>

                            <!-- Le bouton modifier -->
                            <td>
                                <?php
                                if ($arrayObjetParticiper)
                                {
                                    ?>
                                    <h3> <button class="boutonModifierGris" onMouseOver="displayDivInfo('Des tireurs participent à ce tableau');" onMouseOut="displayDivInfo()"> Modifier </button> </h3>
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <h3> <button class="boutonModifierBleu" id="boutonModifier<?= $i ?>"> Modifier </button> </h3>
                                    <?php
                                }                            
                                ?>   
                            </td>

                            <!-- Le bouton supprimer -->
                            <td>
                                <?php
                                if ($arrayObjetParticiper)
                                {
                                    ?>
                                    <h3> <button class="classBoutonSupprimerGris" onMouseOver="displayDivInfo('Des tireurs participent à ce tableau');" onMouseOut="displayDivInfo()"> X </button> </h3>
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <h3> <button class="classBoutonSupprimerRouge" id="boutonSupprimer<?= $i ?>"> X </button> </h3>
                                    <?php
                                }                            
                                ?>                   
                            </td>
                        </tr>

                        <!-- Pop up de suppression -->
                        <div id="bg-modal" class="bg-modal<?= $i ?>">
                            <div class="modal-content">
                                <!-- bouton fermer -->
                                <div id="close" class="close<?= $i ?>">+</div>

                                <!-- Confirmation de suppression -->
                                <img width="50px" height="50px" src="../public/img/pictogramBoxe.png" alt=""/><br/>

                                Voulez-vous vraiment supprimer ce tableau ?<br/><br/>

                                <form name="form" action="ajouterTableaux.php?connect=1" method="POST"> 
                                    <input type="hidden" name="Id_TabSupr" value="<?= $value->getId_Tab() ?>"/>
                                    <input type="hidden" name="Id_Competition" value="<?= $_POST['Id_Competition'] ?>" />

                                    <button> Supprimer </button> <br/>
                                </form>

                                <button class="boutonAnnuler<?= $i ?>"> Annuler </button>
                            </div>
                        </div> 
                        
                        <!-- Pop up de modification -->
                        <div id="bg-modal" class="bg-modalModif<?= $i ?>">
                            <div class="modal-contentModifTableaux">
                                <!-- bouton fermer -->
                                <div id="close" class="closeModif<?= $i ?>">+</div>
                                <br/>
                                <?php
                                $objetTableau3 = $objetTableauRep3->selectionTableauxId($value->getId_Tab());
                                ?>

                                <form method="POST" action="ajouterTableaux.php?connect=1">
                                    Nom : 
                                    <input type="text" name="Libelle_TabModif" value="<?= $value->getLibelle_Tab() ?>" required />
                                    Nombre de rencontre MAX : 
                                    <input type="number" name="Nb_Max_RencontreModif" value="<?= $value->getNb_Max_Rencontre() ?>" required />
                                    <!-- Le sexe -->
                                    Sexe :
                                    <?php
                                    if (!$objetSexe)
                                    {
                                    ?>
                                        <select name="Id_Tableau_SexeModif" required>
                                            <option value=""> -- Choisir un sexe -- </option>
                                            <?php
                                                // Niveau tireur
                                                $arraySexe = $objetSexeRep->selectionSexe();
                                                
                                                foreach ($arraySexe as $valueSexe)
                                                {
                                                    $selected = ($valueSexe->getId_Sexe() == $objetTableau3->getId_Tableau_Sexe()) ? 'selected' : '';
                                                    ?>
                                                        <option value="<?= $valueSexe->getId_Sexe() ?>" <?= $selected ?>> <?= $valueSexe->getLibelle_Sexe() ?> </option> 
                                                    <?php
                                                }
                                    ?>
                                        </select>
                                    <?php    
                                    }
                                    else
                                    {
                                        ?>
                                            <input type="text" value="<?= $objetSexe->getLibelle_Sexe() ?>" disabled>
                                            <input type="hidden" name="Id_Tableau_SexeModif" value="<?= $objetSexe->getId_Sexe() ?>">
                                        <?php
                                    }
                                    ?>

                                    <!-- La catégorie d'age -->
                                    Catégorie d'âge :
                                    <select name="categorieAgeCompetitionModif" required>
                                        <option value=""> -- Choisir une catégorie -- </option>
                                        <?php
                                            $objetConcerneRep = new ConcerneRepository();
                                            $objetConcerne = $objetConcerneRep->selectionConcerneIdTab($value->getId_Tab());
                                            
                                            foreach ($arrayObjetCategorieAge as $value5)
                                            {
                                                $selected = ($value5->getId_Cat_Age() == $objetConcerne->getId_Concerne_Cat_Age()) ? 'selected' : '';
                                                ?>
                                                    <option value="<?= $value5->getId_Cat_Age() ?>" <?= $selected ?>> <?= $value5->getLibelle_Cat_Age() ?> </option>
                                                <?php
                                            }
                                        ?>
                                    </select>

                                    <!-- La catégorie de poids -->
                                    Catégorie de poids : 
                                    <select name="categoriePoidsCompetitionModif" required>
                                        <option value=""> -- Choisir une catégorie -- </option>
                                        <?php
                                            $objetCategoriePoidsRep = new CategoriePoidsRepository();
                                            $arrayObjetCategoriePoids = $objetCategoriePoidsRep->selectionCategoriePoids();
                                            
                                            $objetTableauPoidsRep = new TableauPoidsRepository();
                                            $objetTableauPoids = $objetTableauPoidsRep->selectionTableauPoidsIdTab($value->getId_Tab());

                                            foreach ($arrayObjetCategoriePoids as $value4)
                                            {
                                                $selected = ($value4->getId_Cat_Poids() == $objetTableauPoids->getId_Poids_Cat()) ? 'selected' : '';
                                                ?>
                                                    <option value="<?= $value4->getId_Cat_Poids() ?>" <?= $selected ?>> <?= $value4->getLibelle_Cat_Poids() ?> </option>
                                                <?php
                                            }
                                        ?>
                                    </select>

                                    <!-- Le type d'affrontement -->
                                    Type d'affrontement :
                                    <?php
                                    if (!$objetTypeAffrontement)
                                    {
                                    ?>
                                        <select name="Id_Tableau_Typ_AffrontementModif" required>
                                            <option value=""> -- Choisir un type -- </option>
                                            <?php
                                                // Type affontement
                                                $arrayTypeAffrontement = $objetTypeAffrontementRep->selectionTypeAffrontement();
                                                

                                                foreach ($arrayTypeAffrontement as $value3)
                                                {
                                                    $selected = ($value3->getId_Typ_Affrontement() == $objetTableau3->getId_Tableau_Typ_Affrontement()) ? 'selected' : '';
                                                    ?>
                                                        <option value="<?= $value3->getId_Typ_Affrontement() ?>" <?= $selected ?>> <?= $value3->getLibelle_Typ_Affrontement() ?> </option> 
                                                    <?php
                                                }
                                            ?>  
                                        </select> 
                                    <?php
                                    }
                                    else
                                    {
                                        ?>
                                            <input type="text" value="<?= $objetTypeAffrontement->getLibelle_Typ_Affrontement() ?>" disabled>
                                            <input type="hidden" name="Id_Tableau_Typ_AffrontementModif" value="<?= $objetTypeAffrontement->getId_Typ_Affrontement() ?>">
                                        <?php                            
                                    }
                                    ?>

                                    <!-- Le type de tableau -->
                                    Type de tableau :
                                    <select name="Id_Tableau_Typ_TableauModif" required>
                                        <option value=""> -- Choisir un type -- </option>
                                            <?php
                                                // Type affontement
                                                $objetTypeTableauRep = new TypeTableauRepository();
                                                $arrayTypeTableau = $objetTypeTableauRep->selectionTypeTableau();

                                                foreach ($arrayTypeTableau as $value2)
                                                {
                                                    $selected = ($value2->getId_Typ_Tab() == $objetTableau3->getId_Tableau_Typ_Tableau()) ? 'selected' : '';
                                                    ?>
                                                        <option value="<?= $value2->getId_Typ_Tab() ?>" <?= $selected ?>> <?= $value2->getLibelle_Typ_Tab() ?> </option> 
                                                    <?php
                                                }
                                            ?>  
                                    </select>
                                    <br/><br/>
                                    
                                    <input type="hidden" name="Id_TabModif" value="<?= $value->getId_Tab() ?>"/>                                    
                                    <input type="hidden" name="Id_Competition" value="<?= $_POST['Id_Competition'] ?>" />
                                    <input type="submit" value="Modifier" />
                                </form>

                                <br/><br/>
                                <button class="boutonAnnulerModif<?= $i ?>"> Annuler </button>
                            </div>
                        </div> 

                        <!-- Script -->
                        <script>
                            // Apparition du pop-up "supprimer"
                            document.getElementById('boutonSupprimer<?= $i ?>').addEventListener('click', function() 
                            {
                                document.querySelector('.bg-modal<?= $i ?>').style.display = 'flex';
                            });                    
                            // La croix (ferme le pop-up)
                            document.querySelector('.close<?= $i ?>').addEventListener('click', function() 
                            {
                                document.querySelector('.bg-modal<?= $i ?>').style.display = 'none';
                            });      
                            // Le bouton annuler (ferme le pop-up)
                            document.querySelector('.boutonAnnuler<?= $i ?>').addEventListener('click', function() 
                            {
                                document.querySelector('.bg-modal<?= $i ?>').style.display = 'none';
                            });      
                            
                            
                            // Apparition du pop-up "modifier"
                            document.getElementById('boutonModifier<?= $i ?>').addEventListener('click', function() 
                            {
                                document.querySelector('.bg-modalModif<?= $i ?>').style.display = 'flex';
                            });     
                            // La croix (ferme le pop-up)
                            document.querySelector('.closeModif<?= $i ?>').addEventListener('click', function() 
                            {
                                document.querySelector('.bg-modalModif<?= $i ?>').style.display = 'none';
                            });      
                            // Le bouton annuler (ferme le pop-up)
                            document.querySelector('.boutonAnnulerModif<?= $i ?>').addEventListener('click', function() 
                            {
                                document.querySelector('.bg-modalModif<?= $i ?>').style.display = 'none';
                            });                                 
                        </script>
                    <?php
                        $i++;
                    } 
                    ?>
                </table> 
            <?php
            }
            ?>
   
            
            <!-- Le bouton "+" qui créé un tableau -->
            
            <br/>
            <p> Appuyez sur le bouton "+" pour ajouter un tableau </p>    
            
            
                    <!-- La <div> qui va s'ajouter dans le <form> à chaque clique sur le bouton "+" -->
                    <div class="row formCreat " id='formAjoutTableau' style="display: none; animation-duration: 1s;">
                         <br/>
                        
                        
                        <!-- Le nom -->
                        Nom :
                        <input type="text" name="Libelle_Tab[]" placeholder="nom" required/>

                        <!-- Le nombre MAX de rencontres -->
                        Nombre MAX de rencontres : <input type="number" name="Nb_Max_Rencontre[]" placeholder="123" required/>

                        <!-- Le sexe -->
                        Sexe :
                        <?php
                        if (!$objetSexe)
                        {
                        ?>
                            <select name="Id_Tableau_Sexe[]" required>
                                <option value=""> -- Choisir un sexe -- </option>
                                <?php
                                    // Niveau tireur
                                    $arraySexe = $objetSexeRep->selectionSexe();


                                        foreach ($arraySexe as $value)
                                        {
                                            ?>
                                                <option value="<?= $value->getId_Sexe() ?>"> <?= $value->getLibelle_Sexe() ?> </option> 
                                            <?php
                                        }
                        ?>
                            </select>
                        <?php    
                        }
                        else
                        {
                            ?>
                                <input type="text" value="<?= $objetSexe->getLibelle_Sexe() ?>" disabled>
                                <input type="hidden" name="Id_Tableau_Sexe[]" value="<?= $objetSexe->getId_Sexe() ?>">
                            <?php
                        }
                        ?>
                                
                        <!-- La catégorie d'age -->
                        Catégorie d'âge :
                        <select name="categorieAgeCompetition[]" required>
                            <option value=""> -- Choisir une catégorie -- </option>
                            <?php
                                foreach ($arrayObjetCategorieAge as $value)
                                {
                                    ?>
                                        <option value="<?= $value->getId_Cat_Age() ?>"> <?= $value->getLibelle_Cat_Age() ?> </option>
                                    <?php
                                }
                            ?>
                        </select>
                        
                        <!-- La catégorie de poids -->
                        Catégorie de poids : 
                        <select name="categoriePoidsCompetition[]" required>
                            <option value=""> -- Choisir une catégorie -- </option>
                            <?php
                                $objetCategoriePoidsRep = new CategoriePoidsRepository();
                                $arrayObjetCategoriePoids = $objetCategoriePoidsRep->selectionCategoriePoids();
                            
                                foreach ($arrayObjetCategoriePoids as $value)
                                {
                                    ?>
                                        <option value="<?= $value->getId_Cat_Poids() ?>"> <?= $value->getLibelle_Cat_Poids() ?> </option>
                                    <?php
                                }
                            ?>
                        </select>

                        <!-- Le type d'affrontement -->
                        Type d'affrontement :
                        <?php
                        if (!$objetTypeAffrontement)
                        {
                        ?>
                            <select name="Id_Tableau_Typ_Affrontement[]" required>
                                <option value=""> -- Choisir un type -- </option>
                                <?php
                                    // Type affontement
                                    $arrayTypeAffrontement = $objetTypeAffrontementRep->selectionTypeAffrontement();

                                    foreach ($arrayTypeAffrontement as $value)
                                    {
                                        ?>
                                            <option value="<?= $value->getId_Typ_Affrontement() ?>"> <?= $value->getLibelle_Typ_Affrontement() ?> </option> 
                                        <?php
                                    }
                                ?>  
                            </select> 
                        <?php
                        }
                        else
                        {
                            ?>
                                <input type="text" value="<?= $objetTypeAffrontement->getLibelle_Typ_Affrontement() ?>" disabled>
                                <input type="hidden" name="Id_Tableau_Typ_Affrontement[]" value="<?= $objetTypeAffrontement->getId_Typ_Affrontement() ?>">
                            <?php                            
                        }
                        ?>

                        <!-- Le type de tableau -->
                        Type de tableau :
                        <select name="Id_Tableau_Typ_Tableau[]" required>
                            <option value=""> -- Choisir un type -- </option>
                                <?php
                                    // Type affontement
                                    $objetTypeTableauRep = new TypeTableauRepository();
                                    $arrayTypeTableau = $objetTypeTableauRep->selectionTypeTableau();

                                    foreach ($arrayTypeTableau as $value)
                                    {
                                        ?>
                                            <option value="<?= $value->getId_Typ_Tab() ?>"> <?= $value->getLibelle_Typ_Tab() ?> </option> 
                                        <?php
                                    }
                                ?>  
                        </select>  
                        
                        <!-- Bouton suprimmer -->
                         <button class="btn-remove-row"> X </button> 
                    </div>                

                    <form id="form" name="form" method="POST" action="ajouterTableaux.php<?= $leGet ?>" onsubmit='return verif_champ(document.form.Libelle_Tab[].value);'>
                        <input type="hidden" name="Id_Competition" value="<?= $_POST['Id_Competition'] ?>" />
                        <input type="hidden" name="creationTableau" />
                        
                        <p class="element-wrapper">
                            
                        </p>    
                        
                        
                        
                        <input id="boutonValider" type="submit" value="Valider" />
                        
                    </form> 
                    
                    <!-- Bouton ajout -->
                    <h3> <button class="btn-add-row"> + </button> </h3>
            
                    <br/><br/>

            
            
            
            
            <!-- JavaScript -->
            <script type="text/javascript">
                var MarqueurDeVisibilite = 0;
                
                // Apparition du pop-up d'ajout de tableau
                $(document).on("click", ".btn-add-row", function()
                {
                    var row = $(".row").eq(0).clone().show();
                    $(".element-wrapper").append(row);
                    
                    var bouton = $("#bouton");
                    bouton.css("visibility", "visible");
                    
                    MarqueurDeVisibilite++;
                   
                    // Visibilité du bouton valider
                    if (MarqueurDeVisibilite > 0)
                    {
                        var boutonValider = $("#boutonValider");
                        boutonValider.css("display", "block");                    
                    }
                });
                
                // Suppression du pop-up d'ajout de tableau
                $(document).on("click", ".btn-remove-row", function()
                {
                    var index = $(".btn-remove-row").index(this);
                    console.log(index);
                    $(".row") .eq(index).remove();
                    
                    MarqueurDeVisibilite--;
                    
                    // Visibilité du bouton valider
                    if (!MarqueurDeVisibilite)
                    {
                        var boutonValider = $("#boutonValider");
                        boutonValider.css("display", "none");                    
                    }
                });
                
                // Fonction qui affiche du texte au survol du bouton désactiver
                function displayDivInfo(text)
                {
                    if(text)
                    {
                        //Détection du navigateur
                        is_ie = (navigator.userAgent.toLowerCase().indexOf("msie") != -1) && (navigator.userAgent.toLowerCase().indexOf("opera") == -1);

                        //Création d'une div provisoire
                        var divInfo = document.createElement('div');
                        divInfo.style.position = 'fixed';
                        document.onmousemove = function(e)
                        {
                            x = (!is_ie ? e.pageX-window.pageXOffset : event.x+document.body.scrollLeft);
                            y = (!is_ie ? e.pageY-window.pageYOffset : event.y+document.body.scrollTop);
                            divInfo.style.left = x+15+'px';
                            divInfo.style.top = y+15+'px';
                        }
                        divInfo.id = 'divInfo';
                        divInfo.innerHTML = text;
                        document.body.appendChild(divInfo);
                    }
                    else
                    {
                        document.onmousemove = '';
                        document.body.removeChild(document.getElementById('divInfo'));
                    }
                } 
            </script>
            
        </section>
        
    </body>
</html>
