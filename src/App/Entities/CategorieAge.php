<?php

class CategorieAge {

    // Attributs
    protected $Id_Cat_Age;
    protected $Libelle_Cat_Age;
    protected $Age_Max;
    protected $Age_Min;

    // Constructeur
    public function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas = NULL) {
        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Appelle le mutateur des attributs existant dans le tableau $datas
        foreach ($attrib as $key => $val) {
            // Si, dans vos habitudes, les champs de la bdd sont en minuscules, décommentez la ligne suivante.
            //$key = strtolower($key);
            // Si, dans vos habitudes, les champs de la bdd sont en majuscules, décommentez la ligne suivante.
            //$key = strtoupper($key);
            //  Perso, je met tout en notation camelCase... même dans la BDD.
            if (isset($datas[$key])) {
                $mutateur = 'set' . $key;
                $this->$mutateur($datas[$key]);
            }
        }

        return $this;
    }

    // GET et SET
    function getId_Cat_Age() {
        return $this->Id_Cat_Age;
    }

    function getLibelle_Cat_Age() {
        return $this->Libelle_Cat_Age;
    }

    function getAge_Max() {
        return $this->Age_Max;
    }

    function getAge_Min() {
        return $this->Age_Min;
    }

    function setId_Cat_Age($Id_Cat_Age): void {
        $this->Id_Cat_Age = $Id_Cat_Age;
    }

    function setLibelle_Cat_Age($Libelle_Cat_Age): void {
        $this->Libelle_Cat_Age = $Libelle_Cat_Age;
    }

    function setAge_Max($Age_Max): void {
        $this->Age_Max = $Age_Max;
    }

    function setAge_Min($Age_Min): void {
        $this->Age_Min = $Age_Min;
    }

}
