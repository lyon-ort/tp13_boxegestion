<?php

class Utilisateur {

    protected $Id_Utilisateur;
    protected $Id_Typ_Role_Utilisateur;
    protected $Id_Struct_Utilisateur;
    protected $Nom_Utilisateur;
    protected $Prenom_Utilisateur;
    protected $Login_Utilisateur;
    protected $Email;
    protected $Mdp;
    protected $Tel_Port;
    protected $Date_Creation_Compte;
    protected $Dernier_Login_Utilisateur;
    protected $Inactif_Utilisateur;

    public function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas = NULL) {
        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Appelle le mutateur des attributs existant dans le tableau $datas
        foreach ($attrib as $key => $val) {
            // Si, dans vos habitudes, les champs de la bdd sont en minuscules, décommentez la ligne suivante.
            //$key = strtolower($key);
            // Si, dans vos habitudes, les champs de la bdd sont en majuscules, décommentez la ligne suivante.
            //$key = strtoupper($key);
            //  Perso, je met tout en notation camelCase... même dans la BDD.
            if (isset($datas[$key])) {
                $mutateur = 'set' . $key;
                $this->$mutateur($datas[$key]);
            }
        }

        return $this;
    }
    
        /**
     * Transforme un objet en un tableau associatif.
     * @return array tableau associatif des attributs => valeurs
     */
    public function __toArray() {
        return $this->jsonSerialize();
    }

    /**
     * La classe implémente l'interface JsonSerializable ce qui permet 
     * aux classes concrête de pouvoir être sérialisée en JSON avec json_encode()
     * @return Array le tableau pour conversion en json
     */
    public function jsonSerialize() {
        $array = array();

        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Associe la clé du nom de l'attribut à la valeur de cet attribut
        foreach ($attrib as $key => $val) {
            $array[$key] = $this->get($key);
        }
        return $array;
    }

    /**
     * Lecteur simple d'accés en lecture à un attribut
     * Peut être surchargé.
     * @param type $attribut le nom de l'attribut
     * @return type la valeur de l'attribut
     */
    protected function get($attribut) {
        return $this->$attribut;
    }
    
        /**
     * Surcharge du mutateur pour l'attribut $id qui ne peut pas être modifié.
     * @param int $val ne sert que si id_pers est null...
     */
    public function setid_pers($val=null)
    {
	if($this->Id_Utilisateur === null)
	    $this->Id_Utilisateur = (int)$val;
    }
    
    // Ces deux méthodes ne sont nécessaire QUE si la clé primaire dans la table
    // porte un nom différent de id...
    /**
     * Permet de s'affranchir du fait que le champ id dans la table est différent...
     * @param type $val
     */
    public function setid($val=null)
    {
	$this->setid_pers($val);
    }
    
    function getId_Utilisateur() {
        return $this->Id_Utilisateur;
    }

    function getId_Typ_Role_Utilisateur() {
        return $this->Id_Typ_Role_Utilisateur;
    }

    function getId_Struct_Utilisateur() {
        return $this->Id_Struct_Utilisateur;
    }

    function getNom_Utilisateur() {
        return $this->Nom_Utilisateur;
    }

    function getPrenom_Utilisateur() {
        return $this->Prenom_Utilisateur;
    }

    function getLogin_Utilisateur() {
        return $this->Login_Utilisateur;
    }

    function getEmail() {
        return $this->Email;
    }

    function getMdp() {
        return $this->Mdp;
    }

    function getTel_Port() {
        return $this->Tel_Port;
    }

    function getDate_Creation_Compte() {
        return $this->Date_Creation_Compte;
    }

    function getDernier_Login_Utilisateur() {
        return $this->Dernier_Login_Utilisateur;
    }

    function setId_Utilisateur($Id_Utilisateur): void {
        $this->Id_Utilisateur = $Id_Utilisateur;
    }

    function setId_Typ_Role_Utilisateur($Id_Typ_Role_Utilisateur): void {
        $this->Id_Typ_Role_Utilisateur = $Id_Typ_Role_Utilisateur;
    }

    function setId_Struct_Utilisateur($Id_Struct_Utilisateur): void {
        $this->Id_Struct_Utilisateur = $Id_Struct_Utilisateur;
    }

    function setNom_Utilisateur($Nom_Utilisateur): void {
        $this->Nom_Utilisateur = $Nom_Utilisateur;
    }

    function setPrenom_Utilisateur($Prenom_Utilisateur): void {
        $this->Prenom_Utilisateur = $Prenom_Utilisateur;
    }

    function setLogin_Utilisateur($Login_Utilisateur): void {
        $this->Login_Utilisateur = $Login_Utilisateur;
    }

    function setEmail($Email): void {
        $this->Email = $Email;
    }

    function setMdp($Mdp): void {
        $this->Mdp = $Mdp;
    }

    function setTel_Port($Tel_Port): void {
        $this->Tel_Port = $Tel_Port;
    }

    function setDate_Creation_Compte($Date_Creation_Compte): void {
        $this->Date_Creation_Compte = $Date_Creation_Compte;
    }

    function setDernier_Login_Utilisateur($Dernier_Login_Utilisateur): void {
        $this->Dernier_Login_Utilisateur = $Dernier_Login_Utilisateur;
    }

    function getInactif_Utilisateur() {
        return $this->Inactif_Utilisateur;
    }

    function setInactif_Utilisateur($Inactif_Utilisateur): void {
        $this->Inactif_Utilisateur = $Inactif_Utilisateur;
    }

}
