<?php

require '../src/App/Entities/Rencontre.php';

class RencontreRepository {

    protected $table;     
    protected $classMapped; 
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire)
    public function __construct() {
        $this->table = 'Rencontre';
        $this->classMapped = 'App\Entities\Rencontre';
        $this->idFieldName = 'Id_Rencontre';
    }
    
    // Ajouter / Modifier un élément
    public function sauver(Rencontre $entity) {
        $db = dbConnect();
        $resultSet = NULL;
        if ($entity != NULL) {
            $bindParam = $entity->__toArray();
            if ($entity->getId_Rencontre() == NULL) {
                // Nouvelle entité
                $query = "INSERT INTO $this->table" .
                        " (Date_Rencontre, "
                        . "Heure_Debut_Rencontre, "
                        . "Heure_Fin_Rencontre, "
                        . "Num_Ring_Rencontre, "
                        . "Id_Tireur_Rouge, "
                        . "Id_Tireur_Bleu, "
                        . "Id_Gagnant, "
                        . "Id_Rencontre_Precedent_1, "
                        . "Id_Rencontre_Precedent_2,"
                        . "Id_Rencontre_Suivante,"
                        . "Id_Cat_Rencontre,"
                        . "Id_Tab_Rencontre,"
                        . "Id_Etat_Rencontre)"
                        . " VALUES "
                        . "(:Date_Rencontre, "
                        . ":Heure_Debut_Rencontre, "
                        . ":Heure_Fin_Rencontre, "
                        . ":Num_Ring_Rencontre, "
                        . ":Id_Tireur_Rouge, "
                        . ":Id_Tireur_Bleu, "
                        . ":Id_Gagnant, "
                        . ":Id_Rencontre_Precedent_1, "
                        . ":Id_Rencontre_Precedent_2, "
                        . ":Id_Rencontre_Suivante,"
                        . ":Id_Cat_Rencontre,"
                        . ":Id_Tab_Rencontre,"
                        . ":Id_Etat_Rencontre)";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Date_Rencontre', $bindParam['Date_Rencontre'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Heure_Debut_Rencontre', $bindParam['Heure_Debut_Rencontre'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Heure_Fin_Rencontre', $bindParam['Heure_Fin_Rencontre'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Num_Ring_Rencontre', $bindParam['Num_Ring_Rencontre'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tireur_Rouge', $bindParam['Id_Tireur_Rouge'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tireur_Bleu', $bindParam['Id_Tireur_Bleu'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Gagnant', $bindParam['Id_Gagnant'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Rencontre_Precedent_1', $bindParam['Id_Rencontre_Precedent_1'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Rencontre_Precedent_2', $bindParam['Id_Rencontre_Precedent_2'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Rencontre_Suivante', $bindParam['Id_Rencontre_Suivante'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Cat_Rencontre', $bindParam['Id_Cat_Rencontre'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tab_Rencontre', $bindParam['Id_Tab_Rencontre'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Etat_Rencontre', $bindParam['Id_Etat_Rencontre'], \PDO::PARAM_INT);
                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $entity->setid($db->lastInsertId());
                    $resultSet = $entity;
                }
            } else {
                //  Entité existante
                $query = "UPDATE $this->table"
                        . " SET Date_Rencontre = :Date_Rencontre, "
                        . " Heure_Debut_Rencontre = :Heure_Debut_Rencontre, "
                        . " Heure_Fin_Rencontre = :Heure_Fin_Rencontre, "
                        . " Num_Ring_Rencontre = :Num_Ring_Rencontre, "
                        . " Id_Tireur_Rouge = :Id_Tireur_Rouge, "
                        . " Id_Tireur_Bleu = :Id_Tireur_Bleu, "
                        . " Id_Gagnant = :Id_Gagnant, "
                        . " Id_Rencontre_Precedent_1 = :Id_Rencontre_Precedent_1, "
                        . " Id_Rencontre_Precedent_2 = :Id_Rencontre_Precedent_2, "
                        . " Id_Rencontre_Suivante = :Id_Rencontre_Suivante, "
                        . " Id_Cat_Rencontre = :Id_Cat_Rencontre, " 
                        . " Id_Tab_Rencontre = :Id_Tab_Rencontre, "
                        . " Id_Etat_Rencontre = :Id_Etat_Rencontre "
                        . " WHERE $this->idFieldName = :Id_Rencontre";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Date_Rencontre', $bindParam['Date_Rencontre'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Heure_Debut_Rencontre', $bindParam['Heure_Debut_Rencontre'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Heure_Fin_Rencontre', $bindParam['Heure_Fin_Rencontre'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Num_Ring_Rencontre', $bindParam['Num_Ring_Rencontre'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tireur_Rouge', $bindParam['Id_Tireur_Rouge'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tireur_Bleu', $bindParam['Id_Tireur_Bleu'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Gagnant', $bindParam['Id_Gagnant'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Rencontre_Precedent_1', $bindParam['Id_Rencontre_Precedent_1'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Rencontre_Precedent_2', $bindParam['Id_Rencontre_Precedent_2'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Rencontre_Suivante', $bindParam['Id_Rencontre_Suivante'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Cat_Rencontre', $bindParam['Id_Cat_Rencontre'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tab_Rencontre', $bindParam['Id_Tab_Rencontre'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Etat_Rencontre', $bindParam['Id_Etat_Rencontre'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Rencontre', $bindParam[$this->idFieldName], \PDO::PARAM_INT);
                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $resultSet = $entity;
                }
            }
        }
        return $resultSet;
    }
    
    
    
    // Sélectionner des rencontres selon un id de tableau
    public function selectionRencontreIdTableau($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objet à NULL si jamais la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Tab_Rencontre = :idMark");
        $req->execute(array('idMark' => $id));

        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;        
    }    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // Séléction des rencontres selon un tableau
    public function selectionRencontre($idTab)
    {
        $db = dbConnect();       
        
        // Récupération des idRencontre du tableau
        for ($i = 0; $i != count($idTab); $i++)
        {
            $y = 0; 
            $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Tab_Rencontre = :idTabMark");
            $req->execute(array('idTabMark' => $idTab[$i]));
            $test = array();
            
            while ($data = $req->fetch())
            {
                $test[$y] = array(
                    'Id_Tireur_Rouge' => $data['Id_Tireur_Rouge'], 
                    'Id_Tireur_Bleu' => $data['Id_Tireur_Bleu'],
                    'Id_Gagnant' => $data['Id_Gagnant']
                        );
                $y++;
            }
        }
        
        return $test;
    }
}
