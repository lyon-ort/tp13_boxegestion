            <div class='container'>
                
                <?php
                if (isset($_GET['connect']))
                {
                    $leGet = '?connect=1';
                }
                else
                {
                    $leGet = '';
                }
                ?>
                
                <a href='accueilBis.php<?= $leGet ?>'> 
                    <img class="imageLogo" src="../public/img/ff_savate.jpg" width="60px" height="60px" > 
                </a>
                <a href='accueilBis.php<?= $leGet ?>' class='logo'> Savate Boxe </a>
               
                
                <nav class='menu'>
                    <?php
                    // Non-connecté
                    if (!isset($_GET['connect']))
                    {
                        ?>
                        <a class="titre" href='accueilBis.php'> Accueil </a>
                        <a class="titre" href='accueil.php'> Compétitions </a>     

                        <button class='btn btn-custom' onclick="Connexion()"> Connexion </button>
                        <?php
                    }
                    // Connecté
                    else
                    {
                        ?>
 
                        </span>
                        <?php    
                        // Administrateur
                        if ($_SESSION['idRole'] == 2)
                        { 
                        ?>    
                            <a class="titre" href='accueilBis.php<?= $leGet ?>'> Accueil </a>
                            <a class="titre" href='accueilConnect.php<?= $leGet ?>'> Compétitions </a>                             
                        
                            <span class="dropdown dropdownmenu">            
                                <span>
                                    <li>
                                        <button class='btn btn-custom' id="informationMembre"> <?= $_SESSION['login'] ?> </button>
                                    </li>
                                </span>
                            </span>                        

                            <span class="dropdown dropdownmenu">            
                                <span>
                                    <li>
                                        <a href="#">Gestion d'utilisateurs</a>
                                        <ul id="submenu">
                                            <li><a href="creatUtil.php<?= $leGet ?>">Créer utilisateurs</a></li>
                                            <li><a href="modifSuprUtil.php<?= $leGet ?>">Liste utilisateurs</a></li>
                                        </ul>
                                    </li>
                                </span>
                            </span>   

                            <span class="dropdown dropdownmenu">            
                                <span>
                                    <li>
                                        <a href="#">Gestion de structure</a>
                                        <ul id="submenu">
                                            <li><a href="creatStruct.php<?= $leGet ?>">Créer structures</a></li>
                                            <li><a href="modifSuprStruct.php<?= $leGet ?>">Liste structures</a></li>
                                        </ul>
                                    </li>
                                </span>
                            </span>
                        <?php
                        }
                        // Responsable de club
                        else if ($_SESSION['idRole'] == 1)
                        {
                        ?>       
                            <a class="titre" href='accueilBis.php<?= $leGet ?>'> Accueil </a>
                            <a class="titre" href='accueilConnect.php<?= $leGet ?>'> Compétitions </a>     
                            
                            <span class="dropdown dropdownmenu">            
                                <span>
                                    <li>
                                        <button class='btn btn-custom' id="informationMembre"> <?= $_SESSION['login'] ?> </button>
                                    </li>
                                </span>
                            </span>                        

                            <span class="dropdown dropdownmenu">            
                                <span>
                                    <li>
                                        <a href="#">Gestion des tireurs</a>
                                        <ul id="submenu">
                                            <li><a href="creatTireur.php<?= $leGet ?>">Créer tireurs</a></li>
                                            <li><a href="modifSuprTireur.php<?= $leGet ?>">Liste tireurs</a></li>
                                            <li><a href="tireurInactif.php<?= $leGet ?>">Tireur inactif</a></li>
                                        </ul>
                                    </li>
                                </span>
                            </span>   

                            <span class="dropdown dropdownmenu">            
                                <span>
                                    <li>
                                        <a href="#">Gestion des compétitions</a>
                                        <ul id="submenu">
                                            <li><a href="creationCompetition.php<?= $leGet ?>">Créer compétitions</a></li>
                                            <li><a href="modifSuprimCompet.php<?= $leGet ?>">Liste compétitions</a></li>
                                            <li><a href="competitionAnnulee.php<?= $leGet ?>">Compétitions supprimés</a></li>
                                        </ul>
                                    </li>
                                </span>
                            </span>
                        <?php                            
                        } 
                        // Opérateur
                        else if ($_SESSION['idRole'] == 4)
                        {
                            $display = 'none';
                        ?>      
                            <a class="titre" href='accueilBis.php<?= $leGet ?>'> Accueil </a>
                            
                            <span class="dropdown dropdownmenu">            
                                <span>
                                    <li>
                                        <button class='btn btn-custom' id="informationMembre"> <?= $_SESSION['login'] ?> </button>
                                    </li>
                                </span>
                            </span>                        

                            <a class="titre" href='accueilConnect.php<?= $leGet ?>'> Import / Export des compétitions </a>  
                        <?php                             
                        }
                    }
                    ?>      
                </nav>
               
                <script language="Javascript"> 
                    // Envoie vers la page de connexion
                    function Connexion()
                    {
                        document.location.replace("connexion.php");
                    }
                    
                    // Déconnexion
                    function Deconnexion()
                    {
                        document.location.replace("traitementDeconnexion.php");
                    }
                    
                    // Ouvre les informations de membre
                    // Apparition du pop-up
                    i = 0;

                    document.getElementById('informationMembre').addEventListener('click', function() 
                    {
                    if (i == 0)
                    {
                        document.querySelector('.information').style.display = 'flex';
                        i++;
                    }
                    else
                    {
                        document.querySelector('.information').style.display = 'none';
                        i--;
                    }
                    });  
                </script>
            </div>


