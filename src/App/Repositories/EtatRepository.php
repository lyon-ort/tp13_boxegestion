<?php

require '../src/App/Entities/Etat.php';

class EtatRepository {

    protected $table;     
    protected $classMapped; 
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire)
    public function __construct() {
        $this->table = 'Etat';
        $this->classMapped = 'App\Entities\Etat';
        $this->idFieldName = 'Id_Etat';
    }
}