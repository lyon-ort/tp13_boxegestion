<?php
require '../config/config.php';

// Authentification
if (isset($_POST['login']) AND isset($_POST['password']))
{
    $objetUtilisateurRep = new UtilisateurRepository();
    $objetUtilisateurRep->authentification();
    
    // Envoie vers la page d'accueil connecté si l'authentification est valide
    if ($_SESSION['logged'])
    {
        header('location: ../pages/accueilBis.php?connect=1');
    }
    else
    {
        header('location: ../pages/connexion.php?error=2');
    }
}

