<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';

// Initialisation de l'erreur de connexion
$error = isset($_GET['error']) ? $_GET['error'] : '';

if (isset($_GET['connect']))
{
    // Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
    require_once '../config/session-verif.php';    
    $leGet = '?connect=1';
}
else
{
    $leGet = '';
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Accueil </title>
    </head>
    
    <body>
        
        <!-- Header -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>

        
        <!-- Banners -->
        <section class='banner'>
            
            <div class='ban'>            
   
                <img src='../public/img/vs4.jpg' alt='Bannière du site' />
           
                <div class="inner-banner">
                    <?php
                    if (isset($_GET['connect']))
                    {
                    ?>
                        <div class="information">
                            Login : <?= $_SESSION['login'] ?> <br/>
                            Prénom : <?= $_SESSION['Prenom_Utilisateur'] ?> <br/>
                            Nom : <?= $_SESSION['Nom_Utilisateur'] ?> <br/>
                            Rôle :
                            <?php
                            $objetTypeRoleRepUtil = new TypeRoleRepository(); 
                            $typeRole = $objetTypeRoleRepUtil->selectionTypeRoleId($_SESSION['idRole']);
                            echo $typeRole->getLibelle_Typ_Role();
                            ?>                                 
                            <br/>
                            Structure :
                            <?php
                            $objetStructureRepUtil = new StructureRepository(); 
                            $structure = $objetStructureRepUtil->selectionStructureId($_SESSION['idStruct']);
                            echo $structure->getNom_Struct();
                            ?> <br/><br/><br/>
                            <button class='btn btn-custom' onclick="Deconnexion()"> Déconnexion </button>
                        </div>   
                    <?php
                    }
                    ?>                  
                    <h1> Bienvenue sur Savate Boxe </h1>
                    <a href="#about"> <button class='btn btn-custom'> Plus d'info </button> </a>
                </div>
            </div>
        </section> 
        <!-- End Banner -->       

        
        
        <!-- Section principal -->
        <section class='container-fluid about'>
            
            <div class='container'>
                
                <h2 id='about'> A propos de Savate Boxe </h2>
                <hr class="separator">
                
                <div class='row'>
                    <article class='col-md-4 col-lg-4 col-xs-12 col-sm-12'>
                        <h2> Présentation de l'application </h2>
                        <br>
                        <p><img src="../public/img/LesBoxeurs.jpg" width="300px" /></p>
                        <br>
                        Savate Boxe est une application de gestion de compétitions pour les clubs de boxe. Elle permet de gérer tout ce qui est en rapport avec la compétition. Déchargez-vous de toutes les tâches longues et fastidieuses : inscriptions des participants, établissement des tableaux et des poules, saisie des scores, calcul des classements... Le logiciel s'occupe de tout. Vous pourrez enfin vous concentrer sur l'essentiel : le sport.
                    </article>
                    
                    <article class='col-md-4 col-lg-4 col-xs-12 col-sm-12'>
                        <h2> Application en ligne </h2>
                        <br>
                        <p><img src="../public/img/picrogrammeConnecté.jpg" width="300px" /></p>
                        <br>
                        Vos compétitions sont partout, tout le temps avec vous sur votre ordinateur, tablette et smartphone. Pas besoin d'installation, Savate Boxe est disponible quel que soit votre appareil, via un site Internet. Et tout ceci à la fois pour les organisateurs et les participants. 
                    </article>                     
                    
                    <article class='col-md-4 col-lg-4 col-xs-12 col-sm-12'>
                        <h2> Les types d'utilisateurs </h2>
                        <br>
                        <p><img src="../public/img/utilisateurs.jpg" width="300px" /></p>
                        <br>                        
                        Plusieurs types d'utilisateurs peuvent se connecter à cette application :<br/>
                        - Administrateur (Gestion de structures et d'utilisateurs) <br/>
                        - Responsable de club (Gestion de tireurs et de compétitions) <br/>
                        - Opérateur (Import / Export des données de la compétition) <br/>
                        - Membre de club <br/>
                    </article>
                </div>
            </div>
        <br/><br/><br/><br/><br/>
        </section>
        <!-- End Section principal -->
        
        <!-- Footer -->
        
        <footer class='container-fluid footer'>
            
        </footer>
        <!-- End Footer -->
    </body>
</html>

