<?php

class Competition {

    // Attributs
    protected $Id_Competition;
    protected $Nom_Competition;
    protected $Date_Debut_Competition;
    protected $Date_Fin_Competition;
    protected $Adresse_Competition;
    protected $Cp_Competition;
    protected $Ville_Competition;
    protected $Date_Limite_Inscript_Competition;
    protected $Nb_Round;
    protected $Duree_Round;
    protected $Nb_Ring;
    protected $Id_Competition_Sexe;
    protected $Id_Competition_Typ_Affrontement;
    protected $Id_Competition_Struct;
    protected $Inactif_Competition;

    // Constructeur
    public function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas = NULL) {
        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Appelle le mutateur des attributs existant dans le tableau $datas
        foreach ($attrib as $key => $val) {
            // Si, dans vos habitudes, les champs de la bdd sont en minuscules, décommentez la ligne suivante.
            //$key = strtolower($key);
            // Si, dans vos habitudes, les champs de la bdd sont en majuscules, décommentez la ligne suivante.
            //$key = strtoupper($key);
            //  Perso, je met tout en notation camelCase... même dans la BDD.
            if (isset($datas[$key])) {
                $mutateur = 'set' . $key;
                $this->$mutateur($datas[$key]);
            }
        }

        return $this;
    }
    
        
        /**
     * Transforme un objet en un tableau associatif.
     * @return array tableau associatif des attributs => valeurs
     */
    public function __toArray() {
        return $this->jsonSerialize();
    }

    /**
     * La classe implémente l'interface JsonSerializable ce qui permet 
     * aux classes concrête de pouvoir être sérialisée en JSON avec json_encode()
     * @return Array le tableau pour conversion en json
     */
    public function jsonSerialize() {
        $array = array();

        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Associe la clé du nom de l'attribut à la valeur de cet attribut
        foreach ($attrib as $key => $val) {
            $array[$key] = $this->get($key);
        }
        return $array;
    }

    /**
     * Lecteur simple d'accés en lecture à un attribut
     * Peut être surchargé.
     * @param type $attribut le nom de l'attribut
     * @return type la valeur de l'attribut
     */
    protected function get($attribut) {
        return $this->$attribut;
    }
    
        /**
     * Surcharge du mutateur pour l'attribut $id qui ne peut pas être modifié.
     * @param int $val ne sert que si id_pers est null...
     */
    public function setid_pers($val=null)
    {
	if($this->Id_Competition === null)
	    $this->Id_Competition = (int)$val;
    }
    
    // Ces deux méthodes ne sont nécessaire QUE si la clé primaire dans la table
    // porte un nom différent de id...
    /**
     * Permet de s'affranchir du fait que le champ id dans la table est différent...
     * @param type $val
     */
    public function setid($val=null)
    {
	$this->setid_pers($val);
    }

    // GET et SET
    function getId_Competition() {
        return $this->Id_Competition;
    }

    function getNom_Competition() {
        return $this->Nom_Competition;
    }

    function getDate_Debut_Competition() {
        return $this->Date_Debut_Competition;
    }

    function getDate_Fin_Competition() {
        return $this->Date_Fin_Competition;
    }

    function getAdresse_Competition() {
        return $this->Adresse_Competition;
    }

    function getCp_Competition() {
        return $this->Cp_Competition;
    }

    function getVille_Competition() {
        return $this->Ville_Competition;
    }

    function getDate_Limite_Inscript_Competition() {
        return $this->Date_Limite_Inscript_Competition;
    }

    function getNb_Round() {
        return $this->Nb_Round;
    }

    function getDuree_Round() {
        return $this->Duree_Round;
    }

    function getNb_Ring() {
        return $this->Nb_Ring;
    }

    function getId_Competition_Sexe() {
        return $this->Id_Competition_Sexe;
    }

    function getId_Competition_Typ_Affrontement() {
        return $this->Id_Competition_Typ_Affrontement;
    }

    function getId_Competition_Struct() {
        return $this->Id_Competition_Struct;
    }

    function setId_Competition($Id_Competition): void {
        $this->Id_Competition = $Id_Competition;
    }

    function setNom_Competition($Nom_Competition): void {
        $this->Nom_Competition = $Nom_Competition;
    }

    function setDate_Debut_Competition($Date_Debut_Competition): void {
        $this->Date_Debut_Competition = $Date_Debut_Competition;
    }

    function setDate_Fin_Competition($Date_Fin_Competition): void {
        $this->Date_Fin_Competition = $Date_Fin_Competition;
    }

    function setAdresse_Competition($Adresse_Competition): void {
        $this->Adresse_Competition = $Adresse_Competition;
    }

    function setCp_Competition($Cp_Competition): void {
        $this->Cp_Competition = $Cp_Competition;
    }

    function setVille_Competition($Ville_Competition): void {
        $this->Ville_Competition = $Ville_Competition;
    }

    function setDate_Limite_Inscript_Competition($Date_Limite_Inscript_Competition): void {
        $this->Date_Limite_Inscript_Competition = $Date_Limite_Inscript_Competition;
    }

    function setNb_Round($Nb_Round): void {
        $this->Nb_Round = $Nb_Round;
    }

    function setDuree_Round($Duree_Round): void {
        $this->Duree_Round = $Duree_Round;
    }

    function setNb_Ring($Nb_Ring): void {
        $this->Nb_Ring = $Nb_Ring;
    }

    function setId_Competition_Sexe($Id_Competition_Sexe): void {
        $this->Id_Competition_Sexe = $Id_Competition_Sexe;
    }

    function setId_Competition_Typ_Affrontement($Id_Competition_Typ_Affrontement): void {
        $this->Id_Competition_Typ_Affrontement = $Id_Competition_Typ_Affrontement;
    }

    function setId_Competition_Struct($Id_Competition_Struct): void {
        $this->Id_Competition_Struct = $Id_Competition_Struct;
    }

    function getInactif_Competition() {
        return $this->Inactif_Competition;
    }

    function setInactif_Competition($Inactif_Competition): void {
        $this->Inactif_Competition = $Inactif_Competition;
    }
    
}
