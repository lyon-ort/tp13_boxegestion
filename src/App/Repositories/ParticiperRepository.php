<?php

require '../src/App/Entities/Participer.php';

class ParticiperRepository {

    protected $table;
    protected $classMapped;
    protected $idFieldName;

    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire)
    public function __construct() {
        $this->table = 'Participer';
        $this->classMapped = 'App\Entities\Participer';
        $this->idFieldName = 'Id_Particip';
    }

    //Ajouter ou modifier un champ
    public function sauver(Participer $entity) {
        $db = dbConnect();
        $resultSet = NULL;
        if ($entity != NULL) {
            $bindParam = $entity->__toArray();
            if ($entity->getId_Particip() == NULL) {
                // Nouvelle entité
                $query = "INSERT INTO $this->table" .
                        " (Id_Particip_Tireur, "
                        . "Id_Particip_Competition, "
                        . "Id_Particip_Tableau) "
                        . " VALUES "
                        . "(:Id_Particip_Tireur, "
                        . ":Id_Particip_Competition, "
                        . ":Id_Particip_Tableau)";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Id_Particip_Tireur', $bindParam['Id_Particip_Tireur'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Particip_Competition', $bindParam['Id_Particip_Competition'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Particip_Tableau', $bindParam['Id_Particip_Tableau'], \PDO::PARAM_INT);
                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $entity->setid($db->lastInsertId());
                    $resultSet = $entity;
                }
            } else {
                //  Entité existante
                $query = "UPDATE $this->table"
                        . " SET Nom_Competition = :Nom_Competition, "
                        . " Date_Debut_Competition = :Date_Debut_Competition, "
                        . " Date_Fin_Competition = :Date_Fin_Competition, "
                        . " Adresse_Competition = :Adresse_Competition, "
                        . " Cp_Competition = :Cp_Competition, "
                        . " Ville_Competition = :Ville_Competition, "
                        . " Date_Limite_Inscript_Competition = :Date_Limite_Inscript_Competition, "
                        . " Nb_Round = :Nb_Round, "
                        . " Duree_Round = :Duree_Round, "
                        . " Nb_Ring = :Nb_Ring, "
                        . " Id_Competition_Sexe = :Id_Competition_Sexe, "
                        . " Id_Competition_Typ_Affrontement = :Id_Competition_Typ_Affrontement, "
                        . " Id_Competition_Struct = :Id_Competition_Struct"
                        . " WHERE $this->idFieldName = :Id_Competition";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Nom_Competition', $bindParam['Nom_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Date_Debut_Competition', $bindParam['Date_Debut_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Date_Fin_Competition', $bindParam['Date_Fin_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Adresse_Competition', $bindParam['Adresse_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Cp_Competition', $bindParam['Cp_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Ville_Competition', $bindParam['Ville_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Date_Limite_Inscript_Competition', $bindParam['Date_Limite_Inscript_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Nb_Round', $bindParam['Nb_Round'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Duree_Round', $bindParam['Duree_Round'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Nb_Ring', $bindParam['Nb_Ring'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Competition_Sexe', $bindParam['Id_Competition_Sexe'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Competition_Typ_Affrontement', $bindParam['Id_Competition_Typ_Affrontement'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Competition_Struct', $bindParam['Id_Competition_Struct'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Competition', $bindParam[$this->idFieldName], \PDO::PARAM_INT);

                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $resultSet = $entity;
                }
            }
        }
        return $resultSet;
    }

    
    
    // Sélection de participation selon un id tableau
    public function selectionParticipationIdTableau($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objet à NULL si jamais la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Particip_Tableau = :idMark");
        $req->execute(array('idMark' => $id));

        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;           
    }
    
    
    
    // Sélection de participation selon un id compétition
    public function selectionParticipationIdCompetTireur($idCompetition, $idTireur)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objet à NULL si jamais la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Particip_Competition = :idCompetitionMark AND Id_Particip_Tireur = :idTireurMark");
        $req->execute(array('idCompetitionMark' => $idCompetition, 'idTireurMark' => $idTireur));

        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;           
    }
    
    
    
    // Supprime une participation selon un ID de tireur
    public function SupprimerParticipationIdTireur($idTireur)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objet à NULL si jamais la requête n'est pas bonne
        $testReq = NULL;       
        
        // Requête
        $req = $db->prepare("DELETE FROM $this->table WHERE Id_Particip_Tireur = :idTireurMark");
        $req->execute(array('idTireurMark' => $idTireur));
        
        // Test si la requête est bonne
        if ($req)
        {
            $testReq = true;
        }
        
        return $testReq;
    }
}
