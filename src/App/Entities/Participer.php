<?php

class Participer {

    // Attributs
    protected $Id_Particip;
    protected $Id_Particip_Tireur;
    protected $Id_Particip_Competition;
    protected $Id_Particip_Tableau;


        // Constructeur
    public function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas = NULL) {
        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Appelle le mutateur des attributs existant dans le tableau $datas
        foreach ($attrib as $key => $val) {
            // Si, dans vos habitudes, les champs de la bdd sont en minuscules, décommentez la ligne suivante.
            //$key = strtolower($key);
            // Si, dans vos habitudes, les champs de la bdd sont en majuscules, décommentez la ligne suivante.
            //$key = strtoupper($key);
            //  Perso, je met tout en notation camelCase... même dans la BDD.
            if (isset($datas[$key])) {
                $mutateur = 'set' . $key;
                $this->$mutateur($datas[$key]);
            }
        }

        return $this;
    }
    
    
                /**
     * Transforme un objet en un tableau associatif.
     * @return array tableau associatif des attributs => valeurs
     */
    public function __toArray() {
        return $this->jsonSerialize();
    }

    /**
     * La classe implémente l'interface JsonSerializable ce qui permet 
     * aux classes concrête de pouvoir être sérialisée en JSON avec json_encode()
     * @return Array le tableau pour conversion en json
     */
    public function jsonSerialize() {
        $array = array();

        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Associe la clé du nom de l'attribut à la valeur de cet attribut
        foreach ($attrib as $key => $val) {
            $array[$key] = $this->get($key);
        }
        return $array;
    }

    /**
     * Lecteur simple d'accés en lecture à un attribut
     * Peut être surchargé.
     * @param type $attribut le nom de l'attribut
     * @return type la valeur de l'attribut
     */
    protected function get($attribut) {
        return $this->$attribut;
    }
    
        /**
     * Surcharge du mutateur pour l'attribut $id qui ne peut pas être modifié.
     * @param int $val ne sert que si id_pers est null...
     */
    public function setid_pers($val=null)
    {
	if($this->Id_Particip === null)
	    $this->Id_Particip = (int)$val;
    }
    
    // Ces deux méthodes ne sont nécessaire QUE si la clé primaire dans la table
    // porte un nom différent de id...
    /**
     * Permet de s'affranchir du fait que le champ id dans la table est différent...
     * @param type $val
     */
    public function setid($val=null)
    {
	$this->setid_pers($val);
    }
    

    // GET et SET
    function getId_Particip() {
        return $this->Id_Particip;
    }

    function getId_Particip_Tireur() {
        return $this->Id_Particip_Tireur;
    }

    function getId_Particip_Competition() {
        return $this->Id_Particip_Competition;
    }

    function setId_Particip($Id_Particip): void {
        $this->Id_Particip = $Id_Particip;
    }

    function setId_Particip_Tireur($Id_Particip_Tireur): void {
        $this->Id_Particip_Tireur = $Id_Particip_Tireur;
    }

    function setId_Particip_Competition($Id_Particip_Competition): void {
        $this->Id_Particip_Competition = $Id_Particip_Competition;
    }
    
    function getId_Particip_Tableau() {
        return $this->Id_Particip_Tableau;
    }

    function setId_Particip_Tableau($Id_Particip_Tableau): void {
        $this->Id_Particip_Tableau = $Id_Particip_Tableau;
    }

}
