<?php

require '../config/config.php';

// Déclaration TableauRepository
$tableauRepository = new TableauRepository();

// Déclaration du tableau des valeurs
$valeurTableau = array(
    "Libelle_Tab" => "tata",
    "Nb_Max_Rencontre" => 1,
    "Id_Tableau_Sexe" => 1,
    "Id_Tableau_Typ_Affrontement" => 1,
    "Id_Tableau_Typ_Tableau" => 1,
    "Id_Tableau_Competition" => 1
);

// Initialisation et dump() des valeurs
$tableau = new Tableau($valeurTableau);
var_dump($tableau);

// Fonction sauver pour créer ou modifier un tableau
$tableauRepository->sauver($tableau);

