<?php

require '../src/App/Entities/CategorieRencontre.php';

class CategorieRencontreRepository {

    protected $table;     
    protected $classMapped; 
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire)
    public function __construct() {
        $this->table = 'CategorieRencontre';
        $this->classMapped = 'App\Entities\CategorieRencontre';
        $this->idFieldName = 'Id_Cat_Rencontre';
    }
    
    
    /* Les fonctions de romain
     * 
     * 
     * 
     *  */
    
    // Sélectionne une catégorie de rencontre selon un nombre de rencontre
    public function CatégorieRencontrePyramidale($nbRencontre)
    {
        $db = dbConnect();
        $res = null;
        $query = "SELECT * FROM CategorieRencontre WHERE Nb_Rencontre = $nbRencontre;";
        $req = $db->query($query);
        
        if ($req) 
        {
            while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
               
                $res = new $this->table($row);
            }
        }

        return $res;
    }
    
    
    // Sélectionne une catégorie de rencontre selon un ID
    public function selectionCategorieRencontreId($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $objet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE $this->idFieldName = :idMark");
        $req->execute(array('idMark' => $id));
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$objet = new $this->table($row);
	    }
        }
        
        // Return de l'objet
        return $objet;
    }     
}