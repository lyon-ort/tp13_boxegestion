<?php
require '../config/config.php';

$tireurRepModif = new TireurRepository();

$valeurTireurModif = array(
    "Id_Tireur" => $_POST['Id_Tireur'],
    "Id_Tir_Niv_Tireur" => $_POST['Id_Tir_Niv_Tireur'],
    "Nom_Tireur" => $_POST['Nom_Tireur'],
    "Prenom_Tireur" => $_POST['Prenom_Tireur'],
    "Date_Naissance" => $_POST['Date_Naissance'],
    "Num_Licence" => $_POST['Num_Licence'],
    "Poids_Tireur" => $_POST['Poids_Tireur'],
    "Id_Tir_Cat_Poids" => $_POST['Id_Tir_Cat_Poids'],
    "Id_Tir_Cat_Age" => $_POST['Id_Tir_Cat_Age'],
    "Id_Tir_Sexe" => $_POST['Id_Tir_Sexe'],
    "Id_Tir_Struct" => $_POST['Id_Tir_Struct']
);
$tireurModif = new Tireur($valeurTireurModif);
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form method="POST" action="gestionTireurTraitement.php">
            Id_Tireur : <input type="text" name="Id_Tireur" value="<?= $tireurModif->getId_Tireur() ?>" readonly> <br/>
            Id_Tir_Niv_Tireur : <input type="text" name="Id_Tir_Niv_Tireur" value="<?= $tireurModif->getId_Tir_Niv_Tireur() ?>"> <br/>
            Nom_Tireur : <input type="text" name="Nom_Tireur" value="<?= $tireurModif->getNom_Tireur() ?>"> <br/>
            Prenom_Tireur : <input type="text" name="Prenom_Tireur" value="<?= $tireurModif->getPrenom_Tireur() ?>"> <br/>
            Date_Naissance : <input type="text" name="Date_Naissance" value="<?= $tireurModif->getDate_Naissance() ?>"> <br/>
            Num_Licence : <input type="text" name="Num_Licence" value="<?= $tireurModif->getNum_Licence() ?>"> <br/>
            Poids_Tireur : <input type="text" name="Poids_Tireur" value="<?= $tireurModif->getPoids_Tireur() ?>"> <br/>
            Id_Tir_Cat_Poids : <input type="text" name="Id_Tir_Cat_Poids" value="<?= $tireurModif->getId_Tir_Cat_Poids() ?>"> <br/>
            Id_Tir_Cat_Age : <input type="text" name="Id_Tir_Cat_Age" value="<?= $tireurModif->getId_Tir_Cat_Age() ?>"> <br/>
            Id_Tir_Sexe : <input type="text" name="Id_Tir_Sexe" value="<?= $tireurModif->getId_Tir_Sexe() ?>"> <br/>
            Id_Tir_Struct : <input type="text" name="Id_Tir_Struct" value="<?= $tireurModif->getId_Tir_Struct() ?>"> <br/>
            
            <input type="submit" value="Valider">
        </form>
        
    </body>
</html>
