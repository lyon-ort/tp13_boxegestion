<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';
$leGet = '?connect=1';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Créer utilisateur </title>
    </head>
    
    <body>
        <!-- Header -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>


            <?php
            if (isset($_GET['confirmationCreation']))
            {
                ?>
                    <!-- Pop up de confirmation de suppression -->
                    <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                        <div class="modal-content" style="height: 250px;">
                            <!-- bouton fermer -->
                            <div id="close" class="closeTest">+</div>

                            <!-- Confirmation de suppression -->
                            <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                            L'utilisateur a été créé !<br/><br/>

                            <button class="boutonAnnulerTest"> OK </button>
                        </div>
                    </div>   

                    <script>
                        // La croix (ferme le pop-up)
                        document.querySelector('.closeTest').addEventListener('click', function() 
                        {
                            document.querySelector('.bg-modalTest').style.display = 'none';
                        });      

                        // Le bouton annuler (ferme le pop-up)
                        document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                        {
                            document.querySelector('.bg-modalTest').style.display = 'none';
                        });       
                    </script>            
                <?php  
            }
            ?>
        
        <!-- Section principal -->
        <section class='container-fluid about'>

            <?php
            if (isset($_GET['connect']))
            {
            ?>
                <div class="information">
                    Login : <?= $_SESSION['login'] ?> <br/>
                    Prénom : <?= $_SESSION['Prenom_Utilisateur'] ?> <br/>
                    Nom : <?= $_SESSION['Nom_Utilisateur'] ?> <br/>
                    Rôle :
                    <?php
                    $objetTypeRoleRepUtil = new TypeRoleRepository(); 
                    $typeRole = $objetTypeRoleRepUtil->selectionTypeRoleId($_SESSION['idRole']);
                    echo $typeRole->getLibelle_Typ_Role();
                    ?>                                 
                    <br/>
                    Structure :
                    <?php
                    $objetStructureRepUtil = new StructureRepository(); 
                    $structure = $objetStructureRepUtil->selectionStructureId($_SESSION['idStruct']);
                    echo $structure->getNom_Struct();
                    ?> <br/><br/><br/>
                    <button class='btn btn-custom' onclick="Deconnexion()"> Déconnexion </button>
                </div>   
            <?php
            }
            ?>
            
            <h1> Créer utilisateurs </h1>
            
            <hr class="separator">
            
            <?php
            if (isset($_POST['validation']))
            {
                // Test si les mdp sont identiques
                $confirmMdp = false;
                
                if ($_POST['Mdp'] == $_POST['MdpConfirm'])
                {
                    $confirmMdp = true;
                }  
                else 
                {
                    echo '<p class="error">ERREUR : Les mots de passes inscrit ne sont pas identique</p> <br/><br/>';
                    $errorMdp = 'error';
                }
                
                // Test si le login existe déjà
                $objetUtilisateurRep2 = new UtilisateurRepository();
                $arrayUtilisateur = $objetUtilisateurRep2->selectionUtilisateur();
                    
                $confirmLogin = true;
                    
                foreach ($arrayUtilisateur as $value)
                {
                    if ($value->getLogin_Utilisateur() == $_POST['Login_Utilisateur'])
                    {
                        $confirmLogin = false;
                        echo '<p class="error">ERREUR : Ce login d\'utilisateur existe déjà</p> <br/><br/>';
                        $errorLogin = 'error';
                    }
                }
                
                if ($confirmMdp && $confirmLogin)
                {
                    $confirmationCreation = true;
                }
            }
            ?>


            <!-- Formulaire création compétition -->
            <form class="formCreat" method="POST" action="creatUtil.php?connect=1">
                Type de role: 
                <select name="Id_Typ_Role_Utilisateur" required>
                    <option value=""> -- Choisir un type -- </option>
                    <?php
                        // Type de role
                        $objetTypeRoleRep = new TypeRoleRepository();
                        $arrayTypeRole = $objetTypeRoleRep->selectionTypeRole();
                        
                        foreach($arrayTypeRole as $value)
                        {
                            $selected = ($value->getId_Typ_Role() == $_POST['Id_Typ_Role_Utilisateur']) ? 'selected' : '';
                                ?>
                                    <option value="<?= $value->getId_Typ_Role() ?>" <?= $selected ?>> <?= $value->getLibelle_Typ_Role() ?> </option> 
                                <?php
                        }
                    ?>                           
                </select> 
                <br/><br/>
                    
                Structure : 
                <select name="Id_Struct_Utilisateur" required>
                    <option value=""> -- Choisir une structure -- </option>
                    <?php
                    // Structure
                    $objetStructureRep = new StructureRepository();
                    $arrayObjetStructure = $objetStructureRep->selectionStructure();
                        
                    foreach ($arrayObjetStructure as $value)
                    {
                        $selected = ($value->getId_Struct() == $_POST['Id_Struct_Utilisateur']) ? 'selected' : '';
                        ?>
                            <option value="<?= $value->getId_Struct() ?>" <?= $selected ?>> <?= $value->getNom_Struct() ?> </option> 
                        <?php
                    }
                    ?>
                </select> 
                <br/><br/>
                    
                Nom : 
                <input type="text" name="Nom_Utilisateur" placeholder="nom" value="<?= $valueNom = (isset($_POST['Nom_Utilisateur'])) ? $_POST['Nom_Utilisateur'] : '' ?>" required>
                <br/><br/>
                    
                Prénom : 
                <input type="text" name="Prenom_Utilisateur" placeholder="prénom" value="<?= $valuePrenom = (isset($_POST['Prenom_Utilisateur'])) ? $_POST['Prenom_Utilisateur'] : '' ?>" required> 
                <br/><br/>
                    
                Login : 
                <input id="<?= $errorLogin = (isset($errorLogin)) ? $errorLogin : '' ?>" type="text" name="Login_Utilisateur" placeholder="login" value="<?= $valueLogin = (isset($_POST['Login_Utilisateur'])) ? $_POST['Login_Utilisateur'] : '' ?>" required> 
                <br/><br/>
                    
                Email : 
                <input type="email" name="Email" placeholder="xyz@gmail.com" value="<?= $valueEmail = (isset($_POST['Email'])) ? $_POST['Email'] : '' ?>" required> 
                <br/><br/>
                    
                Mot de passe : 
                <input id="<?= $errorMdp = (isset($errorMdp)) ? $errorMdp : '' ?>" type="password" name="Mdp" required> 
                <br/><br/>
                    
                Confirmation du mot de passe : 
                <input id="<?= $errorMdp = (isset($errorMdp)) ? $errorMdp : '' ?>" type="password" name="MdpConfirm" required>  
                <br/><br/>
                    
                Portable : 
                <input type="tel" minlength="10" maxlength="10" name="Tel_Port" placeholder="0123456789" value="<?= $valueTel_Port = (isset($_POST['Tel_Port'])) ? $_POST['Tel_Port'] : '' ?>" required> 
                <br/><br/>
                    
                <input type="hidden" name="validation">
                    
                <input type="submit" value="Valider">
            </form>

            <?php
                // Créé l'utilisateur
                if (isset($confirmationCreation))
                {
                    $objetUtilisateurRep = new UtilisateurRepository();

                    // Hash mdp
                    $hashMdp = password_hash($_POST['Mdp'], PASSWORD_DEFAULT);

                    // Déclaration du tableau des valeurs
                    $valeurUtilisateur = array(
                        "Id_Typ_Role_Utilisateur" => $_POST['Id_Typ_Role_Utilisateur'],
                        "Id_Struct_Utilisateur" => $_POST['Id_Struct_Utilisateur'],
                        "Nom_Utilisateur" => $_POST['Nom_Utilisateur'],
                        "Prenom_Utilisateur" => $_POST['Prenom_Utilisateur'],
                        "Login_Utilisateur" => $_POST['Login_Utilisateur'],
                        "Email" => $_POST['Email'],
                        "Mdp" => $hashMdp,
                        "Tel_Port" => $_POST['Tel_Port'],
                        "Date_Creation_Compte" => date("Y-m-d H:i:s"),
                        "Dernier_Login_Utilisateur" => null,
                        "Inactif_Utilisateur" => 0
                    );

                    $objetUtilisateur = new Utilisateur($valeurUtilisateur);
                    $objetUtilisateurRep->sauver($objetUtilisateur);

                    ?>
                    <script language="Javascript"> 
                        document.location.replace("creatUtil.php?connect=1&confirmationCreation=1");
                    </script>
                    <?php
                }
            ?>
            <br/>
        </section>
    </body>
</html>

