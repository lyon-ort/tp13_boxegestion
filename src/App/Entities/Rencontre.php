<?php

class Rencontre {

    // Attributs
    protected $Id_Rencontre;
    protected $Date_Rencontre;
    protected $Heure_Debut_Rencontre;
    protected $Heure_Fin_Rencontre;
    protected $Num_Ring_Rencontre;
    protected $Id_Tireur_Rouge;
    protected $Id_Tireur_Bleu;
    protected $Id_Gagnant;
    protected $Id_Rencontre_Precedent_1;
    protected $Id_Rencontre_Precedent_2;
    protected $Id_Rencontre_Suivante;
    protected $Id_Cat_Rencontre;
    protected $Id_Tab_Rencontre;
    protected $Id_Etat_Rencontre;

    // Constructeur
    public function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas = NULL) {
        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Appelle le mutateur des attributs existant dans le tableau $datas
        foreach ($attrib as $key => $val) {
            // Si, dans vos habitudes, les champs de la bdd sont en minuscules, décommentez la ligne suivante.
            //$key = strtolower($key);
            // Si, dans vos habitudes, les champs de la bdd sont en majuscules, décommentez la ligne suivante.
            //$key = strtoupper($key);
            //  Perso, je met tout en notation camelCase... même dans la BDD.
            if (isset($datas[$key])) {
                $mutateur = 'set' . $key;
                $this->$mutateur($datas[$key]);
            }
        }

        return $this;
    }
    
            /**
     * Transforme un objet en un tableau associatif.
     * @return array tableau associatif des attributs => valeurs
     */
    public function __toArray() {
        return $this->jsonSerialize();
    }

    /**
     * La classe implémente l'interface JsonSerializable ce qui permet 
     * aux classes concrête de pouvoir être sérialisée en JSON avec json_encode()
     * @return Array le tableau pour conversion en json
     */
    public function jsonSerialize() {
        $array = array();

        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Associe la clé du nom de l'attribut à la valeur de cet attribut
        foreach ($attrib as $key => $val) {
            $array[$key] = $this->get($key);
        }
        return $array;
    }

    /**
     * Lecteur simple d'accés en lecture à un attribut
     * Peut être surchargé.
     * @param type $attribut le nom de l'attribut
     * @return type la valeur de l'attribut
     */
    protected function get($attribut) {
        return $this->$attribut;
    }
    
        /**
     * Surcharge du mutateur pour l'attribut $id qui ne peut pas être modifié.
     * @param int $val ne sert que si id_pers est null...
     */
    public function setid_pers($val=null)
    {
	if($this->Id_Rencontre === null)
	    $this->Id_Rencontre = (int)$val;
    }
    
    // Ces deux méthodes ne sont nécessaire QUE si la clé primaire dans la table
    // porte un nom différent de id...
    /**
     * Permet de s'affranchir du fait que le champ id dans la table est différent...
     * @param type $val
     */
    public function setid($val=null)
    {
	$this->setid_pers($val);
    }

    // GET et SET
    function getId_Rencontre() {
        return $this->Id_Rencontre;
    }

    function getDate_Rencontre() {
        return $this->Date_Rencontre;
    }

    function getHeure_Debut_Rencontre() {
        return $this->Heure_Debut_Rencontre;
    }

    function getHeure_Fin_Rencontre() {
        return $this->Heure_Fin_Rencontre;
    }

    function getNum_Ring_Rencontre() {
        return $this->Num_Ring_Rencontre;
    }

    function getId_Tireur_Rouge() {
        return $this->Id_Tireur_Rouge;
    }

    function getId_Tireur_Bleu() {
        return $this->Id_Tireur_Bleu;
    }

    function getId_Gagnant() {
        return $this->Id_Gagnant;
    }

    function getId_Rencontre_Precedent_1() {
        return $this->Id_Rencontre_Precedent_1;
    }

    function getId_Rencontre_Precedent_2() {
        return $this->Id_Rencontre_Precedent_2;
    }

    function getId_Rencontre_Suivante() {
        return $this->Id_Rencontre_Suivante;
    }

    function getId_Cat_Rencontre() {
        return $this->Id_Cat_Rencontre;
    }

    function getId_Tab_Rencontre() {
        return $this->Id_Tab_Rencontre;
    }

    function getId_Etat_Rencontre() {
        return $this->Id_Etat_Rencontre;
    }

    function setId_Rencontre($Id_Rencontre): void {
        $this->Id_Rencontre = $Id_Rencontre;
    }

    function setDate_Rencontre($Date_Rencontre): void {
        $this->Date_Rencontre = $Date_Rencontre;
    }

    function setHeure_Debut_Rencontre($Heure_Debut_Rencontre): void {
        $this->Heure_Debut_Rencontre = $Heure_Debut_Rencontre;
    }

    function setHeure_Fin_Rencontre($Heure_Fin_Rencontre): void {
        $this->Heure_Fin_Rencontre = $Heure_Fin_Rencontre;
    }

    function setNum_Ring_Rencontre($Num_Ring_Rencontre): void {
        $this->Num_Ring_Rencontre = $Num_Ring_Rencontre;
    }

    function setId_Tireur_Rouge($Id_Tireur_Rouge): void {
        $this->Id_Tireur_Rouge = $Id_Tireur_Rouge;
    }

    function setId_Tireur_Bleu($Id_Tireur_Bleu): void {
        $this->Id_Tireur_Bleu = $Id_Tireur_Bleu;
    }

    function setId_Gagnant($Id_Gagnant): void {
        $this->Id_Gagnant = $Id_Gagnant;
    }

    function setId_Rencontre_Precedent_1($Id_Rencontre_Precedent_1): void {
        $this->Id_Rencontre_Precedent_1 = $Id_Rencontre_Precedent_1;
    }

    function setId_Rencontre_Precedent_2($Id_Rencontre_Precedent_2): void {
        $this->Id_Rencontre_Precedent_2 = $Id_Rencontre_Precedent_2;
    }

    function setId_Rencontre_Suivante($Id_Rencontre_Suivante): void {
        $this->Id_Rencontre_Suivante = $Id_Rencontre_Suivante;
    }

    function setId_Cat_Rencontre($Id_Cat_Rencontre): void {
        $this->Id_Cat_Rencontre = $Id_Cat_Rencontre;
    }

    function setId_Tab_Rencontre($Id_Tab_Rencontre): void {
        $this->Id_Tab_Rencontre = $Id_Tab_Rencontre;
    }

    function setId_Etat_Rencontre($Id_Etat_Rencontre): void {
        $this->Id_Etat_Rencontre = $Id_Etat_Rencontre;
    }

}
