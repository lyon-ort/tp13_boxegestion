<?php

class TypeRole {

    // Attributs
    protected $Id_Typ_Role;
    protected $Libelle_Typ_Role;

    // Constructeur
    public function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas = NULL) {
        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Appelle le mutateur des attributs existant dans le tableau $datas
        foreach ($attrib as $key => $val) {
            // Si, dans vos habitudes, les champs de la bdd sont en minuscules, décommentez la ligne suivante.
            //$key = strtolower($key);
            // Si, dans vos habitudes, les champs de la bdd sont en majuscules, décommentez la ligne suivante.
            //$key = strtoupper($key);
            //  Perso, je met tout en notation camelCase... même dans la BDD.
            if (isset($datas[$key])) {
                $mutateur = 'set' . $key;
                $this->$mutateur($datas[$key]);
            }
        }

        return $this;
    }

    // GET et SET
    function getId_Typ_Role() {
        return $this->Id_Typ_Role;
    }

    function getLibelle_Typ_Role() {
        return $this->Libelle_Typ_Role;
    }

    function setId_Typ_Role($Id_Typ_Role): void {
        $this->Id_Typ_Role = $Id_Typ_Role;
    }

    function setLibelle_Typ_Role($Libelle_Typ_Role): void {
        $this->Libelle_Typ_Role = $Libelle_Typ_Role;
    }

}
