<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Créer compétition </title>
    </head>

    <body>
        <!-- Header (inc) -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>


         <!-- Section -->
        <section class='container-fluid about'>
            
            <!-- Information de l'utilisateur (inc) -->
            <?php
            include '../inc/information.php';
            ?>
            
            <h1> Créer compétition </h1>
            
            <hr class="separator">
            
            <!-- Les tests lors des validations des formulaires -->
            <?php
            if (isset($_GET['confirmationCreation']))
            {
            ?>
                    
                            <!-- Pop up de confirmation de suppression -->
                            <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                                <div class="modal-content" style="height: 250px;">
                                    <!-- bouton fermer -->
                                    <div id="close" class="closeTest">+</div>

                                    <!-- Confirmation de suppression -->
                                    <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                                    La compétition a été créé !<br/><br/>

                                    <button class="boutonAnnulerTest"> OK </button>
                                </div>
                            </div>   

                             <script>
                                // La croix (ferme le pop-up)
                                document.querySelector('.closeTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });      

                                // Le bouton annuler (ferme le pop-up)
                                document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });       
                            </script>            
                    <?php             
            }
            
            // Les id error et le disabled
            $errorNom = '';
            $errorDateDebut = ''; 
            $errorDateFin = ''; 
            $errorDateLimit = ''; 
            
            // Validation de la compétition
            if (isset($_POST['competition'])) 
            {
                $objetCompetitionRep = new CompetitionRepository;
                $arrayObjetCompetition = $objetCompetitionRep->selectionCompetition();
                
                // Test si le nom de compétition existe déjà
                $testNom = true;
                
                foreach ($arrayObjetCompetition as $value) //"for ($i = 0; $i != count($arrayObjetCompetition); $i++)" //"$arrayObjetCompetition[$i]"
                {
                    if ($value->getNom_Competition() == $_POST['Nom_Competition']) 
                    {
                        $testNom = false;
                        echo 'ERREUR : Ce "nom" de compétition existe déjà<br/><br/>';
                        $errorNom = 'error';
                    }
                }
                
                // Test si la date de fin est inférieur à la date de début
                $testDate = true;
                
                if ($_POST['Date_Fin_Competition'] < $_POST['Date_Debut_Competition']) 
                {
                    $testDate = false;
                    echo 'ERREUR : La date de fin est inférieur à la date de début<br/><br/>';
                    $errorDateDebut = 'error';
                    $errorDateFin = 'error'; 
                }
                
                // Test si la date d'inscription est supérieur à la date de début
                $testDateInscript = true;
                
                if ($_POST['Date_Limite_Inscript_Competition'] > $_POST['Date_Debut_Competition']) 
                {
                    $testDateInscript = false;
                    echo 'ERREUR : La date limite d\'inscription est supérieur à la date de début<br/><br/>';
                    $errorDateLimit = 'error';
                } 
                
                // Validation des 3 tests
                if ($testNom && $testDate && $testDateInscript)
                {
                    $confirmationCompetition = true;
                }
            }
            
            

            ?>

            <!-- Formulaire création compétition -->
            <form class="formCreat" method="POST" action="creationCompetition.php?connect=1">
                
                <strong> Nom : </strong> 
                <input id="<?= $errorNom ?>" type="text" name="Nom_Competition" placeholder="nom" value="<?= $Nom_Competition = (isset($_POST['Nom_Competition'])) ? $_POST['Nom_Competition'] : '' ?>" required> 
                <br/><br/>
                    
                <strong> Date de début : </strong> 
                <input id="<?= $errorDateDebut ?>" type="Date" name="Date_Debut_Competition" min="<?= date("Y-m-d") ?>" value="<?= $Date_Debut_Competition = (isset($_POST['Date_Debut_Competition'])) ? $_POST['Date_Debut_Competition'] : '' ?>" required> 
                <br/><br/>
                    
                <strong> Date de fin : </strong>
                <input id="<?= $errorDateFin ?>" type="Date" name="Date_Fin_Competition" min="<?= date("Y-m-d") ?>" value="<?= $Date_Fin_Competition = (isset($_POST['Date_Fin_Competition'])) ? $_POST['Date_Fin_Competition'] : '' ?>" required> 
                <br/><br/>
                    
                <strong> Adresse : </strong> 
                <input type="text" name="Adresse_Competition" placeholder="adresse" value="<?= $Adresse_Competition = (isset($_POST['Adresse_Competition'])) ? $_POST['Adresse_Competition'] : '' ?>" required> 
                <br/><br/>
                    
                <strong> Code postale : </strong> 
                <input type="text" name="Cp_Competition" placeholder="12345" minlength="5" maxlength="5" value="<?= $Cp_Competition = (isset($_POST['Cp_Competition'])) ? $_POST['Cp_Competition'] : '' ?>" required> 
                <br/><br/>
                    
                <strong> Ville : </strong> 
                <input type="text" name="Ville_Competition" placeholder="ville" value="<?= $Ville_Competition = (isset($_POST['Ville_Competition'])) ? $_POST['Ville_Competition'] : '' ?>" required> 
                <br/><br/>
                    
                <strong> Date limite d'inscription : </strong> 
                <input id="<?= $errorDateLimit ?>" type="Date" name="Date_Limite_Inscript_Competition" min="<?= date("Y-m-d") ?>" value="<?= $Date_Limite_Inscript_Competition = (isset($_POST['Date_Limite_Inscript_Competition'])) ? $_POST['Date_Limite_Inscript_Competition'] : '' ?>" required> 
                <br/><br/>
                             
                
                <!-- Les categories d'age de la compétition "sefaire" -->
                <!-- Influence les catégories d'age des tableaux "concerne" -->
                <strong> Catégorie d'âge : </strong> <br/>
                <fieldset>
                    <?php
                    $objetCategorieAgeRep = new CategorieAgeRepository();
                    $arrayObjetCategorieAge = $objetCategorieAgeRep->selectionCategorieAge();
                    
                    foreach ($arrayObjetCategorieAge as $value)
                    {
                        $checked = '';
                        
                        if (isset($_POST['categorieAgeCompetition']))
                        {
                            for ($i = 0; $i != count($_POST['categorieAgeCompetition']); $i++)
                            {
                                if ($_POST['categorieAgeCompetition'][$i] == $value->getId_Cat_Age())
                                {
                                    $checked = 'checked';
                                }
                            }
                        }
                        
                        ?>
                            <input id="<?= $value->getId_Cat_Age() ?>" type="checkbox" name="categorieAgeCompetition[]" value="<?= $value->getId_Cat_Age() ?>" <?= $checked ?>>
                            <label for="<?= $value->getId_Cat_Age() ?>"> <?= $value->getLibelle_Cat_Age() ?> </label>
                            <br/>
                        <?php
                    }
                    ?>
                </fieldset>
                <br/>
                

                
                
                <strong> Nombre round : </strong> 
                <select name="Nb_Round" required>
                    <option value=""> -- Choisir un nombre -- </option>
                    <?php
                        for ($i = 1; $i < 8; $i++) 
                        {
                            if ($i != 6)
                            {
                                // Préselectionne le type de rôle
                                $selected = ($_POST['Nb_Round'] == $i) ? 'selected' : '';
                                ?>
                                    <option value="<?= $i ?>" <?= $selected ?>> <?= $i ?> </option> 
                                <?php
                            }
                        }
                    ?>
                </select>
                <br/><br/>
                    
                <strong> Durée d'un round : </strong> 
                <br/>
                <input type="time" name="Duree_Round" min="00:00" max="05:00" value="<?= $Duree_Round = (isset($_POST['Duree_Round'])) ? $_POST['Duree_Round'] : '' ?>" required/>
                <br/><br/>
                    
                <strong> Nombre de ring : </strong>
                <select name="Nb_Ring" required >
                    <option value=""> -- Choisir un nombre -- </option>
                    <?php
                        for ($i = 1; $i != 6; $i++) 
                        {
                            // Préselectionne le type de rôle
                            $selected = ($_POST['Nb_Ring'] == $i) ? 'selected' : '';
                            ?>
                                <option value="<?= $i ?>" <?= $selected ?>> <?= $i ?> </option> 
                            <?php
                        }
                    ?>
                </select>
                <br/><br/>
                    
                <!-- Les sexe -->
                <strong> Sexe : </strong> 
                <select name="Id_Competition_Sexe">
                    <option value=""> Les 2 </option>
                    <?php
                        // Sexe
                        $objetSexeRep = new SexeRepository();
                        $arraySexe = $objetSexeRep->selectionSexe();

                        foreach ($arraySexe as $value)
                        {
                            $selected = ($value->getId_Sexe() == $_POST['Id_Competition_Sexe']) ? 'selected' : '';
                            ?>
                                <option value="<?= $value->getId_Sexe() ?>" <?= $selected ?>> <?= $value->getLibelle_Sexe() ?> </option> 
                            <?php
                        }
                    ?>  
                </select> 
                <br/><br/>
                    
                <!-- Les types d'affrontements -->
                <strong>Type d'affrontement : </strong>
                <select name="Id_Competition_Typ_Affrontement">
                    <option value=""> Les 2 </option>
                    <?php
                        // Type affontement
                        $objetTypeAffrontementRep = new TypeAffrontementRepository();
                        $arrayTypeAffrontement = $objetTypeAffrontementRep->selectionTypeAffrontement();

                        foreach ($arrayTypeAffrontement as $value)
                        {
                            $selected = ($value->getId_Typ_Affrontement() == $_POST['Id_Competition_Typ_Affrontement']) ? 'selected' : '';
                            ?>
                                <option value="<?= $value->getId_Typ_Affrontement() ?>" <?= $selected ?>> <?= $value->getLibelle_Typ_Affrontement() ?> </option> 
                            <?php
                        }
                    ?>  
                </select> 
                <br/><br/>
                    
                <strong>Structure :</strong>
                <input type="text" name="nomStruct" value="<?= $structure->getNom_Struct() ?>" readonly> 
                <input type="hidden" name="Id_Competition_Struct" value="<?= $_SESSION['idStruct'] ?>"/>
                <br/><br/>            

                <input type="hidden" name="competition"/>
                    
                <input type="submit" value="Valider">
            </form>   

            
           


            <!-- CREATION DE LA COMPETITION ET DES TABLEAUX -->
            <?php
            if (isset($confirmationCompetition) && $confirmationCompetition) 
            {
                // COMPETITION
                // Converssion i:s en seconde (int)
                $secondes = $_POST['Duree_Round'][1] * 60 + ($_POST['Duree_Round'][3] * 10) + $_POST['Duree_Round'][4];

                // Si le sexe ou le type d'affrontement est nul
                $sexe = ($_POST['Id_Competition_Sexe'] == null) ? null : $_POST['Id_Competition_Sexe'];
                $typeAffrontement = ($_POST['Id_Competition_Typ_Affrontement'] == null) ? null : $_POST['Id_Competition_Typ_Affrontement'];

                // Déclaration repository
                $objetCompetitionRep = new CompetitionRepository();

                // Déclaration du tableau des valeurs
                $valeurCompetition = array(
                    "Nom_Competition" => $_POST['Nom_Competition'],
                    "Date_Debut_Competition" => $_POST['Date_Debut_Competition'],
                    "Date_Fin_Competition" => $_POST['Date_Fin_Competition'],
                    "Adresse_Competition" => $_POST['Adresse_Competition'],
                    "Cp_Competition" => $_POST['Cp_Competition'],
                    "Ville_Competition" => $_POST['Ville_Competition'],
                    "Date_Limite_Inscript_Competition" => $_POST['Date_Limite_Inscript_Competition'],
                    "Nb_Round" => $_POST['Nb_Round'],
                    "Duree_Round" => $secondes,
                    "Nb_Ring" => $_POST['Nb_Ring'],
                    "Id_Competition_Sexe" => $sexe,
                    "Id_Competition_Typ_Affrontement" => $typeAffrontement,
                    "Id_Competition_Struct" => $_POST['Id_Competition_Struct'],
                    "Inactif_Competition" => 0
                );
                
                // Déclaration entity
                $competition = new Competition($valeurCompetition);

                // Utilisation de sauver()
                $objetCompetitionRep->sauver($competition);
                
                // Séléction de l'ID compétition
                $objetCompetition = $objetCompetitionRep->selectionCompetitionNom($_POST['Nom_Competition']);
                $idCompet = $objetCompetition->getId_Competition();       
                
                
                // SE FAIRE
                $objetSeFaireRep = new SeFaireRepository();
                
                foreach ($_POST['categorieAgeCompetition'] as $value)
                {
                    $valeurSeFaire = array(
                    "Id_SeFaire_Competition" => $idCompet,
                    "Id_SeFaire_Cat_Age" => $value
                    );
                    
                    $objetSeFaire = new SeFaire($valeurSeFaire);
                    
                    $objetSeFaireRep->sauver($objetSeFaire);
                }
               



                
                ?>
                <script language="Javascript"> 
                    document.location.replace("creationCompetition.php?connect=1&confirmationCreation=1");
                </script>
                <?php                
            }
            ?>
            <br/>
        </section>
    </body>
</html>