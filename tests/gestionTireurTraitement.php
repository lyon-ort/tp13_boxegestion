<?php

require '../config/config.php';

// Déclaration TireurRepository
$tireurRepository = new TireurRepository();

echo $_POST['Id_Tireur'];

// Déclaration du tableau des valeurs
$valeurTireur = array(
    "Id_Tireur" => $_POST['Id_Tireur'],
    "Id_Tir_Niv_Tireur" => $_POST['Id_Tir_Niv_Tireur'],
    "Nom_Tireur" => $_POST['Nom_Tireur'],
    "Prenom_Tireur" => $_POST['Prenom_Tireur'],
    "Date_Naissance" => $_POST['Date_Naissance'],
    "Num_Licence" => $_POST['Num_Licence'],
    "Poids_Tireur" => $_POST['Poids_Tireur'],
    "Id_Tir_Cat_Poids" => $_POST['Id_Tir_Cat_Poids'],
    "Id_Tir_Cat_Age" => $_POST['Id_Tir_Cat_Age'],
    "Id_Tir_Sexe" => $_POST['Id_Tir_Sexe'],
    "Id_Tir_Struct" => $_POST['Id_Tir_Struct']
);

// Initialisation et dump() des valeurs
$tireur = new Tireur($valeurTireur);
var_dump($tireur);

// Fonction sauver pour créer ou modifier un tireur
$tireurRepository->sauver($tireur);

// Renvoie à la page d'affichage
header('location: gestionTireurAffichage.php');
