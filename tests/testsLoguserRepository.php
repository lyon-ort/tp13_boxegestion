<?php

require '../config/config.php';

// Déclaration LoguserRepository
$loguserRepository = new LoguserRepository();

// Déclaration du tableau des valeurs
$valeurLoguser = array(
    'Login' => 'test',
    'Date_Log' => '2023-12-02 13:04:00',
    'Message' => 'Test',
);

// Initialisation et dump() des valeurs
$loguser = new Loguser($valeurLoguser);
var_dump($loguser);

// Fonction sauver pour créer ou modifier une ligne dans la table de log
$loguserRepository->sauver($loguser);
