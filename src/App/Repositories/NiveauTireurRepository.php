<?php

require '../src/App/Entities/NiveauTireur.php';

class NiveauTireurRepository {

    protected $table;     
    protected $classMapped; 
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire)
    public function __construct() {
        $this->table = 'NiveauTireur';
        $this->classMapped = 'App\Entities\NiveauTireur';
        $this->idFieldName = 'Id_Niv_Tir';
    }
    
    
    
    // Sélectionne tous les niveaux de tireurs
    public function selectionNiveauTireur()
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objets à NULL si jamais elle la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->query("SELECT * FROM $this->table");
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;
    }       
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // Séléctionne tous les niveaux de tireurs
    public function selectionNivTir()
    {
        $db = dbConnect();
        $req = $db->query("SELECT * FROM $this->table");
        $i = 0;
        
        while ($data = $req->fetch())
        {
            $tableauNivTir[$i] = array(
                'Id_Niv_Tir' => $data['Id_Niv_Tir'],
                'Libelle_Niv_Tir' => $data['Libelle_Niv_Tir']
            );
            $i++;
        }
        
        return $tableauNivTir;
    }
}