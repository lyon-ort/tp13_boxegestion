<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';
$leGet = '?connect=1';

// Modification d'utilisateur
if (isset($_POST['Id_UtilisateurModif']))
{
    // Test si le nom existe déjà
    $objetUtilisateurRep2 = new UtilisateurRepository();
    $arrayObjetUtilisateur2 = $objetUtilisateurRep2->selectionUtilisateur();
                
    $test = true;
                
    foreach ($arrayObjetUtilisateur2 as $value)
    {
        if ($value->getLogin_Utilisateur() != $_POST['LoginDeBase'])
        {
            if ($value->getLogin_Utilisateur() == $_POST['Login_UtilisateurModif'])
            {
                $test = false;
            }
        }
    }
    
    if ($test)
    {
        // Si il y a une date de dernière connexion
        $dateConnexion = ($_POST['Dernier_Login_UtilisateurModif'] == null) ? null : $_POST['Dernier_Login_UtilisateurModif'];

        // Déclaration du tableau des valeurs
        $valeurUtilisateur = array(
            "Id_Utilisateur" => $_POST['Id_UtilisateurModif'],
            "Id_Typ_Role_Utilisateur" => $_POST['Id_Typ_Role_UtilisateurModif'],
            "Id_Struct_Utilisateur" => $_POST['Id_Struct_UtilisateurModif'],
            "Nom_Utilisateur" => $_POST['Nom_UtilisateurModif'],
            "Prenom_Utilisateur" => $_POST['Prenom_UtilisateurModif'],
            "Login_Utilisateur" => $_POST['Login_UtilisateurModif'],
            "Email" => $_POST['EmailModif'],
            "Mdp" => $_POST['MdpModif'],
            "Tel_Port" => $_POST['Tel_PortModif'],
            "Date_Creation_Compte" => $_POST['Date_Creation_CompteModif'],
            "Dernier_Login_Utilisateur" => $dateConnexion,
            "Inactif_Utilisateur" => 0
        );

        $utilisateur = new Utilisateur($valeurUtilisateur);
        $objetUtilisateurRep2->sauver($utilisateur);
        
        // Rafraichissement de la page
        header('location: modifSuprUtil.php?connect=1&message=1');        
    }
    else
    {
        ?>
            <!-- Renvoie vers la page de modif -->
            <form name="form" action="modifUtil.php?connect=1" method="POST"> 
                <input type="hidden" name="Id_Utilisateur" value="<?= $_POST['Id_UtilisateurModif'] ?>"/>
                <input type="hidden" name="Id_Typ_Role_Utilisateur" value="<?= $_POST['Id_Typ_Role_UtilisateurModif'] ?>"/>
                <input type="hidden" name="LoginDeBase" value="<?= $_POST['LoginDeBase'] ?>"/>
                <input type="hidden" name="Id_Struct_Utilisateur" value="<?= $_POST['Id_Struct_UtilisateurModif'] ?>"/>
                <input type="hidden" name="Nom_Utilisateur" value="<?= $_POST['Nom_UtilisateurModif'] ?>"/>
                <input type="hidden" name="Prenom_Utilisateur" value="<?= $_POST['Prenom_UtilisateurModif'] ?>"/>
                <input type="hidden" name="Login_Utilisateur" value="<?= $_POST['Login_UtilisateurModif'] ?>"/>
                <input type="hidden" name="Email" value="<?= $_POST['EmailModif'] ?>"/>
                <input type="hidden" name="Mdp" value="<?= $_POST['MdpModif'] ?>"/>
                <input type="hidden" name="Tel_Port" value="<?= $_POST['Tel_PortModif'] ?>"/>
                <input type="hidden" name="Date_Creation_Compte" value="<?= $_POST['Date_Creation_CompteModif'] ?>"/>
                <input type="hidden" name="Dernier_Login_Utilisateur" value="<?= $_POST['Dernier_Login_UtilisateurModif'] ?>"/>
                <input type="hidden" name="error" />
            </form>

            <!-- Script qui renvoie les variables POST vers la page de modification -->
            <script type="text/javascript"> 
                document.form.submit(); //on envoie le formulaire vers la page de modification
            </script> 
        <?php        
    }
}


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Modifier / Supprimer utilisateur </title>
    </head>
    
    <body>
        <!-- Header -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>
        
            <?php
            // Suppression d'utilisateur
            if (isset($_POST['Id_UtilisateurSupr']))
            {
                // UTILISATEUR
                $objetUtilisateurRep2 = new UtilisateurRepository();
                $objetUtilisateur2 = $objetUtilisateurRep2->selectionUtilisateurId($_POST['Id_UtilisateurSupr']);

                // Par rapport au script
                $inactif = $objetUtilisateur2->getInactif_Utilisateur();

                if (!$inactif)
                {
                    // Modifie les "Flag" et supprime les utilisateurs de la structure
                    $objetUtilisateur2->setInactif_Utilisateur(1);
                    $objetUtilisateurRep2->sauver($objetUtilisateur2);
                ?>
                
                    <!-- Pop up de confirmation de suppression -->
                    <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                        <div class="modal-content" style="height: 250px;">
                            <!-- bouton fermer -->
                            <div id="close" class="closeTest">+</div>

                            <!-- Confirmation de suppression -->
                            <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                            L'utilisateur a été supprimé !<br/><br/>

                            <button class="boutonAnnulerTest"> OK </button>
                        </div>
                    </div>   

                    <script>
                        // La croix (ferme le pop-up)
                        document.querySelector('.closeTest').addEventListener('click', function() 
                        {
                            document.querySelector('.bg-modalTest').style.display = 'none';
                        });      

                        // Le bouton annuler (ferme le pop-up)
                        document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                        {
                            document.querySelector('.bg-modalTest').style.display = 'none';
                        });       
                    </script>            
                <?php  
                }
            }            
            
            // Message de confirmation de modification
            if (isset($_GET['message']) && $_GET['message'] == 1)
            {
                ?>
                    <!-- Pop up de confirmation de suppression -->
                    <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                        <div class="modal-content" style="height: 250px;">
                            <!-- bouton fermer -->
                            <div id="close" class="closeTest">+</div>

                            <!-- Confirmation de suppression -->
                            <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                            L'utilisateur a été modifié !<br/><br/>

                            <button class="boutonAnnulerTest"> OK </button>
                        </div>
                    </div>   

                    <script>
                        // La croix (ferme le pop-up)
                        document.querySelector('.closeTest').addEventListener('click', function() 
                        {
                            document.querySelector('.bg-modalTest').style.display = 'none';
                        });      

                        // Le bouton annuler (ferme le pop-up)
                        document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                        {
                            document.querySelector('.bg-modalTest').style.display = 'none';
                        });       
                    </script>            
                <?php  
            }
            ?>
        
        <section class='container-fluid about'>

            <?php
            if (isset($_GET['connect']))
            {
            ?>
                <div class="information">
                    Login : <?= $_SESSION['login'] ?> <br/>
                    Prénom : <?= $_SESSION['Prenom_Utilisateur'] ?> <br/>
                    Nom : <?= $_SESSION['Nom_Utilisateur'] ?> <br/>
                    Rôle :
                    <?php
                    $objetTypeRoleRepUtil = new TypeRoleRepository(); 
                    $typeRole = $objetTypeRoleRepUtil->selectionTypeRoleId($_SESSION['idRole']);
                    echo $typeRole->getLibelle_Typ_Role();
                    ?>                                 
                    <br/>
                    Structure :
                    <?php
                    $objetStructureRepUtil = new StructureRepository(); 
                    $structure = $objetStructureRepUtil->selectionStructureId($_SESSION['idStruct']);
                    echo $structure->getNom_Struct();
                    ?> <br/><br/><br/>
                    <button class='btn btn-custom' onclick="Deconnexion()"> Déconnexion </button>
                </div>   
            <?php
            }
            ?>            
            
            <h1> Liste utilisateurs </h1>
            
            <hr class="separator">            
            

            
            <table>
                
                <th> Prénom - Nom </th> 
                <th> Login </th> 
                <th> Rôle </th> 
                <th> Structure </th> 
                <th> Modifier </th> 
                <th> Supprimer </th>
                
                <?php
                
                $objetUtilisateurRep = new UtilisateurRepository();
                $arrayObjetUtilisateur = $objetUtilisateurRep->selectionUtilisateur();
                
                $i = 0;
                
                foreach ($arrayObjetUtilisateur as $value)
                {
                    ?>
                        <tr>
                            <td>
                                <?= $value->getPrenom_Utilisateur() . ' ' . $value->getNom_Utilisateur() ?>
                            </td>
                            
                            <td>
                                <?= $value->getLogin_Utilisateur() ?>
                            </td>
                            
                            <td>
                                <?php
                                    $objetTypeRoleRep = new TypeRoleRepository();
                                    $objetTypeRole = $objetTypeRoleRep->selectionTypeRoleId($value->getId_Typ_Role_Utilisateur());
                                    echo $objetTypeRole->getLibelle_Typ_Role();
                                ?>
                                
                            </td>

                            <td>
                                <?php
                                    $objetStructureRep = new StructureRepository();
                                    $objetStructure = $objetStructureRep->selectionStructureId($value->getId_Struct_Utilisateur());
                                    echo $objetStructure->getNom_Struct();
                                ?>
                            </td>

                            <td>
                                <form method="POST" action="modifUtil<?= $leGet ?>.php">
                                    <input type="hidden" name="Id_Utilisateur" value="<?= $value->getId_Utilisateur() ?>"/>
                                    <input id="boutonModifierBleu" type="submit" value="Modifier"/>
                                </form>
                            </td>

                            <td>
                                <?php
                                    // Désactive le bouton "supprimer" si l'utilisateur est responsable de structure
                                    $disabled = '';
                                    $message = '';
                                    
                                    if ($value->getId_Utilisateur() == $objetStructure->getId_Responsable_Struct())
                                    {
                                        $classBoutonSupprimer = 'classBoutonSupprimerGris';
                                        ?>
                                        <h3> <button class="<?= $classBoutonSupprimer ?>" onMouseOver="displayDivInfo('<blockquote>Cette utilisateur est responsable de structure</blockquote>');" onMouseOut="displayDivInfo()"> X </button> </h3>
                                        <?php
                                    }
                                    else
                                    {
                                        $classBoutonSupprimer = 'classBoutonSupprimerRouge';
                                        ?>
                                        <h3> <button class="<?= $classBoutonSupprimer ?>" id="boutonSupprimer<?= $i ?>"> X </button> </h3>
                                        <?php                                        
                                    }
                                ?>
                            </td>                            
                        </tr>
                        
                                <!-- Pop up de suppression -->
                                <div id="bg-modal" class="bg-modal<?= $i ?>">
                                    <div class="modal-content">
                                        <!-- bouton fermer -->
                                        <div id="close" class="close<?= $i ?>">+</div>

                                        <!-- Confirmation de suppression -->
                                        <img width="50px" height="50px" src="../public/img/pictogramBoxe.png" alt=""/><br/>

                                        Voulez-vous vraiment supprimer cette utilisateur ?<br/><br/>
                                            
                                        <form name="form" action="modifSuprUtil.php?connect=1" method="POST"> 
                                            <input type="hidden" name="Id_UtilisateurSupr" value="<?= $value->getId_Utilisateur() ?>"/>
                                            <button> Supprimer </button> <br/>
                                        </form>
                                            
                                        <button class="boutonAnnuler<?= $i ?>"> Annuler </button>
                                    </div>
                                </div>                        
                        
                                <!-- JS -->
                                <script type="text/javascript">                 

                                    // Apparition du pop-up
                                    document.getElementById('boutonSupprimer<?= $i ?>').addEventListener('click', function() 
                                    {
                                        document.querySelector('.bg-modal<?= $i ?>').style.display = 'flex';
                                    });                    

                                    // La croix (ferme le pop-up)
                                    document.querySelector('.close<?= $i ?>').addEventListener('click', function() 
                                    {
                                        document.querySelector('.bg-modal<?= $i ?>').style.display = 'none';
                                    });      

                                    // Le bouton annuler (ferme le pop-up)
                                    document.querySelector('.boutonAnnuler<?= $i ?>').addEventListener('click', function() 
                                    {
                                        document.querySelector('.bg-modal<?= $i ?>').style.display = 'none';
                                    }); 
                                    
                                    // Fonction qui affiche du texte au survol du bouton désactiver
                                    function displayDivInfo(text)
                                    {
                                        if(text)
                                        {
                                            //Détection du navigateur
                                            is_ie = (navigator.userAgent.toLowerCase().indexOf("msie") != -1) && (navigator.userAgent.toLowerCase().indexOf("opera") == -1);

                                            //Création d'une div provisoire
                                            var divInfo = document.createElement('div');
                                            divInfo.style.position = 'fixed';
                                            document.onmousemove = function(e)
                                            {
                                                x = (!is_ie ? e.pageX-window.pageXOffset : event.x+document.body.scrollLeft);
                                                y = (!is_ie ? e.pageY-window.pageYOffset : event.y+document.body.scrollTop);
                                                divInfo.style.left = x+15+'px';
                                                divInfo.style.top = y+15+'px';
                                            }
                                            divInfo.id = 'divInfo';
                                            divInfo.innerHTML = text;
                                            document.body.appendChild(divInfo);
                                        }
                                        else
                                        {
                                            document.onmousemove = '';
                                            document.body.removeChild(document.getElementById('divInfo'));
                                        }
                                    }                                    
                                    
                                </script>                        
                    <?php
                    $i++;
                }
                ?>
            </table>
            <br/>
        </section>
    </body>
</html>