<?php

require '../src/App/Entities/Utilisateur.php';

class UtilisateurRepository {

    protected $table;
    protected $classMapped;
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire)
    public function __construct() {
        $this->table = 'Utilisateur';
        $this->classMapped = 'App\Entities\Utilisateur';
        $this->idFieldName = 'Id_Utilisateur';
    }

    
    // Ajouter / Modifier un élément
    public function sauver(Utilisateur $entity) {
        $db = dbConnect();
        $resultSet = NULL;
        if ($entity != NULL) {
            $bindParam = $entity->__toArray();
            if ($entity->getId_Utilisateur() == NULL) {
                // Nouvelle entité
                $query = "INSERT INTO $this->table" .
                        " (Id_Typ_Role_Utilisateur, "
                        . "Id_Struct_Utilisateur, "
                        . "Nom_Utilisateur, "
                        . "Prenom_Utilisateur, "
                        . "Login_Utilisateur, "
                        . "Email, "
                        . "Mdp, "
                        . "Tel_Port, "
                        . "Date_Creation_Compte, "
                        . "Dernier_Login_Utilisateur, "
                        . "Inactif_Utilisateur)"
                        . " VALUES (:Id_Typ_Role_Utilisateur, "
                        . ":Id_Struct_Utilisateur, "
                        . ":Nom_Utilisateur, "
                        . ":Prenom_Utilisateur, "
                        . ":Login_Utilisateur, "
                        . ":Email, "
                        . ":Mdp, "
                        . ":Tel_Port, "
                        . ":Date_Creation_Compte, "
                        . ":Dernier_Login_Utilisateur, "
                        . ":Inactif_Utilisateur)";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Id_Typ_Role_Utilisateur', $bindParam['Id_Typ_Role_Utilisateur'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Struct_Utilisateur', $bindParam['Id_Struct_Utilisateur'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Nom_Utilisateur', $bindParam['Nom_Utilisateur'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Prenom_Utilisateur', $bindParam['Prenom_Utilisateur'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Login_Utilisateur', $bindParam['Login_Utilisateur'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Email', $bindParam['Email'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Mdp', $bindParam['Mdp'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Tel_Port', $bindParam['Tel_Port'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Date_Creation_Compte', $bindParam['Date_Creation_Compte'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Dernier_Login_Utilisateur', $bindParam['Dernier_Login_Utilisateur'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Inactif_Utilisateur', $bindParam['Inactif_Utilisateur'], \PDO::PARAM_STR);
                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $entity->setid($db->lastInsertId());
                    $resultSet = $entity;
                }
            } else {
                //  Entité existante
                $query = "UPDATE $this->table"
                        . " SET Id_Typ_Role_Utilisateur = :Id_Typ_Role_Utilisateur, "
                        . " Id_Struct_Utilisateur = :Id_Struct_Utilisateur, "
                        . " Nom_Utilisateur = :Nom_Utilisateur, "
                        . " Prenom_Utilisateur = :Prenom_Utilisateur, "
                        . " Login_Utilisateur = :Login_Utilisateur, "
                        . " Email = :Email, "
                        . " Mdp = :Mdp, "
                        . " Tel_Port = :Tel_Port, "
                        . " Date_Creation_Compte = :Date_Creation_Compte, "
                        . " Dernier_Login_Utilisateur = :Dernier_Login_Utilisateur, "
                        . " Inactif_Utilisateur = :Inactif_Utilisateur"
                        . " WHERE $this->idFieldName = :Id_Utilisateur";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Id_Typ_Role_Utilisateur', $bindParam['Id_Typ_Role_Utilisateur'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Struct_Utilisateur', $bindParam['Id_Struct_Utilisateur'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Nom_Utilisateur', $bindParam['Nom_Utilisateur'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Prenom_Utilisateur', $bindParam['Prenom_Utilisateur'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Login_Utilisateur', $bindParam['Login_Utilisateur'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Email', $bindParam['Email'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Mdp', $bindParam['Mdp'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Tel_Port', $bindParam['Tel_Port'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Date_Creation_Compte', $bindParam['Date_Creation_Compte'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Dernier_Login_Utilisateur', $bindParam['Dernier_Login_Utilisateur'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Inactif_Utilisateur', $bindParam['Inactif_Utilisateur'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Utilisateur', $bindParam[$this->idFieldName], \PDO::PARAM_INT);

                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $resultSet = $entity;
                }
            }
        }
        return $resultSet;
    }

    
    
    // Fonction de connexion qui reçoit $_POST['login'] et $_POST['password']
    public function authentification() 
    {
        // Connexion à la Base de données
        $db = dbConnect();
		echo "test..<BR/>";
        
        // Récupération du formulaire de connexion
        $login = isset($_POST['login']) ? $_POST['login'] : '';
        $password = isset($_POST['password']) ? $_POST['password'] : '';
		
		// Récupère le login, le mdp, le rôle et la structure de l'utilisateur
					session_start();
                    $_SESSION['login'] = $login;
                    $_SESSION['password'] = $password;
                    $_SESSION['idRole'] = 2;
                    $_SESSION['idStruct'] = 2;
                    $_SESSION['Prenom_Utilisateur'] = "amaPrenom";
                    $_SESSION['Nom_Utilisateur'] = "amaNom";
                    $_SESSION['logged'] = true;
/*         
        // Selection des login et mdp 
        $req = $db->query("SELECT Login_Utilisateur, Mdp FROM $this->table WHERE Inactif_Utilisateur = 0");
        
        $i = 0;
        while ($donnees = $req->fetch()) 
        {
            $loginBdd[$i] = $donnees['Login_Utilisateur'];
            $mdpBdd[$i] = $donnees['Mdp'];
            $i++;
        }
        
        // Si le login et le mdp sont valide, on active l'authentification
		// $i est nb enreg
        for ($y = 0; $y != $i; $y++) 
        {
			echo "test..<BR>";
                // Vérifie la connexion
				if($login != $loginBdd[$y])
				{}
                else{
				if (password_verify($password, $mdpBdd[$y])) 
                {
                    session_start();
                    
                    // Requête de séléction de l'utilisateur
                    $req = $db->prepare("SELECT * FROM $this->table WHERE Login_Utilisateur = :loginMark");
                    $req->execute(array('loginMark' => $login));

                    // Initialisation de l'objet à NULL si jamais la requête n'est pas bonne
                    $objet = NULL;
                    
                    // Si la requête est bonne, alors on fait un tableau d'objet
                    if ($req) 
                    {
                        while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
                        {
                            $objet = new $this->table($row);
                        }
                    }
                    
                    // Sauvegarde la dernière date de connexion
                    $reqConnexion = $db->prepare("UPDATE $this->table SET Dernier_Login_Utilisateur = NOW() WHERE Id_Utilisateur = :idMark");
                    $reqConnexion->execute(array('idMark' => $objet->getId_Utilisateur()));
                    
                    // Récupère le login, le mdp, le rôle et la structure de l'utilisateur
                    $_SESSION['login'] = $login;
                    $_SESSION['password'] = $password;
                    $_SESSION['idRole'] = $objet->getId_Typ_Role_Utilisateur();
                    $_SESSION['idStruct'] = $objet->getId_Struct_Utilisateur();
                    $_SESSION['Prenom_Utilisateur'] = $objet->getPrenom_Utilisateur();
                    $_SESSION['Nom_Utilisateur'] = $objet->getNom_Utilisateur();
                    $_SESSION['logged'] = true;
					$y = $i ; // trouvé, onsort de la boucle 
                } // if mdp ok
				} // else <=> login trouvé 
        }
		*/ 
    }
    
    
    
    // Sélectionne touts les types de roles
    public function selectionUtilisateur()
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objets à NULL si jamais elle la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->query("SELECT * FROM $this->table WHERE Inactif_Utilisateur = 0");
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;
    }     
    
    
    
    // Sélectionne un utilisateur selon un login
    public function selectionUtilisateurLogin($login)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $objet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE Login_Utilisateur = :loginMark");
        $req->execute(array('loginMark' => $login));
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$objet = new $this->table($row);
	    }
        }
        
        // Return de l'objet
        return $objet;
    }    
    
    
    
    // Sélectionne un utilisateur selon un id
    public function selectionUtilisateurId($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $objet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE $this->idFieldName = :idMark");
        $req->execute(array('idMark' => $id));
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$objet = new $this->table($row);
	    }
        }
        
        // Return de l'objet
        return $objet;
    }    
    
    
    
    // Sélectionne des utilisateur selon un ID de structure
    public function selectionArrayUtilisateurIdStructure($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Struct_Utilisateur = :idMark AND Inactif_Utilisateur = 0");
        $req->execute(array('idMark' => $id));
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return de l'objet
        return $arrayObjet;
    }    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
    
    
    // Séléction d'un idRole selon un login
    public function selectionIdRole($login)
    {
        $db = dbConnect();
        
        // Sélection de l'Id_Type_Role
        $req2 = $db->prepare("SELECT Id_Typ_Role_Utilisateur FROM $this->table WHERE Login_Utilisateur = :loginMark");
        $req2->execute(array('loginMark' => $login));
        
        while ($id = $req2->fetch())
        {
            $idRole = $id['Id_Typ_Role_Utilisateur'];
        }
        
        return $idRole;
    }
    
    
    // Séléction d'un idStruct selon un login
    public function selectionIdStruct($login)
    {
        $db = dbConnect();
        
        // Sélection de l'Id_Type_Role
        $req2 = $db->prepare("SELECT Id_Struct_Utilisateur FROM $this->table WHERE Login_Utilisateur = :loginMark");
        $req2->execute(array('loginMark' => $login));
        
        while ($id = $req2->fetch())
        {
            $idStruct = $id['Id_Struct_Utilisateur'];
        }
        
        return $idStruct;
    }
    
    
    // Selectionne tous les champs de toutes les compétitions
    public function selectionUtil()
    {
        $db = dbConnect();
        $req = $db->query("SELECT * FROM $this->table");
        
        return $req;
    }


    // Suppression d'utilisateur
    public function suppressionUtilisateur($idSupr)
    {
        $db = dbConnect();
        
        $req = $db->prepare("DELETE FROM $this->table WHERE $this->idFieldName = :idSuprMark");
        $req->execute(array('idSuprMark' => $idSupr));
        
        header('location: modifSuprUtil.php');
    }
    
    
    // Dernière connexion
    public function insertConnexion($login)
    {
        $db = dbConnect();
        
        $req = $db->prepare("UPDATE $this->table SET Dernier_Login_Utilisateur = NOW() WHERE Login_Utilisateur = :loginMark");
        $req->execute(array('loginMark' => $login));
                       
    }
    
    
    // Séléction de l'id selon le login
    public function selectionId($loginUtilisateur)
    {
        $db = dbConnect();
        
        $req = $db->prepare("SELECT * FROM $this->table WHERE Login_Utilisateur = :loginUtilisateurMark");
        $req->execute(array('loginUtilisateurMark' => $loginUtilisateur));
        
        $data = $req->fetch();
        
        return $data['Id_Utilisateur'];
    }
    
    
    // Update de l'ID de structure de l'utilisateur
    public function ajoutIdStruct($idUtil, $idStruct)
    {
        $db = dbConnect();
        
        $req = $db->prepare("UPDATE $this->table SET Id_Struct_Utilisateur = :idStructMark WHERE $this->idFieldName = :idUtilMark");
        $req->execute(array('idStructMark' => $idStruct, 'idUtilMark' => $idUtil));
    }
    
    
    // Sélectionne un utilsateur selon son id
    public function selectionUtilId($tableauIdUtil)
    {
        $db = dbConnect();
        
        for ($i = 0; $i != count($tableauIdUtil); $i++)
        {
            $req = $db->prepare("SELECT * FROM $this->table WHERE $this->idFieldName = :idMark");
            $req->execute(array('idMark' => $tableauIdUtil[$i]));
        }
        
        $data = $req->fetchAll();

        return $data;           
    }
    
    
    // Suppression des utilisateurs sans structure
    public function suprUtilNull()
    {
        $db = dbConnect();
        
        $req = $db->exec("DELETE FROM Utilisateur WHERE ISNULL(Id_Struct_Utilisateur) = 1");     
    }
    
    
    // Sélectionne "Login_Utilisateur" selon l'id transmis
    public function selectNomPrenomResp($idResp)
    {
        $db = dbConnect();
        $req = $db->prepare("SELECT Login_Utilisateur FROM $this->table WHERE Id_Utilisateur = :idRespMark");
        $req->execute(array('idRespMark' => $idResp));
        
        $data = $req->fetch();
       
        return $data['Login_Utilisateur'];
    }
            
}
