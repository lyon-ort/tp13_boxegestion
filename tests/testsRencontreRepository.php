<?php

require '../config/config.php';

// Déclaration RencontreRepository
$rencontreRepository = new RencontreRepository();

// Déclaration du tableau des valeurs
$valeurRencontre = array(
    "Date_Rencontre" => "2019-08-08",
    "Heure_Debut_Rencontre" => "2020-02-27 03:10:12",
    "Heure_Fin_Rencontre" => "2020-02-27 03:10:12",
    "Num_Ring_Rencontre" => 8,
    "Id_Tireur_Rouge" => 1,
    "Id_Tireur_Bleu" => 2,
    "Id_Gagnant" => 1,
    "Id_Rencontre_Precedent_1" => null,
    "Id_Rencontre_Precedent_2" => null,
    "Id_Rencontre_Suivante" => null,
    "Id_Cat_Rencontre" => 1,
    "Id_Tab_Rencontre" => 1,
    "Id_Etat_Rencontre" => 1
);

// Initialisation et dump() des valeurs
$rencontre = new Rencontre($valeurRencontre);
var_dump($rencontre);

// Fonction sauver pour créer ou modifier une rencontre
$rencontreRepository->sauver($rencontre);

