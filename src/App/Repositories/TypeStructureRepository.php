<?php

require '../src/App/Entities/TypeStructure.php';

class TypeStructureRepository {

    protected $table;     
    protected $classMapped; 
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire)
    public function __construct() {
        $this->table = 'TypeStructure';
        $this->classMapped = 'App\Entities\TypeStructure';
        $this->idFieldName = 'Id_Typ_Struct';
    }
    
    
    
    // Sélectionne touts les types de structures
    public function selectionTypeStructure()
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objets à NULL si jamais elle la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->query("SELECT * FROM $this->table");
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;
    }       
    
    
    
    // Sélectionne un type de structure selon un id
    public function selectionTypeStructureId($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $objet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE $this->idFieldName = :idMark");
        $req->execute(array('idMark' => $id));
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$objet = new $this->table($row);
	    }
        }
        
        // Return de l'objet
        return $objet;
    }    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // Sélectionne tous les types de structures
    public function selectionTypeStructure2()
    {
        $db = dbConnect();
        $req = $db->query("SELECT * FROM $this->table");
        
        return $req;
    }
    
    /*
    // Séléctionne tous les types de structure
    public function selectionTypeStructure()
    {
        $db = dbConnect();
        $req = $db->query("SELECT * FROM $this->table");
        $i = 0;
        
        while ($data = $req->fetch())
        {
            $tableauTypeStruct[$i] = array(
                'Id_Typ_Struct' => $data['Id_Typ_Struct'],
                'Libelle_Typ_Struct' => $data['Libelle_Typ_Struct']
            );
            $i++;
        }
        
        return $tableauTypeStruct;
    }*/
    
    
    // Sélectionne "Libelle_Typ_Struct" selon l'id transmis
    public function selectTypeStructId($idTypeStruct)
    {
        $db = dbConnect();
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Typ_Struct = :idTypeStructMark");
        $req->execute(array('idTypeStructMark' => $idTypeStruct));
        
        $data = $req->fetch();
        
        return $data['Libelle_Typ_Struct'];
    }
}