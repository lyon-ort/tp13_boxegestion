<?php

require '../src/App/Entities/Tireur.php';

class TireurRepository {

    protected $table;
    protected $classMapped;
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire) 
    public function __construct() {
        $this->table = 'Tireur';
        $this->classMapped = 'Tireur';
        $this->idFieldName = 'Id_Tireur';
    }

    
    // Ajouter / Modifier un tireur
    public function sauver(Tireur $entity) {
        $db = dbConnect();
        $resultSet = NULL;
        if ($entity != NULL) {
            $bindParam = $entity->__toArray();
            if ($entity->getId_Tireur() == NULL) {
                // Nouvelle entité
                $query = "INSERT INTO $this->table" .
                        " (Id_Tir_Niv_Tireur, "
                        . "Nom_Tireur, "
                        . "Prenom_Tireur, "
                        . "Date_Naissance, "
                        . "Num_Licence, "
                        . "Poids_Tireur, "
                        . "Id_Tir_Cat_Poids, "
                        . "Id_Tir_Cat_Age, "
                        . "Id_Tir_Sexe, "
                        . "Id_Tir_Struct, "
                        . "Inactif_Tireur)"
                        . " VALUES "
                        . "(:Id_Tir_Niv_Tireur, "
                        . ":Nom_Tireur, "
                        . ":Prenom_Tireur, "
                        . ":Date_Naissance, "
                        . ":Num_Licence, "
                        . ":Poids_Tireur, "
                        . ":Id_Tir_Cat_Poids, "
                        . ":Id_Tir_Cat_Age, "
                        . ":Id_Tir_Sexe, "
                        . ":Id_Tir_Struct, "
                        . ":Inactif_Tireur)";
                

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Id_Tir_Niv_Tireur', $bindParam['Id_Tir_Niv_Tireur'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Nom_Tireur', $bindParam['Nom_Tireur'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Prenom_Tireur', $bindParam['Prenom_Tireur'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Date_Naissance', $bindParam['Date_Naissance'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Num_Licence', $bindParam['Num_Licence'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Poids_Tireur', $bindParam['Poids_Tireur'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tir_Cat_Poids', $bindParam['Id_Tir_Cat_Poids'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tir_Cat_Age', $bindParam['Id_Tir_Cat_Age'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tir_Sexe', $bindParam['Id_Tir_Sexe'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tir_Struct', $bindParam['Id_Tir_Struct'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Inactif_Tireur', $bindParam['Inactif_Tireur'], \PDO::PARAM_INT);
                $reqPrep->execute();
 
                if ($reqPrep != FALSE) {
                    $entity->setid($db->lastInsertId());
                    $resultSet = $entity;
                }
            } else {
                //  Entité existante
                $query = "UPDATE $this->table"
                        . " SET Id_Tir_Niv_Tireur = :Id_Tir_Niv_Tireur, "
                        . " Nom_Tireur = :Nom_Tireur, "
                        . " Prenom_Tireur = :Prenom_Tireur, "
                        . " Date_Naissance = :Date_Naissance, "
                        . " Num_Licence = :Num_Licence, "
                        . " Poids_Tireur = :Poids_Tireur, "
                        . " Id_Tir_Cat_Poids = :Id_Tir_Cat_Poids, "
                        . " Id_Tir_Cat_Age = :Id_Tir_Cat_Age, "
                        . " Id_Tir_Sexe = :Id_Tir_Sexe, "
                        . " Id_Tir_Struct = :Id_Tir_Struct,"
                        . " Inactif_Tireur = :Inactif_Tireur "
                        . " WHERE $this->idFieldName = :Id_Tireur";
                
                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Id_Tir_Niv_Tireur', $bindParam['Id_Tir_Niv_Tireur'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Nom_Tireur', $bindParam['Nom_Tireur'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Prenom_Tireur', $bindParam['Prenom_Tireur'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Date_Naissance', $bindParam['Date_Naissance'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Num_Licence', $bindParam['Num_Licence'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Poids_Tireur', $bindParam['Poids_Tireur'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tir_Cat_Poids', $bindParam['Id_Tir_Cat_Poids'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tir_Cat_Age', $bindParam['Id_Tir_Cat_Age'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tir_Sexe', $bindParam['Id_Tir_Sexe'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tir_Struct', $bindParam['Id_Tir_Struct'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Inactif_Tireur', $bindParam['Inactif_Tireur'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tireur', $bindParam[$this->idFieldName], \PDO::PARAM_INT);

                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $resultSet = $entity;
                }
            }
        }
        return $resultSet;
    }
    
    
    
    // Sélectionne un tireur selon un id 
    public function selectionTireurId($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais la requête n'est pas bonne
        $objet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE $this->idFieldName = :idMark");
        $req->execute(array('idMark' => $id));

        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$objet = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $objet;           
    }
    
    
    
    // Sélectionne des tireurs selon un ID de structure
    public function selectionArrayTireurIdStructure($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Tir_Struct = :idMark AND Inactif_Tireur = 0");
        $req->execute(array('idMark' => $id));
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return de l'objet
        return $arrayObjet;
    }        
    
    
    
    // Sélectionne tous les tireurs
    public function selectionTireur()
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objets à NULL si jamais elle la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->query("SELECT * FROM $this->table WHERE Inactif_Tireur = 0");
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;
    }        
    
    
    
    // Sélectionne des tireurs INACTIF selon un ID de structure
    public function selectionArrayTireurIdStructure_INACTIF($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Tir_Struct = :idMark AND Inactif_Tireur = 1");
        $req->execute(array('idMark' => $id));
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return de l'objet
        return $arrayObjet;
    }         
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
    // Sélectionne tous les tireurs
    function selectionTireur()
    {
        $db = dbConnect();
        $req = $db->query("SELECT * FROM $this->table");
        
        return $req;
    }*/
    
    
    // Séléctionne les tireurs selon une structure
    public function selectionTireurStrucSelect($idStruct) 
    {
        $db = dbConnect();
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Tir_Struct = :idStructMark AND Inactif_Tireur = 0");
        $req->execute(array('idStructMark' => $idStruct));
        
        return $req;
    }
    
    
    // Séléctionne les tireurs selon une structure
    public function selectionTireurStrucSelect2($idStruct) 
    {
        $db = dbConnect();
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Tir_Struct = :idStructMark AND Inactif_Tireur = 1");
        $req->execute(array('idStructMark' => $idStruct));
        
        return $req;
    }
    
    
    // Sélectionne des tireurs selon une certaine structure
    public function selectionTireurClub($idStruct)
    {
        $db = dbConnect();
        $req = $db->prepare("SELECT $this->idFieldName FROM $this->table WHERE Id_Tir_Struct = :idStructMark");
        $req->execute(array('idStructMark' => $idStruct));
        $i = 0;
        
        while ($tableauTireurClub = $req->fetch())
        {
            $tableauReturn[$i] =  $tableauTireurClub[$this->idFieldName];
            $i++;
        }
        
        return $tableauReturn;
    }
    
    
    // Séléctionne des tireurs selon une certaine rencontre
    public function selectionTireurRencontre($idRencontre)
    {
        $db = dbConnect();       
        
        for ($i = 0; $i != count($idRencontre); $i++)
        {
            // Récupération des tireur rouge
            $reqRouge = $db->prepare("SELECT Id_Tireur, Nom_Tireur, Prenom_Tireur FROM $this->table WHERE Id_Tireur = :idTireurRougeMark");
            $reqRouge->execute(array('idTireurRougeMark' => $idRencontre[$i]['Id_Tireur_Rouge']));
            
            // Récupération des tireur rouge
            $reqBleu = $db->prepare("SELECT Id_Tireur, Nom_Tireur, Prenom_Tireur FROM $this->table WHERE Id_Tireur = :idTireurBleuMark");
            $reqBleu->execute(array('idTireurBleuMark' => $idRencontre[$i]['Id_Tireur_Bleu']));   
            
            // Récupération des tireur gagnat
            $reqGagnant = $db->prepare("SELECT Id_Tireur, Nom_Tireur, Prenom_Tireur FROM $this->table WHERE Id_Tireur = :idTireurGagnantMark");
            $reqGagnant->execute(array('idTireurGagnantMark' => $idRencontre[$i]['Id_Gagnant']));              
            
            // Récupération tireur rouge et bleu dans un array()
            $tableauReq[$i] = array('reqRouge' => $reqRouge, 'reqBleu' => $reqBleu, 'reqGagnant' => $reqGagnant);
        }
        
        return $tableauReq;
    }
    
    
    // Séléctionne le nom et prénom des tireurs selon un idTireurParticiper
    public function selectionTireurId_tireur_part($idTireurPart)
    {
        $db = dbConnect();
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Tireur = :idTireurPartMark");
        $req->execute(array('idTireurPartMark' => $idTireurPart));
        
        while($data = $req->fetch())
        {
            $tireur = array('Nom_Tireur' => $data['Nom_Tireur'], 'Prenom_Tireur' => $data['Prenom_Tireur']);
        }
        
        return $tireur;
    }
    
    
    // Supprimer un tireur
    public function suppressionTireur($idSupr)
    {
        $db = dbConnect();
        
        $req = $db->prepare("UPDATE $this->table SET Inactif_Tireur = 1 WHERE Id_Tireur = :idSuprMark");
        $req->execute(array('idSuprMark' => $idSupr));
        
        header('location: modifSuprTireur.php');
    }
    
    
    // Supprimer un tireur
    public function recuperationTireur($idSupr)
    {
        $db = dbConnect();
        
        $req = $db->prepare("UPDATE $this->table SET Inactif_Tireur = 0 WHERE Id_Tireur = :idSuprMark");
        $req->execute(array('idSuprMark' => $idSupr));
        
        header('location: tireurInactif.php');
    }
    
    
    // Sélectionne les tireurs pour une inscription
    public function selectionTireurInscription($idStruct, $idTableau) 
    {
        $db = dbConnect();
        
        // Structure
        // Sélectionne les tireurs selon la structure
        $req = $db->prepare("SELECT * FROM Tireur WHERE Tireur.Id_Tir_Struct = :idStructMark AND Inactif_Tireur = 0");
        $req->execute(array('idStructMark' => $idStruct));
        $tireurStructure = $req->fetchAll();
        

        // Déjà inscrit et sexe
        // Sélectionne les tireurs qui participe au tableau
        $req2 = $db->prepare("SELECT Id_Particip_Tireur FROM Participer WHERE Id_Particip_Tableau = :idTableauMark");
        $req2->execute(array('idTableauMark' => $idTableau[0]['Id_Tab']));
        $tireurDejaInscrit = $req2->fetchAll();
        
        // Complément
        $z = 0;
        $tireurSauver = array();
        
        
        for ($i = 0; $i != count($tireurStructure); $i++)
        {
            $test1 = true;
            $test2 = true;
            
            // Sélectionne les tireurs qui ne sont pas inscrit
            for ($y = 0; $y != count($tireurDejaInscrit); $y++)
            {
                if ($tireurStructure[$i]['Id_Tireur'] == $tireurDejaInscrit[$y]['Id_Particip_Tireur'])
                {
                    $test1 = false;
                }   
            }
            
            // Sélectionne les tireurs avec le bon sexe
            if ($tireurStructure[$i]['Id_Tir_Sexe'] != $idTableau[0]['Id_Tableau_Sexe'])
            {
                $test2 = false;
            }
       
            // Si tous est bon, enregistrer
            if ($test1 AND $test2)
            {
                $tireurSauver[$z] = $tireurStructure[$i]['Id_Tireur'];
                $z++;
            }
        }
        
        // Requête final
        for ($i = 0; $i != count($tireurSauver); $i++)
        {
            $reqFinal[$i] = $db->prepare("SELECT * FROM $this->table WHERE Id_Tireur = :idTireurSauverMark");
            $reqFinal[$i]->execute(array('idTireurSauverMark' => $tireurSauver[$i])); 
            
            $data[$i] = $reqFinal[$i]->fetch();
        }
        
        if (empty($data))
        {
            return null;
        }
        else
        {
            return $data;
        }
    }
    
    /*
    // Sélectionne les nom, prénom des tireurs selon l'id
    public function selectionTireurId($idTireur)
    {
        $db = dbConnect();
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Tireur = :idTireurMark");
        $req->execute(array('idTireurMark' => $idTireur));
        
        $data = $req->fetch();
        
        return $data;
    }    */
}
