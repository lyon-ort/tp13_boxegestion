<?php

class Logbdd {

    // Attributs
    protected $Id_Logbdd;
    protected $Login;
    protected $Date_Modif;
    protected $Nom_Table;
    protected $Type_Operation;
    protected $Id_Element;

    // Constructeur
    public function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas = NULL) {
        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Appelle le mutateur des attributs existant dans le tableau $datas
        foreach ($attrib as $key => $val) {
            // Si, dans vos habitudes, les champs de la bdd sont en minuscules, décommentez la ligne suivante.
            //$key = strtolower($key);
            // Si, dans vos habitudes, les champs de la bdd sont en majuscules, décommentez la ligne suivante.
            //$key = strtoupper($key);
            //  Perso, je met tout en notation camelCase... même dans la BDD.
            if (isset($datas[$key])) {
                $mutateur = 'set' . $key;
                $this->$mutateur($datas[$key]);
            }
        }

        return $this;
    }

    /**
     * Transforme un objet en un tableau associatif.
     * @return array tableau associatif des attributs => valeurs
     */
    public function __toArray() {
        return $this->jsonSerialize();
    }

    /**
     * La classe implémente l'interface JsonSerializable ce qui permet 
     * aux classes concrête de pouvoir être sérialisée en JSON avec json_encode()
     * @return Array le tableau pour conversion en json
     */
    public function jsonSerialize() {
        $array = array();

        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Associe la clé du nom de l'attribut à la valeur de cet attribut
        foreach ($attrib as $key => $val) {
            $array[$key] = $this->get($key);
        }
        return $array;
    }

    /**
     * Lecteur simple d'accés en lecture à un attribut
     * Peut être surchargé.
     * @param type $attribut le nom de l'attribut
     * @return type la valeur de l'attribut
     */
    protected function get($attribut) {
        return $this->$attribut;
    }

    // GET et SET
    function getId_Logbdd() {
        return $this->Id_Logbdd;
    }

    function getLogin() {
        return $this->Login;
    }

    function getDate_Modif() {
        return $this->Date_Modif;
    }

    function getNom_Table() {
        return $this->Nom_Table;
    }

    function getType_Operation() {
        return $this->Type_Operation;
    }

    function getId_Element() {
        return $this->Id_Element;
    }

    function setId_Logbdd($Id_Logbdd): void {
        $this->Id_Logbdd = $Id_Logbdd;
    }

    function setLogin($Login): void {
        $this->Login = $Login;
    }

    function setDate_Modif($Date_Modif): void {
        $this->Date_Modif = $Date_Modif;
    }

    function setNom_Table($Nom_Table): void {
        $this->Nom_Table = $Nom_Table;
    }

    function setType_Operation($Type_Operation): void {
        $this->Type_Operation = $Type_Operation;
    }

    function setId_Element($Id_Element): void {
        $this->Id_Element = $Id_Element;
    }

}
