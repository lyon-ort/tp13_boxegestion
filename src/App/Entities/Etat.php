<?php

class Etat {

    // Attributs
    protected $Id_Etat;
    protected $Libelle_Etat;

    // Constructeur
    public function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas = NULL) {
        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Appelle le mutateur des attributs existant dans le tableau $datas
        foreach ($attrib as $key => $val) {
            // Si, dans vos habitudes, les champs de la bdd sont en minuscules, décommentez la ligne suivante.
            //$key = strtolower($key);
            // Si, dans vos habitudes, les champs de la bdd sont en majuscules, décommentez la ligne suivante.
            //$key = strtoupper($key);
            //  Perso, je met tout en notation camelCase... même dans la BDD.
            if (isset($datas[$key])) {
                $mutateur = 'set' . $key;
                $this->$mutateur($datas[$key]);
            }
        }

        return $this;
    }

    // GET et SET
    function getId_Etat() {
        return $this->Id_Etat;
    }

    function getLibelle_Etat() {
        return $this->Libelle_Etat;
    }

    function setId_Etat($Id_Etat): void {
        $this->Id_Etat = $Id_Etat;
    }

    function setLibelle_Etat($Libelle_Etat): void {
        $this->Libelle_Etat = $Libelle_Etat;
    }

}
