<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';
$leGet = '?connect=1';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Créer structure </title>
    </head>
    
    <body>
        <!-- Header (inc) -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>
        
        <?php
            // Message de confirmation de création
            if (isset($_GET['confirmationCreation']))
            {
                    ?>
                            <!-- Pop up de confirmation de suppression -->
                            <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                                <div class="modal-content" style="height: 250px;">
                                    <!-- bouton fermer -->
                                    <div id="close" class="closeTest">+</div>

                                    <!-- Confirmation de suppression -->
                                    <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                                    La structure et son responsable ont été créé !<br/><br/>

                                    <button class="boutonAnnulerTest"> OK </button>
                                </div>
                            </div>   

                             <script>
                                // La croix (ferme le pop-up)
                                document.querySelector('.closeTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });      

                                // Le bouton annuler (ferme le pop-up)
                                document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });       
                            </script>            
                    <?php 
            }        
        ?>
        
        <!-- Section -->
        <section class='container-fluid about'>
            
            <!-- Information de l'utilisateur (inc) -->
            <?php
            include '../inc/information.php';
            ?>
            
            <h1> Créer structures / responsables </h1>
            
            <hr class="separator">
            
            <?php
            


            // Les données de la structure avant la validation du formulaire
            $disabled = null;
            
            $valueNom = null;
            $valueAdresse = null;
            $valueCode = null;
            $valueVille = null;
            $valueFixe = null;
            $valuePortable = null;

            // Les données de la structure après la validation du formulaire
            if (isset($_POST['responsable']))
            {
                // Test si le nom existe déjà
                $objetStructureRep = new StructureRepository();
                $arrayObjetStructure = $objetStructureRep->selectionStructure();
                
                $test = true;
                
                foreach ($arrayObjetStructure as $value)
                {
                    if ($value->getNom_Struct() == $_POST['Nom_Struct'])
                    {
                        $test = false;
                    }
                }
                
                // Les données de la structures après une validation du formulaire
                $valueNom = $_POST['Nom_Struct'];
                $valueAdresse = $_POST['Adresse_Struct'];
                $valueCode = $_POST['Cp_Struct'];
                $valueVille = $_POST['Ville_Struct'];
                $valueFixe = $_POST['Tel_Fixe'];
                $valuePortable = $_POST['Tel_Port'];                

                if ($test)
                {
                    // Initialise les disabled et les value du formulaire de structure
                    $disabled = 'disabled';
                    
                    $confirmationResponsable = true;
                }
                else
                {
                    echo '<p class="error">ERREUR : Ce "nom" de structure existe déjà</p> <br/><br/>';
                    $errorStructure = 'error';
                }
            }
            
            // Les données du responsable de structure après la validation du formulaire
            if (isset($_POST['creation']))
            {
                $confirmationResponsable = true;
                $disabled = 'disabled';
                
                $valueNom = $_POST['Nom_Struct'];
                $valueAdresse = $_POST['Adresse_Struct'];
                $valueCode = $_POST['Cp_Struct'];
                $valueVille = $_POST['Ville_Struct'];
                $valueFixe = $_POST['Tel_Fixe'];
                $valuePortable = $_POST['Tel_Port'];  
                
                // Confirmation mdp
                $confirm = false;
                if (isset($_POST['Mdp']) && isset($_POST['MdpConfirm']))
                {
                    // Les données des utilisateurs après la validation du formulaire
                    $valueNomUtilisateur = $_POST['Nom_Utilisateur'];
                    $valuePrenomUtilisateur = $_POST['Prenom_Utilisateur'];
                    $valueLoginUtilisateur = $_POST['Login_Utilisateur'];
                    $valueMailUtilisateur = $_POST['Email'];
                    $valueTelUtilisateur = $_POST['Tel_Port'];
                    
                    
                    // Test si les mdp inscrit sont identiques
                    if ($_POST['Mdp'] == $_POST['MdpConfirm'])
                    {
                        $confirm = true;
                    }       
                    else
                    {
                        echo 'ERREUR : Les mot de passe inscrit ne sont pas identique <br/><br/>';
                        $errorMdp = 'error';
                    }    
                    
                    
                    // Test si le login existe déjà
                    $objetUtilisateurRep = new UtilisateurRepository();
                    $arrayUtilisateur = $objetUtilisateurRep->selectionUtilisateur();
                    
                    $test = true;
                    
                    foreach ($arrayUtilisateur as $value)
                    {
                        if ($value->getLogin_Utilisateur() == $_POST['Login_Utilisateur'])
                        {
                            $test = false;
                            echo '<p class="error">ERREUR : Ce "login" d\'utilisateur existe déjà</p> <br/><br/>';
                            $errorLoginUtilisateur = 'error';
                        }
                    }

                    
                    // Créé ou non l'utilisateur et sa structure
                    if ($test && $confirm)
                    {
                        $confirmationCreation = true;
                    }                   
                }
            }
            ?>
            <div id='creationStructure'>
                
                <!-- Formulaire création structure -->
                <div id='structure'>
                    <h2> Structure </h2>
                    <form class="formCreat" method="POST" action="creatStruct.php?connect=1" style="background: <?= $background = isset($confirmationResponsable) ? 'Lightgreen' : '' ?>">

                            <strong> Type de structure: </strong> 
                            <select name="Id_Typ_Struct" <?= $disabled ?> required>
                                <option value=""> -- Choisir un type -- </option>
                                <?php
                                $objetTypeStructureRep = new TypeStructureRepository();
                                $arrayTypeStructure = $objetTypeStructureRep->selectionTypeStructure();

                                foreach($arrayTypeStructure as $value)
                                {
                                    $selected = ($value->getId_Typ_Struct() == $_POST['Id_Typ_Struct']) ? 'selected' : '';
                                    ?>
                                    <option value="<?= $value->getId_Typ_Struct() ?>" <?= $selected ?>> <?= $value->getLibelle_Typ_Struct() ?> </option> 
                                    <?php
                                }
                                ?>
                            </select> 
                            <br/><br/>

                            <strong> Nom : </strong> 
                            <input id="<?= $errorStructure = (isset($errorStructure)) ? $errorStructure : '' ?>" type="text" name="Nom_Struct" placeholder="nom" value="<?= $valueNom ?>" <?= $disabled ?> required>
                            <br/><br/>     

                            <strong> Adresse : </strong> <input type="text" name="Adresse_Struct" placeholder="adresse" value="<?= $valueAdresse ?>" <?= $disabled ?> required>
                            <br/><br/>   

                            <strong> Code postale : </strong> </strong> <input type="text" name="Cp_Struct" placeholder="12345" minlength="5" maxlength="5" pattern="^(([0-8][0-9])|(9[0-5])|(2[ab]))[0-9]{3}$" value="<?= $valueCode ?>" <?= $disabled ?> required> 
                            <br/><br/> 

                            <strong> Ville : </strong> <input type="text" name="Ville_Struct" placeholder="ville" value="<?= $valueVille ?>" <?= $disabled ?> required>
                            <br/><br/>   

                            <strong> Fixe : </strong> <input type="tel" minlength="10" maxlength="10" name="Tel_Fixe" placeholder="0123456789" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" value="<?= $valueFixe ?>" <?= $disabled ?> required> 
                            <br/><br/>

                            <strong> Portable : </strong> <input type="tel" minlength="10" maxlength="10" name="Tel_Port" placeholder="0123456789" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" value="<?= $valuePortable ?>" <?= $disabled ?> required> 
                            <br/><br/>

                            <input type="hidden" name="responsable"/>

                            <input type="submit" value="Valider" <?= $disabled ?>>
                    </form>
                    <br/>
                </div>
                
            <?php
            // Création du responsable de structure
            if (isset($confirmationResponsable))
            {
                ?>
                <div id='responsable'>
                    <h2> Responsable </h2>
                    <form class="formCreat" method="POST" action="creatStruct.php?connect=1">
                            Type de role: 
                            <select name="Id_Typ_Role_Utilisateur" required>
                                    <option value=""> -- Choisir un type -- </option>
                                    <?php
                                    $objetTypeRoleRep = new TypeRoleRepository();
                                    $arrayTypeRole = $objetTypeRoleRep->selectionTypeRole();

                                    foreach($arrayTypeRole as $value)
                                    {
                                        $selected = ($value->getId_Typ_Role() == $_POST['Id_Typ_Role_Utilisateur']) ? 'selected' : '';

                                        // Club
                                        if ($_POST['Id_Typ_Struct'] == 1)
                                        {
                                            if ($value->getId_Typ_Role() != 2 && $value->getId_Typ_Role() != 5) // 2 = administrateur - 5 = membre de club
                                            {
                                                ?>
                                                <option value="<?= $value->getId_Typ_Role() ?>" <?= $selected ?>> <?= $value->getLibelle_Typ_Role() ?> </option> 
                                                <?php
                                            }
                                        }
                                        // Fédération ou comité
                                        else if ($_POST['Id_Typ_Struct'] == 2 || $_POST['Id_Typ_Struct'] == 3)
                                        {
                                            if ($value->getId_Typ_Role() != 1 && $value->getId_Typ_Role() != 5) // 1 = responsable de club - 5 = membre de club
                                            {
                                                ?>
                                                <option value="<?= $value->getId_Typ_Role() ?>" <?= $selected ?>> <?= $value->getLibelle_Typ_Role() ?> </option> 
                                                <?php
                                            }                                        
                                        }
                                    }
                                    ?>                           
                            </select> 
                            <br/><br/>

                            Structure : <input type="text" value="<?= $_POST['Nom_Struct'] ?>" disabled/>
                            <br/><br/>

                            Nom : <input type="text" name="Nom_Utilisateur" placeholder="nom" value="<?= $valueNomUtilisateur = (isset($valueNomUtilisateur)) ? $valueNomUtilisateur : '' ?>" required>
                            <br/><br/>

                            Prénom : <input type="text" name="Prenom_Utilisateur" placeholder="prénom" value="<?= $valuePrenomUtilisateur = (isset($valuePrenomUtilisateur)) ? $valuePrenomUtilisateur : '' ?>" required> 
                            <br/><br/>

                            Login : <input id="<?= $errorLoginUtilisateur = (isset($errorLoginUtilisateur)) ? $errorLoginUtilisateur : '' ?>" type="text" name="Login_Utilisateur" placeholder="login" value="<?= $valueLoginUtilisateur = (isset($valueLoginUtilisateur)) ? $valueLoginUtilisateur : '' ?>" required> 
                            <br/><br/>

                            Email : <input type="email" name="Email" placeholder="xyz@gmail.com" value="<?= $valueMailUtilisateur = (isset($valueMailUtilisateur)) ? $valueMailUtilisateur : '' ?>" required> 
                            <br/><br/>

                            Mot de passe : <input id="<?= $errorMdp = (isset($errorMdp)) ? $errorMdp : '' ?>" type="password" name="Mdp" value="<?= $valueMdpUtilisateur = (isset($valueMdpUtilisateur)) ? $valueMdpUtilisateur : '' ?>" required> 
                            <br/><br/>

                            Confirmation du mot de passe : <input id="<?= $errorMdp = (isset($errorMdp)) ? $errorMdp : '' ?>" type="password" name="MdpConfirm" value="<?= $valueMdpConfirmUtilisateur = (isset($valueMdpConfirmUtilisateur)) ? $valueMdpConfirmUtilisateur : '' ?>" required>  
                            <br/><br/>

                            Portable : <input type="tel" minlength="10" maxlength="10" name="Tel_Port" placeholder="0123456789" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" value="<?= $valueTelUtilisateur = (isset($valueTelUtilisateur)) ? $valueTelUtilisateur : '' ?>" required> 
                            <br/><br/>


                            <!-- Input hidden de la structure -->
                            <input type="hidden" name="Id_Typ_Struct" value="<?= $_POST['Id_Typ_Struct'] ?>"/>
                            <input type="hidden" name="Nom_Struct" value="<?= $_POST['Nom_Struct'] ?>"/>
                            <input type="hidden" name="Adresse_Struct" value="<?= $_POST['Adresse_Struct'] ?>"/>
                            <input type="hidden" name="Cp_Struct" value="<?= $_POST['Cp_Struct'] ?>"/>
                            <input type="hidden" name="Ville_Struct" value="<?= $_POST['Ville_Struct'] ?>"/>
                            <input type="hidden" name="Tel_Fixe" value="<?= $_POST['Tel_Fixe'] ?>"/>
                            <input type="hidden" name="Tel_PortStruct" value="<?= $_POST['Tel_Port'] ?>"/>

                            <input type="hidden" name="creation"/>
                            <input type="submit" value="Valider">
                    </form>
                </div>
                <?php
            }

            // CREATION DE L'UTILISATEUR ET DE SA STRUCTURE
            if (isset($confirmationCreation))
            {
                $objetUtilisateurRep2 = new UtilisateurRepository();
                $objetStructureRep2 = new StructureRepository();
                
                
                // CREATION DE L'UTILISATEUR
                // Hash mdp
                $hashMdp = password_hash($_POST['Mdp'], PASSWORD_DEFAULT);

                // Déclaration du tableau des valeurs
                $valeurUtilisateur = array(
                    "Id_Typ_Role_Utilisateur" => $_POST['Id_Typ_Role_Utilisateur'],
                    "Id_Struct_Utilisateur" => NULL,
                    "Nom_Utilisateur" => $_POST['Nom_Utilisateur'],
                    "Prenom_Utilisateur" => $_POST['Prenom_Utilisateur'],
                    "Login_Utilisateur" => $_POST['Login_Utilisateur'],
                    "Email" => $_POST['Email'],
                    "Mdp" => $hashMdp,
                    "Tel_Port" => $_POST['Tel_Port'],
                    "Date_Creation_Compte" => date("Y-m-d H:i:s"),
                    "Dernier_Login_Utilisateur" => NULL,
                    "Inactif_Utilisateur" => 0
                    );

                // Sauvegarde de l'utilisateur
                $utilisateur = new Utilisateur($valeurUtilisateur);
                $objetUtilisateurRep2->sauver($utilisateur);

                
                // CREATION DE LA STRUCTURE
                // Initialisation de l'ID de l'utilisateur
                $objetUtilisateur = $objetUtilisateurRep2->selectionUtilisateurLogin($_POST['Login_Utilisateur']);

                // Déclaration du tableau des valeurs
                $valeurStructure = array(
                    "Id_Typ_Struct" => $_POST['Id_Typ_Struct'],
                    "Nom_Struct" => $_POST['Nom_Struct'],
                    "Adresse_Struct" => $_POST['Adresse_Struct'],
                    "Cp_Struct" => $_POST['Cp_Struct'],
                    "Ville_Struct" => $_POST['Ville_Struct'],
                    "Id_Responsable_Struct" => $objetUtilisateur->getId_Utilisateur(),
                    "Tel_Fixe" => $_POST['Tel_Fixe'],
                    "Tel_Port" => $_POST['Tel_PortStruct'],
                    "Inactif_Struct" => 0
                );

                // Sauvegarde de la structure
                $structure = new Structure($valeurStructure);
                $objetStructureRep2->sauver($structure);

                // Initialisation et insertion de l'ID de la structure dans l'utilisateur
                $objetStructure = $objetStructureRep2->selectionStructureNom($_POST['Nom_Struct']);
                $objetUtilisateurRep2->ajoutIdStruct($objetUtilisateur->getId_Utilisateur(), $objetStructure->getId_Struct());
                
                ?>
                <script language="Javascript"> 
                    document.location.replace("creatStruct.php?connect=1&confirmationCreation=1");
                </script>
                <?php
            }
            ?>
            <br/>
        </section>
    </body>
</html>
