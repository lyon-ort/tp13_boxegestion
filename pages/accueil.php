<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';

if (isset($_GET['connect']))
{
    // Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
    require_once '../config/session-verif.php';    
    $leGet = '?connect=1';
}
else
{
    $leGet = '';
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Accueil </title>
    </head>
    
    <body>
        
        <!-- Header -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>

        
        <!-- Section principal -->
        <section class='container-fluid about'>
            
            <?php
            if (isset($_GET['connect']))
            {
            ?>
                <div class="information">
                    Login : <?= $_SESSION['login'] ?> <br/>
                    Prénom : <?= $_SESSION['Prenom_Utilisateur'] ?> <br/>
                    Nom : <?= $_SESSION['Nom_Utilisateur'] ?> <br/>
                    Rôle :
                    <?php
                    $objetTypeRoleRepUtil = new TypeRoleRepository(); 
                    $typeRole = $objetTypeRoleRepUtil->selectionTypeRoleId($_SESSION['idRole']);
                    echo $typeRole->getLibelle_Typ_Role();
                    ?>                                 
                    <br/>
                    Structure :
                    <?php
                    $objetStructureRepUtil = new StructureRepository(); 
                    $structure = $objetStructureRepUtil->selectionStructureId($_SESSION['idStruct']);
                    echo $structure->getNom_Struct();
                    ?> <br/><br/><br/>
                    <button class='btn btn-custom' onclick="Deconnexion()"> Déconnexion </button>
                </div>   
            <?php
            }
            ?>
             
            <!-- Affiche les compétitions en cours, fini et à venir -->
                <table class="avectri">
                    
                    <h1> Compétitions </h1>
                    
                    <hr class="separator">
                    <thead>
                        <tr>
                            <th> Nom de la compétition </th> 
                            <th> Date de début </th>
                            <th> Date de fin </th>
                            <th> Statut </th> 
                            <th> Détails </th>
                        </tr>
                    </thead>    
                    <tbody>
                        
                        <?php
                        // Déclaration et utilisation d'un objet CompetitionRepository + déclaration et initialisation de l'array des compétitions
                        $CompetitionRep = new CompetitionRepository();

                        $arrayCompetition = array();

                        // Initialisation de l'array de toutes les compétitions
                        // En cours
                        $arrayCompetitionEnCours = $CompetitionRep->selectionCompetitionEnCours();
                        if ($arrayCompetitionEnCours)
                        {
                            foreach($arrayCompetitionEnCours as $value)
                            {
                                array_push($arrayCompetition, $value);
                            }
                        }

                        // Fini
                        $arrayCompetitionFini = $CompetitionRep->selectionCompetitionFini();
                        if ($arrayCompetitionFini)
                        {
                            foreach($arrayCompetitionFini as $value)
                            {
                                array_push($arrayCompetition, $value);
                            } 
                        }

                        // A venir
                        $arrayCompetitionAVenir = $CompetitionRep->selectionCompetitionAVenir();
                        if ($arrayCompetitionAVenir)
                        {
                            foreach($arrayCompetitionAVenir as $value)
                            {
                                array_push($arrayCompetition, $value);
                            }  
                        }


                        // Affichage de l'array
                        foreach ($arrayCompetition as $value1)
                        {
                            ?>
                                <tr>
                                    <!-- Le nom de la compétition -->
                                    <td> <?= $value1->getNom_Competition() ?> </td>

                                    <!-- Date de début de la compétition -->
                                    <td>
                                        <script>
                                            // Initialisation de l'objet date
                                            var dateDeBase = new Date('<?= $value1->getDate_Debut_Competition() ?>');

                                            // Affichage de la date en format FR
                                            var test = "";
                                            test = dateDeBase.toLocaleString();
                                            document.write(test.substr(0, 10));
                                        </script>
                                    </td>

                                    <!-- Date de fin de la compétition -->
                                    <td>
                                        <script>
                                            // Initialisation de l'objet date
                                            var dateDeBase = new Date('<?= $value1->getDate_Fin_Competition() ?>');

                                            // Affichage de la date en format FR
                                            var test = "";
                                            test = dateDeBase.toLocaleString();
                                            document.write(test.substr(0, 10));
                                        </script>
                                    </td>                                

                                    <!-- Le statut de la compétition -->
                                    <td> 
                                        <?php
                                            if (date("Y-m-d H:i:s") < $value1->getDate_Debut_Competition())
                                            {
                                                echo 'A venir';
                                            }
                                            else if (date("Y-m-d H:i:s") == $value1->getDate_Debut_Competition() || 
                                                    date("Y-m-d H:i:s") > $value1->getDate_Debut_Competition() && date("Y-m-d H:i:s") < $value1->getDate_Fin_Competition()
                                                    )
                                            {
                                                echo 'En cours ...';
                                            }
                                            else 
                                            {
                                                echo 'Fini';
                                            }
                                        ?>                                
                                    </td>

                                    <!-- Les détails de la compétition -->
                                    <td> 
                                        <?php
                                        if (isset ($_POST['Id_Competition']) && $_POST['Id_Competition'] == $value1->getId_Competition())
                                        {
                                            echo ' <strong> Date de début : </strong>' . $value1->getDate_Debut_Competition() . '<br/>';
                                            echo ' <strong> Date de fin : </strong>' . $value1->getDate_Fin_Competition() . '<br/>';                            
                                            echo ' <strong> Adresse : </strong>' . strip_tags($value1->getAdresse_Competition()) . '<br/>';
                                            echo ' <strong> Code postale : </strong>' . strip_tags($value1->getCp_Competition()) . '<br/>';
                                            echo ' <strong> Ville : </strong>' . strip_tags($value1->getVille_Competition()) . '<br/>';
                                            echo ' <strong> Date limite d\'inscription : </strong>' . $value1->getDate_Limite_Inscript_Competition() . '<br/>';
                                            echo ' <strong> Nombre de rounds : </strong>' . $value1->getNb_Round() . '<br/>';
                                            echo ' <strong> Durée des rounds : </strong>' . $value1->getDuree_Round() . 's <br/>';
                                            echo ' <strong> Nombre de rings : </strong>' . $value1->getNb_Ring() . '<br/>';

                                            // Les sexes
                                            echo '<strong> Sexe : </strong>';         
                                            $objetSexeRep = new SexeRepository();
                                            $tableauSexe = $objetSexeRep->selectionSexeId($value1->getId_Competition_Sexe());
                                            echo $sexe = ($tableauSexe == null) ? 'Mixte' : $tableauSexe->getLibelle_Sexe();
                                            echo '<br/>';

                                            // Les types d'affrontements
                                            echo '<strong> Type d\'affrontement : </strong>';
                                            $objetTypeAffrontementRep = new TypeAffrontementRepository();
                                            $tableauTypeAffrontement = $objetTypeAffrontementRep->selectionTypeAffrontementId($value1->getId_Competition_Typ_Affrontement());
                                            echo $typeAffrontement = ($tableauTypeAffrontement == null) ? 'Assaut / Combat' : $tableauTypeAffrontement->getLibelle_Typ_Affrontement();
                                            echo '<br/>';

                                            // Les structures
                                            echo '<strong> Structure : </strong>';
                                            $objetStructureRep = new StructureRepository();
                                            $tableauStructure = $objetStructureRep->selectionStructureId($value1->getId_Competition_Struct());
                                            echo $structure = $tableauStructure->getNom_Struct();
                                            echo '<br/>';
                                            
                                            // Les catégories d'age
                                            echo '<strong> Catégories d\'ages : </strong>';
                                            // Défini catégorie age
                                            $objetSeFaireRep = new SeFaireRepository();
                                            $arrayObjetSeFaire = $objetSeFaireRep->selectionSeFaireIdCompetition($value1->getId_Competition());

                                            $objetCategorieAgeRep = new CategorieAgeRepository();
                                            $arrayObjetCategorieAge = array();
                                            // Initialisation du tableau
                                            foreach ($arrayObjetSeFaire as $value)
                                            {
                                                $arrayObjetCategorieAge[] = $objetCategorieAgeRep->selectionCategorieAgeId($value->getId_SeFaire_Cat_Age());
                                            }
                                            // Ecriture du tableau
                                            foreach ($arrayObjetCategorieAge as $valueCategorieAge)
                                            {
                                                echo '<br/>- ' . $valueCategorieAge->getLibelle_Cat_Age(). '';
                                            }
                                            

                                            // La liste des tireurs inscrits
                                            echo '<br/> <strong> Les tireurs inscrits : </strong> <br/>';

                                            $objetTableauxRep = new TableauRepository();

                                            // Active le code si il y a un moins un tableau dans la compétition
                                            if ($objetTableauxRep->selectionTableauxIdCompet($value1->getId_Competition()))
                                            {
                                                $tableauTableaux = $objetTableauxRep->selectionTableauxIdCompet($value1->getId_Competition());
                                                // Sélection des participation de la compétition
                                                $objetParticiperRep = new ParticiperRepository();

                                                // Active le code il y a au moins un tireur qui participe à la compétition
                                                $validationBis = false;

                                                foreach($tableauTableaux as $value2)
                                                {
                                                    $tableauParticiper[] = $objetParticiperRep->selectionParticipationIdTableau($value2->getId_Tab());

                                                    // Vérifie si il y a au moins un tireur qui participe
                                                    if ($objetParticiperRep->selectionParticipationIdTableau($value2->getId_Tab()))
                                                    {
                                                        $validationBis = true;
                                                    }
                                                }

                                                if ($validationBis)
                                                {
                                                    // Sélection des tireurs de la compétition
                                                    $objetTireurRep = new TireurRepository();
                                                    // Tableau qui va stocker les id des tireurs
                                                    $id = array();

                                                    for ($i = 0; $i != count($tableauParticiper); $i++)
                                                    {
                                                        if ($tableauParticiper[$i])
                                                        {
                                                            for ($y = 0; $y != count($tableauParticiper[$i]); $y++)
                                                            {
                                                                $validation = true;
                                                                // Stocke le tireur
                                                                $tireur = $objetTireurRep->selectionTireurId($tableauParticiper[$i][$y]->getId_Particip_Tireur());
                                                                // Vérifie si le tireur a déjà été affiché
                                                                for ($z = 0; $z != count($id); $z++)
                                                                {
                                                                    if ($id[$z] == $tireur->getId_Tireur())
                                                                    {
                                                                        $validation = false;
                                                                    }
                                                                }
                                                                // Affiche le tireur si il n'a pas déjà été affiché
                                                                if ($validation)
                                                                {
                                                                    echo '- ' . $tireur->getPrenom_Tireur() . ' ' . $tireur->getNom_Tireur() . '<br/>';
                                                                }
                                                                // Récolte l'id du tireur pour ne pas l'afficher en double
                                                                $id[] = $tireur->getId_Tireur();
                                                            }                                                            
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    echo '<em> Il n\'y a aucun tireur inscrit </em> <br/>';
                                                }
                                            }
                                            ?>
                                            <!-- Bouton de consultation des tableaux -->
                                            <br/>
                                            <form action="detailCompetition.php<?= $leGet ?>" method="POST">
                                                <input type="hidden" name="Id_Competition" value="<?= $value1->getId_Competition() ?>"/>
                                                <input type="submit" value="Les tableaux"/>
                                            </form>
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                            <form action="accueil.php<?= $leGet ?>" method="POST">
                                                <input type="hidden" name="Id_Competition" value="<?= $value1->getId_Competition() ?>">
                                                <input type="submit" value="Détails"> 
                                            </form>
                                            <?php
                                        }
                                        ?>                                    
                                    </td>

                                </tr>
                            <?php
                        }
                        ?>
                    </tbody>        
                </table>
                   

           
            
            
            <br/>
         
            <!-- Script qui Trie les tableaux HTML dynamiquement -->
            <script>
                function twInitTableau() 
                {
                    // Initialise chaque tableau de classe « avectri »
                    [].forEach.call( document.getElementsByClassName("avectri"), function(oTableau) 
                    {
                        var oEntete = oTableau.getElementsByTagName("tr")[0];
                        var nI = 1;
                        // Ajoute à chaque entête (th) la capture du clic
                        // Un picto flèche, et deux variable data-*
                        // - Le sens du tri (0 ou 1)
                        // - Le numéro de la colonne
                        [].forEach.call(oEntete.querySelectorAll("th"), function(oTh) 
                        {
                            if (nI < 5)
                            {
                                oTh.addEventListener("click", twTriTableau, false);
                                oTh.setAttribute("data-pos", nI);
                                if (oTh.getAttribute("data-tri")=="1") 
                                {
                                    oTh.innerHTML += "<span class=\"flecheDesc\"></span>";
                                } 
                                else 
                                {
                                    oTh.setAttribute("data-tri", "0");
                                    oTh.innerHTML += "<span class=\"flecheAsc\"></span>";
                                }
                                // Tri par défaut
                                if (oTh.className=="selection") 
                                {
                                    oTh.click();
                                }
                                nI++;
                            }
                        });
                    });
                }

                function twInit() 
                {
                  twInitTableau();
                }

                function twPret(maFonction) 
                {
                    if (document.readyState != "loading")
                    {
                        maFonction();
                    } 
                    else 
                    {
                        document.addEventListener("DOMContentLoaded", maFonction);
                    }
                }

                twPret(twInit);

                function twTriTableau() 
                {
                    // Ajuste le tri
                    var nBoolDir = this.getAttribute("data-tri");
                    this.setAttribute("data-tri", (nBoolDir=="0") ? "1" : "0");
                    // Supprime la classe « selection » de chaque colonne.
                    [].forEach.call( this.parentNode.querySelectorAll("th"), function(oTh) 
                    {
                        oTh.classList.remove("selection");
                    });
                    // Ajoute la classe « selection » à la colonne cliquée.
                    this.className = "selection";
                    // Ajuste la flèche
                    this.querySelector("span").className = (nBoolDir=="0") ? "flecheAsc" : "flecheDesc";

                    // Construit la matrice
                    // Récupère le tableau (tbody)
                    var oTbody = this.parentNode.parentNode.parentNode.getElementsByTagName("tbody")[0]; 
                    var oLigne = oTbody.rows;
                    var nNbrLigne = oLigne.length;
                    var aColonne = new Array(), i, j, oCel;
                    for(i = 0; i < nNbrLigne; i++) 
                    {
                        oCel = oLigne[i].cells;
                        aColonne[i] = new Array();
                        for(j = 0; j < oCel.length; j++)
                        {
                            aColonne[i][j] = oCel[j].innerHTML;
                        }
                    }

                    // Trier la matrice (array)
                    // Récupère le numéro de la colonne
                    var nIndex = this.getAttribute("data-pos");
                    // Récupère le type de tri (numérique ou par défaut « local »)
                    var sFonctionTri = (this.getAttribute("data-type")=="num") ? "compareNombres" : "compareLocale";
                    // Tri
                    aColonne.sort(eval(sFonctionTri));
                    // Tri numérique
                    function compareNombres(a, b) 
                    {
                        return a[nIndex-1] - b[nIndex-1];
                    }
                    // Tri local (pour support utf-8)
                    function compareLocale(a, b) 
                    {
                        return a[nIndex-1].localeCompare(b[nIndex-1]);
                    }
                    // Renverse la matrice dans le cas d’un tri descendant
                    if (nBoolDir==0) aColonne.reverse();

                    // Construit les colonne du nouveau tableau
                    for(i = 0; i < nNbrLigne; i++)
                    {
                        aColonne[i] = "<td>"+aColonne[i].join("</td><td>")+"</td>";
                    }

                    // assigne les lignes au tableau
                    oTbody.innerHTML = "<tr>"+aColonne.join("</tr><tr>")+"</tr>";
                }
            </script>
        
        </section>
        
        <!-- Footer -->
        <footer class='container-fluid footer'>
            
        </footer>
        <!-- End Footer -->
    </body>
</html>

