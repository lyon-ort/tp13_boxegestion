<?php

require '../src/App/Entities/Tableau.php';

class TableauRepository {

    protected $table;     
    protected $classMapped; 
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire)
    public function __construct() {
        $this->table = 'Tableau';
        $this->classMapped = 'App\Entities\Tableau';
        $this->idFieldName = 'Id_Tab';
    }
    
    // Ajouter / Modifier un élément
    public function sauver(Tableau $entity) {
        $db = dbConnect();
        $resultSet = NULL;
        if ($entity != NULL) {
            $bindParam = $entity->__toArray();
            if ($entity->getId_Tab() == NULL) {
                // Nouvelle entité
                $query = "INSERT INTO $this->table" .
                        " (Libelle_Tab, "
                        . "Nb_Max_Rencontre, "
                        . "Id_Tableau_Sexe, "
                        . "Id_Tableau_Typ_Affrontement, "
                        . "Id_Tableau_Typ_Tableau, "
                        . "Id_Tableau_Competition) "
                        . " VALUES "
                        . "(:Libelle_Tab, "
                        . ":Nb_Max_Rencontre, "
                        . ":Id_Tableau_Sexe, "
                        . ":Id_Tableau_Typ_Affrontement, "
                        . ":Id_Tableau_Typ_Tableau, "
                        . ":Id_Tableau_Competition)";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Libelle_Tab', $bindParam['Libelle_Tab'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Nb_Max_Rencontre', $bindParam['Nb_Max_Rencontre'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tableau_Sexe', $bindParam['Id_Tableau_Sexe'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tableau_Typ_Affrontement', $bindParam['Id_Tableau_Typ_Affrontement'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tableau_Typ_Tableau', $bindParam['Id_Tableau_Typ_Tableau'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tableau_Competition', $bindParam['Id_Tableau_Competition'], \PDO::PARAM_INT);
                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $entity->setid($db->lastInsertId());
                    $resultSet = $entity;
                }
            } else {
                //  Entité existante
                $query = "UPDATE $this->table"
                        . " SET Libelle_Tab = :Libelle_Tab, "
                        . " Nb_Max_Rencontre = :Nb_Max_Rencontre, "
                        . " Id_Tableau_Sexe = :Id_Tableau_Sexe, "
                        . " Id_Tableau_Typ_Affrontement = :Id_Tableau_Typ_Affrontement, "
                        . " Id_Tableau_Typ_Tableau = :Id_Tableau_Typ_Tableau, "
                        . " Id_Tableau_Competition = :Id_Tableau_Competition "
                        . " WHERE $this->idFieldName = :Id_Tab";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Libelle_Tab', $bindParam['Libelle_Tab'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Nb_Max_Rencontre', $bindParam['Nb_Max_Rencontre'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tableau_Sexe', $bindParam['Id_Tableau_Sexe'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tableau_Typ_Affrontement', $bindParam['Id_Tableau_Typ_Affrontement'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tableau_Typ_Tableau', $bindParam['Id_Tableau_Typ_Tableau'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tableau_Competition', $bindParam['Id_Tableau_Competition'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tab', $bindParam[$this->idFieldName], \PDO::PARAM_INT);

                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $resultSet = $entity;
                }
            }
        }
        return $resultSet;
    }
    
    
    
    // Sélectionner des tableaux selon un id de compétition
    public function selectionTableauxIdCompet($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objet à NULL si jamais la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Tableau_Competition = :idMark");
        $req->execute(array('idMark' => $id));

        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;        
    }
    
    
    
    // Sélectionner un tableau selon un id
    public function selectionTableauxId($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $objet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE $this->idFieldName = :idMark");
        $req->execute(array('idMark' => $id));

        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$objet = new $this->table($row);
	    }
        }
        
        // Return un objet
        return $objet;        
    }
    
    
    
    // Supprimer le un tableau par rapport à un ID
    public function supprimerTableauId($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Requête
        $req = $db->prepare("DELETE FROM $this->table WHERE Id_Tab = :idTableauMark");
        $req->execute(array('idTableauMark' => $id));
    }        
    
    
    
    
    
    /* Les fonctions de romain 

     * 
     * 
     * 
     * 
     *      */
    
    
    
    
    
    public function renvoisTableau($idTab)
    {
        $db = dbConnect();
        $res = null;
        $query = "SELECT Tireur.Id_Tireur, Tireur.Nom_Tireur, Tireur.Prenom_Tireur FROM Tireur, Participer, Tableau WHERE Tireur.Id_Tireur = Participer.Id_Particip_Tireur AND Participer.Id_Particip_Tableau = Tableau.Id_Tab AND Tableau.Id_Tab = $idTab ;";
        $req = $db->query($query);
        
        if ($req) 
        {
            while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
                // $res['Id_Tireur'] = $row['Id_Tireur'];
                // $res['Nom_Tireur'] = $row['Nom_Tireur'];
                // $res['Prenom_Tireur'] = $row['Prenom_Tireur'];
                $res[] = $row;
            }
        }
        
        return $res;
    }
    
    public function nbMaxRencontre($idTab)
    {
        $db = dbConnect();
        
        $reqTireur = $db->prepare("SELECT Nb_Max_Rencontre FROM Tableau WHERE Tableau.Id_Tab = :idTabMark");
        $reqTireur->execute(array('idTabMark' => $idTab)); 
        
        //var_dump($reqTireur);
        $data = $reqTireur->fetchAll();

        return $data;
    }
    
    public function typeTableau($idTab)
    {
        $db = dbConnect();
        $res = null;
        $query = "SELECT Id_Tableau_Typ_Tableau FROM Tableau WHERE Tableau.Id_Tab = $idTab;";
        $req = $db->query($query);
        
        if ($req) 
        {
            while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
               
                $res[] = $row;
            }
        }
        
        return $res;
       
    }
}

