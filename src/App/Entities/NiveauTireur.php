<?php

class NiveauTireur {

    // Attributs
    protected $Id_Niv_Tir;
    protected $Libelle_Niv_Tir;

    // Constructeur
    public function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas = NULL) {
        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Appelle le mutateur des attributs existant dans le tableau $datas
        foreach ($attrib as $key => $val) {
            // Si, dans vos habitudes, les champs de la bdd sont en minuscules, décommentez la ligne suivante.
            //$key = strtolower($key);
            // Si, dans vos habitudes, les champs de la bdd sont en majuscules, décommentez la ligne suivante.
            //$key = strtoupper($key);
            //  Perso, je met tout en notation camelCase... même dans la BDD.
            if (isset($datas[$key])) {
                $mutateur = 'set' . $key;
                $this->$mutateur($datas[$key]);
            }
        }

        return $this;
    }

    // GET et SET
    function getId_Niv_Tir() {
        return $this->Id_Niv_Tir;
    }

    function getLibelle_Niv_Tir() {
        return $this->Libelle_Niv_Tir;
    }

    function setId_Niv_Tir($Id_Niv_Tir): void {
        $this->Id_Niv_Tir = $Id_Niv_Tir;
    }

    function setLibelle_Niv_Tir($Libelle_Niv_Tir): void {
        $this->Libelle_Niv_Tir = $Libelle_Niv_Tir;
    }

}
