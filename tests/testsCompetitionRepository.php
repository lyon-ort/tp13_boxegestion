<?php

require '../config/config.php';

// Déclaration CompetitionRepository
$competitionRepository = new CompetitionRepository();

// Déclaration du tableau des valeurs
$valeurCompetition = array(
    "Id_Competition" => 5,
    "Nom_Competition" => "nom",
    "Date_Debut_Competition" => "2019-08-08",
    "Date_Fin_Competition" => "2019-08-08",
    "Adresse_Competition" => "adresseTest",
    "Cp_Competition" => "69580",
    "Ville_Competition" => "villeTtest",
    "Date_Limite_Inscript_Competition" => "2019-08-08",
    "Nb_Round" => 1,
    "Duree_Round" => 1,
    "Nb_Ring" => 1,
    "Id_Competition_Sexe" => 1,
    "Id_Competition_Typ_Affrontement" => 1,
    "Id_Competition_Struct" => 1
);

// Initialisation et dump() des valeurs
$competition = new Competition($valeurCompetition);
var_dump($competition);

// Fonction sauver pour créer ou modifier une competition
$competitionRepository->sauver($competition);
