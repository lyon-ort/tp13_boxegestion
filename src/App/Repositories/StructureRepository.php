<?php

require '../src/App/Entities/Structure.php';

class StructureRepository {

    protected $table;     
    protected $classMapped; 
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire)
    public function __construct() {
        $this->table = 'Structure';
        $this->classMapped = 'App\Entities\Structure';
        $this->idFieldName = 'Id_Struct';
    }
    
    // Ajouter / Modifier un élément
    public function sauver(Structure $entity) {
        $db = dbConnect();
        $resultSet = NULL;
        if ($entity != NULL) {
            $bindParam = $entity->__toArray();
            if ($entity->getId_Struct() == NULL) {
                // Nouvelle entité
                $query = "INSERT INTO $this->table" .
                        " (Id_Typ_Struct,"
                        . "Nom_Struct, "
                        . "Adresse_Struct, "
                        . "Cp_Struct, "
                        . "Ville_Struct, "
                        . "Id_Responsable_Struct, "
                        . "Tel_Fixe, "
                        . "Tel_Port, "
                        . "Inactif_Struct) "
                        . " VALUES "
                        . "(:Id_Typ_Struct,"
                        . ":Nom_Struct, "
                        . ":Adresse_Struct, "
                        . ":Cp_Struct, "
                        . ":Ville_Struct, "
                        . ":Id_Responsable_Struct, "
                        . ":Tel_Fixe, "
                        . ":Tel_Port, "
                        . ":Inactif_Struct) ";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Id_Typ_Struct', $bindParam['Id_Typ_Struct'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Nom_Struct', $bindParam['Nom_Struct'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Adresse_Struct', $bindParam['Adresse_Struct'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Cp_Struct', $bindParam['Cp_Struct'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Ville_Struct', $bindParam['Ville_Struct'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Id_Responsable_Struct', $bindParam['Id_Responsable_Struct'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Tel_Fixe', $bindParam['Tel_Fixe'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Tel_Port', $bindParam['Tel_Port'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Inactif_Struct', $bindParam['Inactif_Struct'], \PDO::PARAM_INT);
                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $entity->setid($db->lastInsertId());
                    $resultSet = $entity;
                }
            } else {
                //  Entité existante
                $query = "UPDATE $this->table"
                        . " SET Id_Typ_Struct = :Id_Typ_Struct, "
                        . " Nom_Struct = :Nom_Struct, "
                        . " Adresse_Struct = :Adresse_Struct, "
                        . " Cp_Struct = :Cp_Struct, "
                        . " Ville_Struct = :Ville_Struct, "
                        . " Id_Responsable_Struct = :Id_Responsable_Struct, "
                        . " Tel_Fixe = :Tel_Fixe, "
                        . " Tel_Port = :Tel_Port,"
                        . " Inactif_Struct = :Inactif_Struct "
                        . " WHERE $this->idFieldName = :Id_Struct";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Id_Typ_Struct', $bindParam['Id_Typ_Struct'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Nom_Struct', $bindParam['Nom_Struct'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Adresse_Struct', $bindParam['Adresse_Struct'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Cp_Struct', $bindParam['Cp_Struct'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Ville_Struct', $bindParam['Ville_Struct'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Id_Responsable_Struct', $bindParam['Id_Responsable_Struct'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Tel_Fixe', $bindParam['Tel_Fixe'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Tel_Port', $bindParam['Tel_Port'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Id_Struct', $bindParam[$this->idFieldName], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Inactif_Struct', $bindParam['Inactif_Struct'], \PDO::PARAM_INT);
                $reqPrep->execute();

                if ($reqPrep != FALSE) 
                {
                    $resultSet = $entity;
                }
            }
        }
        return $resultSet;
    }
    
    
    
    // Sélectionne une structure selon un id
    public function selectionStructureId($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $objet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE $this->idFieldName = :idMark");
        $req->execute(array('idMark' => $id));
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$objet = new $this->table($row);
	    }
        }
        
        // Return de l'objet
        return $objet;
    }
    
    
    
    // Sélectionne toutes les structures
    public function selectionStructure()
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objets à NULL si jamais elle la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->query("SELECT * FROM $this->table WHERE Inactif_Struct = 0");
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;
    }    
    
    
    
    // Sélectionne une structure selon un nom
    public function selectionStructureNom($nom)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $objet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE Nom_Struct = :nomMark");
        $req->execute(array('nomMark' => $nom));
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$objet = new $this->table($row);
	    }
        }
        
        // Return de l'objet
        return $objet;
    }    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
    // Séléctionne toutes les structure dans un tableau
    public function selectionStructure()
    {
        $db = dbConnect();
        $req = $db->query("SELECT * FROM $this->table");
        $i = 0;
        
        while ($data = $req->fetch())
        {
            $tableauStruct[$i] = array(
                'Id_Struct' => $data['Id_Struct'],
                'Nom_Struct' => $data['Nom_Struct']
            );
            $i++;
        }
        
        return $tableauStruct;
    }*/
    
    
    // Séléctionne tous les types de rôles
    public function selectionStructure2()
    {
        $db = dbConnect();
        $req = $db->query("SELECT * FROM $this->table");
        
        return $req;
    }
    
    /*
    // Séléctionne une structure selon un idStructUtilisateur
    public function selectionStructureId($idStructUtil)
    {
        $db = dbConnect();
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Struct = :idStructUtilMark");
        $req->execute(array('idStructUtilMark' => $idStructUtil));
        
        while ($data = $req->fetch())
        {
            $nom = $data['Nom_Struct'];
        }
        
        return $nom;
    }*/
    
    /*
    // Séléctionne une structure selon son nom
    public function selectionStructureNom($nomStructure)
    {
        $db = dbConnect();
        $req = $db->prepare("SELECT * FROM $this->table WHERE Nom_Struct = :nomStructureMark");
        $req->execute(array('nomStructureMark' => $nomStructure));
        
        while ($data = $req->fetch())
        {
            $id = $data['Id_Struct'];
        }
        
        return $id;        
    }*/
    
    
    public function selectionStruct()
    {
        $db = dbConnect();
        $req = $db->query("SELECT * FROM $this->table");
        
        return $req;
    }
    
    
    // Supprime la structure
    public function suppressionStructure($idSupr)
    {
        $db = dbConnect();
        
        // Supression des tireurs (si il ne paricipe pas à une compétition)
        $reqTireur = $db->prepare("DELETE FROM Tireur WHERE Id_Tir_Struct = :idSuprMark");
        $reqTireur->execute(array('idSuprMark' => $idSupr));
        $reqTireur->closeCursor();
        
        
        // Supression des idStruct utilisateurs
        $reqUtilisateur1 = $db->prepare("UPDATE Utilisateur SET Id_Struct_Utilisateur = NULL WHERE Id_Struct_Utilisateur = :idSuprMark");
        $reqUtilisateur1->execute(array('idSuprMark' => $idSupr)); 
        $reqUtilisateur1->closeCursor();
        
        
        // Supression de la structure (si elle n'a pas de compétition)
        $reqStructure = $db->prepare("DELETE FROM $this->table WHERE $this->idFieldName = :idSuprMark");
        $reqStructure->execute(array('idSuprMark' => $idSupr));
        $reqStructure->closeCursor();
        
        header('location: modifSuprStruct.php');
    }
    
    
    // Sélectionne une structure selon un id
    public function selectionId($id)
    {
        $db = dbConnect();
        
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Struct = :idMark");
        $req->execute(array('idMark' => $id));
        
        $reqFetch = $req->fetch();
        
        return $reqFetch;
    }
}