<?php

// Démarre la session
session_start();

if (!isset($_SESSION['logged']) || !$_SESSION['logged'])
{
    header("location: ../pages/accueilBis.php?error=3");
}

$login = isset($_SESSION['login']) ? $_SESSION['login'] : '';
