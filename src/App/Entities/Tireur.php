<?php

class Tireur {

    // Attributs
    protected $Id_Tireur;
    protected $Id_Tir_Niv_Tireur;
    protected $Nom_Tireur;
    protected $Prenom_Tireur;
    protected $Date_Naissance;
    protected $Num_Licence;
    protected $Poids_Tireur;
    protected $Id_Tir_Cat_Poids;
    protected $Id_Tir_Cat_Age;
    protected $Id_Tir_Sexe;
    protected $Id_Tir_Struct;
    protected $Inactif_Tireur;


        // Constructeur
    public function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas = NULL) {
        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Appelle le mutateur des attributs existant dans le tableau $datas
        foreach ($attrib as $key => $val) {
            // Si, dans vos habitudes, les champs de la bdd sont en minuscules, décommentez la ligne suivante.
            //$key = strtolower($key);
            // Si, dans vos habitudes, les champs de la bdd sont en majuscules, décommentez la ligne suivante.
            //$key = strtoupper($key);
            //  Perso, je met tout en notation camelCase... même dans la BDD.
            if (isset($datas[$key])) {
                $mutateur = 'set' . $key;
                $this->$mutateur($datas[$key]);
            }
        }

        return $this;
    }
    
            /**
     * Transforme un objet en un tableau associatif.
     * @return array tableau associatif des attributs => valeurs
     */
    public function __toArray() {
        return $this->jsonSerialize();
    }

    /**
     * La classe implémente l'interface JsonSerializable ce qui permet 
     * aux classes concrête de pouvoir être sérialisée en JSON avec json_encode()
     * @return Array le tableau pour conversion en json
     */
    public function jsonSerialize() {
        $array = array();

        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Associe la clé du nom de l'attribut à la valeur de cet attribut
        foreach ($attrib as $key => $val) {
            $array[$key] = $this->get($key);
        }
        return $array;
    }

    /**
     * Lecteur simple d'accés en lecture à un attribut
     * Peut être surchargé.
     * @param type $attribut le nom de l'attribut
     * @return type la valeur de l'attribut
     */
    protected function get($attribut) {
        return $this->$attribut;
    }
    
        /**
     * Surcharge du mutateur pour l'attribut $id qui ne peut pas être modifié.
     * @param int $val ne sert que si id_pers est null...
     */
    public function setid_pers($val=null)
    {
	if($this->Id_Tireur === null)
	    $this->Id_Tireur = (int)$val;
    }
    
    // Ces deux méthodes ne sont nécessaire QUE si la clé primaire dans la table
    // porte un nom différent de id...
    /**
     * Permet de s'affranchir du fait que le champ id dans la table est différent...
     * @param type $val
     */
    public function setid($val=null)
    {
	$this->setid_pers($val);
    }
    

    // GET et SET
    function getId_Tireur() {
        return $this->Id_Tireur;
    }

    function getId_Tir_Niv_Tireur() {
        return $this->Id_Tir_Niv_Tireur;
    }

    function getNom_Tireur() {
        return $this->Nom_Tireur;
    }

    function getPrenom_Tireur() {
        return $this->Prenom_Tireur;
    }

    function getDate_Naissance() {
        return $this->Date_Naissance;
    }

    function getNum_Licence() {
        return $this->Num_Licence;
    }

    function getPoids_Tireur() {
        return $this->Poids_Tireur;
    }

    function getId_Tir_Cat_Poids() {
        return $this->Id_Tir_Cat_Poids;
    }

    function getId_Tir_Cat_Age() {
        return $this->Id_Tir_Cat_Age;
    }

    function getId_Tir_Sexe() {
        return $this->Id_Tir_Sexe;
    }

    function getId_Tir_Struct() {
        return $this->Id_Tir_Struct;
    }

    function setId_Tireur($Id_Tireur): void {
        $this->Id_Tireur = $Id_Tireur;
    }

    function setId_Tir_Niv_Tireur($Id_Tir_Niv_Tireur): void {
        $this->Id_Tir_Niv_Tireur = $Id_Tir_Niv_Tireur;
    }

    function setNom_Tireur($Nom_Tireur): void {
        $this->Nom_Tireur = $Nom_Tireur;
    }

    function setPrenom_Tireur($Prenom_Tireur): void {
        $this->Prenom_Tireur = $Prenom_Tireur;
    }

    function setDate_Naissance($Date_Naissance): void {
        $this->Date_Naissance = $Date_Naissance;
    }

    function setNum_Licence($Num_Licence): void {
        $this->Num_Licence = $Num_Licence;
    }

    function setPoids_Tireur($Poids_Tireur): void {
        $this->Poids_Tireur = $Poids_Tireur;
    }

    function setId_Tir_Cat_Poids($Id_Tir_Cat_Poids): void {
        $this->Id_Tir_Cat_Poids = $Id_Tir_Cat_Poids;
    }

    function setId_Tir_Cat_Age($Id_Tir_Cat_Age): void {
        $this->Id_Tir_Cat_Age = $Id_Tir_Cat_Age;
    }

    function setId_Tir_Sexe($Id_Tir_Sexe): void {
        $this->Id_Tir_Sexe = $Id_Tir_Sexe;
    }

    function setId_Tir_Struct($Id_Tir_Struct): void {
        $this->Id_Tir_Struct = $Id_Tir_Struct;
    }
    
    function getInactif_Tireur() {
        return $this->Inactif_Tireur;
    }

    function setInactif_Tireur($Inactif_Tireur): void {
        $this->Inactif_Tireur = $Inactif_Tireur;
    }

}
