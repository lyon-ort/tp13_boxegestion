<?php

require '../config/config.php';

// Déclaration StructureRepository
$structureRepository = new StructureRepository();

// Déclaration du tableau des valeurs
$valeurStructure = array(
    //"Id_Struct" => 7,
    "Id_Typ_Struct" => 1,
    "Nom_Struct" => "test",
    "Adresse_Struct" => "adresse",
    "Cp_Struct" => "69580",
    "Ville_Struct" => "ville",
    "Id_Responsable_Struct" => 1,
    "Tel_Fixe" => "0640898876",
    "Tel_Port" => "0640898876"
);

// Initialisation et dump() des valeurs
$structure = new Structure($valeurStructure);
var_dump($structure);

// Fonction sauver pour créer ou modifier une structure
$structureRepository->sauver($structure);

