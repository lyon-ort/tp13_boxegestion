<?php

require '../config/config.php';

$db = dbConnect();

$req = $db->prepare('SELECT * FROM tableau WHERE Id_Tableau_Competition = :Id_CompetitionMark');
$req->execute(array('Id_CompetitionMark' => 3));
$data = $req->fetchAll();


// Sélection de tous les tireurs
$listeTireur = array();
$y = 0;
for ($i = 0; $i != count($data); $i++)
{
    $req3 = $db->prepare('SELECT Id_Particip_Tireur FROM participer WHERE Id_Particip_Tableau = :Id_TabMark');
    $req3->execute(array('Id_TabMark' => $data[$i]['Id_Tab']));
    
    while ($data3 = $req3->fetch())
    {
        $listeTireur[$y] = $data3['Id_Particip_Tireur'];
        $y++;
    }
}


// Récupération de la structure de la compétition
$req2 = $db->prepare('SELECT Id_Competition_Struct FROM competition WHERE Id_Competition = :Id_CompetitionMark');
$req2->execute(array('Id_CompetitionMark' => 3));
$data2 = $req2->fetch();

// Récupération des structures des tireurs de la compétition
$listeStruct = array();
$y = 0;
for ($i = 0; $i != count($listeTireur); $i++)
{
    $req2Bis = $db->prepare('SELECT Id_Tir_Struct FROM tireur WHERE Id_Tireur = :Id_TireurMark');
    $req2Bis->execute(array('Id_TireurMark' => $listeTireur[$i]));
    
    while ($data3Bis = $req2Bis->fetch())
    {
        $listeStruct[$y] = $data3Bis['Id_Tir_Struct'];
        $y++;
    }
}

// Rajout de la dernière case (compétition de la structure)
//array_push($listeStruct, $data2);

for ($i = 0; $i != count($listeStruct); $i++)
{
    $select7 = $db->prepare('SELECT * FROM structure WHERE Id_Struct = :Id_StructMark');
    $select7->execute(array('Id_StructMark' => $listeStruct[$i]));
    
    $test = $select7->fetchAll();
    var_dump($test);
}


        