<?php

class Structure {

    // Attributs
    protected $Id_Struct;
    protected $Id_Typ_Struct;
    protected $Nom_Struct;
    protected $Adresse_Struct;
    protected $Cp_Struct;
    protected $Ville_Struct;
    protected $Id_Responsable_Struct;
    protected $Tel_Fixe;
    protected $Tel_Port;
    protected $Inactif_Struct;

    // Constructeur
    public function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas = NULL) {
        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Appelle le mutateur des attributs existant dans le tableau $datas
        foreach ($attrib as $key => $val) {
            // Si, dans vos habitudes, les champs de la bdd sont en minuscules, décommentez la ligne suivante.
            //$key = strtolower($key);
            // Si, dans vos habitudes, les champs de la bdd sont en majuscules, décommentez la ligne suivante.
            //$key = strtoupper($key);
            //  Perso, je met tout en notation camelCase... même dans la BDD.
            if (isset($datas[$key])) {
                $mutateur = 'set' . $key;
                $this->$mutateur($datas[$key]);
            }
        }

        return $this;
    }
    
                /**
     * Transforme un objet en un tableau associatif.
     * @return array tableau associatif des attributs => valeurs
     */
    public function __toArray() {
        return $this->jsonSerialize();
    }

    /**
     * La classe implémente l'interface JsonSerializable ce qui permet 
     * aux classes concrête de pouvoir être sérialisée en JSON avec json_encode()
     * @return Array le tableau pour conversion en json
     */
    public function jsonSerialize() {
        $array = array();

        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Associe la clé du nom de l'attribut à la valeur de cet attribut
        foreach ($attrib as $key => $val) {
            $array[$key] = $this->get($key);
        }
        return $array;
    }

    /**
     * Lecteur simple d'accés en lecture à un attribut
     * Peut être surchargé.
     * @param type $attribut le nom de l'attribut
     * @return type la valeur de l'attribut
     */
    protected function get($attribut) {
        return $this->$attribut;
    }
    
        /**
     * Surcharge du mutateur pour l'attribut $id qui ne peut pas être modifié.
     * @param int $val ne sert que si id_pers est null...
     */
    public function setid_pers($val=null)
    {
	if($this->Id_Struct === null)
	    $this->Id_Struct = (int)$val;
    }
    
    // Ces deux méthodes ne sont nécessaire QUE si la clé primaire dans la table
    // porte un nom différent de id...
    /**
     * Permet de s'affranchir du fait que le champ id dans la table est différent...
     * @param type $val
     */
    public function setid($val=null)
    {
	$this->setid_pers($val);
    }

    // GET et SET
    function getId_Struct() {
        return $this->Id_Struct;
    }

    function getId_Typ_Struct() {
        return $this->Id_Typ_Struct;
    }

    function getNom_Struct() {
        return $this->Nom_Struct;
    }

    function getAdresse_Struct() {
        return $this->Adresse_Struct;
    }

    function getCp_Struct() {
        return $this->Cp_Struct;
    }

    function getVille_Struct() {
        return $this->Ville_Struct;
    }

    function getId_Responsable_Struct() {
        return $this->Id_Responsable_Struct;
    }

    function getTel_Fixe() {
        return $this->Tel_Fixe;
    }

    function getTel_Port() {
        return $this->Tel_Port;
    }

    function setId_Struct($Id_Struct): void {
        $this->Id_Struct = $Id_Struct;
    }

    function setId_Typ_Struct($Id_Typ_Struct): void {
        $this->Id_Typ_Struct = $Id_Typ_Struct;
    }

    function setNom_Struct($Nom_Struct): void {
        $this->Nom_Struct = $Nom_Struct;
    }

    function setAdresse_Struct($Adresse_Struct): void {
        $this->Adresse_Struct = $Adresse_Struct;
    }

    function setCp_Struct($Cp_Struct): void {
        $this->Cp_Struct = $Cp_Struct;
    }

    function setVille_Struct($Ville_Struct): void {
        $this->Ville_Struct = $Ville_Struct;
    }

    function setId_Responsable_Struct($Id_Responsable_Struct): void {
        $this->Id_Responsable_Struct = $Id_Responsable_Struct;
    }

    function setTel_Fixe($Tel_Fixe): void {
        $this->Tel_Fixe = $Tel_Fixe;
    }

    function setTel_Port($Tel_Port): void {
        $this->Tel_Port = $Tel_Port;
    }
    
    function getInactif_Struct() {
        return $this->Inactif_Struct;
    }

    function setInactif_Struct($Inactif_Struct): void {
        $this->Inactif_Struct = $Inactif_Struct;
    }
}
