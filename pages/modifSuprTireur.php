<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';


// Modification d'un tireur
if (isset($_POST['Id_TireurModif']))
{
    // Test si le numéro de licence existe déjà
    $objetTireurRep2 = new TireurRepository();
    $arrayObjetTireur2 = $objetTireurRep2->selectionTireur();

    $confirmNumLicence = true;

    foreach ($arrayObjetTireur2 as $value)
    {
        if ($value->getNum_Licence() != $_POST['NumeDeBase'])
        {
            if ($value->getNum_Licence() == $_POST['Num_LicenceModif'])
            {
                $confirmNumLicence = false;
            }
        }

    }
                
    if ($confirmNumLicence)
    {
        // CATEGORIE DE POIDS
        $objetCategoriePoids2 = new CategoriePoidsRepository();
        $arrayCategoriePoids2 = $objetCategoriePoids2->selectionCategoriePoids();

        // Vérification
        $idCatPoids = 0;

        foreach ($arrayCategoriePoids2 as $value)
        {
            if ($_POST['Poids_TireurModif'] > $value->getPoids_Min() && $_POST['Poids_TireurModif'] < $value->getPoids_Max() || 
                $_POST['Poids_TireurModif'] == $value->getPoids_Min() || 
                $_POST['Poids_TireurModif'] == $value->getPoids_Max())
            {
                $idCatPoids = $value->getId_Cat_Poids();
            }
        }
        
        // Exception
        // 18 = poids lourds
        if ($_POST['Poids_TireurModif'] > 86)
        {
            $idCatPoids = 18; 
        }
        // 1 = moustique
        if ($_POST['Poids_TireurModif'] < 24)
        {
            $idCatPoids = 1; 
        }
        
        
        // CATEGORIE AGE
        $dateNaissance = (int)$_POST['Date_NaissanceModif'];

        $age = date('Y') - $dateNaissance;
        if (date('md') < date('md', strtotime($_POST['Date_NaissanceModif']))) 
        { 
            $ageReturn = $age - 1; 
        } 
        else 
        {
            $ageReturn = $age;
        }            

        //Défini quel catégorie d'age
        $objetCategorieAgeRep = new CategorieAgeRepository();
        $arrayCategorieAge = $objetCategorieAgeRep->selectionCategorieAge();

        // Vérification
        $idCatAge = 0;

        foreach ($arrayCategorieAge as $value)
        {
            if ($ageReturn > $value->getAge_Min() && $ageReturn < $value->getAge_Max() || 
                $ageReturn == $value->getAge_Min() || 
                $ageReturn == $value->getAge_Max())
            {
                $idCatAge = $value->getId_Cat_Age();
            }
        }
        
        // Exception
        // pré-poussins
        if ($ageReturn < 9)
        {
            $idCatAge = 1;
        }
        // Vétérant assaut et combat ne sont pas géré


        // CREATION
        $valeurTireur = array(
            "Id_Tireur" => $_POST['Id_TireurModif'],
            "Id_Tir_Niv_Tireur" => $_POST['Id_Tir_Niv_TireurModif'],
            "Nom_Tireur" => $_POST['Nom_TireurModif'],
            "Prenom_Tireur" => $_POST['Prenom_TireurModif'],
            "Date_Naissance" => $_POST['Date_NaissanceModif'],
            "Num_Licence" => $_POST['Num_LicenceModif'],
            "Poids_Tireur" => $_POST['Poids_TireurModif'],
            "Id_Tir_Cat_Poids" => $idCatPoids,
            "Id_Tir_Cat_Age" => $idCatAge,
            "Id_Tir_Sexe" => $_POST['Id_Tir_SexeModif'],
            "Id_Tir_Struct" => $_POST['Id_Tir_StructModif'],
            "Inactif_Tireur" => 0
        );

        $objetTireur2 = new Tireur($valeurTireur);
        $objetTireurRep2->sauver($objetTireur2);    

        // Rafraichissement de la page
        header('location: modifSuprTireur.php?connect=1&message=1');
    }
    else
    {
        ?>
            <!-- Renvoie vers la page de modif -->
            <form name="form" action="modifTireur.php?connect=1" method="POST"> 
                <input type="hidden" name="Id_Tireur" value="<?= $_POST['Id_TireurModif'] ?>"/>
                <input type="hidden" name="Id_Tir_Niv_Tireur" value="<?= $_POST['Id_Tir_Niv_TireurModif'] ?>"/>
                <input type="hidden" name="NumeDeBase" value="<?= $_POST['NumeDeBase'] ?>"/>
                <input type="hidden" name="Nom_Tireur" value="<?= $_POST['Nom_TireurModif'] ?>"/>
                <input type="hidden" name="Prenom_Tireur" value="<?= $_POST['Prenom_TireurModif'] ?>"/>
                <input type="hidden" name="Date_Naissance" value="<?= $_POST['Date_NaissanceModif'] ?>"/>
                <input type="hidden" name="Num_Licence" value="<?= $_POST['Num_LicenceModif'] ?>"/>
                <input type="hidden" name="Poids_Tireur" value="<?= $_POST['Poids_TireurModif'] ?>"/>
                <input type="hidden" name="Id_Tir_Sexe" value="<?= $_POST['Id_Tir_SexeModif'] ?>"/>
                <input type="hidden" name="error" />
            </form>

            <!-- Script qui renvoie les variables POST vers la page de modification -->
            <script type="text/javascript"> 
                document.form.submit(); //on envoie le formulaire vers la page de modification
            </script> 
        <?php            
    }
}



// Suppression d'un tireur
if (isset($_POST['Id_TireurSupr']))
{
    // UTILISATEUR
    $objetTireurRep2 = new TireurRepository();
    $objetTireur2 = $objetTireurRep2->selectionTireurId($_POST['Id_TireurSupr']);
    
    // Modifie les "Flag" et supprime les utilisateurs de la structure
    $objetTireur2->setInactif_Tireur(1);
    $objetTireurRep2->sauver($objetTireur2);
    
    // Rafraichissement de la page
    header('location: modifSuprTireur.php?message=2&connect=1');
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Modifier / Suprimmer tireur </title>
    </head>
    
    <body>
        <!-- Header (inc) -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>
        

        
        <!-- Section -->
        <section class='container-fluid about'>
            
            <!-- Information de l'utilisateur (inc) -->
            <?php
            include '../inc/information.php';
            ?>
            
            <h1> Liste tireurs </h1>
            
            <hr class="separator">
            
            <?php
            // Message de confirmation de modification
            if (isset($_GET['message']) && $_GET['message'] == 1)
            {
                    ?>
                            <!-- Pop up de confirmation de suppression -->
                            <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                                <div class="modal-content" style="height: 250px;">
                                    <!-- bouton fermer -->
                                    <div id="close" class="closeTest">+</div>

                                    <!-- Confirmation de suppression -->
                                    <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                                    Le tireur a été modifié !<br/><br/>

                                    <button class="boutonAnnulerTest"> OK </button>
                                </div>
                            </div>   

                             <script>
                                // La croix (ferme le pop-up)
                                document.querySelector('.closeTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });      

                                // Le bouton annuler (ferme le pop-up)
                                document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });       
                            </script>            
                    <?php
            }
            // Message de confirmation de supression
            else if (isset($_GET['message']) && $_GET['message'] == 2)
            {
                    ?>
                            <!-- Pop up de confirmation de suppression -->
                            <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                                <div class="modal-content" style="height: 250px;">
                                    <!-- bouton fermer -->
                                    <div id="close" class="closeTest">+</div>

                                    <!-- Confirmation de suppression -->
                                    <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                                    Le tireur a été supprimé !<br/><br/>

                                    <button class="boutonAnnulerTest"> OK </button>
                                </div>
                            </div>   

                             <script>
                                // La croix (ferme le pop-up)
                                document.querySelector('.closeTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });      

                                // Le bouton annuler (ferme le pop-up)
                                document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });       
                            </script>            
                    <?php          
            }
            ?>       
            
            <table>
                
                <th> Prénom / Nom </th> 
                <th> Sexe </th> 
                <th> Catégorie age </th> 
                <th> Catégorie poids </th> 
                <th> Modification </th> 
                <th> Suprimmer </th>
                
                <?php
                    $objetTireurRep = new TireurRepository();
                    $arrayObjetTireur = $objetTireurRep->selectionArrayTireurIdStructure($_SESSION['idStruct']);
                    
                    $i = 0;
                    
                    if ($arrayObjetTireur)
                    {
                        foreach ($arrayObjetTireur as $value)
                        {
                ?>   
                            <tr>
                                <!-- Prénom - Nom -->
                                <td>
                                    <?= $value->getPrenom_Tireur() . ' ' . $value->getNom_Tireur() ?>
                                </td>

                                <!-- Sexe -->
                                <td>
                                    <?php
                                    $objetSexeRep = new SexeRepository();
                                    $objetSexe = $objetSexeRep->selectionSexeId($value->getId_Tir_Sexe());
                                    echo $objetSexe->getLibelle_Sexe();
                                    ?>
                                </td>

                                <!-- Catégorie age -->
                                <td>
                                    <?php
                                    $objetCategorieAgeRep = new CategorieAgeRepository();
                                    $objetCategorieAge = $objetCategorieAgeRep->selectionCategorieAgeId($value->getId_Tir_Cat_Age());
                                    echo $objetCategorieAge->getLibelle_Cat_Age();
                                    ?>
                                </td>

                                <!-- Catégorie poids -->
                                <td>
                                    <?php
                                    $objetCategoriePoidsRep = new CategoriePoidsRepository();
                                    $objetCategoriePoids = $objetCategoriePoidsRep->selectionCategoriePoidsId($value->getId_Tir_Cat_Poids());
                                    echo $objetCategoriePoids->getLibelle_Cat_Poids();
                                    ?>
                                </td>  

                                <!-- Modifier -->
                                <td>
                                    <form name="form<?= $i ?>" method="POST" action="modifTireur.php?connect=1">
                                        <input type="hidden" name="Id_Tireur" value="<?= $value->getId_Tireur() ?>"/>
                                    </form>                 
                                    <h3> <button class="boutonModifierBleu" id="boutonModifier<?= $i ?>"> Modifier </button> </h3>
                                </td>

                                <!-- Supprimer -->
                                <td>
                                    <form  method="POST" action="SuprTireur.php<?= $leGet ?>">
                                        <input type="hidden" name="Id_Tireur" value="<?= $value->getId_Tireur() ?>"/>
                                    </form>  
                                    <h3> <button class="classBoutonSupprimerRouge" id="boutonSupprimer<?= $i ?>"> X </button> </h3>
                                </td>                    
                            </tr>


                            <!-- Pop up de suppression -->
                            <div id="bg-modal" class="bg-modal<?= $i ?>">
                                <div class="modal-content">
                                    <!-- bouton fermer -->
                                    <div id="close" class="close<?= $i ?>">+</div>

                                    <!-- Confirmation de suppression -->
                                    <img width="50px" height="50px" src="../public/img/pictogramBoxe.png" alt=""/><br/>

                                    Voulez-vous vraiment supprimer ce tireur ?<br/><br/>

                                    <form name="form" action="modifSuprTireur.php?connect=1" method="POST"> 
                                        <input type="hidden" name="Id_TireurSupr" value="<?= $value->getId_Tireur() ?>"/>
                                        <button> Supprimer </button> <br/>
                                    </form>

                                    <button class="boutonAnnuler<?= $i ?>"> Annuler </button>
                                </div>
                            </div>                        

                            <!-- JS -->
                            <script type="text/javascript">                 

                                // Apparition du pop-up
                                document.getElementById('boutonSupprimer<?= $i ?>').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modal<?= $i ?>').style.display = 'flex';
                                });                    

                                // La croix (ferme le pop-up)
                                document.querySelector('.close<?= $i ?>').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modal<?= $i ?>').style.display = 'none';
                                });      

                                // Le bouton annuler (ferme le pop-up)
                                document.querySelector('.boutonAnnuler<?= $i ?>').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modal<?= $i ?>').style.display = 'none';
                                }); 
                                
                                // Envoie vers la page de modification
                                document.getElementById('boutonModifier<?= $i ?>').addEventListener('click', function() 
                                {
                                    //document.querySelector('.bg-modal<?= $i ?>').style.display = 'flex';
                                    document.form<?= $i ?>.submit();
                                });  
                            </script>
                <?php
                            $i++;
                        }
                    }
                ?>
            </table>
            <br/>
        </section>  
    </body>
</html>