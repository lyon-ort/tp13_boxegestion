<?php

class CategoriePoids {

    // Attributs
    protected $Id_Cat_Poids;
    protected $Libelle_Cat_Poids;
    protected $Poids_Max;
    protected $Poids_Min;

    // Constructeur
    public function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas = NULL) {
        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Appelle le mutateur des attributs existant dans le tableau $datas
        foreach ($attrib as $key => $val) {
            // Si, dans vos habitudes, les champs de la bdd sont en minuscules, décommentez la ligne suivante.
            //$key = strtolower($key);
            // Si, dans vos habitudes, les champs de la bdd sont en majuscules, décommentez la ligne suivante.
            //$key = strtoupper($key);
            //  Perso, je met tout en notation camelCase... même dans la BDD.
            if (isset($datas[$key])) {
                $mutateur = 'set' . $key;
                $this->$mutateur($datas[$key]);
            }
        }

        return $this;
    }

    // GET et SET
    function getId_Cat_Poids() {
        return $this->Id_Cat_Poids;
    }

    function getLibelle_Cat_Poids() {
        return $this->Libelle_Cat_Poids;
    }

    function getPoids_Max() {
        return $this->Poids_Max;
    }

    function getPoids_Min() {
        return $this->Poids_Min;
    }

    function setId_Cat_Poids($Id_Cat_Poids): void {
        $this->Id_Cat_Poids = $Id_Cat_Poids;
    }

    function setLibelle_Cat_Poids($Libelle_Cat_Poids): void {
        $this->Libelle_Cat_Poids = $Libelle_Cat_Poids;
    }

    function setPoids_Max($Poids_Max): void {
        $this->Poids_Max = $Poids_Max;
    }

    function setPoids_Min($Poids_Min): void {
        $this->Poids_Min = $Poids_Min;
    }

}
