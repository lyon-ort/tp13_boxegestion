<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Tableaux compétition </title>
    </head>
    
    <body>
        <!-- Header (inc) -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>
        
        
        <!-- Section -->
        <section class='container-fluid about'>
            
            <!-- Information de l'utilisateur (inc) -->
            <?php
            include '../inc/information.php';
            ?>
            
            <h1> Les tableaux </h1>
            
            <hr class="separator">
            
            <br/>
            
            <?php
            // Si LES tableaux sont consulter avec le bouton "Les tableaux"
            if (isset($_POST['Id_Competition']))
            {
                // initialisation de l'id compétition
                $idCompet = $_POST['Id_Competition'];

                // séléction des tableaux selon la compétition
                $objetTableauxRep = new TableauRepository();
                
                // Active le code si il y a au moins un tableau dans la compétition
                if ($objetTableauxRep->selectionTableauxIdCompet($idCompet))
                {
                    // Affiche une liste déroulante avec tous les tableaux de la compétition
                    $arrayTableaux = $objetTableauxRep->selectionTableauxIdCompet($idCompet);
                    ?>
                        <form method="POST" class="formCreat" action="detailCompetitionConnect.php?connect=1">
                            <select name="idTableauSelect" required>
                                <option value=""> -- Choisir un tableau -- </option>
                                <?php
                                    foreach($arrayTableaux as $value)
                                    {
                                    ?>
                                        <option value="<?= $value->getId_Tab() ?>"> <?= $value->getLibelle_Tab() ?> </option>
                                    <?php
                                    }
                                ?>
                            </select>
                                
                            <input type="hidden" name="Id_Competition" value="<?= $_POST['Id_Competition'] ?>"/>
                            <input type="submit" value="Visualisation"/>
                        </form>
                    <?php
                    
                    // Active le code si un tableau a été sélectionné
                    if (isset($_POST['idTableauSelect']))
                    {
                        $typeTableauRepo = new TableauRepository;
                        $typeTableauAffich = $typeTableauRepo->typeTableau($_POST['idTableauSelect']);
                        
                        $idTableau = $_POST['idTableauSelect'];
                        
                        if ($typeTableauAffich[0]['Id_Tableau_Typ_Tableau'] == 1)
                        {
                            $leTableauChoisi = $objetTableauxRep->selectionTableauxId($idTableau);

                            echo '<br/>';
                            // Caractéristique du tableau
                            echo '<form class="formCreat">';     

                            echo 'Nom : ' . strip_tags($leTableauChoisi->getLibelle_Tab()) . '<br/>';

                            echo 'Nombre max de rencontre : ' . strip_tags($leTableauChoisi->getNb_Max_Rencontre()) . '<br/>';

                            echo 'Sexe : ';
                            $objetSexeRep = new SexeRepository();
                            $tableauSexe = $objetSexeRep->selectionSexeId($leTableauChoisi->getId_Tableau_Sexe());
                            echo $tableauSexe->getLibelle_Sexe();
                            echo '<br/>';

                            echo 'Type d\'affrontement : ';
                            $objetTypeAffrontementRep = new TypeAffrontementRepository();
                            $tableauTypeAffrontement = $objetTypeAffrontementRep->selectionTypeAffrontementId($leTableauChoisi->getId_Tableau_Typ_Affrontement());
                            echo $tableauTypeAffrontement->getLibelle_Typ_Affrontement();
                            echo '<br/>';

                            echo 'Type de tableau : ';
                            $objetTypeTableauRep = new TypeTableauRepository();
                            $tableauTypeTableau = $objetTypeTableauRep->selectionTypeTableauId($leTableauChoisi->getId_Tableau_Typ_Tableau());
                            echo $tableauTypeTableau->getLibelle_Typ_Tab();
                            echo '<br/>';

                            echo 'Catégorie de Poids : ';
                            // Sélectionne la catégorie de poids du tableau
                            $objetTableauPoidsRep2 = new TableauPoidsRepository();
                            $objetTableauPoids2 = $objetTableauPoidsRep2->selectionTableauPoidsIdTab($leTableauChoisi->getId_Tab());
                            $objetCategoriePoidsRep2 = new CategoriePoidsRepository();
                            $objetCategoriePoids2 = $objetCategoriePoidsRep2->selectionCategoriePoidsId($objetTableauPoids2->getId_Poids_Cat());   
                            echo $objetCategoriePoids2->getLibelle_Cat_Poids();
                            echo '<br/>';
                            
                            echo 'Catégorie d\'age : ';
                            // Sélectionne la catégorie d'age du tableau
                            $objetConcerneRep2 = new ConcerneRepository();
                            $objetConcerne2 = $objetConcerneRep2->selectionConcerneIdTab($leTableauChoisi->getId_Tab());
                            $objetCategorieAgeRep2 = new CategorieAgeRepository();
                            $objetCategorieAge2 = $objetCategorieAgeRep2->selectionCategorieAgeId($objetConcerne2->getId_Concerne_Cat_Age());  
                            echo $objetCategorieAge2->getLibelle_Cat_Age();
                            echo '<br/>';
                            
                            echo 'Compétition : ';
                            $objetCompetitionRep = new CompetitionRepository();
                            $tableauCompetition = $objetCompetitionRep->selectionCompetitionId($leTableauChoisi->getId_Tableau_Competition());
                            echo $tableauCompetition->getNom_Competition();
                            echo '<br/>';

                            echo '</form> <br/>';                            

                            $objetTireurRep = new TireurRepository();
                            $objetRencontreRep = new RencontreRepository();
                            $objetStructureRep = new StructureRepository();
                            $objetCategorieRencontreRep = new CategorieRencontreRepository();
                            
                            if ($objetRencontreRep->selectionRencontreIdTableau($idTableau))
                            {
                                $objetRencontreAffichage = $objetRencontreRep->selectionRencontreIdTableau($idTableau);

                                $j = 0;

                                $categorieRencontre = $objetRencontreAffichage[0]->getId_Cat_Rencontre();

                                for ($i = 0; $i < count($objetRencontreAffichage); $i++)
                                {
                                    $categorieRencontre1 = $categorieRencontre;

                                    $categorieRencontre = $objetRencontreAffichage[$i]->getId_Cat_Rencontre();

                                    if ($categorieRencontre != $categorieRencontre1)
                                    {
                                        $j = 0;
                                        //changer le css pour faire un espace vers le bas de la page entre les deux rencontres qui la compose
                                        //<style type
                                    }                                
                                    if ($j == 0)
                                    {
                                        for ($j = 0; $j < 1; $j++)
                                        {
                                        ?><br/>
                                        <h4 style="text-align: center;"> 
                                            <?php
                                            $objetCategorieRencontre = $objetCategorieRencontreRep->selectionCategorieRencontreId($categorieRencontre);
                                            echo $objetCategorieRencontre->getLibelle_Cat_Rencontre();
                                            ?> 
                                        </h4>
            
                                        <?php
                                        }
                                    }

                                    //Les tireurs
                                    $objetTireurRouge = $objetTireurRep->selectionTireurId($objetRencontreAffichage[$i]->getId_Tireur_Rouge());
                                    $objetTireurBleu = $objetTireurRep->selectionTireurId($objetRencontreAffichage[$i]->getId_Tireur_Bleu());
                                    // Nom Prénom
                                    $PrenomNomRouge = ($objetTireurRouge) ? $objetTireurRouge->getPrenom_Tireur() . " " . $objetTireurRouge->getNom_Tireur() : "";
                                    $PrenomNomBleu = ($objetTireurRouge) ? $objetTireurBleu->getPrenom_Tireur() . " " . $objetTireurBleu->getNom_Tireur() : "";

                                    // Initialisation
                                    $StructureRouge = '';
                                    $StructureBleu = '';

                                    // les structures (^O^)
                                    if ($objetTireurRouge && $objetTireurBleu)
                                    {   
                                        // Structure
                                        $objetStructureRouge = $objetStructureRep->selectionStructureId($objetTireurRouge->getId_Tir_Struct());
                                        $objetStructureBleu = $objetStructureRep->selectionStructureId($objetTireurBleu->getId_Tir_Struct());
                                        // Nom
                                        $StructureRouge = ($objetStructureRouge) ? " (" . $objetStructureRouge->getNom_Struct() . ")" : "";
                                        $StructureBleu = ($objetStructureBleu) ? " (" . $objetStructureBleu->getNom_Struct() . ")" : "";                                    
                                    }

                                    ?>
                                        <br/>
                                        <div style="text-align: center;">
                                            <input style="width: auto;" type="text" value="<?= $PrenomNomRouge . $StructureRouge ?> " readonly />
                                            VS
                                            <input style="width: auto;" type="text" value="<?= $PrenomNomBleu . $StructureBleu ?>" readonly />
                                        </div>
                                        <br/>
                                    <?php

                                }
                            }
                            else
                            {
                                echo '<p>Il n\'y a pas de rencontres pour le moment</p>';
                            }
                        }
                        else if ($typeTableauAffich[0]['Id_Tableau_Typ_Tableau'] == 2)
                        {
                            $leTableauChoisi = $objetTableauxRep->selectionTableauxId($idTableau);

                            echo '<br/>';
                            // Caractéristique du tableau
                            echo '<form class="formCreat">';     

                            echo 'Nom : ' . strip_tags($leTableauChoisi->getLibelle_Tab()) . '<br/>';

                            echo 'Nombre max de rencontre : ' . strip_tags($leTableauChoisi->getNb_Max_Rencontre()) . '<br/>';

                            echo 'Sexe : ';
                            $objetSexeRep = new SexeRepository();
                            $tableauSexe = $objetSexeRep->selectionSexeId($leTableauChoisi->getId_Tableau_Sexe());
                            echo $tableauSexe->getLibelle_Sexe();
                            echo '<br/>';

                            echo 'Type d\'affrontement : ';
                            $objetTypeAffrontementRep = new TypeAffrontementRepository();
                            $tableauTypeAffrontement = $objetTypeAffrontementRep->selectionTypeAffrontementId($leTableauChoisi->getId_Tableau_Typ_Affrontement());
                            echo $tableauTypeAffrontement->getLibelle_Typ_Affrontement();
                            echo '<br/>';

                            echo 'Type de tableau : ';
                            $objetTypeTableauRep = new TypeTableauRepository();
                            $tableauTypeTableau = $objetTypeTableauRep->selectionTypeTableauId($leTableauChoisi->getId_Tableau_Typ_Tableau());
                            echo $tableauTypeTableau->getLibelle_Typ_Tab();
                            echo '<br/>';

                            echo 'Catégorie de Poids : ';
                            // Sélectionne la catégorie de poids du tableau
                            $objetTableauPoidsRep2 = new TableauPoidsRepository();
                            $objetTableauPoids2 = $objetTableauPoidsRep2->selectionTableauPoidsIdTab($leTableauChoisi->getId_Tab());
                            $objetCategoriePoidsRep2 = new CategoriePoidsRepository();
                            $objetCategoriePoids2 = $objetCategoriePoidsRep2->selectionCategoriePoidsId($objetTableauPoids2->getId_Poids_Cat());   
                            echo $objetCategoriePoids2->getLibelle_Cat_Poids();
                            echo '<br/>';
                            
                            echo 'Catégorie d\'age : ';
                            // Sélectionne la catégorie d'age du tableau
                            $objetConcerneRep2 = new ConcerneRepository();
                            $objetConcerne2 = $objetConcerneRep2->selectionConcerneIdTab($leTableauChoisi->getId_Tab());
                            $objetCategorieAgeRep2 = new CategorieAgeRepository();
                            $objetCategorieAge2 = $objetCategorieAgeRep2->selectionCategorieAgeId($objetConcerne2->getId_Concerne_Cat_Age());  
                            echo $objetCategorieAge2->getLibelle_Cat_Age();
                            echo '<br/>';                            
                            
                            echo 'Compétition : ';
                            $objetCompetitionRep = new CompetitionRepository();
                            $tableauCompetition = $objetCompetitionRep->selectionCompetitionId($leTableauChoisi->getId_Tableau_Competition());
                            echo $tableauCompetition->getNom_Competition();
                            echo '<br/>';

                            echo '</form> <br/>';  

                            $objetRencontreRep = new RencontreRepository();                        
                            // Active le code si il y a au moins une rencontre dans le tableau
                            if ($objetRencontreRep->selectionRencontreIdTableau($idTableau))
                            {
                                $arrayRencontre = $objetRencontreRep->selectionRencontreIdTableau($idTableau);

                                // Affiche un tableau HTML avec toutes les rencontres du tableau
                                ?>
                                <table>
                                    <th style="background-color: red;"> Coin rouge </th> 
                                    <th style="background-color: blue;"> Coin bleu </th> 
                                    <th style="background-color: green;"> Gagnant </th>

                                <?php
                                // Affiche les rencontre
                                $objetTireurRep = new TireurRepository();

                                foreach($arrayRencontre as $value)
                                {
                                ?>
                                    <tr>
                                        <!-- Tireur rouge -->
                                        <td>
                                        <?php
                                        $tireurRouge = $objetTireurRep->selectionTireurId($value->getId_Tireur_Rouge());
                                        echo $tireurRouge->getPrenom_Tireur() . ' ';
                                        echo $tireurRouge->getNom_Tireur() . ' ';
                                        ?>  
                                        </td>

                                        <!-- Tireur rouge -->
                                        <td>
                                        <?php
                                        $tireurBleu = $objetTireurRep->selectionTireurId($value->getId_Tireur_Bleu());
                                        echo $tireurBleu->getPrenom_Tireur() . ' ';
                                        echo $tireurBleu->getNom_Tireur() . ' ';
                                        ?> 
                                        </td>

                                        <!-- Tireur gagnant -->
                                        <td>
                                        <?php
                                        if ($objetTireurRep->selectionTireurId($value->getId_Gagnant()))
                                        {
                                            $tireurGagnant = $objetTireurRep->selectionTireurId($value->getId_Gagnant());
                                            echo $tireurGagnant->getPrenom_Tireur() . ' ';
                                            echo $tireurGagnant->getNom_Tireur() . ' ';
                                        }
                                        else
                                        {
                                            echo '?';
                                        }
                                        ?> 
                                        </td>                                
                                    </tr>
                                <?php
                                }
                                ?>                                

                                </table>
                                <?php
                            }
                            else
                            {
                                echo '<p>Il n\'y a pas de rencontre pour le moment</p>';
                            }                            
                        }
                    }
                }
                else
                {
                    echo '<p>Il n\'y a pas de tableaux pour le moment</p>';
                }
            }
            
            
            // Si le tableau est consulter depuis le bouton création/consultation
            else if (isset($_GET['tableau']))
            {
                    $objetTableauxRep = new TableauRepository();
                        $typeTableauRepo = new TableauRepository;
                        $typeTableauAffich = $typeTableauRepo->typeTableau($_GET['tableau']);
                        
                        $idTableau = $_GET['tableau'];
                        
                        if ($typeTableauAffich[0]['Id_Tableau_Typ_Tableau'] == 1)
                        {
                            $leTableauChoisi = $objetTableauxRep->selectionTableauxId($idTableau);

                            echo '<br/>';
                            // Caractéristique du tableau
                            echo '<form class="formCreat">';     

                            echo 'Nom : ' . strip_tags($leTableauChoisi->getLibelle_Tab()) . '<br/>';

                            echo 'Nombre max de rencontre : ' . strip_tags($leTableauChoisi->getNb_Max_Rencontre()) . '<br/>';

                            echo 'Sexe : ';
                            $objetSexeRep = new SexeRepository();
                            $tableauSexe = $objetSexeRep->selectionSexeId($leTableauChoisi->getId_Tableau_Sexe());
                            echo $tableauSexe->getLibelle_Sexe();
                            echo '<br/>';

                            echo 'Type d\'affrontement : ';
                            $objetTypeAffrontementRep = new TypeAffrontementRepository();
                            $tableauTypeAffrontement = $objetTypeAffrontementRep->selectionTypeAffrontementId($leTableauChoisi->getId_Tableau_Typ_Affrontement());
                            echo $tableauTypeAffrontement->getLibelle_Typ_Affrontement();
                            echo '<br/>';

                            echo 'Type de tableau : ';
                            $objetTypeTableauRep = new TypeTableauRepository();
                            $tableauTypeTableau = $objetTypeTableauRep->selectionTypeTableauId($leTableauChoisi->getId_Tableau_Typ_Tableau());
                            echo $tableauTypeTableau->getLibelle_Typ_Tab();
                            echo '<br/>';

                            echo 'Catégorie de Poids : ';
                            // Sélectionne la catégorie de poids du tableau
                            $objetTableauPoidsRep2 = new TableauPoidsRepository();
                            $objetTableauPoids2 = $objetTableauPoidsRep2->selectionTableauPoidsIdTab($leTableauChoisi->getId_Tab());
                            $objetCategoriePoidsRep2 = new CategoriePoidsRepository();
                            $objetCategoriePoids2 = $objetCategoriePoidsRep2->selectionCategoriePoidsId($objetTableauPoids2->getId_Poids_Cat());   
                            echo $objetCategoriePoids2->getLibelle_Cat_Poids();
                            echo '<br/>';
                            
                            echo 'Catégorie d\'age : ';
                            // Sélectionne la catégorie d'age du tableau
                            $objetConcerneRep2 = new ConcerneRepository();
                            $objetConcerne2 = $objetConcerneRep2->selectionConcerneIdTab($leTableauChoisi->getId_Tab());
                            $objetCategorieAgeRep2 = new CategorieAgeRepository();
                            $objetCategorieAge2 = $objetCategorieAgeRep2->selectionCategorieAgeId($objetConcerne2->getId_Concerne_Cat_Age());  
                            echo $objetCategorieAge2->getLibelle_Cat_Age();
                            echo '<br/>';
                            
                            echo 'Compétition : ';
                            $objetCompetitionRep = new CompetitionRepository();
                            $tableauCompetition = $objetCompetitionRep->selectionCompetitionId($leTableauChoisi->getId_Tableau_Competition());
                            echo $tableauCompetition->getNom_Competition();
                            echo '<br/>';

                            echo '</form> <br/>';                            

                            $objetTireurRep = new TireurRepository();
                            $objetRencontreRep = new RencontreRepository();
                            $objetStructureRep = new StructureRepository();
                            $objetCategorieRencontreRep = new CategorieRencontreRepository();
                            
                            if ($objetRencontreRep->selectionRencontreIdTableau($idTableau))
                            {
                                $objetRencontreAffichage = $objetRencontreRep->selectionRencontreIdTableau($idTableau);

                                $j = 0;

                                $categorieRencontre = $objetRencontreAffichage[0]->getId_Cat_Rencontre();

                                for ($i = 0; $i < count($objetRencontreAffichage); $i++)
                                {
                                    $categorieRencontre1 = $categorieRencontre;

                                    $categorieRencontre = $objetRencontreAffichage[$i]->getId_Cat_Rencontre();

                                    if ($categorieRencontre != $categorieRencontre1)
                                    {
                                        $j = 0;
                                        //changer le css pour faire un espace vers le bas de la page entre les deux rencontres qui la compose
                                        //<style type
                                    }                                
                                    if ($j == 0)
                                    {
                                        for ($j = 0; $j < 1; $j++)
                                        {
                                        ?>
                                        <h4> 
                                            <?php
                                            $objetCategorieRencontre = $objetCategorieRencontreRep->selectionCategorieRencontreId($categorieRencontre);
                                            echo $objetCategorieRencontre->getLibelle_Cat_Rencontre();
                                            ?> 
                                        </h4>
                                        <?php
                                        }
                                    }

                                    //Les tireurs
                                    $objetTireurRouge = $objetTireurRep->selectionTireurId($objetRencontreAffichage[$i]->getId_Tireur_Rouge());
                                    $objetTireurBleu = $objetTireurRep->selectionTireurId($objetRencontreAffichage[$i]->getId_Tireur_Bleu());
                                    // Nom Prénom
                                    $PrenomNomRouge = ($objetTireurRouge) ? $objetTireurRouge->getPrenom_Tireur() . " " . $objetTireurRouge->getNom_Tireur() : "";
                                    $PrenomNomBleu = ($objetTireurRouge) ? $objetTireurBleu->getPrenom_Tireur() . " " . $objetTireurBleu->getNom_Tireur() : "";

                                    // Initialisation
                                    $StructureRouge = '';
                                    $StructureBleu = '';

                                    // les structures (^O^)
                                    if ($objetTireurRouge && $objetTireurBleu)
                                    {   
                                        // Structure
                                        $objetStructureRouge = $objetStructureRep->selectionStructureId($objetTireurRouge->getId_Tir_Struct());
                                        $objetStructureBleu = $objetStructureRep->selectionStructureId($objetTireurBleu->getId_Tir_Struct());
                                        // Nom
                                        $StructureRouge = ($objetStructureRouge) ? " (" . $objetStructureRouge->getNom_Struct() . ")" : "";
                                        $StructureBleu = ($objetStructureBleu) ? " (" . $objetStructureBleu->getNom_Struct() . ")" : "";                                    
                                    }

                                    ?>
                                        <br/>
                                        <div style="text-align: center;">
                                            <input style="width: auto;" type="text" value="<?= $PrenomNomRouge . $StructureRouge ?> " readonly />
                                            VS
                                            <input style="width: auto;" type="text" value="<?= $PrenomNomBleu . $StructureBleu ?>" readonly />
                                        </div>
                                        <br/>
                                    <?php
                                }
                            }
                            else
                            {
                                echo '<p>Il n\'y a pas de rencontres pour le moment</p>';
                            }
                        }
                        else if ($typeTableauAffich[0]['Id_Tableau_Typ_Tableau'] == 2)
                        {
                            $leTableauChoisi = $objetTableauxRep->selectionTableauxId($idTableau);

                            echo '<br/>';
                            // Caractéristique du tableau
                            echo '<form class="formCreat">';     

                            echo 'Nom : ' . strip_tags($leTableauChoisi->getLibelle_Tab()) . '<br/>';

                            echo 'Nombre max de rencontre : ' . strip_tags($leTableauChoisi->getNb_Max_Rencontre()) . '<br/>';

                            echo 'Sexe : ';
                            $objetSexeRep = new SexeRepository();
                            $tableauSexe = $objetSexeRep->selectionSexeId($leTableauChoisi->getId_Tableau_Sexe());
                            echo $tableauSexe->getLibelle_Sexe();
                            echo '<br/>';

                            echo 'Type d\'affrontement : ';
                            $objetTypeAffrontementRep = new TypeAffrontementRepository();
                            $tableauTypeAffrontement = $objetTypeAffrontementRep->selectionTypeAffrontementId($leTableauChoisi->getId_Tableau_Typ_Affrontement());
                            echo $tableauTypeAffrontement->getLibelle_Typ_Affrontement();
                            echo '<br/>';

                            echo 'Type de tableau : ';
                            $objetTypeTableauRep = new TypeTableauRepository();
                            $tableauTypeTableau = $objetTypeTableauRep->selectionTypeTableauId($leTableauChoisi->getId_Tableau_Typ_Tableau());
                            echo $tableauTypeTableau->getLibelle_Typ_Tab();
                            echo '<br/>';

                            echo 'Catégorie de Poids : ';
                            // Sélectionne la catégorie de poids du tableau
                            $objetTableauPoidsRep2 = new TableauPoidsRepository();
                            $objetTableauPoids2 = $objetTableauPoidsRep2->selectionTableauPoidsIdTab($leTableauChoisi->getId_Tab());
                            $objetCategoriePoidsRep2 = new CategoriePoidsRepository();
                            $objetCategoriePoids2 = $objetCategoriePoidsRep2->selectionCategoriePoidsId($objetTableauPoids2->getId_Poids_Cat());   
                            echo $objetCategoriePoids2->getLibelle_Cat_Poids();
                            echo '<br/>';
                            
                            echo 'Catégorie d\'age : ';
                            // Sélectionne la catégorie d'age du tableau
                            $objetConcerneRep2 = new ConcerneRepository();
                            $objetConcerne2 = $objetConcerneRep2->selectionConcerneIdTab($leTableauChoisi->getId_Tab());
                            $objetCategorieAgeRep2 = new CategorieAgeRepository();
                            $objetCategorieAge2 = $objetCategorieAgeRep2->selectionCategorieAgeId($objetConcerne2->getId_Concerne_Cat_Age());  
                            echo $objetCategorieAge2->getLibelle_Cat_Age();
                            echo '<br/>';                            
                            
                            echo 'Compétition : ';
                            $objetCompetitionRep = new CompetitionRepository();
                            $tableauCompetition = $objetCompetitionRep->selectionCompetitionId($leTableauChoisi->getId_Tableau_Competition());
                            echo $tableauCompetition->getNom_Competition();
                            echo '<br/>';

                            echo '</form> <br/>';  

                            $objetRencontreRep = new RencontreRepository();                        
                            // Active le code si il y a au moins une rencontre dans le tableau
                            if ($objetRencontreRep->selectionRencontreIdTableau($idTableau))
                            {
                                $arrayRencontre = $objetRencontreRep->selectionRencontreIdTableau($idTableau);

                                // Affiche un tableau HTML avec toutes les rencontres du tableau
                                ?>
                                <table>
                                    <th style="background-color: red;"> Coin rouge </th> 
                                    <th style="background-color: blue;"> Coin bleu </th> 
                                    <th style="background-color: green;"> Gagnant </th>

                                <?php
                                // Affiche les rencontre
                                $objetTireurRep = new TireurRepository();

                                foreach($arrayRencontre as $value)
                                {
                                ?>
                                    <tr>
                                        <!-- Tireur rouge -->
                                        <td>
                                        <?php
                                        $tireurRouge = $objetTireurRep->selectionTireurId($value->getId_Tireur_Rouge());
                                        echo $tireurRouge->getPrenom_Tireur() . ' ';
                                        echo $tireurRouge->getNom_Tireur() . ' ';
                                        ?>  
                                        </td>

                                        <!-- Tireur rouge -->
                                        <td>
                                        <?php
                                        $tireurBleu = $objetTireurRep->selectionTireurId($value->getId_Tireur_Bleu());
                                        echo $tireurBleu->getPrenom_Tireur() . ' ';
                                        echo $tireurBleu->getNom_Tireur() . ' ';
                                        ?> 
                                        </td>

                                        <!-- Tireur gagnant -->
                                        <td>
                                        <?php
                                        if ($objetTireurRep->selectionTireurId($value->getId_Gagnant()))
                                        {
                                            $tireurGagnant = $objetTireurRep->selectionTireurId($value->getId_Gagnant());
                                            echo $tireurGagnant->getPrenom_Tireur() . ' ';
                                            echo $tireurGagnant->getNom_Tireur() . ' ';
                                        }
                                        else
                                        {
                                            echo '?';
                                        }
                                        ?> 
                                        </td>                                
                                    </tr>
                                <?php
                                }
                                ?>                                

                                </table>
                                <?php
                            }
                            else
                            {
                                echo '<p>Il n\'y a pas de rencontre pour le moment</p>';
                            }                            
                        }        
            }
            ?>
            <br/>
        </section>
    </body>
</html>