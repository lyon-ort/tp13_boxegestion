-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : sam. 02 déc. 2023 à 14:11
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bddboxe`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorieage`
--

DROP TABLE IF EXISTS `categorieage`;
CREATE TABLE IF NOT EXISTS `categorieage` (
  `Id_Cat_Age` int(11) NOT NULL AUTO_INCREMENT,
  `Libelle_Cat_Age` varchar(50) NOT NULL,
  `Age_Max` int(11) NOT NULL,
  `Age_Min` int(11) NOT NULL,
  PRIMARY KEY (`Id_Cat_Age`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categorieage`
--

INSERT INTO `categorieage` (`Id_Cat_Age`, `Libelle_Cat_Age`, `Age_Max`, `Age_Min`) VALUES
(1, 'Pré-poussins', 9, 7),
(2, 'poussins', 11, 10),
(3, 'Benjamins', 13, 12),
(4, 'Minimes', 15, 14),
(5, 'Cadets', 17, 16),
(6, 'Juniors', 20, 18),
(7, 'Seniors Combat', 34, 21),
(8, 'Seniors Assaut', 39, 21),
(9, 'Vétérans Combat ', 99, 35),
(10, 'Vétérans Assaut', 99, 40);

-- --------------------------------------------------------

--
-- Structure de la table `categoriepoids`
--

DROP TABLE IF EXISTS `categoriepoids`;
CREATE TABLE IF NOT EXISTS `categoriepoids` (
  `Id_Cat_Poids` int(11) NOT NULL AUTO_INCREMENT,
  `Libelle_Cat_Poids` varchar(50) NOT NULL,
  `Poids_Max` int(11) DEFAULT NULL,
  `Poids_Min` int(11) NOT NULL,
  PRIMARY KEY (`Id_Cat_Poids`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categoriepoids`
--

INSERT INTO `categoriepoids` (`Id_Cat_Poids`, `Libelle_Cat_Poids`, `Poids_Max`, `Poids_Min`) VALUES
(1, 'Moustiques', 24, 24),
(2, 'Pré-Mini-Mouches', 27, 25),
(3, 'Pré-Mini-Coqs', 30, 28),
(4, 'Pré-Mini-Plumes', 33, 31),
(5, 'Pré-Mini-Légers', 36, 34),
(6, 'Mini-Mouches', 39, 37),
(7, 'Mini-Coqs', 42, 39),
(8, 'Mini-Plumes', 45, 43),
(9, 'Mouches', 48, 46),
(10, 'Coqs', 52, 49),
(11, 'Plumes', 56, 53),
(12, 'Légers', 60, 57),
(13, 'Super-Légers', 65, 61),
(14, 'Mi-Moyens', 70, 66),
(15, 'Super-Mi-Moyens', 75, 71),
(16, 'Moyens', 80, 76),
(17, 'Mi-Lourds', 85, 81),
(18, 'Lourds', NULL, 86);

-- --------------------------------------------------------

--
-- Structure de la table `categorierencontre`
--

DROP TABLE IF EXISTS `categorierencontre`;
CREATE TABLE IF NOT EXISTS `categorierencontre` (
  `Id_Cat_Rencontre` int(11) NOT NULL AUTO_INCREMENT,
  `Libelle_Cat_Rencontre` varchar(50) NOT NULL,
  `Nb_Rencontre` int(11) NOT NULL,
  PRIMARY KEY (`Id_Cat_Rencontre`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categorierencontre`
--

INSERT INTO `categorierencontre` (`Id_Cat_Rencontre`, `Libelle_Cat_Rencontre`, `Nb_Rencontre`) VALUES
(1, 'Finale', 1),
(2, 'Demi-Finale', 2),
(3, 'Quart-De-Finale', 4),
(4, 'Huitième-De-Finale', 8),
(5, '16e de Finale', 16),
(6, '32e de Finale', 32);

-- --------------------------------------------------------

--
-- Structure de la table `competition`
--

DROP TABLE IF EXISTS `competition`;
CREATE TABLE IF NOT EXISTS `competition` (
  `Id_Competition` int(11) NOT NULL AUTO_INCREMENT,
  `Nom_Competition` varchar(50) NOT NULL,
  `Date_Debut_Competition` date NOT NULL,
  `Date_Fin_Competition` date DEFAULT NULL,
  `Adresse_Competition` varchar(50) NOT NULL,
  `Cp_Competition` varchar(5) NOT NULL,
  `Ville_Competition` varchar(50) NOT NULL,
  `Date_Limite_Inscript_Competition` date NOT NULL,
  `Nb_Round` int(11) NOT NULL,
  `Duree_Round` int(11) DEFAULT NULL,
  `Nb_Ring` int(11) NOT NULL,
  `Id_Competition_Sexe` int(11) DEFAULT NULL,
  `Id_Competition_Typ_Affrontement` int(11) DEFAULT NULL,
  `Id_Competition_Struct` int(11) NOT NULL,
  `Inactif_Competition` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id_Competition`),
  KEY `STRUCTURE_ORGANISATEUR_FK` (`Id_Competition_Struct`),
  KEY `SEXE_COMPETITION_FK` (`Id_Competition_Sexe`),
  KEY `TYPE_AFFRONTEMENT_FK` (`Id_Competition_Typ_Affrontement`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `competition`
--

INSERT INTO `competition` (`Id_Competition`, `Nom_Competition`, `Date_Debut_Competition`, `Date_Fin_Competition`, `Adresse_Competition`, `Cp_Competition`, `Ville_Competition`, `Date_Limite_Inscript_Competition`, `Nb_Round`, `Duree_Round`, `Nb_Ring`, `Id_Competition_Sexe`, `Id_Competition_Typ_Affrontement`, `Id_Competition_Struct`, `Inactif_Competition`) VALUES
(1, 'Regionale', '2018-05-15', '2018-05-15', ' gymnase des carreaux', '69330', 'Meyzieu', '2018-04-15', 3, 120, 1, 2, 2, 1, 0),
(2, 'Amical du département', '2020-07-20', '2020-07-25', '23 rue Lavoir', '69330', 'Jonage', '2020-07-20', 3, 150, 2, 1, 1, 2, 0),
(3, 'Compète Mixte', '2020-01-12', '2020-01-16', '335 rue garibaldie', '38080', 'Saint-Alban-De-Roche', '2019-12-12', 3, 120, 2, NULL, 2, 3, 0),
(4, 'Demo Revue2', '2020-04-30', '2020-05-02', '15 rue gerard', '12345', 'saint marcelin', '2020-04-29', 2, 125, 2, NULL, NULL, 3, 0),
(5, 'demo', '2020-05-30', '2020-05-31', '15 rue gerard', '12345', 'saint marcelin', '2020-05-29', 2, 120, 4, NULL, NULL, 3, 0);

-- --------------------------------------------------------

--
-- Structure de la table `concerne`
--

DROP TABLE IF EXISTS `concerne`;
CREATE TABLE IF NOT EXISTS `concerne` (
  `Id_Concerne` int(11) NOT NULL AUTO_INCREMENT,
  `Id_Concerne_Tab` int(11) NOT NULL,
  `Id_Concerne_Cat_Age` int(11) NOT NULL,
  PRIMARY KEY (`Id_Concerne`),
  KEY `ID_CONCERNE_CATEGORIE_AGE_FK` (`Id_Concerne_Cat_Age`),
  KEY `ID_CONCERNE_TABLEAU_FK` (`Id_Concerne_Tab`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `concerne`
--

INSERT INTO `concerne` (`Id_Concerne`, `Id_Concerne_Tab`, `Id_Concerne_Cat_Age`) VALUES
(1, 2, 9),
(2, 3, 9);

-- --------------------------------------------------------

--
-- Structure de la table `etat`
--

DROP TABLE IF EXISTS `etat`;
CREATE TABLE IF NOT EXISTS `etat` (
  `Id_Etat` int(11) NOT NULL AUTO_INCREMENT,
  `Libelle_Etat` varchar(50) NOT NULL,
  PRIMARY KEY (`Id_Etat`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `etat`
--

INSERT INTO `etat` (`Id_Etat`, `Libelle_Etat`) VALUES
(1, 'A venir'),
(2, 'En cours'),
(3, 'Resultat');

-- --------------------------------------------------------

--
-- Structure de la table `logbdd`
--

DROP TABLE IF EXISTS `logbdd`;
CREATE TABLE IF NOT EXISTS `logbdd` (
  `Id_Logbdd` int(11) NOT NULL AUTO_INCREMENT,
  `Login` varchar(50) NOT NULL,
  `Date_Modif` datetime NOT NULL,
  `Nom_Table` varchar(50) NOT NULL,
  `Type_Operation` varchar(50) NOT NULL,
  `Id_Element` int(11) NOT NULL,
  PRIMARY KEY (`Id_Logbdd`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `logbdd`
--

INSERT INTO `logbdd` (`Id_Logbdd`, `Login`, `Date_Modif`, `Nom_Table`, `Type_Operation`, `Id_Element`) VALUES
(6, 'Tchenar', '2023-12-02 11:18:08', 'structure', 'Modification', 4);

-- --------------------------------------------------------

--
-- Structure de la table `loguser`
--

DROP TABLE IF EXISTS `loguser`;
CREATE TABLE IF NOT EXISTS `loguser` (
  `Id_loguser` int(11) NOT NULL AUTO_INCREMENT,
  `Login` varchar(100) NOT NULL,
  `Date_Log` datetime NOT NULL,
  `Message` varchar(100) NOT NULL,
  PRIMARY KEY (`Id_loguser`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `loguser`
--

INSERT INTO `loguser` (`Id_loguser`, `Login`, `Date_Log`, `Message`) VALUES
(1, 'test', '2023-12-02 13:04:00', 'Test');

-- --------------------------------------------------------

--
-- Structure de la table `niveautireur`
--

DROP TABLE IF EXISTS `niveautireur`;
CREATE TABLE IF NOT EXISTS `niveautireur` (
  `Id_Niv_Tir` int(11) NOT NULL AUTO_INCREMENT,
  `Libelle_Niv_Tir` varchar(50) NOT NULL,
  PRIMARY KEY (`Id_Niv_Tir`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `niveautireur`
--

INSERT INTO `niveautireur` (`Id_Niv_Tir`, `Libelle_Niv_Tir`) VALUES
(1, 'Gants Bleus'),
(2, 'Gants Verts'),
(3, 'Gants Rouges'),
(4, 'Gants Blancs'),
(5, 'Gants Jaunes'),
(6, 'Gants d\'argent techniques'),
(7, 'Gants de Bronze');

-- --------------------------------------------------------

--
-- Structure de la table `participer`
--

DROP TABLE IF EXISTS `participer`;
CREATE TABLE IF NOT EXISTS `participer` (
  `Id_Particip` int(11) NOT NULL AUTO_INCREMENT,
  `Id_Particip_Tireur` int(11) NOT NULL,
  `Id_Particip_Competition` int(11) NOT NULL,
  `Id_Particip_Tableau` int(11) NOT NULL,
  PRIMARY KEY (`Id_Particip`),
  KEY `ID_PARTICIP_TIREUR_FK` (`Id_Particip_Tireur`),
  KEY `ID_PARTICIP_COMPETITION_FK` (`Id_Particip_Competition`),
  KEY `ID_Particip_TABLEAU_FK` (`Id_Particip_Tableau`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `participer`
--

INSERT INTO `participer` (`Id_Particip`, `Id_Particip_Tireur`, `Id_Particip_Competition`, `Id_Particip_Tableau`) VALUES
(1, 2, 1, 1),
(2, 3, 1, 1),
(3, 6, 1, 1),
(4, 7, 1, 1),
(5, 1, 2, 2),
(6, 4, 2, 2),
(7, 5, 2, 2),
(8, 8, 2, 2),
(9, 1, 3, 3),
(10, 2, 3, 4),
(11, 3, 3, 4),
(12, 4, 3, 3),
(13, 5, 3, 3),
(14, 6, 3, 4),
(15, 7, 3, 4),
(16, 8, 3, 3),
(17, 1, 4, 5),
(18, 4, 4, 5),
(19, 5, 4, 5),
(20, 8, 4, 5),
(25, 2, 4, 6),
(26, 3, 4, 6),
(27, 6, 4, 6),
(28, 7, 4, 6);

-- --------------------------------------------------------

--
-- Structure de la table `rencontre`
--

DROP TABLE IF EXISTS `rencontre`;
CREATE TABLE IF NOT EXISTS `rencontre` (
  `Id_Rencontre` int(11) NOT NULL AUTO_INCREMENT,
  `Date_Rencontre` date NOT NULL,
  `Heure_Debut_Rencontre` datetime NOT NULL,
  `Heure_Fin_Rencontre` datetime DEFAULT NULL,
  `Num_Ring_Rencontre` int(11) DEFAULT NULL,
  `Id_Tireur_Rouge` int(11) DEFAULT NULL,
  `Id_Tireur_Bleu` int(11) DEFAULT NULL,
  `Id_Gagnant` int(11) DEFAULT NULL,
  `Id_Rencontre_Precedent_1` int(11) DEFAULT NULL,
  `Id_Rencontre_Precedent_2` int(11) DEFAULT NULL,
  `Id_Rencontre_Suivante` int(11) DEFAULT NULL,
  `Id_Cat_Rencontre` int(11) DEFAULT NULL,
  `Id_Tab_Rencontre` int(11) NOT NULL,
  `Id_Etat_Rencontre` int(11) DEFAULT NULL,
  `Inactif_Rencontre` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id_Rencontre`),
  KEY `TIREUR_COIN_ROUGE_FK` (`Id_Tireur_Rouge`),
  KEY `TIREUR_COIN_BLEU_FK` (`Id_Tireur_Bleu`),
  KEY `TIREUR_GAGNANT_FK` (`Id_Gagnant`),
  KEY `CATEGORIE_RENCONTRE_FK` (`Id_Cat_Rencontre`),
  KEY `TABLEAU_FK` (`Id_Tab_Rencontre`),
  KEY `ETAT_RENCONTRE_FK` (`Id_Etat_Rencontre`),
  KEY `RENCONTRE_PRECEDENTE_1_FK` (`Id_Rencontre_Precedent_1`),
  KEY `RENCONTRE_PRECEDENTE_2_FK` (`Id_Rencontre_Precedent_2`),
  KEY `RENCONTRE_SUIVANTE_FK` (`Id_Rencontre_Suivante`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `rencontre`
--

INSERT INTO `rencontre` (`Id_Rencontre`, `Date_Rencontre`, `Heure_Debut_Rencontre`, `Heure_Fin_Rencontre`, `Num_Ring_Rencontre`, `Id_Tireur_Rouge`, `Id_Tireur_Bleu`, `Id_Gagnant`, `Id_Rencontre_Precedent_1`, `Id_Rencontre_Precedent_2`, `Id_Rencontre_Suivante`, `Id_Cat_Rencontre`, `Id_Tab_Rencontre`, `Id_Etat_Rencontre`, `Inactif_Rencontre`) VALUES
(1, '2020-07-20', '2020-07-20 10:20:00', '2020-07-20 10:26:00', 1, 2, 3, NULL, NULL, NULL, 3, 2, 2, 1, 0),
(2, '2020-07-20', '2020-07-20 10:30:00', '2020-07-20 10:36:00', 1, 6, 7, NULL, NULL, NULL, 3, 2, 2, 1, 0),
(3, '2020-07-25', '2020-07-25 13:20:00', '2020-07-20 13:26:00', 1, NULL, NULL, NULL, 1, 2, NULL, 1, 2, 1, 0),
(4, '2018-05-15', '2018-05-15 10:20:00', '2018-05-15 10:26:00', 1, 1, 4, 4, NULL, NULL, NULL, NULL, 1, 3, 0),
(5, '2018-05-15', '2018-05-15 10:50:00', '2018-05-15 10:56:00', 2, 1, 5, 1, NULL, NULL, NULL, NULL, 1, 3, 0),
(6, '2018-05-15', '2018-05-15 11:10:00', '2018-05-15 11:16:00', 2, 1, 8, 8, NULL, NULL, NULL, NULL, 1, 3, 0),
(7, '2018-05-15', '2018-05-15 11:30:00', '2018-05-15 11:36:00', 2, 4, 5, 5, NULL, NULL, NULL, NULL, 1, 3, 0),
(8, '2018-05-15', '2018-05-15 16:15:00', '2018-05-15 16:21:00', 1, 4, 8, 8, NULL, NULL, NULL, NULL, 1, 3, 0),
(9, '2018-05-15', '2018-05-15 16:55:00', '2018-05-15 17:00:00', 1, 5, 8, 8, NULL, NULL, NULL, NULL, 1, 3, 0),
(10, '2020-01-12', '2020-01-12 15:00:00', '2020-01-12 15:06:00', 1, 1, 4, 4, NULL, NULL, NULL, NULL, 3, 3, 0),
(11, '2020-01-12', '2020-01-12 15:50:00', '2020-01-12 15:56:00', 1, 1, 5, 1, NULL, NULL, NULL, NULL, 3, 3, 0),
(12, '2020-01-13', '2020-01-13 15:00:00', '2020-01-13 15:06:00', 1, 1, 8, 8, NULL, NULL, NULL, NULL, 3, 3, 0),
(13, '2020-01-13', '2020-01-13 15:50:00', '2020-01-13 15:56:00', 2, 4, 5, 5, NULL, NULL, NULL, NULL, 3, 3, 0),
(14, '2020-01-14', '2020-01-14 15:05:00', '2020-01-14 15:06:00', 2, 4, 8, 8, NULL, NULL, NULL, NULL, 3, 3, 0),
(15, '2020-01-14', '2020-01-14 15:50:00', '2020-01-14 15:56:00', 2, 5, 8, 8, NULL, NULL, NULL, NULL, 3, 3, 0),
(16, '2020-01-15', '2020-01-15 10:05:00', '2020-01-15 15:06:00', 2, 2, 3, 3, NULL, NULL, 18, 2, 4, 3, 0),
(17, '2020-01-15', '2020-01-15 10:50:00', '2020-01-15 15:56:00', 2, 6, 7, 6, NULL, NULL, 18, 2, 4, 3, 0),
(18, '2020-01-16', '2020-01-16 12:30:00', '2020-01-16 12:31:00', 1, 6, 3, 6, 16, 17, NULL, 1, 4, 3, 0);

-- --------------------------------------------------------

--
-- Structure de la table `sefaire`
--

DROP TABLE IF EXISTS `sefaire`;
CREATE TABLE IF NOT EXISTS `sefaire` (
  `Id_SeFaire` int(11) NOT NULL AUTO_INCREMENT,
  `Id_SeFaire_Competition` int(11) NOT NULL,
  `Id_SeFaire_Cat_Age` int(11) NOT NULL,
  PRIMARY KEY (`Id_SeFaire`),
  KEY `ID_SEFAIRE_CATEGORIE_AGE_FK` (`Id_SeFaire_Cat_Age`),
  KEY `ID_SEFAIRE_COMPETITION_FK` (`Id_SeFaire_Competition`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `sefaire`
--

INSERT INTO `sefaire` (`Id_SeFaire`, `Id_SeFaire_Competition`, `Id_SeFaire_Cat_Age`) VALUES
(1, 1, 7),
(2, 2, 9),
(3, 3, 7),
(4, 3, 9);

-- --------------------------------------------------------

--
-- Structure de la table `sexe`
--

DROP TABLE IF EXISTS `sexe`;
CREATE TABLE IF NOT EXISTS `sexe` (
  `Id_Sexe` int(11) NOT NULL AUTO_INCREMENT,
  `Libelle_Sexe` varchar(50) NOT NULL,
  PRIMARY KEY (`Id_Sexe`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `sexe`
--

INSERT INTO `sexe` (`Id_Sexe`, `Libelle_Sexe`) VALUES
(1, 'Homme'),
(2, 'Femme');

-- --------------------------------------------------------

--
-- Structure de la table `structure`
--

DROP TABLE IF EXISTS `structure`;
CREATE TABLE IF NOT EXISTS `structure` (
  `Id_Struct` int(11) NOT NULL AUTO_INCREMENT,
  `Id_Typ_Struct` int(11) NOT NULL,
  `Nom_Struct` varchar(50) NOT NULL,
  `Adresse_Struct` varchar(50) NOT NULL,
  `Cp_Struct` varchar(5) NOT NULL,
  `Ville_Struct` varchar(50) NOT NULL,
  `Id_Responsable_Struct` int(11) NOT NULL,
  `Tel_Fixe` varchar(10) DEFAULT NULL,
  `Tel_Port` varchar(10) DEFAULT NULL,
  `Inactif_Struct` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id_Struct`),
  KEY `TYPE_STRUCTURE_FK` (`Id_Typ_Struct`),
  KEY `RESPONSABLE_STRUCTURE_FK` (`Id_Responsable_Struct`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `structure`
--

INSERT INTO `structure` (`Id_Struct`, `Id_Typ_Struct`, `Nom_Struct`, `Adresse_Struct`, `Cp_Struct`, `Ville_Struct`, `Id_Responsable_Struct`, `Tel_Fixe`, `Tel_Port`, `Inactif_Struct`) VALUES
(1, 1, 'IPPO', '15 rue des marrons', '69005', 'Lyon', 1, '24681214', '135791111', 0),
(2, 1, 'test', '13 rue du test', '69500', 'Testvillage', 3, '147852369', '678945210', 0),
(3, 1, 'DashClub', '127 rue des noises', '57900', 'RacoonCity', 2, NULL, NULL, 0),
(4, 3, 'Fédération des savates unies', '15 rue des étoiles', '43500', 'StarCity', 7, NULL, NULL, 0),
(5, 2, 'Comité du Rhone', '15 rue IceBorn', '47690', 'Moncrabeau', 8, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tableau`
--

DROP TABLE IF EXISTS `tableau`;
CREATE TABLE IF NOT EXISTS `tableau` (
  `Id_Tab` int(11) NOT NULL AUTO_INCREMENT,
  `Libelle_Tab` varchar(50) NOT NULL,
  `Nb_Max_Rencontre` int(11) NOT NULL,
  `Id_Tableau_Sexe` int(11) NOT NULL,
  `Id_Tableau_Typ_Affrontement` int(11) NOT NULL,
  `Id_Tableau_Typ_Tableau` int(11) NOT NULL,
  `Id_Tableau_Competition` int(11) NOT NULL,
  PRIMARY KEY (`Id_Tab`),
  KEY `TABLEAU_SEXE_FK` (`Id_Tableau_Sexe`),
  KEY `TABLEAU_TYPE_AFFRONTEMENT_FK` (`Id_Tableau_Typ_Affrontement`),
  KEY `TABLEAU_TYPE_TABLEAU_FK` (`Id_Tableau_Typ_Tableau`),
  KEY `TABLEAU_COMPETITION_FK` (`Id_Tableau_Competition`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tableau`
--

INSERT INTO `tableau` (`Id_Tab`, `Libelle_Tab`, `Nb_Max_Rencontre`, `Id_Tableau_Sexe`, `Id_Tableau_Typ_Affrontement`, `Id_Tableau_Typ_Tableau`, `Id_Tableau_Competition`) VALUES
(1, 'Compète linéaire', 6, 2, 2, 2, 1),
(2, 'Compète pyramidale', 3, 1, 1, 1, 2),
(3, 'Compète mixte linéaire', 6, 1, 2, 2, 3),
(4, 'compète mixte pyramidale', 3, 2, 2, 1, 3),
(5, 'demo sauvegarde 1', 2, 1, 1, 2, 4),
(6, 'demo sauvegarde 2', 9, 2, 1, 2, 4),
(7, 'Demo 1', 2, 1, 1, 2, 5),
(8, 'demo 2', 90, 2, 2, 1, 5);

-- --------------------------------------------------------

--
-- Structure de la table `tableaupoids`
--

DROP TABLE IF EXISTS `tableaupoids`;
CREATE TABLE IF NOT EXISTS `tableaupoids` (
  `Id_Tab_Poids` int(11) NOT NULL AUTO_INCREMENT,
  `Id_Poids_Tab` int(11) NOT NULL,
  `Id_Poids_Cat` int(11) NOT NULL,
  PRIMARY KEY (`Id_Tab_Poids`),
  KEY `ID_CATEGORIE_TABLEAU_POIDS_FK` (`Id_Poids_Cat`),
  KEY `ID_CATEGORIE_POIDS_TABLEAU_FK` (`Id_Poids_Tab`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tableaupoids`
--

INSERT INTO `tableaupoids` (`Id_Tab_Poids`, `Id_Poids_Tab`, `Id_Poids_Cat`) VALUES
(1, 1, 11),
(2, 4, 11);

-- --------------------------------------------------------

--
-- Structure de la table `tireur`
--

DROP TABLE IF EXISTS `tireur`;
CREATE TABLE IF NOT EXISTS `tireur` (
  `Id_Tireur` int(11) NOT NULL AUTO_INCREMENT,
  `Id_Tir_Niv_Tireur` int(11) NOT NULL,
  `Nom_Tireur` varchar(50) NOT NULL,
  `Prenom_Tireur` varchar(50) NOT NULL,
  `Date_Naissance` date NOT NULL,
  `Num_Licence` int(11) NOT NULL,
  `Poids_Tireur` float NOT NULL,
  `Id_Tir_Cat_Poids` int(11) NOT NULL,
  `Id_Tir_Cat_Age` int(11) NOT NULL,
  `Id_Tir_Sexe` int(11) NOT NULL,
  `Id_Tir_Struct` int(11) NOT NULL,
  `Inactif_Tireur` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id_Tireur`),
  KEY `NIVEAU_TIREUR_FK` (`Id_Tir_Niv_Tireur`),
  KEY `CATEGORIE_POIDS_TIREUR_FK` (`Id_Tir_Cat_Poids`),
  KEY `SEXE_TIREUR_FK` (`Id_Tir_Sexe`),
  KEY `CATEGORIE_AGE_TIREUR_FK` (`Id_Tir_Cat_Age`),
  KEY `STRUCTURE_TIREUR_FK` (`Id_Tir_Struct`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tireur`
--

INSERT INTO `tireur` (`Id_Tireur`, `Id_Tir_Niv_Tireur`, `Nom_Tireur`, `Prenom_Tireur`, `Date_Naissance`, `Num_Licence`, `Poids_Tireur`, `Id_Tir_Cat_Poids`, `Id_Tir_Cat_Age`, `Id_Tir_Sexe`, `Id_Tir_Struct`, `Inactif_Tireur`) VALUES
(1, 6, 'Paul', 'Paul', '1985-05-15', 101010, 84, 17, 9, 1, 1, 0),
(2, 6, 'Sparadra', 'Noob', '1985-05-15', 101899, 84, 17, 9, 1, 1, 0),
(3, 6, 'Nick', 'Robot', '1985-05-15', 101208, 84, 17, 9, 1, 1, 0),
(4, 6, 'Jonny', 'Jacky', '1969-02-01', 444211, 84, 17, 9, 1, 2, 0),
(5, 6, 'Stan', 'Lee', '1945-07-07', 521045, 84, 17, 9, 1, 3, 0),
(6, 6, 'Islass', 'Stan', '1985-05-15', 101050, 84, 17, 9, 1, 1, 0),
(7, 6, 'Lannister', 'Tyrion', '1985-05-15', 101063, 84, 17, 9, 1, 1, 0),
(8, 6, 'Potter', 'Harry', '1982-07-17', 449874, 84, 17, 9, 1, 1, 0),
(9, 6, 'Cochon', 'Spider', '1982-07-17', 446458, 84, 17, 9, 1, 1, 0),
(10, 6, 'Rinner', 'Ted-y', '1982-07-17', 441156, 84, 17, 9, 1, 1, 0),
(11, 6, 'Rex', 'Mister T', '1982-07-17', 441148, 84, 17, 9, 1, 1, 0),
(12, 6, 'Torrent', 'Bit', '1982-07-17', 441196, 84, 17, 9, 1, 1, 0),
(13, 6, 'arab', 'Anub', '1982-07-17', 448144, 84, 17, 9, 1, 1, 0),
(14, 6, 'E.T.C', 'Rock', '1982-07-17', 441244, 84, 17, 9, 1, 1, 0),
(15, 6, 'Houdini', 'Harry', '1982-07-17', 141144, 84, 17, 9, 1, 1, 0),
(16, 6, 'Bros', 'Brother', '1982-07-17', 411144, 84, 17, 9, 1, 1, 0),
(17, 6, 'Gabriel', 'Harry', '1982-07-17', 481844, 84, 17, 9, 1, 1, 0),
(18, 6, 'kedale', 'Raphaël', '1982-07-17', 491184, 84, 17, 9, 1, 1, 0),
(19, 6, 'Vuitton', 'Louis', '1982-07-17', 471147, 84, 17, 9, 1, 1, 0),
(20, 6, 'Leroy', 'Leo', '1982-07-17', 821694, 84, 17, 9, 1, 1, 0),
(21, 6, 'Jeckins', 'Leeroy', '1982-07-17', 4418924, 84, 17, 9, 1, 1, 0),
(22, 6, 'Da Vinci', 'Leonardo', '1982-07-17', 415244, 84, 17, 9, 1, 1, 0),
(23, 6, 'Du Bois', 'Gépetto', '1982-07-17', 447554, 84, 17, 9, 1, 1, 0),
(24, 6, 'Circus', 'Marcus', '1982-07-17', 441444, 84, 17, 9, 1, 1, 0),
(25, 6, 'Nichel', 'Romain', '1982-07-17', 741254, 84, 17, 9, 1, 1, 0),
(26, 6, 'Lupin', 'Arsène', '1982-07-17', 447824, 84, 17, 9, 1, 1, 0),
(27, 6, 'DuBois', 'Auré', '1982-07-17', 447454, 84, 17, 9, 1, 1, 0),
(28, 6, 'Tyson', 'Mike', '1982-07-17', 422639, 84, 17, 9, 1, 1, 0),
(29, 6, 'DesDois', 'Gromain', '1982-07-17', 441253, 84, 17, 9, 1, 1, 0),
(30, 6, 'Tô', 'JoliVet', '1982-07-17', 449527, 84, 17, 9, 1, 1, 0),
(31, 6, 'Bolt', 'Thunder', '1982-07-17', 441847, 84, 17, 9, 1, 1, 0),
(32, 6, 'Theslâ', 'Nikolas', '1982-07-17', 442827, 84, 17, 9, 1, 1, 0),
(33, 6, 'MichMich', 'Pomme', '1985-05-15', 821694, 75, 15, 9, 2, 1, 0),
(34, 6, 'Stasia', 'Hanna', '1985-05-15', 821698, 75, 15, 9, 2, 1, 0),
(35, 6, 'Traillette', 'Mamie', '1985-05-15', 821702, 75, 15, 9, 2, 1, 0),
(36, 6, 'Apple', 'Pie', '1969-02-01', 821706, 75, 15, 9, 2, 2, 0),
(37, 6, 'Maman', 'Bonne', '1945-07-07', 821710, 75, 15, 9, 2, 3, 0),
(38, 6, 'Targaryen', 'Daenerys', '1985-05-15', 821714, 75, 15, 9, 2, 1, 0),
(39, 6, 'PoudreDescampette', 'Wendy', '1985-05-15', 821718, 75, 15, 9, 2, 1, 0),
(40, 6, 'Nitrogen', 'Helium', '1982-07-17', 821722, 75, 15, 9, 2, 1, 0),
(41, 6, 'Iodine', 'tantalium', '1982-07-17', 821726, 75, 15, 9, 2, 1, 0),
(42, 6, 'Crush', 'Candy', '1982-07-17', 821730, 75, 15, 9, 2, 1, 0),
(43, 6, 'Éserge', 'Jeanne', '1982-07-17', 821734, 75, 15, 9, 2, 1, 0),
(44, 6, 'Gazelle', 'Ôme', '1982-07-17', 821738, 75, 15, 9, 2, 1, 0),
(45, 6, 'Portvaillant', 'Jaina', '1982-07-17', 821742, 75, 15, 9, 2, 1, 0),
(46, 6, 'Rine', 'Corre', '1982-07-17', 821746, 75, 15, 9, 2, 1, 0),
(47, 6, 'Peche', 'Princesse', '1982-07-17', 821750, 75, 15, 9, 2, 1, 0),
(48, 6, 'Sy', 'Day', '1982-07-17', 821754, 75, 15, 9, 2, 1, 0),
(49, 6, 'Amarrié', 'Ann', '1982-07-17', 821758, 75, 15, 9, 2, 1, 0),
(50, 6, 'Adine', 'Bette', '1982-07-17', 821762, 75, 15, 9, 2, 1, 0),
(51, 6, 'Cine', 'Becasse', '1982-07-17', 821766, 75, 15, 9, 2, 1, 0),
(52, 6, 'Découdre', 'Savah', '1982-07-17', 821694, 75, 15, 9, 2, 1, 0),
(53, 6, 'Croche', 'Sarah', '1982-07-17', 821770, 75, 15, 9, 2, 1, 0),
(54, 6, 'DiBal', 'Tapas', '1982-07-17', 821774, 75, 15, 9, 2, 1, 0),
(55, 6, 'DesBois', 'Roberta', '1982-07-17', 821778, 75, 15, 9, 2, 1, 0),
(56, 6, '', '', '1982-07-17', 821782, 75, 15, 9, 2, 1, 0),
(57, 6, '', '', '1982-07-17', 821786, 75, 15, 9, 2, 1, 0),
(58, 6, 'Ocrome', 'Mercure', '1982-07-17', 821790, 75, 15, 9, 2, 1, 0),
(59, 6, 'Javel', 'Aude', '1982-07-17', 821794, 75, 15, 9, 2, 1, 0),
(60, 6, 'Cronde', 'Chromie', '1982-07-17', 821798, 75, 15, 9, 2, 1, 0),
(61, 6, 'Hurlorage', 'Tyrande', '1982-07-17', 821802, 75, 15, 9, 2, 1, 0),
(62, 6, 'Necroflop', 'Nemsie', '1982-07-17', 821806, 75, 15, 9, 2, 1, 0),
(63, 6, 'Chantelombre', 'Maiev', '1982-07-17', 821810, 75, 15, 9, 2, 1, 0),
(64, 6, 'Curie', 'Marie', '1982-07-17', 821814, 75, 15, 9, 2, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `typeaffrontement`
--

DROP TABLE IF EXISTS `typeaffrontement`;
CREATE TABLE IF NOT EXISTS `typeaffrontement` (
  `Id_Typ_Affrontement` int(11) NOT NULL AUTO_INCREMENT,
  `Libelle_Typ_Affrontement` varchar(50) NOT NULL,
  PRIMARY KEY (`Id_Typ_Affrontement`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `typeaffrontement`
--

INSERT INTO `typeaffrontement` (`Id_Typ_Affrontement`, `Libelle_Typ_Affrontement`) VALUES
(1, 'Combat'),
(2, 'Assaut');

-- --------------------------------------------------------

--
-- Structure de la table `typerole`
--

DROP TABLE IF EXISTS `typerole`;
CREATE TABLE IF NOT EXISTS `typerole` (
  `Id_Typ_Role` int(11) NOT NULL AUTO_INCREMENT,
  `Libelle_Typ_Role` varchar(50) NOT NULL,
  PRIMARY KEY (`Id_Typ_Role`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `typerole`
--

INSERT INTO `typerole` (`Id_Typ_Role`, `Libelle_Typ_Role`) VALUES
(1, 'Responsable Club'),
(2, 'Administrateur'),
(3, 'Officiel'),
(4, 'Opérateur'),
(5, 'membre club');

-- --------------------------------------------------------

--
-- Structure de la table `typestructure`
--

DROP TABLE IF EXISTS `typestructure`;
CREATE TABLE IF NOT EXISTS `typestructure` (
  `Id_Typ_Struct` int(11) NOT NULL AUTO_INCREMENT,
  `Libelle_Typ_Struct` varchar(50) NOT NULL,
  PRIMARY KEY (`Id_Typ_Struct`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `typestructure`
--

INSERT INTO `typestructure` (`Id_Typ_Struct`, `Libelle_Typ_Struct`) VALUES
(1, 'Club'),
(2, 'Fédération'),
(3, 'Comité');

-- --------------------------------------------------------

--
-- Structure de la table `typetableau`
--

DROP TABLE IF EXISTS `typetableau`;
CREATE TABLE IF NOT EXISTS `typetableau` (
  `Id_Typ_Tab` int(11) NOT NULL AUTO_INCREMENT,
  `Libelle_Typ_Tab` varchar(50) NOT NULL,
  PRIMARY KEY (`Id_Typ_Tab`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `typetableau`
--

INSERT INTO `typetableau` (`Id_Typ_Tab`, `Libelle_Typ_Tab`) VALUES
(1, 'Elimatoire'),
(2, 'Linéaire');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `Id_Utilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `Id_Typ_Role_Utilisateur` int(11) NOT NULL,
  `Id_Struct_Utilisateur` int(11) DEFAULT NULL,
  `Nom_Utilisateur` varchar(50) NOT NULL,
  `Prenom_Utilisateur` varchar(50) NOT NULL,
  `Login_Utilisateur` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Mdp` varchar(255) NOT NULL,
  `Tel_Port` varchar(10) DEFAULT NULL,
  `Date_Creation_Compte` date NOT NULL,
  `Dernier_Login_Utilisateur` datetime DEFAULT NULL,
  `Inactif_Utilisateur` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id_Utilisateur`),
  KEY `ROLE_UTILISATEUR_FK` (`Id_Typ_Role_Utilisateur`),
  KEY `STRUCTURE_TRAVAIL_FK` (`Id_Struct_Utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`Id_Utilisateur`, `Id_Typ_Role_Utilisateur`, `Id_Struct_Utilisateur`, `Nom_Utilisateur`, `Prenom_Utilisateur`, `Login_Utilisateur`, `Email`, `Mdp`, `Tel_Port`, `Date_Creation_Compte`, `Dernier_Login_Utilisateur`, `Inactif_Utilisateur`) VALUES
(1, 1, 1, 'Roger', 'Serge', 'ser', 'serge.roger@gmail.com', '$2y$10$FnZkrZYSzDEltAdvitOtUO/AAKteKZWgO.2avPUIfkpuCorJx2l9K', NULL, '1995-01-01', NULL, 0),
(2, 1, 3, 'Sardine', 'Greg', 'greg', 'assos.france@hotmail.com', '$2y$10$vBQh5w.9eRiD9V9ajctIb.WjQv4E.dWT9PiM9QaPdNTQr1I2P/D.u', NULL, '1980-04-05', NULL, 0),
(3, 1, 2, 'Rob', 'Robert', 'rob', 'rob.rob@outlook.fr', '$2y$10$xAXq.gVS8zdvnhuSesoibeJ5wTHKhOufmKrBDHCqJil5FGlmFJn9e', NULL, '2000-01-01', NULL, 0),
(4, 2, 2, 'Martin', 'Julie', 'juju', 'juju@reddit.com', '$2y$10$88Ww6XST4S2XsmfsjqhEweeLWzfaD9m4PX6mLdfC6XmAbOH9KBrfS', NULL, '1988-12-08', NULL, 0),
(6, 3, 3, 'DelaPorte', 'Jean-Oeude', 'jojo', 'jojo@orange.fr', '$2y$10$.LUdU36hx1saZnyeaENJAeawz9rZakHkiGAsVJnYKW2AFXaIv5i.W', NULL, '2003-09-08', NULL, 0),
(7, 4, 4, 'Leconte', 'Philipe', 'phiphi', 'phiphi.loulou@riri.fr', ' $2y$10$.TrESmnEF/0TCxHO6Xtg2e2RQoIutz.Afje3TLx2dDmG2bb5LtBoG', NULL, '1996-04-05', NULL, 0),
(8, 5, 5, 'Palmeri-Guillet', 'Romain', 'rom', 'rom@gmail.com', '$2y$10$rj.66LEylHw.3KPnsHn65e/70t1u8k1VnheJQbmTf9Re.CZI7fktu', NULL, '1989-04-04', NULL, 0);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `competition`
--
ALTER TABLE `competition`
  ADD CONSTRAINT `SEXE_COMPETITION_FK` FOREIGN KEY (`Id_Competition_Sexe`) REFERENCES `sexe` (`Id_Sexe`),
  ADD CONSTRAINT `STRUCTURE_ORGANISATEUR_FK` FOREIGN KEY (`Id_Competition_Struct`) REFERENCES `structure` (`Id_Struct`),
  ADD CONSTRAINT `TYPE_AFFRONTEMENT_FK` FOREIGN KEY (`Id_Competition_Typ_Affrontement`) REFERENCES `typeaffrontement` (`Id_Typ_Affrontement`);

--
-- Contraintes pour la table `concerne`
--
ALTER TABLE `concerne`
  ADD CONSTRAINT `ID_CONCERNE_CATEGORIE_AGE_FK` FOREIGN KEY (`Id_Concerne_Cat_Age`) REFERENCES `categorieage` (`Id_Cat_Age`),
  ADD CONSTRAINT `ID_CONCERNE_TABLEAU_FK` FOREIGN KEY (`Id_Concerne_Tab`) REFERENCES `tableau` (`Id_Tab`);

--
-- Contraintes pour la table `participer`
--
ALTER TABLE `participer`
  ADD CONSTRAINT `ID_PARTICIP_COMPETITION_FK` FOREIGN KEY (`Id_Particip_Competition`) REFERENCES `competition` (`Id_Competition`),
  ADD CONSTRAINT `ID_PARTICIP_TIREUR_FK` FOREIGN KEY (`Id_Particip_Tireur`) REFERENCES `tireur` (`Id_Tireur`),
  ADD CONSTRAINT `ID_Particip_TABLEAU_FK` FOREIGN KEY (`Id_Particip_Tableau`) REFERENCES `tableau` (`Id_Tab`);

--
-- Contraintes pour la table `rencontre`
--
ALTER TABLE `rencontre`
  ADD CONSTRAINT `CATEGORIE_RENCONTRE_FK` FOREIGN KEY (`Id_Cat_Rencontre`) REFERENCES `categorierencontre` (`Id_Cat_Rencontre`),
  ADD CONSTRAINT `ETAT_RENCONTRE_FK` FOREIGN KEY (`Id_Etat_Rencontre`) REFERENCES `etat` (`Id_Etat`),
  ADD CONSTRAINT `RENCONTRE_PRECEDENTE_1_FK` FOREIGN KEY (`Id_Rencontre_Precedent_1`) REFERENCES `rencontre` (`Id_Rencontre`) ON DELETE CASCADE,
  ADD CONSTRAINT `RENCONTRE_PRECEDENTE_2_FK` FOREIGN KEY (`Id_Rencontre_Precedent_2`) REFERENCES `rencontre` (`Id_Rencontre`) ON DELETE CASCADE,
  ADD CONSTRAINT `RENCONTRE_SUIVANTE_FK` FOREIGN KEY (`Id_Rencontre_Suivante`) REFERENCES `rencontre` (`Id_Rencontre`) ON DELETE CASCADE,
  ADD CONSTRAINT `TABLEAU_FK` FOREIGN KEY (`Id_Tab_Rencontre`) REFERENCES `tableau` (`Id_Tab`),
  ADD CONSTRAINT `TIREUR_COIN_BLEU_FK` FOREIGN KEY (`Id_Tireur_Bleu`) REFERENCES `tireur` (`Id_Tireur`),
  ADD CONSTRAINT `TIREUR_COIN_ROUGE_FK` FOREIGN KEY (`Id_Tireur_Rouge`) REFERENCES `tireur` (`Id_Tireur`),
  ADD CONSTRAINT `TIREUR_GAGNANT_FK` FOREIGN KEY (`Id_Gagnant`) REFERENCES `tireur` (`Id_Tireur`);

--
-- Contraintes pour la table `sefaire`
--
ALTER TABLE `sefaire`
  ADD CONSTRAINT `ID_SEFAIRE_CATEGORIE_AGE_FK` FOREIGN KEY (`Id_SeFaire_Cat_Age`) REFERENCES `categorieage` (`Id_Cat_Age`),
  ADD CONSTRAINT `ID_SEFAIRE_COMPETITION_FK` FOREIGN KEY (`Id_SeFaire_Competition`) REFERENCES `competition` (`Id_Competition`);

--
-- Contraintes pour la table `structure`
--
ALTER TABLE `structure`
  ADD CONSTRAINT `RESPONSABLE_STRUCTURE_FK` FOREIGN KEY (`Id_Responsable_Struct`) REFERENCES `utilisateur` (`Id_Utilisateur`),
  ADD CONSTRAINT `TYPE_STRUCTURE_FK` FOREIGN KEY (`Id_Typ_Struct`) REFERENCES `typestructure` (`Id_Typ_Struct`);

--
-- Contraintes pour la table `tableau`
--
ALTER TABLE `tableau`
  ADD CONSTRAINT `TABLEAU_COMPETITION_FK` FOREIGN KEY (`Id_Tableau_Competition`) REFERENCES `competition` (`Id_Competition`),
  ADD CONSTRAINT `TABLEAU_SEXE_FK` FOREIGN KEY (`Id_Tableau_Sexe`) REFERENCES `sexe` (`Id_Sexe`),
  ADD CONSTRAINT `TABLEAU_TYPE_AFFRONTEMENT_FK` FOREIGN KEY (`Id_Tableau_Typ_Affrontement`) REFERENCES `typeaffrontement` (`Id_Typ_Affrontement`),
  ADD CONSTRAINT `TABLEAU_TYPE_TABLEAU_FK` FOREIGN KEY (`Id_Tableau_Typ_Tableau`) REFERENCES `typetableau` (`Id_Typ_Tab`);

--
-- Contraintes pour la table `tableaupoids`
--
ALTER TABLE `tableaupoids`
  ADD CONSTRAINT `ID_CATEGORIE_POIDS_TABLEAU_FK` FOREIGN KEY (`Id_Poids_Tab`) REFERENCES `tableau` (`Id_Tab`),
  ADD CONSTRAINT `ID_CATEGORIE_TABLEAU_POIDS_FK` FOREIGN KEY (`Id_Poids_Cat`) REFERENCES `categoriepoids` (`Id_Cat_Poids`);

--
-- Contraintes pour la table `tireur`
--
ALTER TABLE `tireur`
  ADD CONSTRAINT `CATEGORIE_AGE_TIREUR_FK` FOREIGN KEY (`Id_Tir_Cat_Age`) REFERENCES `categorieage` (`Id_Cat_Age`),
  ADD CONSTRAINT `CATEGORIE_POIDS_TIREUR_FK` FOREIGN KEY (`Id_Tir_Cat_Poids`) REFERENCES `categoriepoids` (`Id_Cat_Poids`),
  ADD CONSTRAINT `NIVEAU_TIREUR_FK` FOREIGN KEY (`Id_Tir_Niv_Tireur`) REFERENCES `niveautireur` (`Id_Niv_Tir`),
  ADD CONSTRAINT `SEXE_TIREUR_FK` FOREIGN KEY (`Id_Tir_Sexe`) REFERENCES `sexe` (`Id_Sexe`),
  ADD CONSTRAINT `STRUCTURE_TIREUR_FK` FOREIGN KEY (`Id_Tir_Struct`) REFERENCES `structure` (`Id_Struct`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `ROLE_UTILISATEUR_FK` FOREIGN KEY (`Id_Typ_Role_Utilisateur`) REFERENCES `typerole` (`Id_Typ_Role`),
  ADD CONSTRAINT `STRUCTURE_TRAVAIL_FK` FOREIGN KEY (`Id_Struct_Utilisateur`) REFERENCES `structure` (`Id_Struct`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
