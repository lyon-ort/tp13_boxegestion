<?php

require '../config/config.php';

// Déclaration UtilisateurRepository
$utilisateurRepository = new UtilisateurRepository();

// Déclaration du tableau des valeurs
$valeurUtilisateur = array(
    "Id_Typ_Role_Utilisateur" => 1,
    "Id_Struct_Utilisateur" => 1,
    "Nom_Utilisateur" => "nom",
    "Prenom_Utilisateur" => "test",
    "Login_Utilisateur" => "loginTest",
    "Email" => "test@test.comTest",
    "Mdp" => "mdpTtest",
    "Tel_Port" => "0874568923",
    "Date_Creation_Compte" => "2019-08-08",
    "Dernier_Login_Utilisateur" => "2020-02-27 03:10:12"
);

// Initialisation et dump() des valeurs
$utilisateur = new Utilisateur($valeurUtilisateur);
var_dump($utilisateur);

// Fonction sauver pour créer ou modifier un utilisateur
$utilisateurRepository->sauver($utilisateur);

