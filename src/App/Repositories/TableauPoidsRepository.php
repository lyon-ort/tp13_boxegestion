<?php

require '../src/App/Entities/TableauPoids.php';

class TableauPoidsRepository {

    protected $table;     
    protected $classMapped; 
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire)
    public function __construct() {
        $this->table = 'TableauPoids';
        $this->classMapped = 'App\Entities\TableauPoids';
        $this->idFieldName = 'Id_Tab_Poids';
    }
    
    
    public function sauver(TableauPoids $entity) {
        $db = dbConnect();
        $resultSet = NULL;
        if ($entity != NULL) {
            $bindParam = $entity->__toArray();
            if ($entity->getId_Tab_Poids() == NULL) {
                // Nouvelle entité
                $query = "INSERT INTO $this->table" .
                        " (Id_Poids_Tab, "
                        . "Id_Poids_Cat) "
                        . " VALUES "
                        . "(:Id_Poids_Tab, "
                        . ":Id_Poids_Cat)";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Id_Poids_Tab', $bindParam['Id_Poids_Tab'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Poids_Cat', $bindParam['Id_Poids_Cat'], \PDO::PARAM_INT);
                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $entity->setid($db->lastInsertId());
                    $resultSet = $entity;
                }
            } else {
                //  Entité existante
                $query = "UPDATE $this->table"
                        . " SET Id_Poids_Tab = :Id_Poids_Tab, "
                        . " Id_Poids_Cat = :Id_Poids_Cat "
                        . " WHERE $this->idFieldName = :Id_Tab_Poids";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Id_Poids_Tab', $bindParam['Id_Poids_Tab'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Poids_Cat', $bindParam['Id_Poids_Cat'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Tab_Poids', $bindParam[$this->idFieldName], \PDO::PARAM_INT);

                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $resultSet = $entity;
                }
            }
        }
        return $resultSet;
    }  
    
    
    // Sélectionner un tableau selon un id
    public function selectionTableauPoidsIdTab($idTableau)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $objet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Poids_Tab = :idTableauMark");
        $req->execute(array('idTableauMark' => $idTableau));

        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$objet = new $this->table($row);
	    }
        }
        
        // Return un objet
        return $objet;        
    }
    
    
    // Supprimer le tableauPoids à partie d'un ID de tableau
    public function supprimerTableauPoidsIdTab($idTableau)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Requête
        $req = $db->prepare("DELETE FROM $this->table WHERE Id_Poids_Tab = :idTableauMark");
        $req->execute(array('idTableauMark' => $idTableau));
    }    
}