<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Modifier tireur </title>
    </head>
    
    <body>
        <!-- Header (inc) -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>

        <!-- Section -->
        <section class='container-fluid about'>
            
            <!-- Information de l'utilisateur (inc) -->
            <?php
            include '../inc/information.php';
            ?>
            
            <h1> Modification tireur </h1>
            
            <hr class="separator">

            <?php
            // Message d'erreur si le nom existe déjà
            if (isset($_POST['error']))
            {
                echo 'ERREUR : Ce "Numéro de licence" de tireur existe déjà <br/><br/>';
                $error = 'error';
            }
            else
            {
                $error = '';
            }
            
            // Récupération du tireur
            $objetTireurRep = new TireurRepository();
            $objetTireur = $objetTireurRep->selectionTireurId($_POST['Id_Tireur']);
            ?>


            <!-- Formulaire Modifier tireur -->
            <form class="formCreat" method="POST" action="modifSuprTireur.php?connect=1">
            
                <!-- Récupération des niveaux de tireurs -->
                Niveau de tireur : 
                <select name="Id_Tir_Niv_TireurModif" required>
                    <option value=""> -- Choisir un niveau -- </option>
                    <?php
                        // Niveau tireur
                        $objetNiveauTireurRep = new NiveauTireurRepository();
                        $arrayNiveauTireur = $objetNiveauTireurRep->selectionNiveauTireur();
                        
                        foreach ($arrayNiveauTireur as $value)
                        {
                            $POSTouGetObjet = (isset($_POST['Id_Tir_Niv_Tireur']) ? $_POST['Id_Tir_Niv_Tireur'] : $objetTireur->getId_Tir_Niv_Tireur());
                            $selected = ($value->getId_Niv_Tir() == $POSTouGetObjet) ? 'selected' : '';
                            ?>
                                <option value="<?= $value->getId_Niv_Tir() ?>" <?= $selected ?>> <?= $value->getLibelle_Niv_Tir() ?> </option> 
                            <?php
                        }
                    ?>  
                </select> 
                <br/><br/>

                Nom : 
                <input type="text" name="Nom_TireurModif" value="<?= $valueNom = (isset($_POST['Nom_Tireur'])) ? $_POST['Nom_Tireur'] : $objetTireur->getNom_Tireur()?>"> 
                <br/><br/>

                Prénom : 
                <input type="text" name="Prenom_TireurModif" value="<?= $valuePrenom = (isset($_POST['Prenom_Tireur'])) ? $_POST['Prenom_Tireur'] : $objetTireur->getPrenom_Tireur()?>"> 
                <br/><br/>

                Date de naissance : 
                <input type="date" name="Date_NaissanceModif" value="<?= $valueDate = (isset($_POST['Date_Naissance'])) ? $_POST['Date_Naissance'] : $objetTireur->getDate_Naissance()?>"> 
                <br/><br/>

                Numéro de licence : 
                <input id="<?= $error ?>" type="number" name="Num_LicenceModif" value="<?= $valueNum = (isset($_POST['Num_Licence'])) ? $_POST['Num_Licence'] : $objetTireur->getNum_Licence()?>"> 
                <input type="hidden" name="NumeDeBase" value="<?= $valueNum = (isset($_POST['NumeDeBase'])) ? $_POST['NumeDeBase'] : $objetTireur->getNum_Licence()?>"> 
                <br/><br/>

                Poids : 
                <input type="number" min="0" max="600" name="Poids_TireurModif" value="<?= $valueNum = (isset($_POST['Poids_Tireur'])) ? $_POST['Poids_Tireur'] : $objetTireur->getPoids_Tireur()?>"> 
                <br/><br/>

                <!-- Récupération des sexe -->
                Sexe : 
                <select name="Id_Tir_SexeModif" required>
                    <option value=""> -- Choisir un sexe -- </option>
                    <?php
                        // Niveau tireur
                        $objetSexeRep = new SexeRepository();
                        $arraySexe = $objetSexeRep->selectionSexe();

                        foreach ($arraySexe as $value)
                        {
                            $POSTouGetObjet = (isset($_POST['Id_Tir_Sexe']) ? $_POST['Id_Tir_Sexe'] : $objetTireur->getId_Tir_Sexe());
                            $selected = ($value->getId_Sexe() == $POSTouGetObjet) ? 'selected' : '';
                            ?>
                                <option value="<?= $value->getId_Sexe() ?>" <?= $selected ?>> <?= $value->getLibelle_Sexe() ?> </option> 
                            <?php
                        }
                    ?>  
                </select> 
                <br/><br/>

                Structure :
                <?php
                $objetStructureRepUtil = new StructureRepository(); 
                $structure = $objetStructureRepUtil->selectionStructureId($_SESSION['idStruct']);
                ?>   
                <input type="text" value="<?= $structure->getNom_Struct() ?>" readonly> 
                <input type="hidden" name="Id_Tir_StructModif" value="<?= $_SESSION['idStruct'] ?>"> 
                <br/><br/>
                    
                <!-- L'ID du tireur -->
                <input type="hidden" name="Id_TireurModif" value="<?= $_POST['Id_Tireur'] ?>" />
                    
                <input type="submit" value="Modifier">
            </form>        
        </section>
    </body>
</html>