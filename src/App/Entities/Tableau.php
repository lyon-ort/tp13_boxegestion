<?php

class Tableau {

    // Attributs
    protected $Id_Tab;
    protected $Libelle_Tab;
    protected $Nb_Max_Rencontre;
    protected $Id_Tableau_Sexe;
    protected $Id_Tableau_Typ_Affrontement;
    protected $Id_Tableau_Typ_Tableau;
    protected $Id_Tableau_Competition;

    // Constructeur
    public function __construct(array $data = NULL) {
        $this->hydrate($data);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas = NULL) {
        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Appelle le mutateur des attributs existant dans le tableau $datas
        foreach ($attrib as $key => $val) {
            // Si, dans vos habitudes, les champs de la bdd sont en minuscules, décommentez la ligne suivante.
            //$key = strtolower($key);
            // Si, dans vos habitudes, les champs de la bdd sont en majuscules, décommentez la ligne suivante.
            //$key = strtoupper($key);
            //  Perso, je met tout en notation camelCase... même dans la BDD.
            if (isset($datas[$key])) {
                $mutateur = 'set' . $key;
                $this->$mutateur($datas[$key]);
            }
        }

        return $this;
    }
    
            
        /**
     * Transforme un objet en un tableau associatif.
     * @return array tableau associatif des attributs => valeurs
     */
    public function __toArray() {
        return $this->jsonSerialize();
    }

    /**
     * La classe implémente l'interface JsonSerializable ce qui permet 
     * aux classes concrête de pouvoir être sérialisée en JSON avec json_encode()
     * @return Array le tableau pour conversion en json
     */
    public function jsonSerialize() {
        $array = array();

        //  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
        $attrib = get_class_vars(get_class($this));

        // Associe la clé du nom de l'attribut à la valeur de cet attribut
        foreach ($attrib as $key => $val) {
            $array[$key] = $this->get($key);
        }
        return $array;
    }

    /**
     * Lecteur simple d'accés en lecture à un attribut
     * Peut être surchargé.
     * @param type $attribut le nom de l'attribut
     * @return type la valeur de l'attribut
     */
    protected function get($attribut) {
        return $this->$attribut;
    }
    
        /**
     * Surcharge du mutateur pour l'attribut $id qui ne peut pas être modifié.
     * @param int $val ne sert que si id_pers est null...
     */
    public function setid_pers($val=null)
    {
	if($this->Id_Tab === null)
	    $this->Id_Tab = (int)$val;
    }
    
    // Ces deux méthodes ne sont nécessaire QUE si la clé primaire dans la table
    // porte un nom différent de id...
    /**
     * Permet de s'affranchir du fait que le champ id dans la table est différent...
     * @param type $val
     */
    public function setid($val=null)
    {
	$this->setid_pers($val);
    }

    // GET et SET
    function getId_Tab() {
        return $this->Id_Tab;
    }

    function getLibelle_Tab() {
        return $this->Libelle_Tab;
    }

    function getNb_Max_Rencontre() {
        return $this->Nb_Max_Rencontre;
    }

    function getId_Tableau_Sexe() {
        return $this->Id_Tableau_Sexe;
    }

    function getId_Tableau_Typ_Affrontement() {
        return $this->Id_Tableau_Typ_Affrontement;
    }

    function getId_Tableau_Typ_Tableau() {
        return $this->Id_Tableau_Typ_Tableau;
    }

    function getId_Tableau_Competition() {
        return $this->Id_Tableau_Competition;
    }

    function setId_Tab($Id_Tab): void {
        $this->Id_Tab = $Id_Tab;
    }

    function setLibelle_Tab($Libelle_Tab): void {
        $this->Libelle_Tab = $Libelle_Tab;
    }

    function setNb_Max_Rencontre($Nb_Max_Rencontre): void {
        $this->Nb_Max_Rencontre = $Nb_Max_Rencontre;
    }

    function setId_Tableau_Sexe($Id_Tableau_Sexe): void {
        $this->Id_Tableau_Sexe = $Id_Tableau_Sexe;
    }

    function setId_Tableau_Typ_Affrontement($Id_Tableau_Typ_Affrontement): void {
        $this->Id_Tableau_Typ_Affrontement = $Id_Tableau_Typ_Affrontement;
    }

    function setId_Tableau_Typ_Tableau($Id_Tableau_Typ_Tableau): void {
        $this->Id_Tableau_Typ_Tableau = $Id_Tableau_Typ_Tableau;
    }

    function setId_Tableau_Competition($Id_Tableau_Competition): void {
        $this->Id_Tableau_Competition = $Id_Tableau_Competition;
    }

}
