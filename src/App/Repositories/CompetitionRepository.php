<?php

require '../src/App/Entities/Competition.php';

class CompetitionRepository {

    protected $table;     
    protected $classMapped; 
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire) */
    public function __construct() {
        $this->table = 'Competition';
        $this->classMapped = 'App/Entities/Competition';
        $this->idFieldName = 'Id_Competition';
    }
    
    
    //Ajouter ou modifier un champ
    public function sauver(Competition $entity) {
        $db = dbConnect();
        $resultSet = NULL;
        if ($entity != NULL) {
            $bindParam = $entity->__toArray();
            if ($entity->getId_Competition() == NULL) {
                // Nouvelle entité
                $query = "INSERT INTO $this->table" .
                        " (Nom_Competition, "
                        . "Date_Debut_Competition, "
                        . "Date_Fin_Competition, "
                        . "Adresse_Competition, "
                        . "Cp_Competition, "
                        . "Ville_Competition, "
                        . "Date_Limite_Inscript_Competition, "
                        . "Nb_Round, "
                        . "Duree_Round, "
                        . "Nb_Ring, "
                        . "Id_Competition_Sexe, "
                        . "Id_Competition_Typ_Affrontement, "
                        . "Id_Competition_Struct, "
                        . "Inactif_Competition)"
                        . " VALUES "
                        . "(:Nom_Competition, "
                        . ":Date_Debut_Competition, "
                        . ":Date_Fin_Competition, "
                        . ":Adresse_Competition, "
                        . ":Cp_Competition, "
                        . ":Ville_Competition, "
                        . ":Date_Limite_Inscript_Competition, "
                        . ":Nb_Round, "
                        . ":Duree_Round, "
                        . ":Nb_Ring, "
                        . ":Id_Competition_Sexe, "
                        . ":Id_Competition_Typ_Affrontement, "
                        . ":Id_Competition_Struct, "
                        . ":Inactif_Competition)";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Nom_Competition', $bindParam['Nom_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Date_Debut_Competition', $bindParam['Date_Debut_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Date_Fin_Competition', $bindParam['Date_Fin_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Adresse_Competition', $bindParam['Adresse_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Cp_Competition', $bindParam['Cp_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Ville_Competition', $bindParam['Ville_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Date_Limite_Inscript_Competition', $bindParam['Date_Limite_Inscript_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Nb_Round', $bindParam['Nb_Round'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Duree_Round', $bindParam['Duree_Round'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Nb_Ring', $bindParam['Nb_Ring'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Competition_Sexe', $bindParam['Id_Competition_Sexe'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Competition_Typ_Affrontement', $bindParam['Id_Competition_Typ_Affrontement'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Competition_Struct', $bindParam['Id_Competition_Struct'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Inactif_Competition', $bindParam['Inactif_Competition'], \PDO::PARAM_INT);
                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $entity->setid($db->lastInsertId());
                    $resultSet = $entity;
                }
            } else {
                //  Entité existante
                $query = "UPDATE $this->table"
                        . " SET Nom_Competition = :Nom_Competition, "
                        . " Date_Debut_Competition = :Date_Debut_Competition, "
                        . " Date_Fin_Competition = :Date_Fin_Competition, "
                        . " Adresse_Competition = :Adresse_Competition, "
                        . " Cp_Competition = :Cp_Competition, "
                        . " Ville_Competition = :Ville_Competition, "
                        . " Date_Limite_Inscript_Competition = :Date_Limite_Inscript_Competition, "
                        . " Nb_Round = :Nb_Round, "
                        . " Duree_Round = :Duree_Round, "
                        . " Nb_Ring = :Nb_Ring, "
                        . " Id_Competition_Sexe = :Id_Competition_Sexe, "
                        . " Id_Competition_Typ_Affrontement = :Id_Competition_Typ_Affrontement, "
                        . " Id_Competition_Struct = :Id_Competition_Struct, "
                        . " Inactif_Competition = :Inactif_Competition "
                        . " WHERE $this->idFieldName = :Id_Competition";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Nom_Competition', $bindParam['Nom_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Date_Debut_Competition', $bindParam['Date_Debut_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Date_Fin_Competition', $bindParam['Date_Fin_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Adresse_Competition', $bindParam['Adresse_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Cp_Competition', $bindParam['Cp_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Ville_Competition', $bindParam['Ville_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Date_Limite_Inscript_Competition', $bindParam['Date_Limite_Inscript_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Nb_Round', $bindParam['Nb_Round'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Duree_Round', $bindParam['Duree_Round'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Nb_Ring', $bindParam['Nb_Ring'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Competition_Sexe', $bindParam['Id_Competition_Sexe'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Competition_Typ_Affrontement', $bindParam['Id_Competition_Typ_Affrontement'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Competition_Struct', $bindParam['Id_Competition_Struct'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Inactif_Competition', $bindParam['Inactif_Competition'], \PDO::PARAM_INT);                
                $reqPrep->bindParam(':Id_Competition', $bindParam[$this->idFieldName], \PDO::PARAM_INT);

                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $resultSet = $entity;
                }
            }
        }
        return $resultSet;
    }
    
    
    
    // Sélectionne les compétition en cours
    public function selectionCompetitionEnCours()
    {        
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objet à NULL si jamais la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->query("SELECT * FROM $this->table "
                . "WHERE NOW() > Date_Debut_Competition AND NOW() < Date_Fin_Competition AND Inactif_Competition = 0 "
                . "ORDER BY Date_Debut_Competition");

        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;
    }
    
    
    
    // Sélectionne les compétition fini
    public function selectionCompetitionFini()
    {        
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objet à NULL si jamais elle la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->query("SELECT * FROM $this->table "
                . "WHERE NOW() > Date_Fin_Competition AND Inactif_Competition = 0 "
                . "ORDER BY Date_Debut_Competition");

        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;
    }

    
    
    // Sélectionne les compétition à venir
    public function selectionCompetitionAVenir()
    {        
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objet à NULL si jamais elle la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->query("SELECT * FROM $this->table "
                . "WHERE NOW() < Date_Debut_Competition AND Inactif_Competition = 0 "
                . "ORDER BY Date_Limite_Inscript_Competition");

        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;
    }
    
    
    
    // Sélectionner une compétition selon un id
    public function selectionCompetitionId($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $objet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE $this->idFieldName = :idMark");
        $req->execute(array('idMark' => $id));

        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$objet = new $this->table($row);
	    }
        }
        
        // Return un objet
        return $objet;        
    }
    
    
    
    // Sélectionne toutes les compétitions
    public function selectionCompetition()
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objets à NULL si jamais elle la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->query("SELECT * FROM $this->table WHERE Inactif_Competition = 0");
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;
    }
    
    
    
    // Sélectionner une compétition selon un id
    public function selectionCompetitionNom($nom)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $objet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE Nom_Competition = :nomMark");
        $req->execute(array('nomMark' => $nom));

        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$objet = new $this->table($row);
	    }
        }
        
        // Return un objet
        return $objet;        
    }    
    
    
    
    // Sélectionne toutes les compétitions
    public function selectionCompetitionidStructure($idStructure)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objets à NULL si jamais la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Competition_Struct = :idStructureMark"); // Le flag n'est pas utilisé dans cette requête
        $req->execute(array('idStructureMark' => $idStructure));
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;
    }    
    
    
    
    public function numRing($idTab)
    {
        $db = dbConnect();
        
        $res = NULL;
        
        $query = "SELECT Competition.Nb_Ring FROM Competition, Tableau WHERE Competition.Id_Competition = Tableau.Id_Tableau_Competition AND Tableau.Id_Tab = $idTab ;";
        $req = $db->query($query);
        
        if ($req) 
        {
            while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
                $res['Nb_Ring'] = (int) $row['Nb_Ring'];
            }
        }
        return $res;
    }
    
    
    
    
    public function limiteDateRencontre($idTab)
    {
        $db = dbConnect();
        $req = $db->prepare("SELECT Competition.Date_Debut_Competition, Competition.Date_Fin_Competition FROM Competition,Tableau WHERE Competition.Id_Competition = Tableau.Id_Tableau_Competition AND Tableau.Id_Tab = :idTabMark ");
        $req->execute(array('idTabMark' => $idTab));
        $data = $req->fetchAll();
        
        return $data;
    }    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // Selectionne tous les champs de toutes les compétitions
    function selectionCompet()
    {
        $db = dbConnect();
        $req = $db->query("SELECT * FROM $this->table WHERE Inactif_Competition = 0");
        
        return $req;
    }
    
    
    // Séléctionne le nom des compétitions selon un idCompetParticiper
    public function selectionCompetId_compet_part($idCompetPart)
    {
        $db = dbConnect();
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Competition = :idCompetPartMark");
        $req->execute(array('idCompetPartMark' => $idCompetPart));
        
        while($data = $req->fetch())
        {
            $competition = array('Nom_Competition' => $data['Nom_Competition']);
        }
        
        return $competition;
    }
    
    
    
    // Supprimer la compétition
    public function suppressionCompetition($idSupr)
    {
        $db = dbConnect();        
        
        $req2 = $db->prepare("UPDATE competition SET Inactif_Competition = 1 WHERE Id_Competition = :idSuprMark");
        $req2->execute(array('idSuprMark' => $idSupr));
        
        header('location: modifSuprimCompet.php');
    }
    
    
    // Séléctionne l'ID de la compétition selon son nom
    public function selectionId_Nom($nomCompet)
    {
        $db = dbConnect();
        
        $req = $db->prepare("SELECT Id_Competition FROM $this->table WHERE Nom_Competition = :nomCompetMark");
        $req->execute(array('nomCompetMark' => $nomCompet));
        
        while ($data = $req->fetch())
        {
            $id = $data['Id_Competition'];
        }
        
        return $id;
    }
    
    
    // Sélectionner une compétition avec l'idCompetition d'un tableau
    public function selectionCompetIdTab($idCompetTab)
    {
        $db = dbConnect();
        
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Competition = :idCompetTabMark");
        $req->execute(array('idCompetTabMark' => $idCompetTab[0]['Id_Tableau_Competition']));

        $laCompetition = $req->fetchAll();
        
        return $laCompetition;
    }
    
    
    // Sélectionner une compétition avec l'idCompetition
    public function selectionCompetId($idCompetTab)
    {
        $db = dbConnect();
        
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Competition = :idCompetTabMark");
        $req->execute(array('idCompetTabMark' => $idCompetTab));

        $laCompetition = $req->fetch();
        
        return $laCompetition['Nom_Competition'];
    }    
}