<?php

require '../src/App/Entities/CategorieAge.php';

class CategorieAgeRepository {

    protected $table;     
    protected $classMapped; 
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire)
    public function __construct() 
    {
        $this->table = 'CategorieAge';
        $this->classMapped = 'App\Entities\CategorieAge';
        $this->idFieldName = 'Id_Cat_Age';
    }
    
    
    
    // Sélectionne toutes les catégories d'e poids'age
    public function selectionCategorieAge()
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objets à NULL si jamais elle la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->query("SELECT * FROM $this->table");
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;
    }       
    
    
    
    // Sélectionne la categorie age selon un id
    public function selectionCategorieAgeId($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $objet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE $this->idFieldName = :idMark");
        $req->execute(array('idMark' => $id));
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$objet = new $this->table($row);
	    }
        }
        
        // Return de l'objet
        return $objet;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // Sélectionne toutes les catégories d'age dans un tableau
    public function selectCatAge()
    {
        $db = dbConnect();
        $req = $db->query("SELECT * FROM $this->table");
        $i = 0;
        
        while ($data = $req->fetch())
        {
            $tableauCatAge[$i] = array(
                'Id_Cat_Age' => $data['Id_Cat_Age'],
                'Libelle_Cat_Age' => $data['Libelle_Cat_Age'],
                'Age_Max' => $data['Age_Max'],
                'Age_Min' => $data['Age_Min']
            );
            $i++;
        }
        
        return $tableauCatAge;
    }
    
    
    // Sélectionne "Libelle_Cat_Age" selon l'id transmis
    public function selectCatAgeId($idCatAge)
    {
        $db = dbConnect();
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Cat_Age = :idCatAgeMark");
        $req->execute(array('idCatAgeMark' => $idCatAge));
        
        $data = $req->fetch();
        
        return $data['Libelle_Cat_Age'];
    }
}
