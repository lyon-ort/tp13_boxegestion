<?php

require '../src/App/Entities/Logbdd.php';

class LogbddRepository {

    protected $table;
    protected $classMapped;
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire) 
    public function __construct() {
        $this->table = 'Logbdd';
        $this->classMapped = 'Logbdd';
        $this->idFieldName = 'Id_Logbdd';
    }

    
    // Ajouter / Modifier un log
    private function sauver(Logbdd $entity) {
        $db = dbConnect();
        $resultSet = NULL;
        if ($entity != NULL) {
            $bindParam = $entity->__toArray();
            $query = "INSERT INTO $this->table" .
                    " (Login, "
                    . "Date_Modif, "
                    . "Nom_Table, "
                    . "Type_Operation, "
                    . "Id_Element)"
                    . " VALUES "
                    . "(:Login, "
                    . ":Date_Modif, "
                    . ":Nom_Table, "
                    . ":Type_Operation, "
                    . ":Id_Element)";

            $reqPrep = $db->prepare($query);
            $reqPrep->bindParam(':Login', $bindParam['Login'], \PDO::PARAM_STR);
            $reqPrep->bindParam(':Date_Modif', $bindParam['Date_Modif'], \PDO::PARAM_STR);
            $reqPrep->bindParam(':Nom_Table', $bindParam['Nom_Table'], \PDO::PARAM_STR);
            $reqPrep->bindParam(':Type_Operation', $bindParam['Type_Operation'], \PDO::PARAM_STR);
            $reqPrep->bindParam(':Id_Element', $bindParam['Id_Element'], \PDO::PARAM_STR);
            $reqPrep->execute();

            if ($reqPrep != FALSE) {
                $entity->setId_Logbdd($db->lastInsertId());
                $resultSet = $entity;
            }
        }
        return $resultSet;
    }
	
	public function sauverInsertion($Login,$Nom_Table,$Id_Element) {
		$valeurLogbdd = array(
			"Login" => $Login,
			"Date_Modif" => date("Y-m-d H:i:s"),
			"Nom_Table" => $Nom_Table,
			"Type_Operation" => "Insertion",
			"Id_Element" => $Id_Element
		);
		$logbdd = new Logbdd($valeurLogbdd);
		return $this->sauver($logbdd);
	}
	
	public function sauverModification($Login,$Nom_Table,$Id_Element) {
		$valeurLogbdd = array(
			"Login" => $Login,
			"Date_Modif" => date("Y-m-d H:i:s"),
			"Nom_Table" => $Nom_Table,
			"Type_Operation" => "Modification",
			"Id_Element" => $Id_Element
		);
		$logbdd = new Logbdd($valeurLogbdd);
		return $this->sauver($logbdd);
	}
	
	public function sauverSuppression($Login,$Nom_Table,$Id_Element) {
		$valeurLogbdd = array(
			"Login" => $Login,
			"Date_Modif" => date("Y-m-d H:i:s"),
			"Nom_Table" => $Nom_Table,
			"Type_Operation" => "Suppression",
			"Id_Element" => $Id_Element
		);
		$logbdd = new Logbdd($valeurLogbdd);
		return $this->sauver($logbdd);
	}
}
