<?php

require '../src/App/Entities/SeFaire.php';

class SeFaireRepository {

    protected $table;     
    protected $classMapped; 
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire)
    public function __construct() {
        $this->table = 'SeFaire';
        $this->classMapped = 'App\Entities\SeFaire';
        $this->idFieldName = 'Id_SeFaire';
    }
    
    
    
        //Ajouter ou modifier un champ
    public function sauver(SeFaire $entity) {
        $db = dbConnect();
        $resultSet = NULL;
        if ($entity != NULL) {
            $bindParam = $entity->__toArray();
            if ($entity->getId_SeFaire() == NULL) {
                // Nouvelle entité
                $query = "INSERT INTO $this->table" .
                        " (Id_SeFaire_Competition, "
                        . "Id_SeFaire_Cat_Age) "
                        . " VALUES "
                        . "(:Id_SeFaire_Competition, "
                        . ":Id_SeFaire_Cat_Age)";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Id_SeFaire_Competition', $bindParam['Id_SeFaire_Competition'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_SeFaire_Cat_Age', $bindParam['Id_SeFaire_Cat_Age'], \PDO::PARAM_INT);
                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $entity->setid($db->lastInsertId());
                    $resultSet = $entity;
                }
            } else {
                //  Entité existante
                $query = "UPDATE $this->table"
                        . " SET Nom_Competition = :Nom_Competition, "
                        . " Date_Debut_Competition = :Date_Debut_Competition, "
                        . " Date_Fin_Competition = :Date_Fin_Competition, "
                        . " Adresse_Competition = :Adresse_Competition, "
                        . " Cp_Competition = :Cp_Competition, "
                        . " Ville_Competition = :Ville_Competition, "
                        . " Date_Limite_Inscript_Competition = :Date_Limite_Inscript_Competition, "
                        . " Nb_Round = :Nb_Round, "
                        . " Duree_Round = :Duree_Round, "
                        . " Nb_Ring = :Nb_Ring, "
                        . " Id_Competition_Sexe = :Id_Competition_Sexe, "
                        . " Id_Competition_Typ_Affrontement = :Id_Competition_Typ_Affrontement, "
                        . " Id_Competition_Struct = :Id_Competition_Struct"
                        . " WHERE $this->idFieldName = :Id_Competition";

                $reqPrep = $db->prepare($query);
                $reqPrep->bindParam(':Nom_Competition', $bindParam['Nom_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Date_Debut_Competition', $bindParam['Date_Debut_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Date_Fin_Competition', $bindParam['Date_Fin_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Adresse_Competition', $bindParam['Adresse_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Cp_Competition', $bindParam['Cp_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Ville_Competition', $bindParam['Ville_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Date_Limite_Inscript_Competition', $bindParam['Date_Limite_Inscript_Competition'], \PDO::PARAM_STR);
                $reqPrep->bindParam(':Nb_Round', $bindParam['Nb_Round'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Duree_Round', $bindParam['Duree_Round'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Nb_Ring', $bindParam['Nb_Ring'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Competition_Sexe', $bindParam['Id_Competition_Sexe'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Competition_Typ_Affrontement', $bindParam['Id_Competition_Typ_Affrontement'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Competition_Struct', $bindParam['Id_Competition_Struct'], \PDO::PARAM_INT);
                $reqPrep->bindParam(':Id_Competition', $bindParam[$this->idFieldName], \PDO::PARAM_INT);

                $reqPrep->execute();

                if ($reqPrep != FALSE) {
                    $resultSet = $entity;
                }
            }
        }
        return $resultSet;
    }
    
    
    
    // Sélectionne toutes les catégorie de poids des compétitions selon un id de compétition
    public function selectionSeFaireIdCompetition($idCompetition)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objets à NULL si jamais elle la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_SeFaire_Competition = :idCompetitionMark");
        $req->execute(array('idCompetitionMark' => $idCompetition));
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;
    }    
    
    
    // Supprime toutes les catégories d'age d'une compétition selon un id de compétition
    public function SupprimerSeFaireIdCompetition($idCompetition)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du test d'objet à NULL si jamais la requête n'est pas bonne
        $testReq = NULL;       
        
        // Requête
        $req = $db->prepare("DELETE FROM $this->table WHERE Id_SeFaire_Competition = :idCompetitionMark");
        $req->execute(array('idCompetitionMark' => $idCompetition));
        
        // Test si la requête est bonne
        if ($req)
        {
            $testReq = true;
        }
        
        return $testReq;
    }    
}