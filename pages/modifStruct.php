<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Modifier structure </title>
    </head>
    
    <body>
        <!-- Header (inc) -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>
        
        
        <!-- Section -->
        <section class='container-fluid about'>
            
            <!-- Information de l'utilisateur (inc) -->
            <?php
            include '../inc/information.php';
            ?>
            
            <h1> Modifier structures </h1>
            
            <hr class="separator">
            
            <?php
            // Message d'erreur si le nom existe déjà
            if (isset($_POST['error']))
            {
                echo '<p class="error">ERREUR : Ce "nom" de structure existe déjà</p> <br/><br/>';
                $error = 'error';
            }
            else
            {
                $error = '';
            }
            
            // Objet de la structure à modifier
            $objetStructureRep = new StructureRepository();
            $objetStructure = $objetStructureRep->selectionStructureId($_POST['Id_Struct']);
            ?>

            <!-- Formulaire création structure -->
            <form class="formCreat" method="POST" action="modifSuprStruct.php?connect=1">
                
                <!-- L'ID de la structure en "hidden" -->
                <input type="hidden" name="Id_StructModif" value="<?= $objetStructure->getId_Struct() ?>"/>
                
                <!-- Responsable de structure -->
                <?php
                    $objetTypeStructureRep = new TypeStructureRepository();
                    $objetTypeStructure = $objetTypeStructureRep->selectionTypeStructureId($objetStructure->getId_Typ_Struct());
                ?>
                <strong> Type de structure: </strong> 
                <input type="text" value="<?= $objetTypeStructure->getLibelle_Typ_Struct() ?>" disabled>
                <input type="hidden" name="Id_Typ_StructModif" value="<?= $objetTypeStructure->getId_Typ_Struct() ?>"/>
                <br/><br/>

                <strong> Responsable de structure : </strong>
                <?php
                    // Récupère tous les utilisateurs de la structure
                    $objetUtilisateurRep = new UtilisateurRepository;
                    $arrayObjetUtilisateur = $objetUtilisateurRep->selectionArrayUtilisateurIdStructure($_POST['Id_Struct']);
                ?>
                <select name="Id_Responsable_StructModif">
                    <?php
                        foreach ($arrayObjetUtilisateur as $value)
                        {
                            if ($value->getId_Typ_Role_Utilisateur() != 5) // 5 = membre de club
                            {
                                $POSTouGetObjet = (isset($_POST['Id_Responsable_Struct']) ? $_POST['Id_Responsable_Struct'] : $objetStructure->getId_Responsable_Struct());
                                $selected = ($value->getId_Utilisateur() == $POSTouGetObjet) ? 'selected' : '';
                                // Préselectionne l'id du responsable de structure
                                //$selected = ($objetStructure->getId_Responsable_Struct() == $value->getId_Utilisateur()) ? 'selected' : '';
                                
                                ?>
                                    <option value="<?= $value->getId_Utilisateur() ?>" <?= $selected ?>> <?= $value->getLogin_Utilisateur() ?> </option>
                                <?php
                            }
                        }
                    ?>
                </select>
                <br/><br/>
                

                <!-- Nom -->
                <strong> Nom : </strong> 
                <input id="<?= $error ?>" type="text" name="Nom_StructModif" placeholder="nom" value="<?=$valueNom=(isset($_POST['Nom_Struct']))?$_POST['Nom_Struct']:$objetStructure->getNom_Struct()?>" required>
                <input type="hidden" name="NomDeBase" value="<?= $valueNomDeBase = (isset($_POST['NomDeBase'])) ? $_POST['NomDeBase'] : $objetStructure->getNom_Struct() ?>" />
                <br/><br/>     

                <!-- Adresse -->
                <strong> Adresse : </strong> 
                <input type="text" name="Adresse_StructModif" placeholder="adresse" value="<?= $objetStructure->getAdresse_Struct() ?>" required>
                <br/><br/>   

                <!-- Code postale -->
                <strong> Code postale : </strong> </strong> 
                <input type="text" name="Cp_StructModif" placeholder="12345" minlength="5" maxlength="5" value="<?= $objetStructure->getCp_Struct() ?>" required> 
                <br/><br/> 

                <!-- Ville -->
                <strong> Ville : </strong> 
                <input type="text" name="Ville_StructModif" placeholder="ville" value="<?= $objetStructure->getVille_Struct() ?>" required>
                <br/><br/>   

                <!-- Fixe -->
                <strong> Fixe : </strong> 
                <input type="tel" minlength="10" maxlength="10" name="Tel_FixeModif" placeholder="0123456789" value="<?= $objetStructure->getTel_Fixe() ?>" required> 
                <br/><br/>

                <!-- Portable -->
                <strong> Portable : </strong> 
                <input type="tel" minlength="10" maxlength="10" name="Tel_PortModif" placeholder="0123456789" value="<?= $objetStructure->getTel_Port() ?>" required> 
                <br/><br/>

                <input type="submit" value="Modifier">
            </form>            
        </section>
    </body>
</html>