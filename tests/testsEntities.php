<?php

// Inclusion de toutes les classes Entities
function chargerClasse($classe) {
    require "../src/App/Entities/" . $classe . '.php';
}
echo $_POST['libelle'];
spl_autoload_register('chargerClasse');


// Utilisateur
$valeurUtilisateur = array(
    "Id_Utilisateur" => 1,
    "Id_Typ_Role_Utilisateur" => 1,
    "Id_Struct_Utilisateur" => 1,
    "Nom_Utilisateur" => "nom",
    "Prenom_Utilisateur" => "prenom",
    "Login_Utilisateur" => "login",
    "Email" => "test@test.com",
    "Mdp" => "mdp",
    "Tel_Port" => "0874568923",
    "Date_Creation_Compte" => "2019-08-08",
    "Dernier_Login_Utilisateur" => "2020-02-27 03:10:12"
);
$utilisateur = new Utilisateur($valeurUtilisateur);
var_dump($utilisateur);


// TypeTableau
$valeurTypeTableau = array(
    "Id_Typ_Tab" => 1,
    "Libelle_Typ_Tab" => "libelle"
);
$typeTableau = new TypeTableau($valeurTypeTableau);
var_dump($typeTableau);


// TypeStructure
$valeurTypeStructure = array(
    "Id_Typ_Struct" => 1,
    "Libelle_Typ_Struct" => "libelle"
);
$typeStructure = new TypeStructure($valeurTypeStructure);
var_dump($typeStructure);

