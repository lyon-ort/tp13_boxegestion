<?php
require_once '../config/config.php';


// Connexion à la BDD
$db = dbConnect();



// Exportation bddboxe

// SELECT de competition
$select = $db->prepare('SELECT * FROM competition WHERE Id_Competition = :Id_CompetitionMark');
$select->setFetchMode(PDO::FETCH_ASSOC);
$select->execute(array('Id_CompetitionMark' => $_POST['Id_Competition']));
$newReservations = $select->fetchAll();

// Ecriture de Competition
$excel = "Competition\n";

foreach($newReservations as $row)
{
    // Vérifie si les valeurs sont null
    $row['Id_Competition_Sexe'] = ($row['Id_Competition_Sexe']) ? $row['Id_Competition_Sexe'] : '\N';
    $row['Id_Competition_Typ_Affrontement'] = ($row['Id_Competition_Typ_Affrontement']) ? $row['Id_Competition_Typ_Affrontement'] : '\N';
   
    $excel .= "$row[Id_Competition],"
            . "$row[Nom_Competition],"
            . "$row[Date_Debut_Competition],"
            . "$row[Date_Fin_Competition],"
            . "$row[Adresse_Competition],"
            . "$row[Cp_Competition],"
            . "$row[Ville_Competition],"
            . "$row[Date_Limite_Inscript_Competition],"
            . "$row[Nb_Round],"
            . "$row[Duree_Round],"
            . "$row[Nb_Ring],"
            . "$row[Id_Competition_Sexe],"
            . "$row[Id_Competition_Typ_Affrontement],"
            . "$row[Id_Competition_Struct],"
            . "$row[Inactif_Competition]\n";
}



// SELECT de tableau
$select2 = $db->prepare('SELECT * FROM tableau WHERE Id_Tableau_Competition = :Id_CompetitionMark');
$select2->setFetchMode(PDO::FETCH_ASSOC);
$select2->execute(array('Id_CompetitionMark' => $_POST['Id_Competition']));
$newReservations2 = $select2->fetchAll();

// Ecriture de Tableau
$excel .= "Tableau\n";

foreach($newReservations2 as $row)
{
    
    $excel .= "$row[Id_Tab],"
            . "$row[Libelle_Tab],"
            . "$row[Nb_Max_Rencontre],"
            . "$row[Id_Tableau_Sexe],"
            . "$row[Id_Tableau_Typ_Affrontement],"
            . "$row[Id_Tableau_Typ_Tableau],"
            . "$row[Id_Tableau_Competition]\n";
}



// SELECT de concerne
$req = $db->prepare('SELECT * FROM tableau WHERE Id_Tableau_Competition = :Id_CompetitionMark');
$req->execute(array('Id_CompetitionMark' => $_POST['Id_Competition']));
$data = $req->fetchAll();

// Ecriture de Concerne
$excel .= "Concerne\n";

for ($i = 0; $i != count($data); $i++)
{
    $select3 = $db->prepare('SELECT * FROM concerne WHERE Id_Concerne_Tab = :Id_TabMark');
    $select3->setFetchMode(PDO::FETCH_ASSOC);
    $select3->execute(array('Id_TabMark' => $data[$i]['Id_Tab']));
    $newReservations3 = $select3->fetchAll();
    // Ecriture de Competition.csv
    foreach($newReservations3 as $row)
    {
        $excel .= "$row[Id_Concerne],"
                . "$row[Id_Concerne_Tab],"
                . "$row[Id_Concerne_Cat_Age]\n";
    }
}



// SELECT de participer
$excel .= "Participer\n";

for ($i = 0; $i != count($data); $i++)
{
    $select4 = $db->prepare('SELECT * FROM participer WHERE Id_Particip_Tableau = :Id_TabMark');
    $select4->setFetchMode(PDO::FETCH_ASSOC);
    $select4->execute(array('Id_TabMark' => $data[$i]['Id_Tab']));
    $newReservations4 = $select4->fetchAll();
    
    // Ecriture de participer.csv
    foreach($newReservations4 as $row)
    {
        $excel .= "$row[Id_Particip],"
                . "$row[Id_Particip_Tireur],"
                . "$row[Id_Particip_Competition],"
                . "$row[Id_Particip_Tableau]\n";
    }
}



// Exportation de rencontre
// SELECT de rencontre
$excel .= "Rencontre\n";
for ($i = 0; $i != count($data); $i++)
{
    $select5 = $db->prepare('SELECT * FROM rencontre WHERE Id_Tab_Rencontre = :Id_TabMark');
    $select5->setFetchMode(PDO::FETCH_ASSOC);
    $select5->execute(array('Id_TabMark' => $data[$i]['Id_Tab']));
    $newReservations5 = $select5->fetchAll();
    // Ecriture de rencontre.csv
    foreach($newReservations5 as $row)
    {
        // Vérifie si les valeurs sont null
        $row['Id_Gagnant'] = ($row['Id_Gagnant']) ? $row['Id_Gagnant'] : '\N';
        $row['Id_Rencontre_Precedent_1'] = ($row['Id_Rencontre_Precedent_1']) ? $row['Id_Rencontre_Precedent_1'] : '\N';
        $row['Id_Rencontre_Precedent_2'] = ($row['Id_Rencontre_Precedent_2']) ? $row['Id_Rencontre_Precedent_2'] : '\N';
        $row['Id_Rencontre_Suivante'] = ($row['Id_Rencontre_Suivante']) ? $row['Id_Rencontre_Suivante'] : '\N';
        $row['Id_Cat_Rencontre'] = ($row['Id_Cat_Rencontre']) ? $row['Id_Cat_Rencontre'] : '\N';
        $row['Id_Tireur_Rouge'] = ($row['Id_Tireur_Rouge']) ? $row['Id_Tireur_Rouge'] : '\N';
        $row['Id_Tireur_Bleu'] = ($row['Id_Tireur_Bleu']) ? $row['Id_Tireur_Bleu'] : '\N';

        $excel .= "$row[Id_Rencontre],"
                . "$row[Date_Rencontre],"
                . "$row[Heure_Debut_Rencontre],"
                . "$row[Heure_Fin_Rencontre],"
                . "$row[Num_Ring_Rencontre],"
                . "$row[Id_Tireur_Rouge],"
                . "$row[Id_Tireur_Bleu],"
                . "$row[Id_Gagnant],"
                . "$row[Id_Rencontre_Precedent_1],"
                . "$row[Id_Rencontre_Precedent_2],"
                . "$row[Id_Rencontre_Suivante],"
                . "$row[Id_Cat_Rencontre],"
                . "$row[Id_Tab_Rencontre],"
                . "$row[Id_Etat_Rencontre],"
                . "$row[Inactif_Rencontre]\n";
    }
}



// Exportation de sefaire
// SELECT de sefaire
$select6 = $db->prepare('SELECT * FROM sefaire WHERE Id_SeFaire_Competition = :Id_CompetitionMark');
$select6->setFetchMode(PDO::FETCH_ASSOC);
$select6->execute(array('Id_CompetitionMark' => $_POST['Id_Competition']));
$newReservations6 = $select6->fetchAll();
// Ecriture de sefaire.csv
$excel .= "SeFaire\n";
foreach($newReservations6 as $row)
{
    $excel .= "$row[Id_SeFaire],"
            . "$row[Id_SeFaire_Competition],"
            . "$row[Id_SeFaire_Cat_Age]\n";
}




// Exportation de tableaupoids
// SELECT de tableaupoids
$excel .= "TableauPoids\n";
for ($i = 0; $i != count($data); $i++)
{
    $select8 = $db->prepare('SELECT * FROM tableaupoids WHERE Id_Poids_Tab = :Id_TabMark');
    $select8->setFetchMode(PDO::FETCH_ASSOC);
    $select8->execute(array('Id_TabMark' => $data[$i]['Id_Tab']));
    $newReservations8 = $select8->fetchAll();
    // Ecriture de tableaupoids.csv
    foreach($newReservations8 as $row)
    {
        $excel .= "$row[Id_Tab_Poids],"
                . "$row[Id_Poids_Tab],"
                . "$row[Id_Poids_Cat]\n";
    }
}



// Exportation de tireur
// Sélection des tireurs dans la table participer
$listeTireur = array();
$y = 0;
$excel .= "Tireur\n";
for ($i = 0; $i != count($data); $i++)
{
    $req3 = $db->prepare('SELECT Id_Particip_Tireur FROM participer WHERE Id_Particip_Tableau = :Id_TabMark');
    $req3->execute(array('Id_TabMark' => $data[$i]['Id_Tab']));
    
    while ($data3 = $req3->fetch())
    {
        $listeTireur[$y] = $data3['Id_Particip_Tireur'];
        $y++;
    }
}
// SELECT de tireur
for ($i = 0; $i != count($listeTireur); $i++)
{
    $select9 = $db->prepare('SELECT * FROM tireur WHERE Id_Tireur = :Id_Particip_TireurMark');
    $select9->setFetchMode(PDO::FETCH_ASSOC);
    $select9->execute(array('Id_Particip_TireurMark' => $listeTireur[$i]));
    $newReservations9 = $select9->fetchAll();
    // Ecriture de tableaupoids.csv
    foreach($newReservations9 as $row)
    {
        $excel .= "$row[Id_Tireur],"
                . "$row[Id_Tir_Niv_Tireur],"
                . "$row[Nom_Tireur],"
                . "$row[Prenom_Tireur],"
                . "$row[Date_Naissance],"
                . "$row[Num_Licence],"
                . "$row[Poids_Tireur],"
                . "$row[Id_Tir_Cat_Poids],"
                . "$row[Id_Tir_Cat_Age],"
                . "$row[Id_Tir_Sexe],"
                . "$row[Id_Tir_Struct],"
                . "$row[Inactif_Tireur]\n";
    }
}



// Exportation de structure
$listeTireur2 = array();
$y2 = 0;
for ($i = 0; $i != count($data); $i++)
{
    $req3 = $db->prepare('SELECT Id_Particip_Tireur FROM participer WHERE Id_Particip_Tableau = :Id_TabMark');
    $req3->execute(array('Id_TabMark' => $data[$i]['Id_Tab']));
    
    while ($data3 = $req3->fetch())
    {
        $listeTireur2[$y2] = $data3['Id_Particip_Tireur'];
        $y2++;
    }
}
// Récupération des structures des tireurs de la compétition
$listeStruct = array();
$z = 0;
for ($i = 0; $i != count($listeTireur2); $i++)
{
    $req2Bis = $db->prepare('SELECT Id_Tir_Struct FROM tireur WHERE Id_Tireur = :Id_TireurMark');
    $req2Bis->execute(array('Id_TireurMark' => $listeTireur2[$i]));
    
    while ($data3Bis = $req2Bis->fetch())
    {
        $listeStruct[$z] = $data3Bis['Id_Tir_Struct'];
        $z++;
    }
}
// Ajout de la structure organisatrice
// Récupération de la structure organisatrice de la compétition
$req2 = $db->prepare('SELECT Id_Competition_Struct FROM competition WHERE Id_Competition = :Id_CompetitionMark');
$req2->execute(array('Id_CompetitionMark' => $_POST['Id_Competition']));
$data2 = $req2->fetch();
// Ajoute
array_push($listeStruct, $data2['Id_Competition_Struct']);
$excel .= "Structure\n";
// SELECT de structure
for ($i = 0; $i != count($listeStruct); $i++)
{
    $select7 = $db->prepare('SELECT * FROM structure WHERE Id_Struct = :Id_StructMark');
    $select7->setFetchMode(PDO::FETCH_ASSOC);
    $select7->execute(array('Id_StructMark' => $listeStruct[$i]));
    $newReservations7 = $select7->fetchAll();
    // Ecriture de structure.csv
    foreach($newReservations7 as $row)
    {
        $excel .= "$row[Id_Struct],"
                . "$row[Id_Typ_Struct],"
                . "$row[Nom_Struct],"
                . "$row[Adresse_Struct],"
                . "$row[Cp_Struct],"
                . "$row[Ville_Struct],"
                . "$row[Id_Responsable_Struct],"
                . "$row[Tel_Fixe],"
                . "$row[Tel_Port],"
                . "$row[Inactif_Struct]\n";
    }
}



// Exportation de utilisateur
// SELECT de utilisateur
$excel .= "Utilisateur\n";

for ($i = 0; $i != count($listeStruct); $i++)
{
    $select10 = $db->prepare('SELECT * FROM utilisateur WHERE Id_Struct_Utilisateur = :Id_Struct_UtilisateurMark');
    $select10->setFetchMode(PDO::FETCH_ASSOC);
    $select10->execute(array('Id_Struct_UtilisateurMark' => $listeStruct[$i]));
    $newReservations10 = $select10->fetchAll();
    // Ecriture de utilisateur.csv
    foreach($newReservations10 as $row)
    {
        // Vérifie si les valeurs sont null
        $row['Dernier_Login_Utilisateur'] = ($row['Dernier_Login_Utilisateur']) ? $row['Dernier_Login_Utilisateur'] : '\N';    
                
        $excel .= "$row[Id_Utilisateur],"
                . "$row[Id_Typ_Role_Utilisateur],"
                . "$row[Id_Struct_Utilisateur],"
                . "$row[Nom_Utilisateur],"
                . "$row[Prenom_Utilisateur],"
                . "$row[Login_Utilisateur],"
                . "$row[Email],"
                . "$row[Mdp],"
                . "$row[Tel_Port],"
                . "$row[Date_Creation_Compte],"
                . "$row[Dernier_Login_Utilisateur],"
                . "$row[Inactif_Utilisateur]\n";
    }
}

// Fin du fichier
$excel .= "End\n";

// Création de bddboxe.csv
//header("Content-type: application/vnd.ms-excel");
$nom_file = "bddboxe.csv";
$f = fopen($nom_file, "x+");
fputs($f, $excel);
fclose($f);

header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename='.basename($nom_file));
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($nom_file));
ob_clean();
flush();
readfile($nom_file);



// Suppression des fichiers csv
unlink('bddboxe.csv');
