<?php
// Connexion à la BDD et inclusion des classes
require '../config/config.php';
// Vérifie l'authentification (à chaque page où l'utilisateur peut être connecté)
require_once '../config/session-verif.php';

// Inscription
if (isset($_POST['Id_Competition']) && isset($_POST['Id_Tab']) && isset($_POST['TireurAInscrire']))
{
    $objetParticiperRep = new ParticiperRepository();
    
    foreach ($_POST['TireurAInscrire'] as $valueIdTireur)
    {
        $valeurParticiper = array(
            "Id_Particip_Tireur" => $valueIdTireur,
            "Id_Particip_Competition" => $_POST['Id_Competition'],
            "Id_Particip_Tableau" => $_POST['Id_Tab']
        );
        
        $objetParticiper = new Participer($valeurParticiper);
        $objetParticiperRep->sauver($objetParticiper);
    }
        ?>
            <!-- Renvoie vers la page de modif -->
            <form name="form" action="inscription.php?connect=1" method="POST"> 
                <input type="hidden" name="Id_Competition" value="<?= $_POST['Id_Competition'] ?>"/>
                <input type="hidden" name="Id_Tab" value="<?= $_POST['Id_Tab'] ?>"/>
                <input type="hidden" name="inscriptionReussi" />
            </form>

            <!-- Script qui renvoie les variables POST vers la page d'inscription -->
            <script type="text/javascript"> 
                document.form.submit(); // on envoie le formulaire vers la page d'inscription
            </script> 
            </script> 
        <?php        
}

// Désinscription
if (isset($_POST['Id_Competition']) && isset($_POST['Id_Tab']) && isset($_POST['TireurADesinscrire']))
{
    $objetParticiperRep = new ParticiperRepository();
    
    foreach ($_POST['TireurADesinscrire'] as $valueIdTireur)
    {
        $objetParticiperRep->SupprimerParticipationIdTireur($valueIdTireur);
    }
    
    ?>
        <!-- Renvoie vers la page de modif -->
        <form name="form" action="inscription.php?connect=1" method="POST"> 
            <input type="hidden" name="Id_Competition" value="<?= $_POST['Id_Competition'] ?>"/>
            <input type="hidden" name="Id_Tab" value="<?= $_POST['Id_Tab'] ?>"/>
            <input type="hidden" name="desinscriptionReussi" />
        </form>

        <!-- Script qui renvoie les variables POST vers la page d'inscription -->
        <script type="text/javascript"> 
            document.form.submit(); // on envoie le formulaire vers la page d'inscription
        </script> 
    <?php    
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="../public/css/style.css"/>
        <link rel='stylesheet' type='text/css' href='../public/js/jquery-3.5.1.js' />
        <link rel='stylesheet' type='text/css' href='../public/css/bootstrap.css' />
        <link rel='stylesheet' type='text/css' href='../public/js/bootstrap.js' />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet">
        <link rel="icon" href="../public/img/ff_savate.jpg" />
        <title> Inscrire tireur </title>
    </head>
    <body>
        <!-- Header (inc) -->
        <header class='container-fluid header'>
            <?php
                include '../inc/header.php';
            ?>
        </header>
        
        
        <!-- Section -->
        <section class='container-fluid about'>
            
            <!-- Information de l'utilisateur (inc) -->
            <?php
            include '../inc/information.php';
            ?>
            
            <h1> Inscription </h1>
            
            <hr class="separator">
            
            <!-- Liste déroulante des tableaux -->
            <?php
                // Sélectionne les tableaux de la compétition
                $objetTableauRep = new TableauRepository();
                $arrayObjetTableau = $objetTableauRep->selectionTableauxIdCompet($_POST['Id_Competition']);
            ?>            
            
            <!-- Inscription message de confirmation -->
            <?php
            if (isset($_POST['inscriptionReussi']))
            {
                   ?>
                            <!-- Pop up de confirmation de suppression -->
                            <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                                <div class="modal-content" style="height: 250px;">
                                    <!-- bouton fermer -->
                                    <div id="close" class="closeTest">+</div>

                                    <!-- Confirmation de suppression -->
                                    <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                                    L'inscription à été effectué !<br/><br/>

                                    <button class="boutonAnnulerTest"> OK </button>
                                </div>
                            </div>   

                             <script>
                                // La croix (ferme le pop-up)
                                document.querySelector('.closeTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });      

                                // Le bouton annuler (ferme le pop-up)
                                document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });       
                            </script>            
                    <?php                 
            }
            ?>
                            
            <!-- Desinscription message de confirmation -->
            <?php
            if (isset($_POST['desinscriptionReussi']))
            {
                   ?>
                            <!-- Pop up de confirmation de suppression -->
                            <div id="bg-modal" class="bg-modalTest" style="display: flex;">
                                <div class="modal-content" style="height: 250px;">
                                    <!-- bouton fermer -->
                                    <div id="close" class="closeTest">+</div>

                                    <!-- Confirmation de suppression -->
                                    <img width="50px" height="50px" src="../public/img/pictogrammeValidation.png" alt="pictogramme de validation"/><br/>

                                    La désinscription à été effectué !<br/><br/>

                                    <button class="boutonAnnulerTest"> OK </button>
                                </div>
                            </div>   

                             <script>
                                // La croix (ferme le pop-up)
                                document.querySelector('.closeTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });      

                                // Le bouton annuler (ferme le pop-up)
                                document.querySelector('.boutonAnnulerTest').addEventListener('click', function() 
                                {
                                    document.querySelector('.bg-modalTest').style.display = 'none';
                                });       
                            </script>            
                    <?php                 
            }
            ?>
            
            <form action="inscription.php?connect=1" method="POST" style="width: 500px;">
                <select name="Id_Tab" required>
                    <option value=""> -- Choisir le tableau -- </option>
                    <?php
                        foreach ($arrayObjetTableau as $value)
                        {
                    ?>
                            <option value="<?= $value->getId_Tab() ?>"> <?= $value->getLibelle_Tab() ?> </option>
                    <?php
                        }
                    ?>
                </select>  
                
                <input type="hidden" name="Id_Competition" value="<?= $_POST['Id_Competition'] ?>" />
                <input type="submit" value="Valider" />
            </form>
            
            <!-- Liste des tireurs qui peuvent être inscrit à ce tableau (ce qui sont déjà inscrit sont grisé) -->
            <?php
                if (isset($_POST['Id_Tab']))
                {
                    // Présentation du tableau
                        $idTableau = $_POST['Id_Tab'];
                        $objetTableauxRep = new TableauRepository();
                        $leTableauChoisi = $objetTableauxRep->selectionTableauxId($idTableau);
                        
                        echo '<br/>';
                        // Caractéristique du tableau
                        echo '<form class="formCreat">';     
                        
                            echo 'Nom : ' . strip_tags($leTableauChoisi->getLibelle_Tab()) . '<br/>';
                            
                            echo 'Nombre max de rencontre : ' . strip_tags($leTableauChoisi->getNb_Max_Rencontre()) . '<br/>';
                            
                            echo 'Sexe : ';
                            $objetSexeRep2 = new SexeRepository();
                            $tableauSexe = $objetSexeRep2->selectionSexeId($leTableauChoisi->getId_Tableau_Sexe());
                            echo $tableauSexe->getLibelle_Sexe();
                            echo '<br/>';
                            
                            echo 'Type d\'affrontement : ';
                            $objetTypeAffrontementRep = new TypeAffrontementRepository();
                            $tableauTypeAffrontement = $objetTypeAffrontementRep->selectionTypeAffrontementId($leTableauChoisi->getId_Tableau_Typ_Affrontement());
                            echo $tableauTypeAffrontement->getLibelle_Typ_Affrontement();
                            echo '<br/>';
                            
                            echo 'Type de tableau : ';
                            $objetTypeTableauRep = new TypeTableauRepository();
                            $tableauTypeTableau = $objetTypeTableauRep->selectionTypeTableauId($leTableauChoisi->getId_Tableau_Typ_Tableau());
                            echo $tableauTypeTableau->getLibelle_Typ_Tab();
                            echo '<br/>';
                            
                            echo 'Catégorie de Poids : ';
                            // Sélectionne la catégorie de poids du tableau
                            $objetTableauPoidsRep2 = new TableauPoidsRepository();
                            $objetTableauPoids2 = $objetTableauPoidsRep2->selectionTableauPoidsIdTab($leTableauChoisi->getId_Tab());
                            $objetCategoriePoidsRep2 = new CategoriePoidsRepository();
                            $objetCategoriePoids2 = $objetCategoriePoidsRep2->selectionCategoriePoidsId($objetTableauPoids2->getId_Poids_Cat());   
                            echo $objetCategoriePoids2->getLibelle_Cat_Poids();
                            echo '<br/>';
                            
                            echo 'Catégorie d\'age : ';
                            // Sélectionne la catégorie d'age du tableau
                            $objetConcerneRep2 = new ConcerneRepository();
                            $objetConcerne2 = $objetConcerneRep2->selectionConcerneIdTab($leTableauChoisi->getId_Tab());
                            $objetCategorieAgeRep2 = new CategorieAgeRepository();
                            $objetCategorieAge2 = $objetCategorieAgeRep2->selectionCategorieAgeId($objetConcerne2->getId_Concerne_Cat_Age());  
                            echo $objetCategorieAge2->getLibelle_Cat_Age();
                            echo '<br/>';
                            
                            echo 'Compétition : ';
                            $objetCompetitionRep = new CompetitionRepository();
                            $tableauCompetition = $objetCompetitionRep->selectionCompetitionId($leTableauChoisi->getId_Tableau_Competition());
                            echo $tableauCompetition->getNom_Competition();
                            echo '<br/>';
                            
                        echo '</form> <br/>';
                    
                    // Séléction du tableau
                    // Sélectionne le tableau choisi
                    $objetTableau = $objetTableauRep->selectionTableauxId($_POST['Id_Tab']);
                    
                    // Sélectionne la catégorie de poids du tableau
                    $objetTableauPoidsRep = new TableauPoidsRepository();
                    $objetTableauPoids = $objetTableauPoidsRep->selectionTableauPoidsIdTab($objetTableau->getId_Tab());
                    
                    $objetCategoriePoidsRep = new CategoriePoidsRepository();
                    $objetCategoriePoids = $objetCategoriePoidsRep->selectionCategoriePoidsId($objetTableauPoids->getId_Poids_Cat());
                    
                    // Sélectionne la catégorie d'age du tableau
                    $objetConcerneRep = new ConcerneRepository();
                    $objetConcerne = $objetConcerneRep->selectionConcerneIdTab($objetTableau->getId_Tab());
                    
                    $objetCategorieAgeRep = new CategorieAgeRepository();
                    $objetCategorieAge = $objetCategorieAgeRep->selectionCategorieAgeId($objetConcerne->getId_Concerne_Cat_Age());
                    
                    //Sélectionne le sexe du tableau
                    $objetSexeRep = new SexeRepository();
                    $objetSexe = $objetSexeRep->selectionSexeId($objetTableau->getId_Tableau_Sexe());
                    
                    
                    // Sélection des tireurs
                    // Sélectionne les tireurs de la structure avec le bon POIDS, le bonne AGE et le bon SEXE
                    $objetTireurRep = new TireurRepository();
                    $arrayObjetTireur = $objetTireurRep->selectionArrayTireurIdStructure($_SESSION['idStruct']);
                    
                    // Catégorie de poids
                    $arrayObjetTireurPoids = NULL;
                    
                    if ($arrayObjetTireur)
                    {
                        foreach ($arrayObjetTireur as $valueTireur)
                        {
                            if ($valueTireur->getId_Tir_Cat_Poids() == $objetCategoriePoids->getId_Cat_Poids())
                            {
                                $arrayObjetTireurPoids[] = $valueTireur;
                            }
                        }                        
                    }

                    
                    // Catégorie d'age  
                    $arrayObjetTireurPoidsAge = NULL;
                    
                    if ($arrayObjetTireurPoids)
                    {
                        foreach ($arrayObjetTireurPoids as $valueTireurPoids)
                        {
                            if ($valueTireurPoids->getId_Tir_Cat_Age() == $objetCategorieAge->getId_Cat_Age())
                            {
                                $arrayObjetTireurPoidsAge[] = $valueTireurPoids;
                            }
                        }                        
                    }
                    
                    // Le sexe
                    $arrayObjetTireurPoidsAgeSexe = NULL;
                    
                    if ($arrayObjetTireurPoidsAge)
                    {
                        foreach ($arrayObjetTireurPoidsAge as $valueTireurPoidsAge)
                        {
                            if ($valueTireurPoidsAge->getId_Tir_Sexe() == $objetSexe->getId_Sexe())
                            {
                                $arrayObjetTireurPoidsAgeSexe[] = $valueTireurPoidsAge;
                            }
                        }                        
                    }
                    
                    
                    
                    // Les tireurs déjà inscrit et non inscrit                    
                    $objetParticiperRep2 = new ParticiperRepository();
                    $arrayObjetParticiper2 = $objetParticiperRep2->selectionParticipationIdTableau($_POST['Id_Tab']);
                                        
                    $arrayTireurDejaInscrit = NULL;
                    $arrayTireurNonInscrit = NULL;
                    
                    if ($arrayObjetTireurPoidsAgeSexe)
                    { 
                        // Si il y a des tireurs qui participe
                        if ($arrayObjetParticiper2)
                        {
                            // Sélectionne les tireurs déjà inscrit
                            foreach ($arrayObjetTireurPoidsAgeSexe as $valueTireur)
                            {
                                foreach ($arrayObjetParticiper2 as $valueTireurQuiParticipe)
                                {
                                    if ($valueTireur->getId_Tireur() == $valueTireurQuiParticipe->getId_Particip_Tireur())
                                    {
                                        $arrayTireurDejaInscrit[] = $valueTireur;
                                        
                                        // Sélectionne les tireurs non-inscrit
                                        // array_search() = cherche l'élément à supprimer
                                        // unset() = supprimer l'élément
                                        unset($arrayObjetTireurPoidsAgeSexe[array_search($valueTireur, $arrayObjetTireurPoidsAgeSexe)]);
                                    }
                                }
                            }
                        }
                    }
                    
                    ?>
                        <br/>
                        <div id="creationStructure">
                            <div id="structure">
                                <p>Veuillez cocher les cases des tireurs que vous voulez inscrire</p><br/>
                                <form action="inscription.php?connect=1" method="post" class="formCreat">
                                    <h3> Les tireurs pouvant participer </h3>

                                    <fieldset>
                                        <?php
                                        if ($arrayObjetTireurPoidsAgeSexe)
                                        {
                                            foreach ($arrayObjetTireurPoidsAgeSexe as $value)
                                            {
                                                ?>

                                                    <input id="<?= $value->getId_Tireur() ?>" type="checkbox" name="TireurAInscrire[]" value="<?= $value->getId_Tireur() ?>" />
                                                    <label for="<?= $value->getId_Tireur() ?>"> <?= $value->getPrenom_Tireur() . ' ' . $value->getNom_Tireur() ?> </label>

                                                    <br/>
                                                <?php
                                            }                                
                                        }
                                        else
                                        {
                                            echo 'Il n\'y a aucun tireurs disponible';
                                        }

                                        ?>
                                    </fieldset>

                                    <br/>

                                    <input type="hidden" name="Id_Tab" value="<?= $_POST['Id_Tab'] ?>" />
                                    <input type="hidden" name="Id_Competition" value="<?= $_POST['Id_Competition'] ?>" />

                                    <input type="submit" value="Inscrire" />
                                </form>  
                                </div>

                                <div id="responsable">
                                <!-- Les tireurs qui sont déjà inscrit au tableau -->
                                <p>Veuillez cocher les cases des tireurs que vous voulez désinscrire</p><br/>
                                <form action="inscription.php?connect=1" method="post" class="formCreat">
                                    <h3> Les tireurs déjà inscrit </h3>

                                    <fieldset>
                                        <?php
                                        if ($arrayTireurDejaInscrit)
                                        {
                                            foreach ($arrayTireurDejaInscrit as $value)
                                            {
                                                ?>

                                                    <input id="<?= $value->getId_Tireur() ?>" type="checkbox" name="TireurADesinscrire[]" value="<?= $value->getId_Tireur() ?>" />
                                                    <label for="<?= $value->getId_Tireur() ?>"> <?= $value->getPrenom_Tireur() . ' ' . $value->getNom_Tireur() ?> </label>

                                                    <br/>
                                                <?php
                                            }                                
                                        }
                                        else
                                        {
                                            echo 'Il n\'y a aucun tireurs inscrit';
                                        }

                                        ?>
                                    </fieldset>

                                    <br/>

                                    <input type="hidden" name="Id_Tab" value="<?= $_POST['Id_Tab'] ?>" />
                                    <input type="hidden" name="Id_Competition" value="<?= $_POST['Id_Competition'] ?>" />

                                    <input type="submit" value="Désinscrire" />
                                </form>  
                            </div>
                        </div>
                    <?php
                }
            ?>
            <br/>
        </section>
    </body>
</html>