<?php

require '../src/App/Entities/TypeRole.php';

class TypeRoleRepository 
{

    protected $table;     
    protected $classMapped; 
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire)
    public function __construct() 
    {
        $this->table = 'TypeRole';
        $this->classMapped = 'App\Entities\TypeRole';
        $this->idFieldName = 'Id_Typ_Role';
    }
    
    
    
    // Sélectionne un type de rôle selon un id
    public function selectionTypeRoleId($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $objet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE $this->idFieldName = :idMark");
        $req->execute(array('idMark' => $id));
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$objet = new $this->table($row);
	    }
        }
        
        // Return de l'objet
        return $objet;
    }    
    
    
    
    // Sélectionne tous les types de roles
    public function selectionTypeRole()
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objets à NULL si jamais elle la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->query("SELECT * FROM $this->table");
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;
    }      
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
    // Séléctionne tous les types de rôles
    public function selectionTypeRole()
    {
        $db = dbConnect();
        $req = $db->query("SELECT * FROM $this->table");
        $i = 0;
        
        while ($data = $req->fetch())
        {
            $tableauTypeRole[$i] = array(
                'Id_Typ_Role' => $data['Id_Typ_Role'],
                'Libelle_Typ_Role' => $data['Libelle_Typ_Role']
            );
            $i++;
        }
        
        return $tableauTypeRole;
    }*/
    
    
    // Séléctionne une structure selon un idStructUtilisateur
    public function selectionRoleId($idRoleUtil)
    {
        $db = dbConnect();
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Typ_Role = :idRoleUtilMark");
        $req->execute(array('idRoleUtilMark' => $idRoleUtil));
        
        while ($data = $req->fetch())
        {
            $nom = $data['Libelle_Typ_Role'];
        }
        
        return $nom;
    }
}
