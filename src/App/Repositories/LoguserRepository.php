<?php

require '../src/App/Entities/Loguser.php';

class LoguserRepository {

    protected $table;
    protected $classMapped;
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire) 
    public function __construct() {
        $this->table = 'Loguser';
        $this->classMapped = 'Loguser';
        $this->idFieldName = 'Id_Loguser';
    }

    
    // Ajouter / Modifier un log
    public function sauver(Loguser $entity) {
        $db = dbConnect();
        $resultSet = NULL;
        if ($entity != NULL) {
            $bindParam = $entity->__toArray();
            $query = "INSERT INTO $this->table" .
                    " (Login, "
                    . "Date_Log, "
                    . "Message)"
                    . " VALUES "
                    . "(:Login, "
                    . ":Date_Log, "
                    . ":Message)";

            $reqPrep = $db->prepare($query);
            $reqPrep->bindParam(':Login', $bindParam['Login'], \PDO::PARAM_STR);
            $reqPrep->bindParam(':Date_Log', $bindParam['Date_Log'], \PDO::PARAM_STR);
            $reqPrep->bindParam(':Message', $bindParam['Message'], \PDO::PARAM_STR);
            $reqPrep->execute();

            if ($reqPrep != FALSE) {
                $entity->setId_Loguser($db->lastInsertId());
                $resultSet = $entity;
            }
        }
        return $resultSet;
    }
}
