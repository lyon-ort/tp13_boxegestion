<?php

require '../src/App/Entities/TypeTableau.php';

class TypeTableauRepository {

    protected $table;     
    protected $classMapped; 
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire)
    public function __construct() {
        $this->table = 'TypeTableau';
        $this->classMapped = 'App\Entities\TypeTableau';
        $this->idFieldName = 'Id_Typ_Tab';
    }
    
    
    
    // Sélectionner une type de tableau selon un id
    public function selectionTypeTableauId($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $objet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE $this->idFieldName = :idMark");
        $req->execute(array('idMark' => $id));

        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$objet = new $this->table($row);
	    }
        }
        
        // Return un objet
        return $objet;        
    }
    
    
    
  // Sélectionne tous les types de tableaux
    public function selectionTypeTableau()
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objets à NULL si jamais elle la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->query("SELECT * FROM $this->table");
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;
    }          
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
    // Séléctionne tous les sexes
    public function selectionTypeTableau()
    {
        $db = dbConnect();
        $req = $db->query("SELECT * FROM $this->table");
        $i = 0;
        
        while ($data = $req->fetch())
        {
            $tableauTypeTableau[$i] = array(
                'Id_Typ_Tab' => $data['Id_Typ_Tab'],
                'Libelle_Typ_Tab' => $data['Libelle_Typ_Tab']
            );
            $i++;
        }
        
        return $tableauTypeTableau;
    }*/
    
    /*
    // Sélectionne le type d'affrontement par rapport à son id, et renvoie le libelle
    public function selectionTypeTableauId($idTypeTableau)
    {
        $db = dbConnect();
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Typ_Tab = :idTypeTableauMark");
        $req->execute(array('idTypeTableauMark' => $idTypeTableau));
        
        $data = $req->fetch();
        
        return $data['Libelle_Typ_Tab'];
    }     */   
}