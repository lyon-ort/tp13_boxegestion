<?php

require '../src/App/Entities/TypeAffrontement.php';

class TypeAffrontementRepository {

    protected $table;     
    protected $classMapped; 
    protected $idFieldName;

    
    // Constructeur (avec le nom de la table, le chemin d'accès à son entity, et sa clef primaire)
    public function __construct() {
        $this->table = 'TypeAffrontement';
        $this->classMapped = 'App\Entities\TypeAffrontement';
        $this->idFieldName = 'Id_Typ_Affrontement';
    }
    
    
    
    // Sélectionne le type d'affrontement selon un id
    public function selectionTypeAffrontementId($id)
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation de l'objet à NULL si jamais elle la requête n'est pas bonne
        $objet = NULL;
        
        // Requête
        $req = $db->prepare("SELECT * FROM $this->table WHERE $this->idFieldName = :idMark");
        $req->execute(array('idMark' => $id));
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$objet = new $this->table($row);
	    }
        }
        
        // Return de l'objet
        return $objet;
    }    
    
    
    
    
    // Sélectionne tous les types d'affrontements
    public function selectionTypeAffrontement()
    {
        // Connexion à la Base de données
        $db = dbConnect();
        
        // Initialisation du tableau d'objets à NULL si jamais elle la requête n'est pas bonne
        $arrayObjet = NULL;
        
        // Requête
        $req = $db->query("SELECT * FROM $this->table");
        
        // Si la requête est bonne, alors on fait un tableau d'objet
	if ($req) 
        {
	    while ($row = $req->fetch(\PDO::FETCH_ASSOC)) 
            {
		$arrayObjet[] = new $this->table($row);
	    }
        }
        
        // Return du tableau d'objets
        return $arrayObjet;
    }         
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
    // Séléctionne tous les types d'affrontements
    public function selectionTypeAffrontement()
    {
        $db = dbConnect();
        $req = $db->query("SELECT * FROM $this->table");
        $i = 0;
        
        while ($data = $req->fetch())
        {
            $tableauTypeAffrontement[$i] = array(
                'Id_Typ_Affrontement' => $data['Id_Typ_Affrontement'],
                'Libelle_Typ_Affrontement' => $data['Libelle_Typ_Affrontement']
            );
            $i++;
        }
        
        return $tableauTypeAffrontement;
    }*/
    
    
    // Sélectionne le type d'affrontement par rapport à son id, et renvoie le libelle
    public function selectionTypeAffrontId($idTypeAffront)
    {
        $db = dbConnect();
        $req = $db->prepare("SELECT * FROM $this->table WHERE Id_Typ_Affrontement = :idTypeAffrontMark");
        $req->execute(array('idTypeAffrontMark' => $idTypeAffront));
        
        $data = $req->fetch();
        
        return $data['Libelle_Typ_Affrontement'];
    }    
}